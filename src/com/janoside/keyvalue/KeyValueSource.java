package com.janoside.keyvalue;

public interface KeyValueSource<T> {
	
	T get(String key);
}