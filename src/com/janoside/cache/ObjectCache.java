package com.janoside.cache;

import com.janoside.storage.KeyValueStore;

public interface ObjectCache<T> extends KeyValueStore<T> {
	
	void put(String key, T object, long lifetime);
	
	void clear();
	
	int getSize();
}