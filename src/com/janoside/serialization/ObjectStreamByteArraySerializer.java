package com.janoside.serialization;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.LoggingExceptionHandler;

public class ObjectStreamByteArraySerializer<T> implements Serializer<T, byte[]>, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	public ObjectStreamByteArraySerializer() {
		this.exceptionHandler = new LoggingExceptionHandler();
	}
	
	public byte[] serialize(T object) {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		ObjectOutputStream objectStream = null;
		
		try {
			objectStream = new ObjectOutputStream(byteStream);
			objectStream.writeObject(object);
			
			return byteStream.toByteArray();
			
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
			
		} finally {
			if (objectStream != null) {
				try {
					objectStream.close();
					
				} catch (IOException ioe) {
					this.exceptionHandler.handleException(ioe);
				}
			}
		}
	}
	
	public T deserialize(byte[] transport) {
		ObjectInputStream objectStream = null;
		
		try {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(transport);
			 
			objectStream = new ObjectInputStream(inputStream);
			
			T value = (T) objectStream.readObject();
			
			return value;
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
			
		} finally {
			if (objectStream != null) {
				try {
					objectStream.close();
					
				} catch (IOException ioe) {
					this.exceptionHandler.handleException(ioe);
				}
			}
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}