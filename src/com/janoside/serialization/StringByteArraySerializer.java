package com.janoside.serialization;

import java.nio.charset.Charset;

public class StringByteArraySerializer implements Serializer<String, byte[]> {
	
	private Charset charset;
	
	public StringByteArraySerializer() {
		this.charset = Charset.forName("UTF-8");
	}
	
	public byte[] serialize(String object) {
		return object.getBytes(this.charset);
	}
	
	public String deserialize(byte[] transport) {
		return new String(transport, this.charset);
	}
	
	public void setCharsetName(String charsetName) {
		this.charset = Charset.forName(charsetName);
	}
}