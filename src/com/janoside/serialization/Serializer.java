package com.janoside.serialization;

public interface Serializer<ObjectType, TransportType> {
	
	TransportType serialize(ObjectType object);
	
	ObjectType deserialize(TransportType transport);
}