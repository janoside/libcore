package com.janoside.serialization;

import java.util.List;

public class ChainSerializer<FromType, ToType> implements Serializer<FromType, ToType> {
	
	private List<Serializer> serializers;
	
	public ToType serialize(FromType object) {
		Object value = object;
		
		for (int i = 0; i < this.serializers.size(); i++) {
			value = this.serializers.get(i).serialize(value);
		}
		
		return (ToType) value;
	}
	
	public FromType deserialize(ToType transport) {
		Object value = transport;
		
		for (int i = this.serializers.size() - 1; i >= 0; i--) {
			value = this.serializers.get(i).deserialize(value);
		}
		
		return (FromType) value;
	}
	
	public void setSerializers(List<Serializer> serializers) {
		this.serializers = serializers;
	}
}