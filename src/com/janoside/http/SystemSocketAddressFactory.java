package com.janoside.http;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

public class SystemSocketAddressFactory implements SocketAddressFactory {
	
	public SocketAddress create(InetAddress host, int port) {
		return new InetSocketAddress(host, port);
	}
	
	public SocketAddress create(String host, int port) {
		return new InetSocketAddress(host, port);
	}
}