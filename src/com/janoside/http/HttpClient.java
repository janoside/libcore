package com.janoside.http;

import java.util.Map;

import org.apache.commons.httpclient.NameValuePair;

public interface HttpClient {

	String get(String url);
	
	String get(String url, long timeout);
	
	String get(String url, Map<String, String> parameters, long timeout);
	
	String post(String url, Map<String, String> parameters, long timeout);
	
	String post(String url, Map<String, String> parameters);
	
	String post(String url, NameValuePair[] data);
	
	String post(String url, NameValuePair[] data, long timeout);
	
	String post(String url, Map<String, String> requestHeaders, String postData, String contentType, String encoding, long timeout);
	
	String post(String url, String postData, String contentType, String encoding, long timeout);
	
	String post(String url, String postData, long timeout);
	
	String post(String url, String postData);
	
	byte[] getBytes(String url, long timeout);
}