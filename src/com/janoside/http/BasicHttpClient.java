package com.janoside.http;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.util.StreamUtil;
import com.janoside.util.StringUtil;
import com.janoside.util.UrlUtil;

public class BasicHttpClient implements com.janoside.http.HttpClient {
	
	private static final Logger logger = LoggerFactory.getLogger(BasicHttpClient.class);
	
	private HttpClient httpClient;
	
	private String userAgent;
	
	private long successfulGetRequestCount;
	
	private long failedGetRequestCount;
	
	private long successfulPostRequestCount;
	
	private long failedPostRequestCount;
	
	private long bytesReceived;
	
	public BasicHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		
		this.userAgent = "apache-commons-3.1";
		this.successfulGetRequestCount = 0;
		this.failedGetRequestCount = 0;
		this.successfulPostRequestCount = 0;
		this.failedPostRequestCount = 0;
		this.bytesReceived = 0;
		
		if (this.httpClient != null) {
			this.httpClient.getParams().setParameter(HttpMethodParams.USER_AGENT, this.userAgent);
		}
	}
	
	public BasicHttpClient() {
		HttpConnectionManagerParams connectionManagerParams = new HttpConnectionManagerParams();
		connectionManagerParams.setDefaultMaxConnectionsPerHost(40);
		connectionManagerParams.setMaxTotalConnections(200);
		connectionManagerParams.setConnectionTimeout(2000);
		connectionManagerParams.setSoTimeout(60000);
		
		HttpClientParams clientParams = new HttpClientParams();
		clientParams.setConnectionManagerTimeout(15000);
		
		MultiThreadedHttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
		connectionManager.setParams(connectionManagerParams);
		
		this.httpClient = new HttpClient();
		this.httpClient.setHttpConnectionManager(connectionManager);
		this.httpClient.setParams(clientParams);
		
		this.userAgent = "apache-commons-3.1";
		this.successfulGetRequestCount = 0;
		this.failedGetRequestCount = 0;
		this.successfulPostRequestCount = 0;
		this.failedPostRequestCount = 0;
		this.bytesReceived = 0;
		
		if (this.httpClient != null) {
			this.httpClient.getParams().setParameter(HttpMethodParams.USER_AGENT, this.userAgent);
		}
	}
	
	public String get(String url, long timeout) {
		GetMethod get = new GetMethod(url);
		
		try {
			if (timeout > 0) {
				get.getParams().setSoTimeout((int) timeout);
			}
	
			int statusCode = this.httpClient.executeMethod(get);
	
			if ((statusCode / 200) != 1) {
				logger.error("Status code " + statusCode + " for " + url);
				
				this.failedGetRequestCount++;
				
				throw new HttpException("Status code " + statusCode + " for " + UrlUtil.getDomain(url));
			}

			String httpContent = this.getResponseBodyFromMethod(get);
			this.bytesReceived += httpContent.getBytes("UTF-8").length;
			
			this.successfulGetRequestCount++;
	
			return httpContent;
			
		} catch (HttpException he) {
			this.failedGetRequestCount++;
			
			throw new RuntimeException("Http exception", he);
			
		} catch (IOException ioe) {
			this.failedGetRequestCount++;
			
			throw new RuntimeException("Http IO exception", ioe);
			
		} finally {
			get.releaseConnection();
		}
	}
	
	public byte[] getBytes(String url, long timeout) {
		GetMethod get = new GetMethod(url);
		
		try {
			if (timeout > 0) {
				get.getParams().setSoTimeout((int) timeout);
			}
			
			int statusCode = this.httpClient.executeMethod(get);
	
			if ((statusCode / 200) != 1) {
				logger.error("Status code " + statusCode + " for " + url);
				
				this.failedGetRequestCount++;
				
				throw new HttpException("Status code " + statusCode + " for " + UrlUtil.getDomain(url));
			}
			
			this.bytesReceived += get.getResponseContentLength();
			
			this.successfulGetRequestCount++;
	
			return get.getResponseBody();
			
		} catch (HttpException he) {
			this.failedGetRequestCount++;
			
			throw new RuntimeException("Http exception", he);
			
		} catch (IOException ioe) {
			this.failedGetRequestCount++;
			
			throw new RuntimeException("Http IO exception", ioe);
			
		} finally {
			get.releaseConnection();
		}
	}
	
	public String get(String url, Map<String, String> parameters, long timeout) {
		StringBuilder buffer = new StringBuilder(50);
		
		buffer.append("?");
		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			buffer.append(UrlUtil.encode(entry.getKey()));
			buffer.append("=");
			buffer.append(UrlUtil.encode(entry.getValue()));
			buffer.append("&");
		}
		
		buffer.deleteCharAt(buffer.length() - 1);
		
		GetMethod get = new GetMethod(url + buffer.toString());
		
		try {
			if (timeout > 0) {
				get.getParams().setSoTimeout((int) timeout);
			}
	
			int statusCode = this.httpClient.executeMethod(get);
	
			if ((statusCode / 200) != 1) {
				logger.error("Status code " + statusCode + " for " + url);
				
				this.failedGetRequestCount++;
				
				throw new HttpException("Status code " + statusCode + " for " + UrlUtil.getDomain(url));
			}
			
			String httpContent = this.getResponseBodyFromMethod(get);
			this.bytesReceived += httpContent.getBytes("UTF-8").length;
			
			this.successfulGetRequestCount++;
	
			return httpContent;
			
		} catch (HttpException he) {
			this.failedGetRequestCount++;
			
			throw new RuntimeException("Http exception", he);
			
		} catch (IOException ioe) {
			this.failedGetRequestCount++;
			
			throw new RuntimeException("Http IO exception", ioe);
			
		} finally {
			get.releaseConnection();
		}
	}
	
	public String get(String url) {
		return this.get(url, 0);
	}
	
	public String post(String url, Map<String, String> parameters, long timeout) {
		PostMethod post = new PostMethod(url + "?" + UrlUtil.buildParameterString(parameters));
		
		try {
			if (timeout > 0) {
				post.getParams().setSoTimeout((int) timeout);
			}
	
			int statusCode = this.httpClient.executeMethod(post);
			
			if ((statusCode / 200) != 1) {
				logger.error("Status code " + statusCode + " for " + url + ", data=" + parameters);
				
				this.failedPostRequestCount++;
				
				throw new HttpException("Status code " + statusCode + " for " + UrlUtil.getDomain(url));
			}
			
			this.successfulPostRequestCount++;
			
			return this.getResponseBodyFromMethod(post);
			
		} catch (HttpException he) {
			this.failedPostRequestCount++;
			
			throw new RuntimeException("Http exception", he);
			
		} catch (IOException ioe) {
			this.failedPostRequestCount++;
			
			throw new RuntimeException("Http IO exception", ioe);
			
		} finally {
			post.releaseConnection();
		}
	}
	
	public String post(String url, Map<String, String> parameters) {
		return this.post(url, parameters, 0);
	}

	public String post(String url, NameValuePair[] postData, long timeout) {
		PostMethod post = new PostMethod(url);
		
		try {
			post.setRequestBody(postData);
			
			if (timeout > 0) {
				post.getParams().setSoTimeout((int) timeout);
			}
			
			int statusCode = this.httpClient.executeMethod(post);
			
			if ((statusCode / 200) != 1) {
				logger.error("Status code " + statusCode + " for " + url + ", data=" + Arrays.toString(postData));
				
				this.failedPostRequestCount++;
				
				throw new HttpException("Status code " + statusCode + " for " + UrlUtil.getDomain(url));
			}
			
			this.successfulPostRequestCount++;
			
			return this.getResponseBodyFromMethod(post);
			
		} catch (HttpException he) {
			this.failedPostRequestCount++;
			
			throw new RuntimeException("Http exception", he);
			
		} catch (IOException ioe) {
			this.failedPostRequestCount++;
			
			throw new RuntimeException("Http IO exception", ioe);
			
		} finally {
			post.releaseConnection();
		}
	}

	public String post(String url, NameValuePair[] postData) {
		return this.post(url, postData, 0);
	}
	
	public String post(String url, Map<String, String> requestHeaders, String postData, String contentType, String encoding, long timeout) {
		StringRequestEntity requestEntity = null;
		
		try {
			requestEntity = new StringRequestEntity(
					postData,
					contentType,
					encoding);
			
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException(uee);
		}
		
		PostMethod postMethod = new PostMethod(url);
		postMethod.setRequestEntity(requestEntity);
		
		if (StringUtil.hasText(contentType)) {
			postMethod.setRequestHeader("Content-Type", contentType);
		}
		
		for (Map.Entry<String, String> entry : requestHeaders.entrySet()) {
			postMethod.setRequestHeader(entry.getKey(), entry.getValue());
		}
		
		if (timeout > 0) {
			postMethod.getParams().setSoTimeout((int) timeout);
		}
		
		int statusCode = 0;
		
		try {
			statusCode = httpClient.executeMethod(postMethod);
			
			if ((statusCode / 200) != 1) {
				logger.error("Http POST error (" + statusCode + "), url=" + url + ", postData=" + postData + ", contentType=" + contentType + ", encoding=" + encoding + ", timeout=" + timeout + " - responseBody=" + postMethod.getResponseBodyAsString() + ", responseStream=" + StreamUtil.convertStreamToString(postMethod.getResponseBodyAsStream(), "UTF-8"));
				
				this.failedPostRequestCount++;
				
				throw new RuntimeException("Http POST error (" + statusCode + ")");
			}
			
			this.successfulPostRequestCount++;
			
			return this.getResponseBodyFromMethod(postMethod);
			
		} catch (IOException ioe) {
			logger.error("Http POST failed, url=" + url + ", postData=" + postData + ", contentType=" + contentType + ", encoding=" + encoding + ", timeout=" + timeout);
			
			this.failedPostRequestCount++;
			
			throw new RuntimeException("Http POST failed", ioe);
			
		} finally {
			postMethod.releaseConnection();
		}
	}
	
	public String post(String url, Map<String, String> requestHeaders, byte[] postData, String contentType, long timeout) {
		ByteArrayRequestEntity requestEntity = new ByteArrayRequestEntity(
				postData,
				contentType);
		
		PostMethod postMethod = new PostMethod(url);
		postMethod.setRequestEntity(requestEntity);
		
		if (StringUtil.hasText(contentType)) {
			postMethod.setRequestHeader("Content-Type", contentType);
		}
		
		for (Map.Entry<String, String> entry : requestHeaders.entrySet()) {
			postMethod.setRequestHeader(entry.getKey(), entry.getValue());
		}
		
		if (timeout > 0) {
			postMethod.getParams().setSoTimeout((int) timeout);
		}
		
		int statusCode = 0;
		
		try {
			statusCode = httpClient.executeMethod(postMethod);
			
			if ((statusCode / 200) != 1) {
				logger.error("Http POST error (" + statusCode + "), url=" + url + ", postData=" + Arrays.toString(postData) + ", contentType=" + contentType + ", timeout=" + timeout + " - responseBody=" + postMethod.getResponseBodyAsString() + ", responseStream=" + StreamUtil.convertStreamToString(postMethod.getResponseBodyAsStream(), "UTF-8"));
				
				this.failedPostRequestCount++;
				
				throw new RuntimeException("Http POST error (" + statusCode + ")");
			}
			
			this.successfulPostRequestCount++;
			
			return this.getResponseBodyFromMethod(postMethod);
			
		} catch (IOException ioe) {
			logger.error("Http POST failed, url=" + url + ", postData=" + Arrays.toString(postData) + ", contentType=" + contentType + ", timeout=" + timeout);
			
			this.failedPostRequestCount++;
			
			throw new RuntimeException("Http POST failed", ioe);
			
		} finally {
			postMethod.releaseConnection();
		}
	}
	
	public String post(String url, String postData, String contentType, String encoding, long timeout) {
		return this.post(
				url,
				new HashMap<String, String>(),
				postData,
				contentType,
				encoding,
				timeout);
	}
	
	public String post(String url, String postData, long timeout) {
		return this.post(
				url,
				postData,
				"text/plain",
				"UTF-8",
				timeout);
	}
	
	public String post(String url, String postData) {
		return this.post(
				url,
				postData,
				"text/plain",
				"UTF-8",
				0);
	}
	
	private String getResponseBodyFromMethod(HttpMethodBase method) throws IOException {
		String charset = "UTF-8";
		
		if (method.getResponseCharSet() != null) {
			charset = method.getResponseCharSet();
		}

		String httpContent;
		
		try {
			httpContent = StreamUtil.convertStreamToString(method.getResponseBodyAsStream(), charset);
			
		} catch (UnsupportedEncodingException e) {
			try {
				logger.warn("Unsupported charset " + charset + " returned from " + method.getURI().getHost());
				
			} catch (URIException e1) {
				logger.warn("Unsupported charset " + charset + " returned");
			}
			
			httpContent = StreamUtil.convertStreamToUtf8String(method.getResponseBodyAsStream());
		}
		
		return httpContent;
	}
	
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
		
		this.httpClient.getParams().setParameter(HttpMethodParams.USER_AGENT, this.userAgent);
	}
	
	public String getUserAgent() {
		return this.userAgent;
	}
	
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
		
		if (this.httpClient != null) {
			this.httpClient.getParams().setParameter(HttpMethodParams.USER_AGENT, this.userAgent);
		}
	}
	
	public long getGetRequestCount() {
		return (this.successfulGetRequestCount + this.failedGetRequestCount);
	}
	
	public long getPostRequestCount() {
		return (this.successfulPostRequestCount + this.failedPostRequestCount);
	}
	
	public float getGetRequestSuccessRate() {
		return ((float) this.successfulGetRequestCount / (this.successfulGetRequestCount + this.failedGetRequestCount));
	}
	
	public float getPostRequestSuccessRate() {
		return ((float) this.successfulPostRequestCount / (this.successfulPostRequestCount + this.failedPostRequestCount));
	}
	
	public long getBytesReceived() {
		return this.bytesReceived;
	}
	
	public void setAllowSelfSignedCertificates(boolean allowSelfSignedCertificates) {
		if (allowSelfSignedCertificates) {
			ProtocolSocketFactory socketFactory = new EasySSLProtocolSocketFactory();
			Protocol https = new Protocol("https", socketFactory, 443);
			Protocol.registerProtocol("https", https);
		}
	}
}