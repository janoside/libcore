package com.janoside.http;

import java.net.InetAddress;
import java.net.SocketAddress;

public interface SocketAddressFactory {
	
	SocketAddress create(InetAddress host, int port);
	
	SocketAddress create(String host, int port);
}