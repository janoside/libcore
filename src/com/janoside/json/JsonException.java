package com.janoside.json;

/**
 * The JsonException is thrown by the Json.org classes then things are amiss.
 * @author Json.org
 * @version 2008-09-18
 */
public class JsonException extends RuntimeException {
	
	private static final long serialVersionUID = 5894276831604379907L;
	
	private Throwable cause;

	/**
	 * Constructs a JsonException with an explanatory message.
	 * @param message Detail about the reason for the exception.
	 */
	public JsonException(String message) {
		super(message);
	}

	public JsonException(Throwable t) {
		super(t.getMessage());
		this.cause = t;
	}

	public Throwable getCause() {
		return this.cause;
	}
}