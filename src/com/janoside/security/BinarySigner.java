package com.janoside.security;

public interface BinarySigner {
	
	byte[] sign(byte[] date, byte[] key);
}