package com.janoside.security;

import javax.crypto.Mac;

public class HmacSha1BinarySigner implements BinarySigner {

	public byte[] sign(byte[] content, byte[] key) {
		Mac mac = SecurityUtil.getMac(key, "HmacSHA1");
		
		byte[] bytes = mac.doFinal(content);
		
		return bytes;
	}
}