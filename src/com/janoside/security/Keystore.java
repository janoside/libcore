package com.janoside.security;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.janoside.security.encryption.Encryptor;
import com.janoside.security.encryption.EncryptorAware;
import com.janoside.util.StringUtil;

public class Keystore implements EncryptorAware {
	
	private Encryptor encryptor;
	
	private Map<String, Set<Keypair>> keypairListsByGroupName;
	
	private Map<String, Keypair> keypairsByPublicKey;
	
	public Keystore() {
		this.keypairListsByGroupName = new HashMap<String, Set<Keypair>>();
		this.keypairsByPublicKey = new HashMap<String, Keypair>();
	}
	
	public Set<Keypair> getKeypairGroup(String groupName) {
		return this.keypairListsByGroupName.get(groupName);
	}
	
	public Keypair getKeypairByPublicKey(String publicKey) {
		return this.keypairsByPublicKey.get(publicKey);
	}
	
	public String getPrivateKey(String publicKey) {
		return this.encryptor.decrypt(this.keypairsByPublicKey.get(publicKey).getEncryptedPrivateKey());
	}
	
	public void addKeypair(Keypair keypair) {
		this.addKeypairs(Arrays.asList(keypair));
	}
	
	public void addKeypair(String groupName, String publicKey, String encryptedPrivateKey) {
		Keypair keypair = new Keypair();
		keypair.setGroupName(groupName);
		keypair.setPublicKey(publicKey);
		keypair.setEncryptedPrivateKey(encryptedPrivateKey);
		
		this.addKeypair(keypair);
	}
	
	private void addKeypairs(List<Keypair> keypairs) {
		for (Keypair keypair : keypairs) {
			this.keypairsByPublicKey.put(keypair.getPublicKey(), keypair);
			
			if (StringUtil.hasText(keypair.getGroupName())) {
				if (!this.keypairListsByGroupName.containsKey(keypair.getGroupName())) {
					this.keypairListsByGroupName.put(keypair.getGroupName(), new HashSet<Keypair>());
				}
				
				this.keypairListsByGroupName.get(keypair.getGroupName()).add(keypair);
			}
		}
	}
	
	public void setEncryptor(Encryptor encryptor) {
		this.encryptor = encryptor;
	}
}