package com.janoside.security;

public interface Authenticator {

	String authenticate(String context, String id, String password);
}