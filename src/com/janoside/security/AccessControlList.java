package com.janoside.security;

public interface AccessControlList<EntityType, ObjectType> {
	
	boolean canAccess(EntityType entity, ObjectType object);
}