package com.janoside.security.encryption;

import java.nio.charset.Charset;

import com.janoside.codec.BinaryEncoder;

public class TextEncryptor implements Encryptor {
	
	private BinaryEncryptor encryptor;
	
	private BinaryEncoder encoder;
	
	private Charset charset;
	
	public TextEncryptor() {
		this.charset = Charset.forName("UTF-8");
	}
	
	public String encrypt(String plaintext) {
		return new String(this.encoder.encode(this.encryptor.encrypt(plaintext.getBytes(this.charset))), this.charset);
	}
	
	public String decrypt(String ciphertext) {
		return new String(this.encryptor.decrypt(this.encoder.decode(ciphertext.getBytes(this.charset))), this.charset);
	}
	
	public void setEncryptor(BinaryEncryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setEncoder(BinaryEncoder encoder) {
		this.encoder = encoder;
	}
}