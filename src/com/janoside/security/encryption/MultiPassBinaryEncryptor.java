package com.janoside.security.encryption;


public class MultiPassBinaryEncryptor implements BinaryEncryptor {
	
	private BinaryEncryptor encryptor;
	
	private int passCount;
	
	public MultiPassBinaryEncryptor() {
		this.passCount = 10;
	}
	
	public byte[] encrypt(byte[] input) {
		byte[] temp = input;
		
		for (int i = 0; i < this.passCount; i++) {
			temp = this.encryptor.encrypt(temp);
		}
		
		return temp;
	}
	
	public byte[] decrypt(byte[] input) {
		byte[] temp = input;
		
		for (int i = 0; i < this.passCount; i++) {
			temp = this.encryptor.decrypt(temp);
		}
		
		return temp;
	}
	
	public void setEncryptor(BinaryEncryptor encryptor) {
		this.encryptor = encryptor;
	}
	
	public void setPassCount(int passCount) {
		this.passCount = passCount;
	}
}