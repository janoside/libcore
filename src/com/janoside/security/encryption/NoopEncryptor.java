package com.janoside.security.encryption;

public class NoopEncryptor implements Encryptor {
	
	public String encrypt(String plaintext) {
		return plaintext;
	}
	
	public String decrypt(String ciphertext) {
		return ciphertext;
	}
}