package com.janoside.security.encryption;

import com.janoside.util.ByteUtils;
import com.janoside.util.StringUtil;

public class XorBinaryEncryptor implements BinaryEncryptor {
	
	private String password;
	
	public XorBinaryEncryptor(String password) {
		this.password = password;
	}
	
	public byte[] encrypt(byte[] cleartext) {
		return ByteUtils.xor(cleartext, StringUtil.utf8BytesFromString(this.password));
	}
	
	public byte[] decrypt(byte[] ciphertext) {
		return ByteUtils.xor(ciphertext, StringUtil.utf8BytesFromString(this.password));
	}
}