package com.janoside.security.encryption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;

public class MigrationEncryptor implements Encryptor, ExceptionHandlerAware, StatTrackerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(MigrationEncryptor.class);
	
	private Encryptor oldEncryptor;
	
	private Encryptor newEncryptor;
	
	private Encryptor lastSuccessfulEncryptor;
	
	private ExceptionHandler exceptionHandler;
	
	private StatTracker statTracker;
	
	public MigrationEncryptor() {
		this.exceptionHandler = new StandardErrorExceptionHandler();
		this.lastSuccessfulEncryptor = null;
	}
	
	public String encrypt(String plaintext) {
		return this.newEncryptor.encrypt(plaintext);
	}
	
	public String decrypt(String ciphertext) {
		if (this.lastSuccessfulEncryptor == null) {
			this.lastSuccessfulEncryptor = this.oldEncryptor;
		}
		
		try {
			String value = this.lastSuccessfulEncryptor.decrypt(ciphertext);
			
			this.trackEncryptionSuccess();
			
			return value;
			
		} catch (Throwable t) {
			this.trackEncryptionFailure();
			
			logger.warn("Error decrypting data with " + (this.lastSuccessfulEncryptor == this.oldEncryptor ? "old encryptor; trying new encryptor" : "new encryptor; trying old encryptor"));
			
			if (this.lastSuccessfulEncryptor == this.oldEncryptor) {
				String value = this.newEncryptor.decrypt(ciphertext);
				
				this.lastSuccessfulEncryptor = this.newEncryptor;
				
				this.trackEncryptionSuccess();
				
				return value;
				
			} else {
				
				String value = this.oldEncryptor.decrypt(ciphertext);
				
				this.lastSuccessfulEncryptor = this.oldEncryptor;
				
				this.trackEncryptionSuccess();
				
				return value;
			}
		}
	}
	
	private void trackEncryptionSuccess() {
		if (this.statTracker != null) {
			if (this.lastSuccessfulEncryptor == this.oldEncryptor) {
				this.statTracker.trackEvent("encryption.migration.old-encryptor.success");
				
			} else {
				this.statTracker.trackEvent("encryption.migration.new-encryptor.success");
			}
		}
	}
	
	private void trackEncryptionFailure() {
		if (this.statTracker != null) {
			if (this.lastSuccessfulEncryptor == this.oldEncryptor) {
				this.statTracker.trackEvent("encryption.migration.old-encryptor.failure");
				
			} else {
				this.statTracker.trackEvent("encryption.migration.new-encryptor.failure");
			}
		}
	}
	
	public void setOldEncryptor(Encryptor oldEncryptor) {
		this.oldEncryptor = oldEncryptor;
	}
	
	public void setNewEncryptor(Encryptor newEncryptor) {
		this.newEncryptor = newEncryptor;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
}