package com.janoside.security.encryption;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.AlgorithmParameters;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.janoside.security.SecurityUtil;
import com.janoside.util.ByteUtils;
import com.janoside.util.RandomUtil;
import com.janoside.util.StringUtil;

// http://stackoverflow.com/questions/1284771/java-pbewithmd5anddes
// http://stackoverflow.com/questions/992019/java-256bit-aes-encryption
public class AesBinaryEncryptor implements BinaryEncryptor {
	
	private static final Charset Utf8 = Charset.forName("UTF-8");
	
	// the number of bytes in the final ciphertext wrapper allotted for the key-derivation iterations
	private static final int KeyIterationsByteCount = 10;
	
	// the number of bytes in the final ciphertext wrapper allotted for the key length
	private static final int KeyLengthByteCount = 4;
	
	private static final int TimestampByteCount = 8;
	
	private static final int SaltByteCount = 8;
	
	private static final int IvByteCount = 16;
	
	private SecretKeyFactory secretKeyFactory;
	
	private SecureRandom secureRandom;
	
	private char[] password;
	
	private int keyLength;
	
	private int keyDerivationBaseIterations;
	
	public AesBinaryEncryptor(String password, int keyLength, int keyDerivationIterations) {
		this.password = password.toCharArray();
		
		this.keyDerivationBaseIterations = keyDerivationIterations;
		this.keyLength = keyLength;
		
		this.afterPropertiesSet();
	}
	
	public AesBinaryEncryptor(String password) {
		this.password = password.toCharArray();
		
		this.keyDerivationBaseIterations = 200;
		this.keyLength = 256;
		
		this.afterPropertiesSet();
	}
	
	public void afterPropertiesSet() {
		this.secureRandom = new SecureRandom();
		
		this.secretKeyFactory = SecurityUtil.getSecretKeyFactory("PBKDF2WithHmacSHA1");
	}
	
	public byte[] encrypt(byte[] plaintext) {
		try {
			byte[] salt = new byte[SaltByteCount];
			
			this.secureRandom.nextBytes(salt);
			
			// random number of iterations
			int keyDerivationIterations = (int) (this.keyDerivationBaseIterations * RandomUtil.randomFloat(1.1f, 1.6f));
			
			KeySpec keySpec = new PBEKeySpec(
					this.password,
					salt,
					keyDerivationIterations,
					this.keyLength);
			
			SecretKey secretKey = new SecretKeySpec(
					this.secretKeyFactory.generateSecret(keySpec).getEncoded(),
					"AES");
			
			Cipher encryptionCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			encryptionCipher.init(Cipher.ENCRYPT_MODE, secretKey);
			
			byte[] ciphertext = encryptionCipher.doFinal(plaintext);
			
			
			AlgorithmParameters params = encryptionCipher.getParameters();
			byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			
			Date date = new Date();
			long timestamp = date.getTime() - 1262304000000L;
			
			stream.write(ciphertext); // 0
			stream.write(ByteBuffer.allocate(8).putLong(timestamp).array()); // 1
			stream.write(iv); // 2
			stream.write(salt); // 3
			stream.write(StringUtil.padIntWithZeroes(this.keyLength, KeyLengthByteCount).getBytes(Utf8)); // 4
			stream.write(StringUtil.padIntWithZeroes(keyDerivationIterations, KeyIterationsByteCount).getBytes(Utf8)); // 5
			
			return stream.toByteArray();
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public byte[] decrypt(byte[] ciphertext) {
		try {
			int ciphertextLength = ciphertext.length - KeyIterationsByteCount - KeyLengthByteCount - TimestampByteCount - IvByteCount - SaltByteCount;
			
			List<byte[]> arrayChunks = ByteUtils.splitByteArrayByLengths(ciphertext, new int[] {ciphertextLength, TimestampByteCount, IvByteCount, SaltByteCount, KeyLengthByteCount, KeyIterationsByteCount}); 
			
			byte[] ciphertextBytes = arrayChunks.get(0);
			//byte[] timestampBytes = arrayChunks.get(1); // for use with list of time-limited keys
			byte[] ivBytes = arrayChunks.get(2);
			byte[] saltBytes = arrayChunks.get(3);
			byte[] keyLengthBytes = arrayChunks.get(4);
			byte[] keyIterationsBytes = arrayChunks.get(5);
			
			int thisKeyLength = Integer.parseInt(new String(keyLengthBytes, Utf8));
			int thisKeyDerivationIterations = Integer.parseInt(new String(keyIterationsBytes, Utf8));
			
			KeySpec keySpec = new PBEKeySpec(
					this.password,
					saltBytes,
					thisKeyDerivationIterations,
					thisKeyLength);
			
			SecretKey secretKey = new SecretKeySpec(
					this.secretKeyFactory.generateSecret(keySpec).getEncoded(),
					"AES");
			
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(ivBytes));
			
			return cipher.doFinal(ciphertextBytes);
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
}