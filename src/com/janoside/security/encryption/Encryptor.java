package com.janoside.security.encryption;

public interface Encryptor {

	String encrypt(String plaintext);
	
	String decrypt(String ciphertext);
}