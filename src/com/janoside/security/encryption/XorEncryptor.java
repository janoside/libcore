package com.janoside.security.encryption;

import java.nio.charset.Charset;

import com.janoside.codec.Base64Encoder;
import com.janoside.codec.BinaryEncoder;

public class XorEncryptor implements Encryptor {
	
private static final Charset Utf8 = Charset.forName("UTF-8");
	
	private XorBinaryEncryptor encryptor;
	
	private BinaryEncoder encoder;
	
	public XorEncryptor(String password) {
		this.encryptor = new XorBinaryEncryptor(password);
		this.encoder = new Base64Encoder();
	}
	
	public String encrypt(String plaintext) {
		return new String(this.encoder.encode(this.encryptor.encrypt(plaintext.getBytes(Utf8))), Utf8);
	}
	
	public String decrypt(String ciphertext) {
		return new String(this.encryptor.decrypt(this.encoder.decode(ciphertext.getBytes(Utf8))), Utf8);
	}
	
	public void setEncoder(BinaryEncoder encoder) {
		this.encoder = encoder;
	}
}