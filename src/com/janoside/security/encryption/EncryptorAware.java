package com.janoside.security.encryption;

public interface EncryptorAware {
	
	void setEncryptor(Encryptor encryptor);
}