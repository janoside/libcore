package com.janoside.security.encryption;

public interface BinaryEncryptor {
	
	byte[] encrypt(byte[] cleartext);
	
	byte[] decrypt(byte[] ciphertext);
}