package com.janoside.security;

import javax.net.ssl.SSLSocket;

public interface SSLSocketFactory {
	
	SSLSocket createSSLSocket();
}