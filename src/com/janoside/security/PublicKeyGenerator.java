package com.janoside.security;

import java.security.PublicKey;

public interface PublicKeyGenerator {
	
	PublicKey generatePublicKey(String base64EncodedPublicKey);
}