package com.janoside.security;

public interface IntegerObfuscator {

	String obfuscate(long key) throws Exception;
	
	long deobfuscate(String str) throws Exception;
}