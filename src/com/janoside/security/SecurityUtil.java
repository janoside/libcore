package com.janoside.security;

import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.SecretKeySpec;

import com.janoside.hash.Hasher;

public final class SecurityUtil {
	
	private SecurityUtil() {}
	
	public static SecretKeyFactory getSecretKeyFactory(String algorithm) {
		try {
			return SecretKeyFactory.getInstance(algorithm);
			
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

    public static byte[] generateKeystore(KeyPair keyPair, X509Certificate cert, String keystorePassword, String keyPassword) {
		try {
			KeyStore keystore = KeyStore.getInstance("PKCS12");
			keystore.load(null, keystorePassword.toCharArray());
			
			keystore.setKeyEntry(
					"test",
					keyPair.getPrivate(),
					keyPassword.toCharArray(),
					new java.security.cert.Certificate[] { cert });
			
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			
			keystore.store(outputStream, keystorePassword.toCharArray());
			
			return outputStream.toByteArray();
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public static String sign(Signer signer, Hasher<String, String> privateKeyHasher, Map parameters, long timestamp, String publicKey, String privateKey) {
		return SecurityUtil.sign(
				signer,
				privateKeyHasher,
				parameters,
				timestamp,
				publicKey,
				privateKey,
				new HashSet<String>());
	}
	
	public static String sign(Signer signer, Hasher<String, String> privateKeyHasher, Map<String, String> parameters, long timestamp, String publicKey, String privateKey, Set<String> signatureIgnoredKeys) {
		HashMap map = new HashMap(parameters);
		map.put("timestamp", Long.toString(timestamp));
		map.put("publicKey", publicKey);
		
		ArrayList<String> keys = new ArrayList<String>(map.keySet());
		Collections.sort(keys);
		
		StringBuilder buffer = new StringBuilder();
		for (String key : keys) {
			if (!signatureIgnoredKeys.contains(key)) {
				buffer.append(key);
				buffer.append('=');
				buffer.append(map.get(key));
				buffer.append(',');
			}
		}
		
		buffer.deleteCharAt(buffer.length() - 1);
		
		return signer.sign(buffer.toString(), privateKeyHasher.hash(privateKey));
	}
	
	public static String getProvidersAndSupportedAlgorithms() {
		StringBuilder buffer = new StringBuilder();
		
		for (Provider provider : Security.getProviders()) {
			buffer.append(String.format("Provider: %s%n", provider.getName()));
			
			for (Provider.Service service : provider.getServices()) {
				buffer.append(String.format("  Algorithm: %s%n", service.getAlgorithm()));
			}
		}
		
		return buffer.toString();
	}
	
	public static KeyFactory getKeyFactory(String algorithm) {
		try {
			return KeyFactory.getInstance(algorithm);
			
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Mac getMac(byte[] key, String algorithm) {
		SecretKeySpec privateKey = new SecretKeySpec(key, algorithm);
		
		return SecurityUtil.getMac(privateKey, algorithm);
	}
	
	public static Mac getMac(SecretKeySpec secretKey, String algorithm) {
		try {
			Mac mac = Mac.getInstance(algorithm);
			mac.init(secretKey);
			
			return mac;
			
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Unsupported algorithm HmacSHA1", e);
			
		} catch (InvalidKeyException e) {
			throw new RuntimeException("Invalid key", e);
		}
	}
}