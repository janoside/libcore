package com.janoside.security;

/**
 * Simple helper class that exposes a single method <code>canPerform()</code>.
 * When this method returns true the RateLimiter assumes that the caller
 * actually performed the action and counts it as an occurrence in the current
 * occurrence-limited window.
 * 
 * @author janoside
 */
public class RateLimiter {
	
	private long period;
	
	private long currentPeriodStart;
	
	private int maxOccurrencesPerPeriod;
	
	private int currentPeriodOccurrences;
	
	public boolean canPerform() {
		if (this.maxOccurrencesPerPeriod < 1) {
			return false;
		}
		
		if (this.currentPeriodStart == 0 && this.maxOccurrencesPerPeriod > 0) {
			// hasn't been performed yet
			this.onPerformed();
			
			return true;
		}
		
		if (System.currentTimeMillis() < (this.currentPeriodStart + this.period)) {
			if (this.currentPeriodOccurrences < this.maxOccurrencesPerPeriod) {
				this.onPerformed();
				
				return true;
				
			} else {
				return false;
			}
		} else {
			// window has passed, this starts a new one
			this.onPerformed();
			
			return true;
		}
	}
	
	private void onPerformed() {
		if (this.currentPeriodStart == 0) {
			// first run
			this.currentPeriodStart = System.currentTimeMillis();
		}
		
		if (System.currentTimeMillis() >= (this.currentPeriodStart + this.period)) {
			// period expired, reset
			this.currentPeriodStart = System.currentTimeMillis();
			this.currentPeriodOccurrences = 0;
		}
		
		this.currentPeriodOccurrences++;
	}
	
	public long getPeriod() {
		return this.period;
	}
	
	public void setPeriod(long period) {
		this.period = period;
	}
	
	public long getCurrentPeriodStart() {
		return this.currentPeriodStart;
	}
	
	public int getMaxOccurrencesPerPeriod() {
		return this.maxOccurrencesPerPeriod;
	}
	
	public void setMaxOccurrencesPerPeriod(int maxOccurrencesPerPeriod) {
		this.maxOccurrencesPerPeriod = maxOccurrencesPerPeriod;
	}
	
	public int getCurrentPeriodOccurrences() {
		return this.currentPeriodOccurrences;
	}
}