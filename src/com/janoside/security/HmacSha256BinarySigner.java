package com.janoside.security;

import javax.crypto.Mac;

public class HmacSha256BinarySigner implements BinarySigner {
	
	public byte[] sign(byte[] content, byte[] key) {
		Mac mac = SecurityUtil.getMac(key, "HmacSHA256");
		
		byte[] bytes = mac.doFinal(content);
		
		return bytes;
	}
}