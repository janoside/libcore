package com.janoside.security;

import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import com.janoside.codec.Base64Encoder;
import com.janoside.util.StringUtil;

public class RsaPublicKeyGenerator implements PublicKeyGenerator {
	
	private static final String KeyFactoryAlgorithm = "RSA";
	
	private Base64Encoder base64Encoder;
	
	public RsaPublicKeyGenerator() {
		this.base64Encoder = new Base64Encoder();
	}
	
	public PublicKey generatePublicKey(String base64EncodedPublicKey) {
		try {
			byte[] decodedKey = this.base64Encoder.decode(StringUtil.utf8BytesFromString(base64EncodedPublicKey));
			
			KeyFactory keyFactory = SecurityUtil.getKeyFactory(KeyFactoryAlgorithm);
			
			return keyFactory.generatePublic(new X509EncodedKeySpec(decodedKey));
			
		} catch (InvalidKeySpecException e) {
			throw new IllegalArgumentException(e);
		}
	}
}