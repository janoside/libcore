package com.janoside.security;

import com.janoside.codec.HexByteArrayStringEncoder;

public class HmacSha1Signer extends DelegatingSigner {

	public HmacSha1Signer() {
		super(new HmacSha1BinarySigner(), new HexByteArrayStringEncoder());
	}
}