package com.janoside.security;

import com.janoside.codec.ByteArrayStringEncoder;
import com.janoside.util.StringUtil;

public class DelegatingSigner implements Signer {
	
	private BinarySigner binarySigner;
	
	private ByteArrayStringEncoder encoder;
	
	public DelegatingSigner(BinarySigner binarySigner, ByteArrayStringEncoder encoder) {
		this.binarySigner = binarySigner;
		this.encoder = encoder;
	}
	
	public String sign(String content, String key) {
		byte[] contentBytes = StringUtil.utf8BytesFromString(content);
		byte[] keyBytes = StringUtil.utf8BytesFromString(key);
		
		return this.encoder.encode(this.binarySigner.sign(contentBytes, keyBytes));
	}
}