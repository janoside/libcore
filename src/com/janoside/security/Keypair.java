package com.janoside.security;

public final class Keypair {
	
	private String groupName;
	
	private String publicKey;
	
	private String encryptedPrivateKey;
	
	public String getGroupName() {
		return groupName;
	}
	
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getPublicKey() {
		return publicKey;
	}
	
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	
	public String getEncryptedPrivateKey() {
		return encryptedPrivateKey;
	}
	
	public void setEncryptedPrivateKey(String encryptedPrivateKey) {
		this.encryptedPrivateKey = encryptedPrivateKey;
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof Keypair) {
			return ((Keypair) o).getPublicKey().equals(this.publicKey);
			
		} else {
			return false;
		}
	}
	
	public int hashCode() {
		return this.publicKey.hashCode();
	}
}