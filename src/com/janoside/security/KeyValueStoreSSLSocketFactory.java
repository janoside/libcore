package com.janoside.security;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;

import com.janoside.storage.KeyValueStore;

public class KeyValueStoreSSLSocketFactory implements SSLSocketFactory {
	
	private KeyValueStore<byte[]> keystoreKeyValueStore;
	
	private String certificateKey;
	
	private String host;
	
	private String keystorePassword;
	
	private String keyDataPassword;
	
	private String protocol;
	
	private int port;
	
	public KeyValueStoreSSLSocketFactory() {
		this.protocol = "TLS";
	}
	
	public SSLSocket createSSLSocket() {
		InputStream inputStream;
		
		try {
			byte[] certificateData = this.keystoreKeyValueStore.get(this.certificateKey);
			
			inputStream = new ByteArrayInputStream(certificateData);
			
			SSLContext sslContext = SSLContext.getInstance(this.protocol);
			KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
			KeyStore keyStore = KeyStore.getInstance("PKCS12");
			
			keyStore.load(inputStream, this.keystorePassword.toCharArray());
			keyManagerFactory.init(keyStore, this.keyDataPassword.toCharArray());
			sslContext.init(keyManagerFactory.getKeyManagers(), null, null);
			
			javax.net.ssl.SSLSocketFactory socketFactory = sslContext.getSocketFactory();
			
			SSLSocket socket = (SSLSocket) socketFactory.createSocket(this.host, this.port);
			
			socket.setKeepAlive(true);
			socket.startHandshake();
			
			return socket;
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public void setKeystoreKeyValueStore(KeyValueStore<byte[]> keystoreKeyValueStore) {
		this.keystoreKeyValueStore = keystoreKeyValueStore;
	}
	
	public void setCertificateKey(String certificateKey) {
		this.certificateKey = certificateKey;
	}
	
	public void setHost(String host) {
		this.host = host;
	}
	
	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}
	
	public void setKeyDataPassword(String keyDataPassword) {
		this.keyDataPassword = keyDataPassword;
	}
	
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
}