package com.janoside.security;

public interface Signer {

	String sign(String content, String key);
}