package com.janoside.security;

public interface ActionProtector {
	
	boolean canPerform(String actionName, Object performer);
}