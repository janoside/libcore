package com.janoside.security;

import com.janoside.codec.HexByteArrayStringEncoder;

public class HmacSha256Signer extends DelegatingSigner {
	
	public HmacSha256Signer() {
		super(new HmacSha256BinarySigner(), new HexByteArrayStringEncoder());
	}
}