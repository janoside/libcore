package com.janoside.hash;

import java.security.MessageDigest;

import com.janoside.util.ByteUtils;
import com.janoside.util.HashUtil;
import com.janoside.util.StringUtil;

public class Sha256StringHasher implements Hasher<String, String> {
	
	public String hash(String value) {
		MessageDigest md = HashUtil.getMessageDigest("SHA-256");
		
		md.update(StringUtil.utf8BytesFromString(value), 0, value.length());
		
		byte[] sha1hash = md.digest();
		
		return ByteUtils.convertToHex(sha1hash);
	}
}