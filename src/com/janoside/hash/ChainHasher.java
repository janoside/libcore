package com.janoside.hash;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("unchecked")
public class ChainHasher<T, U extends Serializable> implements Hasher<T, U> {
	
	private List<Hasher> hashers;
	
	public U hash(T object) {
		Object value = object;
		
		for (Hasher hasher : this.hashers) {
			value = hasher.hash(value);
		}
		
		return (U) value;
	}
	
	public void setHashers(List<Hasher> hashers) {
		this.hashers = hashers;
	}
}