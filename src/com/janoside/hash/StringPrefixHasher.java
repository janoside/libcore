package com.janoside.hash;

import java.util.HashMap;
import java.util.Map;

public class StringPrefixHasher implements Hasher<String, Integer> {
	
	private Map<String, Integer> hashcodesByPrefix;
	
	public Integer hash(String key) {
		for (Map.Entry<String, Integer> entry : this.hashcodesByPrefix.entrySet()) {
			if (key.startsWith(entry.getKey())) {
				return entry.getValue();
			}
		}
		
		return -1;
	}
	
	public void setHashcodesByPrefix(Map<String, Integer> hashcodesByPrefix) {
		this.hashcodesByPrefix = new HashMap<String, Integer>(hashcodesByPrefix);
	}
}