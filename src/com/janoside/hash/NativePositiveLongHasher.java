package com.janoside.hash;

public class NativePositiveLongHasher<T> implements Hasher<T, Long> {
	
	public Long hash(T object) {
		long value = (long) object.hashCode();
		
		if (value < 0) {
			return -value;
			
		} else {
			return value;
		}
	}
}