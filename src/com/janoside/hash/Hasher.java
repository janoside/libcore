package com.janoside.hash;

import java.io.Serializable;

public interface Hasher<T, U extends Serializable> {
	
	U hash(T object);
}