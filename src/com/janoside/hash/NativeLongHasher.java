package com.janoside.hash;

public class NativeLongHasher<T> implements Hasher<T, Long> {
	
	public Long hash(T object) {
		return (long) object.hashCode();
	}
}