package com.janoside.hash;

public class NativeHasher<T> implements Hasher<T, Integer> {
	
	public Integer hash(T object) {
		return object.hashCode();
	}
}