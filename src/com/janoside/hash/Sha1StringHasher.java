package com.janoside.hash;

import java.security.MessageDigest;

import com.janoside.util.ByteUtils;
import com.janoside.util.HashUtil;
import com.janoside.util.StringUtil;

public class Sha1StringHasher implements Hasher<String, String> {
	
	private String salt;
	
	public Sha1StringHasher() {
		this.salt = "monkey-house";
	}
	
	public String hash(String value) {
		String saltyValue = value + salt;
		
		MessageDigest md = HashUtil.getMessageDigest("SHA-1");
		
		md.update(StringUtil.utf8BytesFromString(saltyValue), 0, saltyValue.length());
		
		byte[] sha1hash = md.digest();
		
		return ByteUtils.convertToHex(sha1hash);
	}
	
	public void setSalt(String salt) {
		this.salt = salt;
	}
}