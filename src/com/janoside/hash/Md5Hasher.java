package com.janoside.hash;

import java.security.MessageDigest;

import com.janoside.util.HashUtil;
import com.janoside.util.StringUtil;

public class Md5Hasher implements Hasher<String, String> {
	
	private static final String HEXES = "0123456789abcdef";
	
	public String hash(String value) {
		MessageDigest messageDigest = HashUtil.getMessageDigest("MD5");
		messageDigest.update(StringUtil.utf8BytesFromString(value), 0, value.length());
		
		byte[] sum = messageDigest.digest();
		
		final StringBuilder hex = new StringBuilder(2 * sum.length);
		for (final byte b : sum) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
		}
		
		return hex.toString();
	}
}