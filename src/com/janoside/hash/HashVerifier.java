package com.janoside.hash;

import java.io.Serializable;

public interface HashVerifier<T extends Serializable> {
	
	boolean verify(T input, T hash);
}