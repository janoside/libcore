package com.janoside.hash;

public class MultiPassStringHasher implements Hasher<String, String> {
	
	private Hasher<String, String> hasher;
	
	private int passCount;
	
	public MultiPassStringHasher() {
		this.passCount = 5;
	}
	
	public String hash(String string) {
		String temp = string;
		
		for (int i = 0; i < this.passCount; i++) {
			temp = this.hasher.hash(temp);
		}
		
		return temp;
	}
	
	public void setHasher(Hasher<String, String> hasher) {
		this.hasher = hasher;
	}
	
	public void setPassCount(int passCount) {
		this.passCount = passCount;
	}
}