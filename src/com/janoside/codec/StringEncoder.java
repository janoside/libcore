package com.janoside.codec;

public interface StringEncoder {
	
	String encode(String input);
	
	String decode(String input);
}