package com.janoside.codec;

import com.janoside.util.StringUtil;

public class DelegatingByteArrayStringEncoder implements ByteArrayStringEncoder {
	
	private BinaryEncoder encoder;
	
	public String encode(byte[] input) {
		return StringUtil.utf8StringFromBytes(this.encoder.encode(input));
	}
	
	public byte[] decode(String input) {
		return this.encoder.decode(StringUtil.utf8BytesFromString(input));
	}
	
	public void setEncoder(BinaryEncoder encoder) {
		this.encoder = encoder;
	}
}