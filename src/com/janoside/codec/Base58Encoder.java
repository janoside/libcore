package com.janoside.codec;

public class Base58Encoder implements BinaryEncoder {
	
	public byte[] encode(byte[] binaryData) {
		return Base58.encode(binaryData);
	}
	
	public byte[] decode(byte[] binaryData) {
		return Base58.decode(binaryData);
	}
}