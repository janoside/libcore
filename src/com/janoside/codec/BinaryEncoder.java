package com.janoside.codec;

public interface BinaryEncoder {
	
	byte[] encode(byte[] binaryData);
	
	byte[] decode(byte[] binaryData);
}