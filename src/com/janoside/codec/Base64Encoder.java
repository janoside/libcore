package com.janoside.codec;

import org.apache.commons.codec.binary.Base64;

public class Base64Encoder implements BinaryEncoder {
	
	public byte[] encode(byte[] binaryData) {
		return Base64.encodeBase64(binaryData);
	}
	
	public byte[] decode(byte[] binaryData) {
		return Base64.decodeBase64(binaryData);
	}
}