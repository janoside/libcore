package com.janoside.codec;

import com.janoside.util.StringUtil;

public class DelegatingStringEncoder implements StringEncoder {
	
	private BinaryEncoder encoder;
	
	public String encode(String input) {
		return StringUtil.utf8StringFromBytes(this.encoder.encode(StringUtil.utf8BytesFromString(input)));
	}
	
	public String decode(String input) {
		return StringUtil.utf8StringFromBytes(this.encoder.decode(StringUtil.utf8BytesFromString(input)));
	}
	
	public void setEncoder(BinaryEncoder encoder) {
		this.encoder = encoder;
	}
}