package com.janoside.codec;

import java.util.ArrayList;

public class GzipBase64StringEncoder extends DelegatingStringEncoder {
	
	public GzipBase64StringEncoder() {
		ArrayList<BinaryEncoder> encoders = new ArrayList<BinaryEncoder>();
		encoders.add(new GzipEncoder());
		encoders.add(new Base64Encoder());
		
		ChainEncoder encoder = new ChainEncoder(encoders);
		
		this.setEncoder(encoder);
	}
}