package com.janoside.codec;

import java.util.List;

public class ChainEncoder implements BinaryEncoder {
	
	private List<BinaryEncoder> encoders;
	
	public ChainEncoder(List<BinaryEncoder> encoders) {
		this.encoders = encoders;
	}
	
	public byte[] encode(byte[] binaryData) {
		byte[] data = binaryData;
		for (int i = 0; i < this.encoders.size(); i++) {
			data = this.encoders.get(i).encode(data);
		}
		
		return data;
	}
	
	public byte[] decode(byte[] binaryData) {
		byte[] data = binaryData;
		for (int i = (this.encoders.size() - 1); i >= 0; i--) {
			data = this.encoders.get(i).decode(data);
		}
		
		return data;
	}
}