package com.janoside.codec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

public class HexByteArrayStringEncoder implements ByteArrayStringEncoder {
	
	public byte[] decode(String s) {
		try {
			return Hex.decodeHex(s.toCharArray());
			
		} catch (DecoderException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String encode(byte[] bytes) {
		return new String(Hex.encodeHex(bytes));
	}
}