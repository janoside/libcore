package com.janoside.codec;

import com.janoside.util.GzipUtil;

public class GzipEncoder implements BinaryEncoder {
	
	public byte[] encode(byte[] binaryData) {
		return GzipUtil.gzip(binaryData);
	}
	
	public byte[] decode(byte[] binaryData) {
		return GzipUtil.gunzip(binaryData);
	}
}