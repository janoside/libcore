package com.janoside.codec;

public interface ByteArrayStringEncoder {
	
	byte[] decode(String s);
	
	String encode(byte[] bytes);
}