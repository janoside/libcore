package com.janoside.codec;

import java.util.ArrayList;

public class GzipBase58StringEncoder extends DelegatingStringEncoder {
	
	public GzipBase58StringEncoder() {
		ArrayList<BinaryEncoder> encoders = new ArrayList<BinaryEncoder>();
		encoders.add(new GzipEncoder());
		encoders.add(new Base58Encoder());
		
		ChainEncoder encoder = new ChainEncoder(encoders);
		
		this.setEncoder(encoder);
	}
}