package com.janoside.text;

public class WhitespaceQuoteRemover implements TextTransformer {

	public String transform(String input) {
		String output = input.trim();
		
		while (output.startsWith("\"") || output.endsWith("\"") || output.startsWith(" ") || output.endsWith(" ")) {
			if (output.endsWith("\"") || output.endsWith(" ")) {
				output = output.substring(0, output.length() - 1);
			}

			if (output.startsWith("\"") || output.startsWith(" ")) {
				output = output.substring(1);
			}
		}
		
		return output;
	}
}
