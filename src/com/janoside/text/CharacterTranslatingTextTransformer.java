package com.janoside.text;

import java.util.Map;

import com.janoside.transform.ObjectTransformer;

public class CharacterTranslatingTextTransformer implements ObjectTransformer<String, String> {

	private Map<Character, Character> translationMap;
	
	public String transform(String value) {
		StringBuilder output = new StringBuilder(value.length());

		char[] characters = value.toCharArray();
		
		for (int n = 0; n < characters.length; n++) {
			if (translationMap.containsKey(characters[n])) {
				output.append(translationMap.get(characters[n]));
			} else {
				output.append(characters[n]);
			}
		}
		
		return output.toString();
	}

	public void setTranslationMap(Map<Character, Character> translationMap) {
		this.translationMap = translationMap;
	}
}
