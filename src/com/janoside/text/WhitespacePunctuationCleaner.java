package com.janoside.text;

public class WhitespacePunctuationCleaner implements TextTransformer {
	
	public String transform(String input) {
		String output = input.trim();
		
		while (output.endsWith("?") || output.endsWith(".") || output.endsWith("!")) {
			if (output.endsWith(".")) {
				output = output.substring(0, output.length() - 1);
			}
			
			if (output.endsWith("?")) {
				output = output.substring(0, output.length() - 1);
			}
			
			if (output.endsWith("!")) {
				output = output.substring(0, output.length() - 1);
			}
		}
		
		return output;
	}
}