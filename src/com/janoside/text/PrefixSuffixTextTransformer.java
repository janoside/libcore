package com.janoside.text;

import com.janoside.util.StringUtil;

public class PrefixSuffixTextTransformer implements TextTransformer {
	
	private String prefix;
	
	private String suffix;
	
	private boolean hasPrefix;
	
	private boolean hasSuffix;
	
	public PrefixSuffixTextTransformer() {
		this.hasPrefix = false;
		this.hasSuffix = false;
	}
	
	public String transform(String input) {
		StringBuilder buffer = new StringBuilder(50);
		
		if (this.hasPrefix) {
			buffer.append(this.prefix);
		}
		
		buffer.append(input);
		
		if (this.hasSuffix) {
			buffer.append(this.suffix);
		}
		
		return buffer.toString();
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
		
		this.hasPrefix = StringUtil.hasText(this.prefix);
	}
	
	public void setSuffix(String suffix) {
		this.suffix = suffix;
		
		this.hasSuffix = StringUtil.hasText(this.suffix);
	}
}