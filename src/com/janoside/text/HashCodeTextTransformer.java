package com.janoside.text;

public class HashCodeTextTransformer implements TextTransformer {

	public String transform(String value) {
		return String.valueOf(value.hashCode());
	}

}