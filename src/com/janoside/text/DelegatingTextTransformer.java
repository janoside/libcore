package com.janoside.text;

import java.util.List;

public class DelegatingTextTransformer implements TextTransformer {
	
	private List<TextTransformer> textTransformers;
	
	public String transform(String input) {
		String output = input;
		
		for (TextTransformer textTransformer : this.textTransformers) {
			output = textTransformer.transform(output);
		}
		
		return output;
	}
	
	public void setTextTransformers(List<TextTransformer> textTransformers) {
		this.textTransformers = textTransformers;
	}
}