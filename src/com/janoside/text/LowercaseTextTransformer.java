package com.janoside.text;

public class LowercaseTextTransformer implements TextTransformer {
	
	public String transform(String value) {
		return value.toLowerCase();
	}
}