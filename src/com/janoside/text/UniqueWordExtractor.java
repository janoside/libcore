package com.janoside.text;

import java.util.Set;

public interface UniqueWordExtractor {

	Set<String> getUniqueWords(String document);
}
