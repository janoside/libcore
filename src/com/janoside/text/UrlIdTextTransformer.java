package com.janoside.text;

import java.util.Locale;

public class UrlIdTextTransformer implements TextTransformer {
	
	public String transform(String value) {
		String normalizedValue = value.toLowerCase(Locale.US).trim().replaceAll("\\W+", "-");
		
		if (normalizedValue.startsWith("-")) {
			normalizedValue = normalizedValue.substring(1);
		}
		
		if (normalizedValue.endsWith("-")) {
			normalizedValue = normalizedValue.substring(0, normalizedValue.length() - 1);
		}
		
		return normalizedValue;
	}
}