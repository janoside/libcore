package com.janoside.text;

import com.janoside.transform.ObjectTransformer;

public class MultiPassDelegatingTextTransformer implements ObjectTransformer<String, String> {
	
	private ObjectTransformer<String, String> internalTextTransformer;
	
	private int passCount;
	
	public String transform(String value) {
		String transformedValue = value;
		
		for (int i = 0; i < this.passCount; i++) {
			transformedValue = this.internalTextTransformer.transform(transformedValue);
		}
		
		return transformedValue;
	}
	
	public void setInternalTextTransformer(ObjectTransformer<String, String> internalTextTransformer) {
		this.internalTextTransformer = internalTextTransformer;
	}
	
	public void setPassCount(int passCount) {
		this.passCount = passCount;
	}
}