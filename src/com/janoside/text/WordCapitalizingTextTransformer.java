package com.janoside.text;

public class WordCapitalizingTextTransformer implements TextTransformer {
	
	public String transform(String input) {
		StringBuilder buffer = new StringBuilder(input.length());
		
		char previous = ' ';
		for (int i = 0; i < input.length(); i++) {
			char current = input.charAt(i);
			
			if (!Character.isLetter(previous) && !Character.isDigit(previous)) {
				buffer.append(Character.toUpperCase(current));
				
			} else {
				buffer.append(current);
			}
			
			previous = current;
		}
		
		return buffer.toString();
	}
}