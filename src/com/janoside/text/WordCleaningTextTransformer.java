package com.janoside.text;

import java.util.ArrayList;

import com.janoside.util.StringUtil;

/**
 * TextTransformer implementation that removes all punctuation from
 * between words, lowercases, and replaces all whitespace between
 * words with single spaces.
 * 
 * @author janoside
 */
public class WordCleaningTextTransformer implements TextTransformer {
	
	public String transform(String input) {
		String[] words = input.toLowerCase().split("\\s+");
		
		ArrayList<String> wordList = new ArrayList<String>(words.length);
		for (String word : words) {
			while (word.length() > 0 && !Character.isLetterOrDigit(word.charAt(0))) {
				word = word.substring(1);
			}
			
			while (word.length() > 0 && !Character.isLetterOrDigit(word.charAt(word.length() - 1))) {
				word = word.substring(0, word.length() - 1);
			}
			
			if (word.length() > 0) {
				wordList.add(word);
			}
		}
		
		return StringUtil.collectionToDelimitedString(wordList, " ", "", "");
	}
}