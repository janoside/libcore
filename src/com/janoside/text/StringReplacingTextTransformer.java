package com.janoside.text;

import java.util.Map;

public class StringReplacingTextTransformer implements TextTransformer {
	
	private Map<String, String> stringsToReplace;
	
	public String transform(String value) {
		String output = value;
		
		for (Map.Entry<String, String> entry : this.stringsToReplace.entrySet()) {
			output = output.replace(entry.getKey(), entry.getValue());
		}
		
		return output;
	}
	
	public String viewStringsToReplace() {
		return this.stringsToReplace.toString();
	}
	
	public void setStringsToReplace(Map<String, String> stringsToReplace) {
		this.stringsToReplace = stringsToReplace;
	}
}