package com.janoside.text;

import com.janoside.transform.ObjectTransformer;

public interface TextTransformer extends ObjectTransformer<String, String> {
}