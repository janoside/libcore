package com.janoside.storage;

import com.janoside.keyvalue.KeyValueSource;

public interface KeyValueStore<T> extends KeyValueSource<T> {
	
	void put(String key, T value);
	
	void remove(String key);
}