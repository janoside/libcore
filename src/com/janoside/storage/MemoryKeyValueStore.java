package com.janoside.storage;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.janoside.util.CollectionUtil;
import com.janoside.util.Predicate;

public class MemoryKeyValueStore<T> implements KeyValueStore<T>, KeySearcher {
	
	private Map<String, T> store;
	
	public MemoryKeyValueStore() {
		this.store = new ConcurrentHashMap<String, T>();
	}
	
	public Set<String> getKeys(final String prefix) {
		return CollectionUtil.filterSet(this.store.keySet(), new Predicate<String>() {
			public boolean apply(String key) {
				return key.startsWith(prefix);
			}
		});
	}
	
	public void put(String key, T value) {
		this.store.put(key, value);
	}
	
	public T get(String key) {
		return this.store.get(key);
	}
	
	public void remove(String key) {
		this.store.remove(key);
	}
	
	public void clear() {
		this.store.clear();
	}
	
	public int getSize() {
		return this.store.size();
	}
	
	public Set<String> getKeys() {
		return this.store.keySet();
	}
}