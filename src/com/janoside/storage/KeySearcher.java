package com.janoside.storage;

import java.util.Set;

public interface KeySearcher {
	
	Set<String> getKeys(String prefix);
}