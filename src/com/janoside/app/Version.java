package com.janoside.app;

import java.util.List;

import com.janoside.util.StringUtil;

public class Version implements Comparable<Version> {
	
	private List<Integer> versionParts;
	
	private Version betaVersion;
	
	public Version(String versionString) {
		if (versionString.contains("b")) {
			String beta = versionString.substring(versionString.indexOf('b') + 1);
			
			this.betaVersion = new Version(beta);
			this.versionParts = StringUtil.splitAndParseInt(versionString.substring(0, versionString.indexOf('b')), "\\.");
			
		} else {
			this.versionParts = StringUtil.splitAndParseInt(versionString, "\\.");
		}
	}
	
	public int compareTo(Version v) {
		if (this.versionParts.size() == v.versionParts.size()) {
			for (int i = 0; i < this.versionParts.size(); i++) {
				if (this.versionParts.get(i) > v.versionParts.get(i)) {
					return 1;
					
				} else if (this.versionParts.get(i) < v.versionParts.get(i)) {
					return -1;
				}
			}
			
			if (this.betaVersion != null && v.betaVersion != null) {
				return this.betaVersion.compareTo(v.betaVersion);
				
			} else if (this.betaVersion != null) {
				return -1;
				
			} else if (v.betaVersion != null) {
				return 1;
			}
			
			return 0;
			
		} else if (this.versionParts.size() < v.versionParts.size()) {
			for (int i = 0; i < this.versionParts.size(); i++) {
				if (this.versionParts.get(i) > v.versionParts.get(i)) {
					return 1;
					
				} else if (this.versionParts.get(i) < v.versionParts.get(i)) {
					return -1;
				}
			}
			
			return -1;
			
		} else {
			for (int i = 0; i < v.versionParts.size(); i++) {
				if (this.versionParts.get(i) > v.versionParts.get(i)) {
					return 1;
					
				} else if (this.versionParts.get(i) < v.versionParts.get(i)) {
					return -1;
				}
			}
			
			return 1;
		}
	}
	
	public boolean lessThan(Version v) {
		return this.compareTo(v) < 0;
	}
	
	public boolean greaterThan(Version v) {
		return this.compareTo(v) > 0;
	}
	
	public String toString() {
		if (this.betaVersion != null) {
			return StringUtil.collectionToDelimitedString(this.versionParts, ".", "", "") + "b" + this.betaVersion.toString();
			
		} else {
			return StringUtil.collectionToDelimitedString(this.versionParts, ".", "", "");
		}
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof Version) {
			return this.compareTo((Version) o) == 0;
			
		} else {
			return false;
		}
	}
}