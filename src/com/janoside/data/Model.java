package com.janoside.data;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public abstract class Model implements Serializable {
	
	protected long id;
	
	protected Date createdAt;
	
	protected Date updatedAt;
	
	protected int createdBy;
	
	protected int updatedBy;
	
	/**
	 * Flag, usually not persisted, to identify that a model has been encrypted prior to persistence.
	 */
	private boolean encrypted;
	
	/**
	 * Flag, usually not persisted, to identify that a model has been decrypted after being read from storage.
	 */
	private boolean decrypted;
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public Date getCreatedAt() {
		if (this.createdAt != null) {
			return (Date) this.createdAt.clone();
			
		} else {
			return null;
		}
	}
	
	public void setCreatedAt(Date createdAt) {
		if (createdAt != null) {
			this.createdAt = (Date) createdAt.clone();
			
		} else {
			this.createdAt = null;
		}
	}
	
	public int getCreatedBy() {
		return this.createdBy;
	}
	
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	
	public Date getUpdatedAt() {
		if (this.updatedAt != null) {
			return (Date) this.updatedAt.clone();
			
		} else {
			return null;
		}
	}
	
	public void setUpdatedAt(Date updatedAt) {
		if (updatedAt != null) {
			this.updatedAt = (Date) updatedAt.clone();
			
		} else {
			this.updatedAt = null;
		}
	}
	
	public int getUpdatedBy() {
		return this.updatedBy;
	}
	
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public boolean isEncrypted() {
		return encrypted;
	}
	
	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}
	
	public boolean isDecrypted() {
		return decrypted;
	}
	
	public void setDecrypted(boolean decrypted) {
		this.decrypted = decrypted;
	}
}