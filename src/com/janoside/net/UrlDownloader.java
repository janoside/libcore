package com.janoside.net;

import java.util.Date;

public interface UrlDownloader {
	
	byte[] getData(String url);
	
	String getContentType(String url);
	
	Date getLastModified(String url);
}