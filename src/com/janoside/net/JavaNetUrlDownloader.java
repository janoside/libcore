package com.janoside.net;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import com.janoside.util.StreamUtil;

public class JavaNetUrlDownloader implements UrlDownloader {
	
	public byte[] getData(String url) {
		URLConnection urlConnection = this.getUrlConnection(url);
		
		try {
			return StreamUtil.streamToByteArray(urlConnection.getInputStream());
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String getContentType(String url) {
		URLConnection urlConnection = this.getUrlConnection(url);
		
		return urlConnection.getContentType();
	}
	
	public Date getLastModified(String url) {
		URLConnection urlConnection = this.getUrlConnection(url);
		
		long lastModified = urlConnection.getLastModified();
		
		if (lastModified > 0) {
			return new Date(lastModified);
			
		} else {
			return null;
		}
	}
	
	private URLConnection getUrlConnection(String url) {
		try {
			URL urlObject = new URL(url);
			
			return urlObject.openConnection();
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
}