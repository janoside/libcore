package com.janoside.net;

import java.util.HashMap;

import com.janoside.http.BasicHttpClient;

public class FileUploader {
	
	public String upload(String url, byte[] data) {
		BasicHttpClient http = new BasicHttpClient();
		
		return http.post(url, new HashMap<String, String>(), data, "multipart/form-data", 10000);
	}
}