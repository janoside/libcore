package com.janoside.queue;

public interface ObjectConsumer<T> {
	
	void consume(T object);
}