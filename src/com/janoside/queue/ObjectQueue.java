package com.janoside.queue;

import java.util.List;

public interface ObjectQueue<T> {

	void enqueue(T object);
	
	List<T> dequeue(int count);
	
	void clear();
	
	int getSize();
}