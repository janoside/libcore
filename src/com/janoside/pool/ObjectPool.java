package com.janoside.pool;

public interface ObjectPool<T> {

	void addObject();
	
	T borrowObject();
	
	void returnObject(T object);
	
	void invalidateObject(T object);
	
	int getPoolSize();
	
	int getNumActive();
	
	int getNumIdle();
	
	void clear();
	
	void close();
	
	void setFactory(PoolableObjectFactory<T> factory);
}
