package com.janoside.pool;

public interface SimpleObjectPool<T> {
	
	T borrowObject();
	
	void returnObject(T object);
	
	void removeObject(T object);
	
	void clear();
}