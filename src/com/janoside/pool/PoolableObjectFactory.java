package com.janoside.pool;

public interface PoolableObjectFactory<T> {

	T createObject();
	
	void destroyObject(T object);
	
	void activateObject(T object);
	
	void passivateObject(T object);
}