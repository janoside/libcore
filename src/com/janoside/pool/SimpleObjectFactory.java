package com.janoside.pool;

public interface SimpleObjectFactory<T> {
	
	T createObject();
}