package com.janoside.pool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;

public class ManagedObjectPool<T extends Poolable> implements SimpleObjectPool<T> {
	
	private GenericObjectPool internalPool;
	
	private Set<T> objectSet;
	
	private Set<T> unreturnedObjectSet;
	
	private List<String> objectNameList;
	
	public ManagedObjectPool() {
		this.internalPool = new GenericObjectPool();
		this.internalPool.setMaxIdle(10);
		
		this.objectSet = Collections.synchronizedSet(new HashSet<T>());
		this.unreturnedObjectSet = Collections.synchronizedSet(new HashSet<T>());
		this.objectNameList = Collections.synchronizedList(new ArrayList<String>());
	}
	
	@SuppressWarnings("unchecked")
	public T borrowObject() {
		try {
			T object = null;
			
			while (object == null || !object.isValid()) {
				object = (T) this.internalPool.borrowObject();
				
				if (!object.isValid()) {
					this.internalPool.invalidateObject(object);
				}
			}
			
			this.objectSet.add(object);
			this.unreturnedObjectSet.add(object);
			this.objectNameList.add(object.toString());
			
			return object;
			
		} catch (Exception e) {
			throw new RuntimeException("Failed to borrow object from " + this, e);
		}
	}
	
	public void returnObject(T object) {
		try {
			// clear() can occur while object is "checked out" - don't return if this is the case
			if (this.unreturnedObjectSet.contains(object)) {
				this.internalPool.returnObject(object);
				
				this.unreturnedObjectSet.remove(object);
			}
		} catch (Exception e) {
			throw new RuntimeException("Failed to return object to " + this, e);
		}
	}
	
	public void removeObject(T object) {
		try {
			this.internalPool.invalidateObject(object);
			
			this.objectSet.remove(object);
			this.unreturnedObjectSet.remove(object);
			this.objectNameList.remove(object.toString());
			
		} catch (Exception e) {
			throw new RuntimeException("Failed to remove object from " + this, e);
		}
	}
	
	public void clear() {
		for (T object : this.unreturnedObjectSet) {
			try {
				this.internalPool.invalidateObject(object);
				
			} catch (Exception e) {
				throw new RuntimeException("Failed to invalid object " + object);
			}
		}
		
		this.internalPool.clear();
		this.objectSet.clear();
		this.objectNameList.clear();
		this.unreturnedObjectSet.clear();
	}
	
	public ArrayList<String> viewUnreturnedObjects() {
		ArrayList<String> unreturnedObjects = new ArrayList<String>(this.unreturnedObjectSet.size());
		
		for (T object : this.unreturnedObjectSet) {
			unreturnedObjects.add(object.toString());
		}
		
		Collections.sort(unreturnedObjects);
		
		return unreturnedObjects;
	}
	
	public void setFactory(SimpleObjectFactory<T> factory) {
		this.internalPool.setFactory(new ManagedPoolableObjectFactory<T>(factory));
	}
	
	public int getMaxActive() {
		return this.internalPool.getMaxActive();
	}
	
	public void setMaxActive(int maxActive) {
		this.internalPool.setMaxActive(maxActive);
	}
	
	public int getActiveCount() {
		return this.internalPool.getNumActive();
	}
	
	public boolean getFailWhenExhausted() {
		return (this.internalPool.getWhenExhaustedAction() == GenericObjectPool.WHEN_EXHAUSTED_FAIL);
	}
	
	public void setFailWhenExhausted(boolean failWhenExhasted) {
		if (failWhenExhasted) {
			this.internalPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_FAIL);
			
		} else {
			this.internalPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
		}
	}
	
	private class ManagedPoolableObjectFactory<FactoryType> implements PoolableObjectFactory {
		
		private SimpleObjectFactory<FactoryType> simpleObjectFactory;
		
		public ManagedPoolableObjectFactory(SimpleObjectFactory<FactoryType> simpleObjectFactory) {
			this.simpleObjectFactory = simpleObjectFactory;
			
			validateObject(null);
		}
		
		public void activateObject(Object arg0) throws Exception {
		}
		
		public void destroyObject(Object arg0) throws Exception {
		}
		
		public Object makeObject() throws Exception {
			return this.simpleObjectFactory.createObject();
		}
		
		public void passivateObject(Object arg0) throws Exception {
		}
		
		public boolean validateObject(Object arg0) {
			return false;
		}
	}
}