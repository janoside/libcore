package com.janoside.pool;

public interface Poolable {
	
	boolean isValid();
}