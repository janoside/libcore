package com.janoside.exception;

import com.janoside.stats.MemoryStats;

public class MemoryStatsExceptionHandler implements ExceptionHandler {

	private MemoryStats memoryStats;
	
	public void handleException(Throwable t) {
		this.memoryStats.countException(t);
	}
	
	public void setMemoryStats(MemoryStats memoryStats) {
		this.memoryStats = memoryStats;
	}
}