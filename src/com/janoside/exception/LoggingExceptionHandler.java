package com.janoside.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingExceptionHandler implements ExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(LoggingExceptionHandler.class);
	
	public void handleException(Throwable t) {
		if (t.getMessage() != null) {
			logger.error(t.getMessage(), t);
			
		} else {
			logger.error(t.toString(), t);
		}
	}
}