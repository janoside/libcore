package com.janoside.exception;

public interface ExceptionHandlerAware {

	void setExceptionHandler(ExceptionHandler exceptionHandler);
}