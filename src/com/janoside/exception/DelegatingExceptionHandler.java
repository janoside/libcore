package com.janoside.exception;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DelegatingExceptionHandler implements ExceptionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(DelegatingExceptionHandler.class);
	
	private List<ExceptionHandler> exceptionHandlers;
	
	public DelegatingExceptionHandler() {
		this.exceptionHandlers = new ArrayList<ExceptionHandler>();
	}

	public void handleException(Throwable t) {
		int attempt = 0;
		
		Throwable throwable = t;
		while ((throwable.getStackTrace() == null || throwable.getStackTrace().length == 0) && throwable.getCause() != null && attempt < 5 && (throwable.getCause() instanceof Exception)) {
			throwable = (Exception) throwable.getCause();
			
			attempt++;
		}
		
		if (throwable instanceof StackOverflowError) {
			try {
				throw new RuntimeException("StackOverflowError", throwable);
				
			} catch (Exception invokedException) {
				throwable = invokedException;
			}
		}
		
		if (throwable.getStackTrace() == null || throwable.getStackTrace().length == 0) {
			try {
				throw new RuntimeException("Unable to obtain stacktrace, invoking new exception", throwable);
				
			} catch (Exception invokedException) {
				throwable = invokedException;
			}
		}
		
		final Throwable finalThrowable = throwable;
		
		for (final ExceptionHandler exceptionHandler : this.exceptionHandlers) {
			try {
				exceptionHandler.handleException(finalThrowable);
				
			} catch (Throwable th) {
				logger.error("DANGEROUS: Exception handler " + exceptionHandler + " threw exception: " + th, th);
				logger.error("DANGEROUS: Exception handler " + exceptionHandler + " threw exception while handling: " + finalThrowable, finalThrowable);
			}
		}
	}
	
	public void addExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandlers.add(exceptionHandler);
	}
	
	public void setExceptionHandlers(List<ExceptionHandler> exceptionHandlers) {
		this.exceptionHandlers = new ArrayList<ExceptionHandler>(exceptionHandlers);
	}
}