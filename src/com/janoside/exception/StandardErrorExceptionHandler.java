package com.janoside.exception;

public class StandardErrorExceptionHandler implements ExceptionHandler {
	
	public void handleException(Throwable t) {
		t.printStackTrace();
	}
}