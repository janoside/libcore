package com.janoside.exception;

public interface ExceptionHandler {

	void handleException(Throwable t);
}