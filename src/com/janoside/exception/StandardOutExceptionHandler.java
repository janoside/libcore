package com.janoside.exception;

public class StandardOutExceptionHandler implements ExceptionHandler {
	
	public void handleException(Throwable t) {
		t.printStackTrace();
	}
}