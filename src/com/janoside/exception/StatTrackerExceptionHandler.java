package com.janoside.exception;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.janoside.stats.StatTracker;
import com.janoside.stats.StatTrackerAware;
import com.janoside.util.StringUtil;

public class StatTrackerExceptionHandler implements ExceptionHandler, StatTrackerAware {

	private StatTracker statTracker;
	
	/**
	 * List of package names (replace "." with "_") for which you want to report
	 * errors by name. The deepest exception cause in one of these packages
	 * will be reported.
	 */
	private List<String> relevantPackages;
	
	public StatTrackerExceptionHandler() {
		this.relevantPackages = Arrays.asList("com_janoside");
	}
	
	public void handleException(Throwable t) {
		this.statTracker.trackEvent("exceptions.total");
		
		ArrayList<Throwable> throwableChain = new ArrayList<Throwable>();
		
		Throwable rootCause = t;
		throwableChain.add(rootCause);
		
		while (rootCause.getCause() != null) {
			throwableChain.add(rootCause);
			
			rootCause = rootCause.getCause();
		}
		
		Collections.reverse(throwableChain);
		for (Throwable throwable : throwableChain) {
			if (throwable.getStackTrace() != null && throwable.getStackTrace().length > 0) {
				StackTraceElement ste = throwable.getStackTrace()[0];
				String classNameString = ste.getClassName();
				classNameString = classNameString.substring(classNameString.lastIndexOf('.') + 1);
				
				for (String relevantPackage : this.relevantPackages) {
					if (ste.getClassName().startsWith(relevantPackage.replace('_', '.'))) {
						String eventName = StringUtil.camelCaseToLowercaseDashSeparated(classNameString) + "-" + ste.getLineNumber();
						
						this.statTracker.trackEvent("exceptions." + eventName);
						
						return;
					}
				}
			}
		}
	}
	
	public void setStatTracker(StatTracker statTracker) {
		this.statTracker = statTracker;
	}
	
	public void setRelevantPackages(List<String> relevantPackages) {
		this.relevantPackages = relevantPackages;
	}
}