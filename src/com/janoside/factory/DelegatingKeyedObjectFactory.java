package com.janoside.factory;

import java.util.List;

public class DelegatingKeyedObjectFactory implements KeyedObjectFactory {
	
	private List<KeyedObjectFactory> factories;
	
	public <T> T createObject(String type) {
		for (KeyedObjectFactory factory : this.factories) {
			Object object = factory.createObject(type);
			
			if (object != null) {
				return (T) object;
			}
		}
		
		return null;
	}
	
	public void setFactories(List<KeyedObjectFactory> factories) {
		this.factories = factories;
	}
}