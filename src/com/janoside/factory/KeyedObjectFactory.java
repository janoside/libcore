package com.janoside.factory;

public interface KeyedObjectFactory {
	
	<T> T createObject(String type);
}