package com.janoside.scheduling;

import java.util.Timer;
import java.util.TimerTask;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.StandardErrorExceptionHandler;

public class BasicScheduler implements Scheduler, ExceptionHandlerAware {
	
	private Timer timer;
	
	private ExceptionHandler exceptionHandler;
	
	public BasicScheduler() {
		this.timer = new Timer("Scheduler-Timer-" + Integer.toHexString(this.hashCode()));
		this.exceptionHandler = new StandardErrorExceptionHandler();
	}
	
	public TimerTask scheduleTask(final Runnable runnable, long delay) {
		TimerTask timerTask = new TimerTask() {
			public void run() {
				try {
					runnable.run();
					
				} catch (Throwable t) {
					exceptionHandler.handleException(t);
				}
			}
		};
		
		this.timer.schedule(timerTask, delay);
		
		return timerTask;
	}
	
	public TimerTask scheduleTaskAtConstantRate(final Runnable runnable, long delay, long period) {
		TimerTask timerTask = new TimerTask() {
			public void run() {
				try {
					runnable.run();
					
				} catch (Throwable t) {
					exceptionHandler.handleException(t);
				}
			}
		};
		
		this.timer.scheduleAtFixedRate(timerTask, delay, period);
		
		return timerTask;
	}
	
	public TimerTask scheduleTaskWithConstantIntervalBetween(final Runnable runnable, final long delay, final long intervalBetween) {
		TimerTask timerTask = new TimerTask() {
			public void run() {
				try {
					runnable.run();
					
				} catch (Throwable t) {
					exceptionHandler.handleException(t);
					
				} finally {
					scheduleTaskWithConstantIntervalBetween(runnable, intervalBetween, intervalBetween);
				}
			}
		};
		
		this.timer.schedule(timerTask, delay);
		
		return timerTask;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}