package com.janoside.scheduling;

public interface SchedulerAware {
	
	void setScheduler(Scheduler scheduler);
}