package com.janoside.scheduling;

import java.util.TimerTask;

/**
 * High-level scheduling framework. Should be used instead of Timer.schedule where possible
 * since it will automatically handle exceptions.
 * 
 * @author janoside
 *
 */
public interface Scheduler {
	
	/**
	 * Schedule a one-time task.
	 * @param runnable
	 * @param delay
	 * @return
	 */
	TimerTask scheduleTask(Runnable runnable, long delay);
	
	/**
	 * Schedule a task to *begin* executing at a constant interval, regardless of how long
	 * the task takes to finish executing. This can lead to task "pile-up" and "catch-up"
	 * when tasks take non-negligible time to execute. Identical to calling
	 * Timer.scheduleAtFixedRate.
	 * 
	 * @see scheduleTaskWithConstantIntervalBetween
	 * @param runnable
	 * @param delay
	 * @param rate
	 * @return
	 */
	TimerTask scheduleTaskAtConstantRate(Runnable runnable, long delay, long rate);
	
	/**
	 * Schedule a task to repeatedly execute with a constant interval between the finishing
	 * of the most recent execution and the beginning of the next execution. For example,
	 * if a task takes 100ms to execute and you schedule it with a constant "interval between"
	 * of 50ms the task will execute as follows:
	 * 
	 * 0ms    : start exeuction #1
	 * 100ms  : end execution #1
	 * 150ms  : start execution #2
	 * 250ms  : end execution #2
	 * 300ms  : start execution #3
	 * etc
	 * 
	 * @param runnable
	 * @param delay
	 * @param intervalBetween
	 * @return
	 */
	TimerTask scheduleTaskWithConstantIntervalBetween(Runnable runnable, long delay, long intervalBetween);
}