package com.janoside.environment;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Environment implements Serializable {
	
	private String name;
	
	private String hardwareId;
	
	private String appName;
	
	private String sourcecodeVersion;
	
	public Environment() {
		this.name = "local";
		this.hardwareId = "unspecified-server";
		this.appName = "unspecified-app";
		
		if (System.getProperty("astar.sourcecodeVersion") != null) {
			this.sourcecodeVersion = System.getProperty("astar.sourcecodeVersion");
			
		} else {
			this.sourcecodeVersion = "unknown";
		}
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getHardwareId() {
		return hardwareId;
	}
	
	public void setHardwareId(String hardwareId) {
		this.hardwareId = hardwareId;
	}
	
	public String getAppName() {
		return appName;
	}
	
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	public String getSourcecodeVersion() {
		return sourcecodeVersion;
	}
	
	public void setSourcecodeVersion(String sourcecodeVersion) {
		this.sourcecodeVersion = sourcecodeVersion;
	}
}