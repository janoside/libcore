package com.janoside.environment;

public interface EnvironmentAware {
	
	void setEnvironment(Environment environment);
}