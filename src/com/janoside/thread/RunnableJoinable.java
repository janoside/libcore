package com.janoside.thread;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class RunnableJoinable implements Runnable, Joinable {
	
	private Runnable runnable;
	
	private CountDownLatch latch;
	
	public RunnableJoinable(Runnable runnable) {
		this.runnable = runnable;
		this.latch = new CountDownLatch(1);
	}
	
	public void run() {
		try {
			this.runnable.run();
			
		} finally {
			latch.countDown();
		}
	}
	
	public void join(long waitMillis) {
		try {
			this.latch.await(waitMillis, TimeUnit.MILLISECONDS);
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
	}
	
	public void join() {
		try {
			this.latch.await();
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
	}
}