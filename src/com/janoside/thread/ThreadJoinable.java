package com.janoside.thread;

public class ThreadJoinable implements Joinable {
	
	private Thread thread;
	
	public ThreadJoinable(Thread thread) {
		this.thread = thread;
	}
	
	public void join(long waitMillis) {
		try {
			this.thread.join(waitMillis);
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
	}
	
	public void join() {
		try {
			this.thread.join();
			
		} catch (InterruptedException ie) {
			throw new RuntimeException(ie);
		}
	}
}