package com.janoside.thread;

public interface Joinable {
	
	void join(long waitMillis);
	
	void join();
}