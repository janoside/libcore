package com.janoside.thread;

import java.util.ArrayList;
import java.util.List;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.exception.StandardErrorExceptionHandler;

public class DelegatingJoinable implements Joinable, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	private List<Joinable> joinables;
	
	public DelegatingJoinable() {
		this.exceptionHandler = new StandardErrorExceptionHandler();
		this.joinables = new ArrayList<Joinable>();
	}
	
	public DelegatingJoinable(List<Joinable> joinables) {
		this.joinables = new ArrayList<Joinable>(joinables);
	}
	
	public void join(long waitMillis) {
		Throwable throwable = null;
		
		long startTime = System.currentTimeMillis();
		long timeRemaining = waitMillis;
		
		for (Joinable joinable : joinables) {
			if (timeRemaining > 0) {
				try {
					joinable.join(timeRemaining);
					
				} catch (Throwable t) {
					if (throwable == null) {
						throwable = t;
					}
				}
				
				timeRemaining -= (System.currentTimeMillis() - startTime);
			}
		}
		
		if (throwable != null) {
			this.exceptionHandler.handleException(throwable);
		}
	}
	
	public void join() {
		Throwable throwable = null;
		
		for (Joinable joinable : joinables) {
			try {
				joinable.join();
				
			} catch (Throwable t) {
				if (throwable == null) {
					throwable = t;
				}
			}
		}
		
		if (throwable != null) {
			this.exceptionHandler.handleException(throwable);
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setJoinables(List<Joinable> joinables) {
		this.joinables = joinables;
	}
}