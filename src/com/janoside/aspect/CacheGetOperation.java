package com.janoside.aspect;

import java.util.Set;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.cache.ObjectCache;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.util.Function;
import com.janoside.util.Task;
import com.janoside.util.Tuple;

public class CacheGetOperation<T> implements Callable<T>, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(CacheGetOperation.class);
	
	private ObjectCache<T> cache;
	
	private ExceptionHandler exceptionHandler;
	
	private Set<String> emptyKeysSet;
	
	private Callable<T> nonCacheOperation;
	
	private Function<Tuple<String, T>> cacheHitCallback;
	
	private Function<String> cacheMissCallback;
	
	private String key;
	
	private long cacheKeyLifespan;
	
	public CacheGetOperation(String key, Callable<T> nonCacheOperation) {
		this.setDefaults();
		
		this.key = key;
		this.nonCacheOperation = nonCacheOperation;
	}
	
	public CacheGetOperation() {
		this.setDefaults();
	}
	
	public T call() {
		T value = null;
		
		try {
			value = this.cache.get(key);
			
		} catch (Throwable t) {
			this.exceptionHandler.handleException(t);
		}
		
		if (value == null) {
			this.cacheMissCallback.run(key);
			
			try {
				if (this.emptyKeysSet != null && this.emptyKeysSet.contains(key)) {
					return null;
				}
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
			
			try {
				value = this.nonCacheOperation.call();
				
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			
			if (value != null) {
				try {
					if (this.cacheKeyLifespan > 0) {
						this.cache.put(
								key,
								value,
								this.cacheKeyLifespan);
						
					} else {
						this.cache.put(
								key,
								value);
					}
				} catch (Throwable t) {
					this.exceptionHandler.handleException(t);
				}
			} else {
				if (this.emptyKeysSet != null) {
					this.emptyKeysSet.add(key);
				}
			}
		} else {
			this.cacheHitCallback.run(new Tuple<String, T>(this.key, value));
			
			if (this.emptyKeysSet != null && this.emptyKeysSet.contains(key)) {
				this.emptyKeysSet.remove(key);
			}
		}
		
		return value;
	}
	
	private void setDefaults() {
		this.emptyKeysSet = null;
		
		this.cacheKeyLifespan = 0;
		
		this.cacheHitCallback = new Function<Tuple<String, T>>() {
			public void run(Tuple<String, T> params) {
				logger.info(
						"Cache hit for key {}",
						params.getItem1());
			}
		};
		
		this.cacheMissCallback = new Function<String>() {
			public void run(String key) {
				logger.info(
						"Cache miss for key {}",
						key);
			}
		};
	}
	
	private Task<Long, Integer> getExponentialBackoffFunction(final long scale, final int exponent) {
		return new Task<Long, Integer>() {
			public Long run(Integer param) {
				return (long)(scale * Math.pow(param, exponent));
			}
		};
	}
	
	public void setCache(ObjectCache<T> cache) {
		this.cache = cache;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setEmptyKeysSet(Set<String> emptyKeysSet) {
		this.emptyKeysSet = emptyKeysSet;
	}
	
	public void setNonCacheOperation(Callable<T> nonCacheOperation) {
		this.nonCacheOperation = nonCacheOperation;
	}
	
	public void setCacheHitCallback(Function<Tuple<String, T>> cacheHitCallback) {
		this.cacheHitCallback = cacheHitCallback;
	}
	
	public void setCacheMissCallback(Function<String> cacheMissCallback) {
		this.cacheMissCallback = cacheMissCallback;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public void setCacheKeyLifespan(long cacheKeyLifespan) {
		this.cacheKeyLifespan = cacheKeyLifespan;
	}
}