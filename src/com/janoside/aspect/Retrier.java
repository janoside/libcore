package com.janoside.aspect;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.util.Function;
import com.janoside.util.Task;
import com.janoside.util.Tuple;

public class Retrier<T> implements Callable<T>, ExceptionHandlerAware {
	
	private static final Logger logger = LoggerFactory.getLogger(Retrier.class);
	
	private ExceptionHandler exceptionHandler;
	
	private Callable<T> callable;
	
	private Task<Long, Integer> backoffFunction;
	
	private Function<Tuple<Integer, Long>> successCallback;
	
	private Function<Tuple<Integer, Long>> failureCallback;
	
	private int maxAttemptCount;
	
	public Retrier(Callable<T> callable, int maxAttemptCount) {
		this.setDefaults();
		
		this.callable = callable;
		this.maxAttemptCount = maxAttemptCount;
	}
	
	public Retrier(Callable<T> callable) {
		this.setDefaults();
		
		this.callable = callable;
	}
	
	public Retrier() {
		this.setDefaults();
	}
	
	public T call() {
		Throwable firstThrown = null;
		int attempt = 0;
		long totalDelay = 0;
		
		while (attempt < this.maxAttemptCount) {
			try {
				T returnValue = this.callable.call();
				
				if (firstThrown != null) {
					// a retry succeeded, but we should still handle the exception
					// from the first failure, instead of swallowing it
					this.exceptionHandler.handleException(firstThrown);
				}
				
				this.successCallback.run(new Tuple<Integer, Long>(attempt + 1, totalDelay));
				
				return returnValue;
				
			} catch (Throwable t) {
				if (firstThrown == null) {
					firstThrown = t;
					
				} else {
					this.exceptionHandler.handleException(t);
				}
				
				try {
					long delay = this.backoffFunction.run(attempt);
					
					totalDelay += delay;
					
					Thread.sleep(delay);
					
				} catch (InterruptedException e) {
					this.exceptionHandler.handleException(e);
				}
				
				attempt++;
			}
		}
		
		this.failureCallback.run(new Tuple<Integer, Long>(this.maxAttemptCount, totalDelay));
		
		throw new RuntimeException(firstThrown);
	}
	
	private void setDefaults() {
		this.maxAttemptCount = 3;
		
		this.backoffFunction = this.getExponentialBackoffFunction(50, 2);
		
		this.successCallback = new Function<Tuple<Integer,Long>>() {
			public void run(Tuple<Integer, Long> params) {
				logger.info(
						"Retry succeeded on attempt #{} after delaying for {} ms",
						params.getItem1(),
						params.getItem2());
			}
		};
		
		this.failureCallback = new Function<Tuple<Integer, Long>>() {
			public void run(Tuple<Integer, Long> params) {
				logger.info(
						"Retry failed after attempt #{} and after delaying for {} ms",
						params.getItem1(),
						params.getItem2());
			}
		};
	}
	
	private Task<Long, Integer> getExponentialBackoffFunction(final long scale, final int exponent) {
		return new Task<Long, Integer>() {
			public Long run(Integer param) {
				return (long)(scale * Math.pow(param, exponent));
			}
		};
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
	
	public void setCallable(Callable<T> callable) {
		this.callable = callable;
	}
	
	public void setBackoffFunction(Task<Long, Integer> backoffFunction) {
		this.backoffFunction = backoffFunction;
	}
	
	public void setSuccessCallback(Function<Tuple<Integer, Long>> successCallback) {
		this.successCallback = successCallback;
	}
	
	public void setFailureCallback(Function<Tuple<Integer, Long>> failureCallback) {
		this.failureCallback = failureCallback;
	}
	
	public void setMaxAttemptCount(int maxAttemptCount) {
		this.maxAttemptCount = maxAttemptCount;
	}
}