package com.janoside.stats;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public class MemoryStats implements TimeSourceAware {
	
	private TimeSource timeSource;
	
	private Map<String, EventTracker> events;
	
	private Map<String, EventTracker> exceptions;
	
	private Map<String, PerformanceStats> performances;
	
	private Map<String, ValueStats> values;
	
	private ReentrantReadWriteLock performancesLock;
	
	private boolean threadSafe;
	
	public MemoryStats() {
		this.timeSource = new SystemTimeSource();
		
		this.events = new ConcurrentHashMap<String, EventTracker>();
		this.exceptions = new ConcurrentHashMap<String, EventTracker>();
		this.performances = new ConcurrentHashMap<String, PerformanceStats>();
		this.values = new ConcurrentHashMap<String, ValueStats>();
		
		this.performancesLock = new ReentrantReadWriteLock();
		
		this.threadSafe = false;
	}
	
	public Map<String, EventTracker> getEvents() {
		return this.events;
	}
	
	public Map<String, EventTracker> getExceptions() {
		return this.exceptions;
	}
	
	public Map<String, ValueStats> getValues() {
		return this.values;
	}
	
	public String printEventReport() {
		ArrayList<String> names = new ArrayList<String>(this.events.keySet());
		Collections.sort(names);
		
		StringBuilder buffer = new StringBuilder(100 * names.size());
		buffer.append("name, count, last occurrence (s ago)\n");
		for (String name : names) {
			String count = Long.toString(this.events.get(name).getCount());
			long secsAgo = (this.timeSource.getCurrentTime() - this.events.get(name).getLastOccurenceTime()) / 1000;
			
			buffer.append(String.format("%s, %s, %s%n", name, count, secsAgo));
		}
		
		return buffer.toString();
	}
	
	public String printExceptionReport() {
		final Map<String, EventTracker> exceptions = this.exceptions;
		
		ArrayList<String> names = new ArrayList<String>(this.exceptions.keySet());
		Collections.sort(names, new Comparator<String>() {
			public int compare(String s1, String s2) {
				long count1 = exceptions.get(s1).getCount();
				long count2 = exceptions.get(s2).getCount();
				
				return (int) (count2 - count1);
			}
		});
		
		StringBuilder buffer = new StringBuilder(400 * names.size());
		for (String name : names) {
			String count = Long.toString(this.exceptions.get(name).getCount());
			long secsAgo = (this.timeSource.getCurrentTime() - this.exceptions.get(name).getLastOccurenceTime()) / 1000;
			
			buffer.append(String.format("%s [%s]%n%s%n-----------------%n", count, secsAgo, name));
		}
		
		return buffer.toString();
	}
	
	public String printPerformanceReport() {
		ArrayList<String> names = new ArrayList<String>(this.performances.keySet());
		Collections.sort(names);
		
		StringBuilder buffer = new StringBuilder(100 * names.size());
		buffer.append("name, count, average (ms), slowest (ms)\n");
		for (String name : names) {
			PerformanceStats perf = this.performances.get(name);
			
			buffer.append(String.format("%s, %s, %s, %s%n", name, perf.getCount(), perf.getAverage(), perf.getMax()));
		}
		
		return buffer.toString();
	}
	
	public String printValuesReport() {
		ArrayList<String> names = new ArrayList<String>(this.values.keySet());
		Collections.sort(names);
		
		StringBuilder buffer = new StringBuilder(100 * names.size());
		buffer.append("name,count,avg,max,min\n");
		for (String name : names) {
			ValueStats perf = this.values.get(name);
			
			buffer.append(String.format(
					"%s,%s,%s,%s,%s%n",
					name,
					perf.getCount(),
					perf.getAverage(),
					perf.getMax(),
					perf.getMin()));
		}
		
		return buffer.toString();
	}
	
	public ArrayList<String> printUnfinishedPerformances() {
		ArrayList<String> activeKeys = new ArrayList<String>();
		
		for (String key : this.performances.keySet()) {
			if (this.performances.get(key).getActiveCount() > 0) {
				activeKeys.add(key);
			}
		}
		
		Collections.sort(activeKeys);
		
		return activeKeys;
	}
	
	public void countEvent(String name) {
		if (!this.events.containsKey(name)) {
			this.events.put(name, new EventTracker());
		}
		
		this.events.get(name).increment();
	}
	
	public void countEvent(String name, long count) {
		if (!this.events.containsKey(name)) {
			this.events.put(name, new EventTracker());
		}
		
		this.events.get(name).increment(count);
	}
	
	public void countException(Throwable t, String data) {
		StringWriter stringWriter = new StringWriter(15000);
		
		t.printStackTrace(new PrintWriter(stringWriter));
		
		String name = stringWriter.toString();
		
		if (!this.exceptions.containsKey(name)) {
			this.exceptions.put(name, new EventTracker());
		}
		
		this.exceptions.get(name).increment(data);
	}
	
	public void countException(Throwable t) {
		this.countException(t, null);
	}
	
	public void countPerformance(String name, long performance) {
		if (!this.performances.containsKey(name)) {
			this.performances.put(name, new PerformanceStats());
		}
		
		this.performances.get(name).count(performance);
	}
	
	public void countValue(String name, float value) {
		if (!this.values.containsKey(name)) {
			this.values.put(name, new ValueStats());
		}
		
		this.values.get(name).count(value);
	}
	
	public void countPerformanceStart(String s) {
		if (this.threadSafe) {
			this.performancesLock.readLock().lock();
			
			if (!this.performances.containsKey(s)) {
				this.performancesLock.readLock().unlock();
				this.performancesLock.writeLock().lock();
				
				this.performances.put(s, new PerformanceStats());
				
				this.performancesLock.readLock().lock();
				this.performancesLock.writeLock().unlock();
			}
			
			this.performances.get(s).countStart(Thread.currentThread().getId());
			
			this.performancesLock.readLock().unlock();
			
		} else {
			if (!this.performances.containsKey(s)) {
				this.performances.put(s, new PerformanceStats());
			}
			
			this.performances.get(s).countStart(Thread.currentThread().getId());
		}
	}
	
	public void countPerformanceEnd(String s) {
		if (this.performances.containsKey(s)) {
			this.performances.get(s).countEnd(Thread.currentThread().getId());
		}
	}
	
	public Map<String, PerformanceStats> getPerformances() {
		return this.performances;
	}
	
	public void clearEvents() {
		this.events.clear();
	}
	
	public void clearExceptions() {
		this.exceptions.clear();
	}
	
	public void clearPerformances() {
		if (this.threadSafe) {
			this.performancesLock.writeLock().lock();
			
			for (Map.Entry<String, PerformanceStats> entry : this.performances.entrySet()) {
				if (entry.getValue().getActiveCount() == 0) {
					this.performances.remove(entry.getKey());
				}
			}
			
			this.performancesLock.writeLock().unlock();
			
		} else {
			this.performances.clear();
		}
	}
	
	public void clearValues() {
		this.values.clear();
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public boolean getThreadSafe() {
		return this.threadSafe;
	}
	
	public void setThreadSafe(boolean threadSafe) {
		this.threadSafe = threadSafe;
	}
}