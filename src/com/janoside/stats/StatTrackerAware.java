package com.janoside.stats;

public interface StatTrackerAware {

	void setStatTracker(StatTracker statTracker);
}