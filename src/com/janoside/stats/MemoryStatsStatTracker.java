package com.janoside.stats;

public class MemoryStatsStatTracker extends BaseStatTracker {
	
	private MemoryStats memoryStats;
	
	public MemoryStatsStatTracker() {
	}
	
	public MemoryStatsStatTracker(long cleanupPeriod) {
		super(cleanupPeriod);
	}
	
	protected void trackEventInternal(String name, int count) {
		this.memoryStats.countEvent(name, count);
	}
	
	protected void trackPerformanceInternal(String name, long millis) {
		this.memoryStats.countPerformance(name, millis);
	}
	
	protected void trackValueInternal(String name, float value) {
		this.memoryStats.countValue(name, value);
	}
	
	public void setMemoryStats(MemoryStats memoryStats) {
		this.memoryStats = memoryStats;
	}
}