package com.janoside.stats;

import java.util.ArrayList;
import java.util.List;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;

public class DelegatingStatTracker implements StatTracker, ExceptionHandlerAware {
	
	private ExceptionHandler exceptionHandler;
	
	private List<StatTracker> statTrackers;
	
	public DelegatingStatTracker() {
		this.statTrackers = new ArrayList<StatTracker>();
	}
	
	public void trackEvent(final String name, final int count) {
		List<StatTracker> statTrackersList = this.getCurrentStatTrackersList();
		
		for (StatTracker statTracker : statTrackersList) {
			try {
				statTracker.trackEvent(name, count);
				
			} catch (Throwable t) {
				exceptionHandler.handleException(t);
			}
		}
	}
	
	public void trackEvent(final String name) {
		List<StatTracker> statTrackersList = this.getCurrentStatTrackersList();
		
		for (StatTracker statTracker : statTrackersList) {
			try {
				statTracker.trackEvent(name);
				
			} catch (Throwable t) {
				exceptionHandler.handleException(t);
			}
		}
	}
	
	public void trackPerformance(final String name, final long millis) {
		List<StatTracker> statTrackersList = this.getCurrentStatTrackersList();
		
		for (StatTracker statTracker : statTrackersList) {
			try {
				statTracker.trackPerformance(name, millis);
				
			} catch (Throwable t) {
				exceptionHandler.handleException(t);
			}
		}
	}
	
	public void trackThreadPerformanceStart(final String name) {
		List<StatTracker> statTrackersList = this.getCurrentStatTrackersList();
		
		for (StatTracker statTracker : statTrackersList) {
			try {
				statTracker.trackThreadPerformanceStart(name);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public void trackThreadPerformanceEnd(final String name) {
		List<StatTracker> statTrackersList = this.getCurrentStatTrackersList();
		
		for (StatTracker statTracker : statTrackersList) {
			try {
				statTracker.trackThreadPerformanceEnd(name);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	public void trackValue(final String name, final float value) {
		List<StatTracker> statTrackersList = this.getCurrentStatTrackersList();
		
		for (final StatTracker statTracker : statTrackersList) {
			try {
				statTracker.trackValue(name, value);
				
			} catch (Throwable t) {
				this.exceptionHandler.handleException(t);
			}
		}
	}
	
	private List<StatTracker> getCurrentStatTrackersList() {
		synchronized (this.statTrackers) {
			return new ArrayList<StatTracker>(this.statTrackers);
		}
	}
	
	public void addStatTracker(StatTracker statTracker) {
		synchronized (this.statTrackers) {
			this.statTrackers.add(statTracker);
		}
	}
	
	public void removeStatTracker(StatTracker statTracker) {
		synchronized (this.statTrackers) {
			this.statTrackers.remove(statTracker);
		}
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}