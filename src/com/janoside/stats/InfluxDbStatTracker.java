package com.janoside.stats;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import com.janoside.environment.Environment;
import com.janoside.environment.EnvironmentAware;
import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public class InfluxDbStatTracker extends BaseStatTracker implements TimeSourceAware, EnvironmentAware {
	
	private InfluxDB influxDb;
	
	private TimeSource timeSource;
	
	private Environment environment;
	
	private Map<String, String> defaultTags;
	
	private String url;
	
	private String dbName;
	
	private String username;
	
	private String password;
	
	public InfluxDbStatTracker() {
		this.timeSource = new SystemTimeSource();
	}
	
	protected void trackEventInternal(String name, int count) {
		this.setupIfNeeded();
		
		Point point = Point.measurement(name)
				.time(this.timeSource.getCurrentTime(), TimeUnit.MILLISECONDS)
				.addField("count", count)
				.tag(this.defaultTags)
				.build();
		
		this.influxDb.write(this.dbName, "default", point);
	}
	
	protected void trackPerformanceInternal(String name, long millis) {
		this.setupIfNeeded();
		
		Point point = Point.measurement(name)
				.time(this.timeSource.getCurrentTime(), TimeUnit.MILLISECONDS)
				.addField("performance", millis)
				.tag(this.defaultTags)
				.build();
		
		this.influxDb.write(this.dbName, "default", point);
	}
	
	protected void trackValueInternal(String name, float value) {
		this.setupIfNeeded();
		
		Point point = Point.measurement(name)
				.time(this.timeSource.getCurrentTime(), TimeUnit.MILLISECONDS)
				.addField("value", value)
				.tag(this.defaultTags)
				.build();
		
		this.influxDb.write(this.dbName, "default", point);
	}
	
	private void setupIfNeeded() {
		if (this.influxDb == null) {
			synchronized (this) {
				if (this.influxDb == null) {
					this.influxDb = InfluxDBFactory.connect(this.url, this.username, this.password);
					
					// set up batching params
					this.influxDb.enableBatch(
							5000, // flush every X points...
							1500, // at least every Y millis
							TimeUnit.MILLISECONDS);
					
					this.influxDb.createDatabase(this.dbName);
					
					this.defaultTags = new HashMap<String, String>() {{
						put("environment", environment.getName());
						put("app", environment.getAppName());
						put("appVersion", environment.getSourcecodeVersion());
						put("hardwareId", environment.getHardwareId());
					}};
				}
			}
		}
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
}