package com.janoside.stats;

public class BlackHoleStatTracker implements StatTracker {
	
	public void trackEvent(String name, int count) {
	}
	
	public void trackEvent(String name) {
	}
	
	public void trackPerformance(String name, long millis) {
	}
	
	public void trackThreadPerformanceStart(String name) {
	}
	
	public void trackThreadPerformanceEnd(String name) {
	}
	
	public void trackValue(String name, float value) {
	}
}