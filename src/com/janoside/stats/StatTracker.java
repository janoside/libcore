package com.janoside.stats;

public interface StatTracker {
	
	void trackEvent(String name, int count);
	
	void trackEvent(String name);
	
	void trackPerformance(String name, long millis);
	
	/**
	 * Convenience method for tracking performance on a single thread. Calling
	 * trackThreadPerformanceStart followed by trackThreadPerformanceEnd with the
	 * same performance "name" and from the same Thread will result in a call to
	 * trackPerformance passing in the given "name" and the milliseconds elapsed
	 * between the start-end calls.
	 * @param name
	 */
	void trackThreadPerformanceStart(String name);
	
	/**
	 * Convenience method for tracking performance on a single thread. Calling
	 * trackThreadPerformanceStart followed by trackThreadPerformanceEnd with the
	 * same performance "name" and from the same Thread will result in a call to
	 * trackPerformance passing in the given "name" and the milliseconds elapsed
	 * between the start-end calls.
	 * @param name
	 */
	void trackThreadPerformanceEnd(String name);
	
	void trackValue(String name, float value);
}