package com.janoside.stats;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.ExceptionHandlerAware;
import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public abstract class BaseStatTracker implements StatTracker, TimeSourceAware, ExceptionHandlerAware {
	
	protected ExceptionHandler exceptionHandler;
	
	private Map<Thread, Map<String, Long>> performanceStartTimesByThread;
	
	private TimeSource timeSource;
	
	public BaseStatTracker() {
		this(30000);
	}
	
	public BaseStatTracker(long cleanupPeriod) {
		this.timeSource = new SystemTimeSource();
		
		this.performanceStartTimesByThread = new ConcurrentHashMap<Thread, Map<String, Long>>();
		
		// periodically clean up performances map
		new Timer("StatTracker-CleanupTimer-" + Integer.toHexString(this.hashCode())).scheduleAtFixedRate(new TimerTask() {
				public void run() {
					try {
						HashSet<Thread> threadsToRemove = new HashSet<Thread>();
						
						for (Map.Entry<Thread, Map<String, Long>> entry : performanceStartTimesByThread.entrySet()) {
							if (!entry.getKey().isAlive()) {
								threadsToRemove.add(entry.getKey());
							}
						}
						
						for (Thread threadToRemove : threadsToRemove) {
							performanceStartTimesByThread.remove(threadToRemove);
						}
					} catch (Throwable t) {
						exceptionHandler.handleException(t);
					}
				}
			},
			cleanupPeriod,
			cleanupPeriod);
	}
	
	public void trackEvent(String name, final int count) {
		this.trackEventInternal(name, count);
	}
	
	public void trackEvent(String name) {
		this.trackEventInternal(name, 1);
	}
	
	public void trackPerformance(String name, final long millis) {
		this.trackPerformanceInternal(name, millis);
	}
	
	public void trackThreadPerformanceStart(String name) {
		Thread callingThread = Thread.currentThread();
		long startTime = this.timeSource.getCurrentTime();
		
		if (!this.performanceStartTimesByThread.containsKey(callingThread)) {
			this.performanceStartTimesByThread.put(callingThread, new HashMap<String, Long>());
		}
		
		this.performanceStartTimesByThread.get(callingThread).put(name, startTime);
	}
	
	public void trackThreadPerformanceEnd(String name) {
		Thread callingThread = Thread.currentThread();
		long endTime = this.timeSource.getCurrentTime();
		
		if (this.performanceStartTimesByThread.containsKey(callingThread)) {
			Map<String, Long> map = this.performanceStartTimesByThread.get(callingThread);
			Long startTimeObject = map.remove(name);
			
			long elapsedTime = (endTime - startTimeObject.longValue());
			
			this.trackPerformance(name, elapsedTime);
		}
	}
	
	public void trackValue(String name, final float value) {
		this.trackValueInternal(name, value);
	}
	
	protected abstract void trackEventInternal(String name, int count);
	
	protected abstract void trackPerformanceInternal(String name, long millis);
	
	protected abstract void trackValueInternal(String name, float value);
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public void setExceptionHandler(ExceptionHandler exceptionHandler) {
		this.exceptionHandler = exceptionHandler;
	}
}