package com.janoside.stats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class EventTracker {
	
	private List<Long> rateData;
	
	private long lastOccurenceTime;
	
	private AtomicLong count;
	
	private String name;
	
	private int rateMeasureWindowSize;
	
	public EventTracker() {
		this.rateMeasureWindowSize = 250;
		
		this.rateData = Collections.synchronizedList(new ArrayList<Long>(this.rateMeasureWindowSize));
		
		this.count = new AtomicLong(0L);
	}
	
	public void reset() {
		this.count = new AtomicLong(0);
		this.rateData.clear();
	}
	
	public long getLastOccurenceTime() {
		return this.lastOccurenceTime;
	}
	
	public int getSecondsSinceLastOccurrence() {
		if (this.lastOccurenceTime > 0) {
			return (int) ((System.currentTimeMillis() - this.lastOccurenceTime) / 1000.0);
			
		} else {
			return -1;
		}
	}
	
	public long getCount() {
		return this.count.get();
	}
	
	public float getRate() {
		if (this.rateData.size() > 0) {
			long timeDiff = (this.rateData.get(this.rateData.size() - 1) - this.rateData.get(0));
			if (timeDiff == 0) {
				timeDiff = 1;
			}
			
			return 1000.0f * this.rateData.size() / timeDiff;
			
		} else {
			return 0;
		}
	}
	
	public void increment() {
		this.lastOccurenceTime = System.currentTimeMillis();
		this.count.getAndIncrement();
		
		synchronized (this.rateData) {
			this.rateData.add(this.lastOccurenceTime);
			while (this.rateData.size() > this.rateMeasureWindowSize) {
				this.rateData.remove(0);
			}
		}
	}
	
	public void increment(long count) {
		this.lastOccurenceTime = System.currentTimeMillis();
		this.count.addAndGet(count);
	}
	
	public void increment(String data) {
		this.lastOccurenceTime = System.currentTimeMillis();
		this.count.getAndIncrement();
		
		synchronized (this.rateData) {
			this.rateData.add(System.currentTimeMillis());
			while (this.rateData.size() > this.rateMeasureWindowSize) {
				this.rateData.remove(0);
			}
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getRateMeasureWindowSize() {
		return this.rateMeasureWindowSize;
	}
	
	public void setRateMeasureWindowSize(int rateMeasureWindowSize) {
		this.rateMeasureWindowSize = rateMeasureWindowSize;
	}
}