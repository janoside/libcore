package com.janoside.stats;

import java.util.ArrayList;
import java.util.List;

public class DataSeries<T extends Number> {
	
	private List<T> history;
	
	private T max;
	
	private T min;
	
	private double maxValue;
	
	private double minValue;
	
	private double runningTotal;
	
	private int windowSize;
	
	public DataSeries() {
		this.windowSize = 20;
		
		this.history = new ArrayList<T>();
		
		this.setWindowSizeInternal(20);
	}
	
	public synchronized void addValue(T value) {
		this.history.add(value);
		this.runningTotal += value.doubleValue();
		
		if (value.doubleValue() > this.maxValue) {
			this.max = value;
			this.maxValue = value.doubleValue();
		}
		
		if (value.doubleValue() < this.minValue) {
			this.min = value;
			this.minValue = value.doubleValue();
		}
		
		while (this.history.size() > this.windowSize) {
			this.runningTotal -= this.history.remove(0).doubleValue();
		}
	}
	
	public double getAverage() {
		if (this.history.isEmpty()) {
			return 0;
			
		} else {
			return this.runningTotal / this.history.size();
		}
	}
	
	public boolean isSpikingUpward(double ratioThresholdAboveMovingAverage) {
		if (this.history.size() < this.windowSize) {
			return false;
			
		}  else {
			// last item is the latest (and is excluded from the moving average)
			double latest = this.getAverageOfLatestPoints(1);
			double ratio = latest / this.getAverage();
			
			if (ratio <= 1) {
				return false;
				
			} else {
				return ((ratio - 1) >= ratioThresholdAboveMovingAverage);
			}
		}
	}
	
	private double getAverageOfLatestPoints(int n) {
		double total = 0;
		
		for (int i = 0; i < n; i++) {
			total += this.history.get(this.history.size() - 1 - i).doubleValue();
		}
		
		return total / n;
	}
	
	private synchronized void setWindowSizeInternal(int newWindowSize) {
		ArrayList<T> oldHistory = new ArrayList<T>(this.history);
		
		this.runningTotal = 0;
		
		this.minValue = Double.MAX_VALUE;
		this.maxValue = Double.MIN_VALUE;
		
		this.history = new ArrayList<T>(newWindowSize);
		for (T item : oldHistory) {
			this.addValue(item);
		}
	}
	
	public T getMax() {
		return this.max;
	}
	
	public T getMin() {
		return this.min;
	}
	
	public T getLatestValue() {
		return this.history.get(this.history.size() - 1);
	}
	
	public double getTotal() {
		return this.runningTotal;
	}
	
	public int getWindowSize() {
		return this.windowSize;
	}
	
	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
		
		this.setWindowSizeInternal(this.windowSize);
	}
}