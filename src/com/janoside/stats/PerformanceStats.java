package com.janoside.stats;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import com.janoside.util.SystemTimeSource;
import com.janoside.util.TimeSource;
import com.janoside.util.TimeSourceAware;

public class PerformanceStats implements TimeSourceAware {
	
	private TimeSource timeSource;
	
	private List<Long> performances;
	
	private ConcurrentHashMap<Long, Long> activeTrackers;
	
	private AtomicLong count;
	
	private AtomicLong total;
	
	private long max;
	
	private long min;
	
	private AtomicLong activeCount;
	
	public PerformanceStats() {
		this.timeSource = new SystemTimeSource();
		
		this.performances = Collections.synchronizedList(new ArrayList<Long>());
		this.activeTrackers = new ConcurrentHashMap<Long, Long>();
		
		this.count = new AtomicLong(0);
		this.total = new AtomicLong(0);
		this.max = 0;
		this.min = 10000000;
		this.activeCount = new AtomicLong(0);
	}
	
	public void count(long perf) {
		this.total.getAndAdd(perf);
		
		this.count.getAndIncrement();
		
		if (perf > this.max) {
			this.max = perf;
		}
		
		if (perf < this.min) {
			this.min = perf;
		}
		
		this.performances.add(0, perf);
		synchronized (this.performances) {
			while (this.performances.size() > 25) {
				this.performances.remove(this.performances.size() - 1);
			}
		}
	}
	
	public void countStart(long threadId) {
		this.activeTrackers.put(threadId, this.timeSource.getCurrentTime());
		this.activeCount.getAndIncrement();
	}
	
	public long countEnd(long threadId) {
		if (this.activeTrackers.containsKey(threadId)) {
			Long startTime = this.activeTrackers.get(threadId);
			
			if (startTime != null) {
				this.activeTrackers.remove(threadId);
				
				long elapsedTime = this.timeSource.getCurrentTime() - startTime;
				
				this.count(elapsedTime);
				
				this.activeCount.getAndDecrement();
				
				return elapsedTime;
				
			} else {
				return 0;
			}
		} else {
			return -1;
		}
	}
	
	public void setTimeSource(TimeSource timeSource) {
		this.timeSource = timeSource;
	}
	
	public String getPerformanceList() {
		StringBuilder buffer = new StringBuilder(100);
		
		for (int i = 0; i < this.performances.size(); i++) {
			buffer.append(this.performances.get(i));

			if (i < this.performances.size() - 1) {
				buffer.append(",");
			}
		}
		
		return buffer.toString();
	}
	
	public long getCount() {
		return this.count.get();
	}

	public long getTotal() {
		return this.total.get();
	}
	
	public long getMax() {
		return this.max;
	}
	
	public long getMin() {
		return this.min;
	}
	
	public long getActiveCount() {
		return this.activeCount.get();
	}
	
	public float getAverage() {
		return ((float) this.total.get()) / this.count.get();
	}
}