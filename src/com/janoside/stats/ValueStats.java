package com.janoside.stats;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ValueStats {
	
	private List<Float> values;
	
	private AtomicLong count;
	
	private BigDecimal total;
	
	private float max;
	
	private float min;
	
	public ValueStats() {
		this.values = Collections.synchronizedList(new ArrayList<Float>());
		
		this.count = new AtomicLong(0);
		this.total = new BigDecimal(0);
		this.max = 0;
		this.min = Float.MAX_VALUE;
	}
	
	public void count(float value) {
		this.total = this.total.add(new BigDecimal(value));
		
		this.count.getAndIncrement();
		
		if (value > this.max) {
			this.max = value;
		}
		
		if (value < this.min) {
			this.min = value;
		}
		
		this.values.add(0, value);
		synchronized (this.values) {
			while (this.values.size() > 25) {
				this.values.remove(this.values.size() - 1);
			}
		}
	}
	
	public long getCount() {
		return this.count.get();
	}
	
	public float getMax() {
		return this.max;
	}
	
	public float getMin() {
		return this.min;
	}
	
	public float getAverage() {
		BigDecimal divisor = new BigDecimal(this.count.get());
		
		return this.total.divide(divisor, 2, RoundingMode.HALF_UP).floatValue();
	}
}