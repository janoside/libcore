package com.janoside.transform;

public interface ObjectTransformer<FromType, ToType> {
	
	ToType transform(FromType value);
}