package com.janoside.transform;

import java.util.List;

@SuppressWarnings("unchecked")
public class DelegatingObjectTransformer<T, U> implements ObjectTransformer<T, U> {
	
	private List<ObjectTransformer> transformers;
	
	public U transform(T input) {
		Object output = input;
		
		for (ObjectTransformer transformer : this.transformers) {
			output = transformer.transform(output);
		}
		
		return (U) output;
	}
	
	public void setTransformers(List<ObjectTransformer> transformers) {
		this.transformers = transformers;
	}
}