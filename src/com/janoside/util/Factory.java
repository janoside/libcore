package com.janoside.util;

public interface Factory<T> {
	
	T create();
}