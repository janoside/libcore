package com.janoside.util;

public interface TimeSourceAware {
	
	void setTimeSource(TimeSource timeSource);
}