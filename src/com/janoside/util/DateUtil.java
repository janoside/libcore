package com.janoside.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.apache.commons.lang.time.FastDateFormat;
import org.joda.time.MutableDateTime;

public final class DateUtil {
	
	private DateUtil() {}
	
	private static final Pattern DateWithSecondsPattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}-\\d{2}:\\d{2}:\\d{2}");
	
	private static final Pattern DateWithSecondsPatternNoDash = Pattern.compile("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}");
	
	private static final Pattern DateTimePattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}-\\d{2}:\\d{2}");
	
	private static final Pattern DateTimePatternNoDash = Pattern.compile("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}");
	
	private static final Pattern DatePattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
	
	private static final Pattern TimeWithSecondsPattern = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
	
	private static final Pattern TimePattern = Pattern.compile("\\d{2}:\\d{2}");
	
	private static final Pattern TimeDifferencePattern = Pattern.compile("\\d+(s|m|h|d|w|M)");
	
	
	
	public static final long MinuteMillis = 1000L * 60L;
	
	public static final long HourMillis = 1000L * 60L * 60L;
	
	public static final long DayMillis = 1000L * 60L * 60L * 24L;
	
	public static final long WeekMillis = 1000L * 60L * 60L * 24L * 7L;
	
	public static final long MonthMillis = 1000L * 60L * 60L * 24L * 31L;
	
	public static final long YearMillis = (long) (365.2422 * DateUtil.DayMillis);
	
	
	public static final int MinuteSeconds = 60;
	
	public static final int HourSeconds = 60 * 60;
	
	public static final int DaySeconds = 60 * 60 * 24;
	
	public static final int WeekSeconds = 60 * 60 * 24 * 7;
	
	public static final int MonthSeconds = 60 * 60 * 24 * 31;
	
	public static final int YearSeconds = (int) (365.2422 * DateUtil.DaySeconds);
	
	private static FastDateFormat dateYearTimeFormatter = FastDateFormat.getInstance("MMM dd yyyy, hh:mmaa");
	
	private static FastDateFormat dateTimeFormatter = FastDateFormat.getInstance("MMM dd, hh:mmaa");
	
	private static FastDateFormat timeFormatter = FastDateFormat.getInstance("hh:mmaa");
	
	public static List<Date> getDaysBetween(Date startDate, Date endDate) {
		ArrayList<Date> days = new ArrayList<Date>();
		
		MutableDateTime current = new MutableDateTime(startDate);
		current.setMillisOfDay(0);
		
		MutableDateTime end = new MutableDateTime(endDate);
		end.setMillisOfDay(0);
		
		while (current.isBefore(end)) {
			days.add(current.toDate());
			
			current.addDays(1);
		}
		
		days.add(current.toDate());
		
		return days;
	}
	
	public static List<String> getDaysBetween(String startDate, String endDate, String format) {
		List<Date> days = DateUtil.getDaysBetween(DateUtil.parse(format, startDate), DateUtil.parse(format, endDate));
		
		ArrayList<String> dayStrings = new ArrayList<String>();
		for (Date day : days) {
			dayStrings.add(DateUtil.format(format, day));
		}
		
		return dayStrings;
	}
	
	public static List<Date> getMonthsBetween(Date start, Date end) {
		MutableDateTime date = new MutableDateTime(start);
		date.setDayOfMonth(1);
		date.setMillisOfDay(0);
		
		ArrayList<Date> dates = new ArrayList<Date>();
		do {
			dates.add(date.toDate());
			
			date.addMonths(1);
			
		} while (date.getMillis() <= end.getTime());
		
		return dates;
	}
	
	public static Date[] getMonthStartAndEnd(int year, int month) {
		Date start = DateUtil.parse("yyyy-MM", DateUtil.formatYearMonth(year, month));
		
		MutableDateTime mutableEnd = new MutableDateTime(start.getTime());
		mutableEnd.setDayOfMonth(mutableEnd.dayOfMonth().getMaximumValue());
		mutableEnd.setMillisOfDay(mutableEnd.millisOfDay().getMaximumValue());
		
		Date end = mutableEnd.toDate();
		
		return new Date[] {start, end};
	}
	
	public static String formatYearMonth(int year, int month) {
		return String.format("%s-%s", Integer.toString(year), StringUtil.padIntWithZeroes(month, 2));
	}
	
	public static List<Date> getDatesBetween(Date start, Date end, long resolution) {
		ArrayList<Date> dates = new ArrayList<Date>();
		dates.add(new Date(start.getTime()));
		
		Date currentDate = new Date(start.getTime());
		while (currentDate.before(end)) {
			currentDate.setTime(currentDate.getTime() + resolution);
			
			dates.add(new Date(currentDate.getTime()));
		}
		
		if (currentDate.after(end)) {
			dates.add(new Date(end.getTime()));
		}
		
		return dates;
	}
	
	public static Date parse(String pattern, String content) {
		return ThreadSafeDateParser.parse(content, pattern);
	}
	
	public static Date getToday() {
		return DateUtil.getDay(new Date());
	}
	
	/**
	 * Returns a Date representing the most-recent exact hour. For example,
	 * if executed at 11:03am today, it should return 11:00am today.
	 * @return
	 */
	public static Date getHourFloored() {
		return DateUtil.parse("yyyy-MM-dd HH:mm:ss", DateUtil.format("yyyy-MM-dd HH:00:00", new Date()));
	}
	
	public static Date getDay(Date time) {
		MutableDateTime day = new MutableDateTime(time);
		day.setMillisOfDay(0);
		
		return day.toDate();
	}
	
	public static Date parseSimple(String input) {
		if (DateWithSecondsPattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd-HH:mm:ss", input);
			
		} else if (DateWithSecondsPatternNoDash.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd HH:mm:ss", input);
			
		} else if (DateTimePattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd-HH:mm", input);
			
		} else if (DateTimePatternNoDash.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd HH:mm", input);
			
		} else if (DatePattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd", input);
			
		} else if (TimeWithSecondsPattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd-HH:mm:ss", DateUtil.format("yyyy-MM-dd", new Date()) + "-" + input);
			
		} else if (TimePattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd-HH:mm", DateUtil.format("yyyy-MM-dd", new Date()) + "-" + input);
		}
		
		throw new IllegalArgumentException("Unable to parse date: " + input);
	}
	
	public static Date parseAsDateOrRelativeTimeSpan(String input, TimeSource timeSource) {
		if (DateWithSecondsPattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd-HH:mm:ss", input);
			
		} else if (DateWithSecondsPatternNoDash.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd HH:mm:ss", input);
			
		} else if (DateTimePattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd-HH:mm", input);
			
		} else if (DateTimePatternNoDash.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd HH:mm", input);
			
		} else if (DatePattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd", input);
			
		} else if (TimeWithSecondsPattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd-HH:mm:ss", DateUtil.format("yyyy-MM-dd", new Date(timeSource.getCurrentTime())) + "-" + input);
			
		} else if (TimePattern.matcher(input).matches()) {
			return DateUtil.parse("yyyy-MM-dd-HH:mm", DateUtil.format("yyyy-MM-dd", new Date(timeSource.getCurrentTime())) + "-" + input);
			
		} else if (TimeDifferencePattern.matcher(input).matches()) {
			return new Date(timeSource.getCurrentTime() - DateUtil.parseTimeDifference(input));
		}
		
		throw new IllegalArgumentException("Unable to parse date: " + input);
	}
	
	public static String format(String pattern, Date date) {
		return ThreadSafeDateParser.format(date, pattern);
	}
	
	public static String formatDate(Date date) {
		if (date == null) {
			return "";
		}
		
		long millisAgo = System.currentTimeMillis() - date.getTime();
		
		if (millisAgo < MinuteMillis) {
			return (millisAgo / 1000) + " sec ago";
			
		} else if (millisAgo < HourMillis) {
			return (millisAgo / MinuteMillis) + " min ago";
		
		} else if (millisAgo < DayMillis) {
			long hours = millisAgo / HourMillis;
			long minutes = (millisAgo - (hours * HourMillis)) / MinuteMillis;
			
			if (minutes > 0) {
				return (hours + " hr " + minutes + " min ago");
				
			} else {
				return (hours + " hr ago");	
			}
		} else if (millisAgo < WeekMillis) {
			long days = millisAgo / DayMillis;
			
			if (days == 1) {
				return "Yesterday, " + DateUtil.timeFormatter.format(date).toLowerCase(Locale.US);
				
			} else {
				return (days + " days ago, " + DateUtil.timeFormatter.format(date).toLowerCase(Locale.US));
			}
		} else if (millisAgo < MonthMillis) {
			return DateUtil.dateTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
			
		} else {
			return DateUtil.dateYearTimeFormatter.format(date).replaceAll("AM", "am").replaceAll("PM", "pm");
		}
	}
	
	public static int getLastDayOfMonth(int month, int year) {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.set(year, month - 1, 1);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * Parses a "time difference" string and returns
	 * the number of milliseconds it represents.
	 * 
	 * @param timeDifference
	 * @return
	 */
	public static long parseTimeDifference(String timeDifference) {
		int lastChar = timeDifference.charAt(timeDifference.length() - 1);
		
		long timeUnits = Long.parseLong(timeDifference.substring(0, timeDifference.length() - 1));
		
		switch (lastChar) {
			case 'h':
				return timeUnits * HourMillis;
			case 'd':
				return timeUnits * DayMillis;
			case 's':
				return timeUnits * 1000;
			case 'm':
				return timeUnits * MinuteMillis;
			case 'w':
				return timeUnits * WeekMillis;
			case 'M':
				return timeUnits * MonthMillis;
			default:
				throw new IllegalArgumentException("Unable to parse time difference, must be in format [0-9]+[s|m|h|d|w|M]");
		}
	}
	
	public static String formatDuration(long millis) {
		return String.format("%02d:%02d:%02d",
				TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
				TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))); 
	}
	
	public static String formatDurationAsText(long millis) {
		return DurationFormatUtils.formatDurationWords(millis, true, true);
	}
	
	public static int getYear(Date date) {
		return Integer.parseInt(DateUtil.format("yyyy", date));
	}
	
	public static int getCurrentYear() {
		return DateUtil.getYear(new Date());
	}
}