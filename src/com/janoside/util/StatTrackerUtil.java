package com.janoside.util;

import java.util.Locale;

public final class StatTrackerUtil {
	
	private StatTrackerUtil() {}
	
	/**
	 * 1. Replaces:
	 * 
	 * "." -> "-"
	 * ":" -> "-"
	 * " " -> "-"
	 * 
	 * 2. Camel-case converted to dash-separated
	 * 3. To lower case
	 * 
	 * @param input
	 * @return
	 */
	public static String normalizeNameComponent(String input) {
		return StringUtil.camelCaseToLowercaseDashSeparated(input.replace('.', '-').replace(':', '.').replace(' ', '-')).toLowerCase(Locale.US);
	}
}