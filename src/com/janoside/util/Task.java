package com.janoside.util;

public interface Task<ReturnType, ParamType> {
	
	ReturnType run(ParamType param);
}