package com.janoside.util;

public class Tuple4<T, U, V, W> {
	
	private T item1;
	
	private U item2;
	
	private V item3;
	
	private W item4;
	
	public Tuple4(T item1, U item2, V item3, W item4) {
		this.item1 = item1;
		this.item2 = item2;
		this.item3 = item3;
		this.item4 = item4;
	}
	
	public T getItem1() {
		return item1;
	}
	
	public void setItem1(T item1) {
		this.item1 = item1;
	}
	
	public U getItem2() {
		return item2;
	}
	
	public void setItem2(U item2) {
		this.item2 = item2;
	}
	
	public V getItem3() {
		return item3;
	}
	
	public void setItem3(V item3) {
		this.item3 = item3;
	}
	
	public W getItem4() {
		return item4;
	}
	
	public void setItem4(W item4) {
		this.item4 = item4;
	}
	
	public String toString() {
		return String.format("%s-%s-%s-%s", item1, item2, item3, item4);
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof Tuple4) {
			Tuple4 t = (Tuple4) o;
			
			if ((this.item1 == null) != (t.getItem1() == null)) {
				return false;
			}

			if ((this.item2 == null) != (t.getItem2() == null)) {
				return false;
			}
			
			if ((this.item3 == null) != (t.getItem3() == null)) {
				return false;
			}
			
			if ((this.item4 == null) != (t.getItem4() == null)) {
				return false;
			}
			
			if (this.item1 != null && !this.item1.equals(t.getItem1())) {
				return false;
			}
			
			if (this.item2 != null && !this.item2.equals(t.getItem2())) {
				return false;
			}
			
			if (this.item3 != null && !this.item3.equals(t.getItem3())) {
				return false;
			}
			
			if (this.item4 != null && !this.item4.equals(t.getItem4())) {
				return false;
			}
			
			return true;
			
		} else {
			return false;
		}
	}
}