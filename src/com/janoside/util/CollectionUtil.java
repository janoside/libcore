package com.janoside.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.janoside.transform.ObjectTransformer;

@SuppressWarnings("unchecked")
public final class CollectionUtil {
	
	private CollectionUtil() {}
	
	public static <KeyType, ValueType> Map<KeyType, List<ValueType>> listToGroupedMap(List<ValueType> list, ObjectTransformer<ValueType, KeyType> transformer) {
		HashMap<KeyType, List<ValueType>> map = new HashMap<KeyType, List<ValueType>>(list.size());
		
		for (ValueType item : list) {
			KeyType key = transformer.transform(item);
			
			if (!map.containsKey(key)) {
				map.put(key, new ArrayList<ValueType>());
			}
			
			map.get(key).add(item);
		}
		
		return map;
	}
	
	public static <KeyType, ValueType> Map<KeyType, ValueType> listToMap(List<ValueType> list, ObjectTransformer<ValueType, KeyType> transformer) {
		HashMap<KeyType, ValueType> map = new HashMap<KeyType, ValueType>(list.size());
		
		for (ValueType item : list) {
			map.put(transformer.transform(item), item);
		}
		
		return map;
	}
	
	public static <KeyType, ValueType> Map<KeyType, ValueType> collectionToMap(Collection<ValueType> collection, ObjectTransformer<ValueType, KeyType> transformer) {
		HashMap<KeyType, ValueType> map = new HashMap<KeyType, ValueType>(collection.size());
		
		for (ValueType item : collection) {
			map.put(transformer.transform(item), item);
		}
		
		return map;
	}
	
	public static <Type1, Type2> Set<Type2> collectionToSet(Collection<Type1> collection, ObjectTransformer<Type1, Type2> transformer) {
		HashSet<Type2> set = new HashSet<Type2>(collection.size());
		
		for (Type1 item : collection) {
			set.add(transformer.transform(item));
		}
		
		return set;
	}
	
	public static <ValueType> Collection<ValueType> filterCollection(Collection<ValueType> set, Predicate<ValueType> predicate) {
		HashSet<ValueType> returnSet = new HashSet<ValueType>();
		
		for (ValueType value : set) {
			if (predicate.apply(value)) {
				returnSet.add(value);
			}
		}
		
		return returnSet;
	}
	
	public static <ValueType> Set<ValueType> filterSet(Set<ValueType> set, Predicate<ValueType> predicate) {
		HashSet<ValueType> returnSet = new HashSet<ValueType>();
		
		for (ValueType value : set) {
			if (predicate.apply(value)) {
				returnSet.add(value);
			}
		}
		
		return returnSet;
	}
	
	public static <ValueType> List<ValueType> filterList(List<ValueType> set, Predicate<ValueType> predicate) {
		ArrayList<ValueType> returnList = new ArrayList<ValueType>();
		
		for (ValueType value : set) {
			if (predicate.apply(value)) {
				returnList.add(value);
			}
		}
		
		return returnList;
	}
	
	public static <T> List<List<T>> splitList(List<T> sourceList, int maxSize) {
		int listCount = sourceList.size() / maxSize + ((sourceList.size() % maxSize > 0) ? 1 : 0);
		
		ArrayList<T> copyList = new ArrayList<T>(sourceList);
		
		ArrayList<List<T>> listList = new ArrayList<List<T>>(listCount);
		for (int i = 0; i < listCount; i++) {
			ArrayList smallList = new ArrayList(maxSize);
			
			int innerListCount = 0;
			while (innerListCount < maxSize && copyList.size() > 0) {
				smallList.add(copyList.remove(0));
				
				innerListCount++;
			}
			
			listList.add(smallList);
		}
		
		return listList;
	}
	
	public static <T> List<List<T>> splitListByListCount(List<T> sourceList, int listCount) {
		int listSize = (sourceList.size() / listCount) + ((sourceList.size() % listCount > 0) ? 1 : 0);
		
		return CollectionUtil.splitList(sourceList, listSize);
	}
	
	public static <T> List<T> cloneList(List<T> sourceList) {
		if (sourceList == null) {
			return new ArrayList<T>();
			
		} else {
			return new ArrayList<T>(sourceList);
		}
	}
	
	public static <T, U> List<U> transformList(List<T> sourceList, ObjectTransformer<T, U> transformer) {
		if (sourceList == null) {
			return new ArrayList<U>();
			
		} else {
			ArrayList<U> destinationList = new ArrayList<U>(sourceList.size());
			for (T item : sourceList) {
				destinationList.add(transformer.transform(item));
			}
			
			return destinationList;
		}
	}
	
	public static <T, U> Collection<U> transformCollection(Collection<T> sourceList, ObjectTransformer<T, U> transformer) {
		if (sourceList == null) {
			return new ArrayList<U>();
			
		} else {
			ArrayList<U> destinationList = new ArrayList<U>(sourceList.size());
			for (T item : sourceList) {
				destinationList.add(transformer.transform(item));
			}
			
			return destinationList;
		}
	}
	
	public static <T> List<T> reversedList(List<T> list) {
		ArrayList<T> reversedList = new ArrayList<T>(list);
		
		Collections.reverse(reversedList);
		
		return reversedList;
	}
	
	public static <T> List<T> subList(List<T> sourceList, int startFrom) {
		ArrayList<T> clonedList = new ArrayList<T>(sourceList);
		for (int i = 0; i < startFrom; i++) {
			clonedList.remove(0);
		}
		
		return clonedList;
	}
	
	public static <T> List<T> subList(List<T> sourceList, int startFrom, int length) {
		ArrayList<T> clonedList = new ArrayList<T>(sourceList.size());
		for (int i = 0; i < sourceList.size(); i++) {
			if (i >= startFrom && i < (startFrom + length)) {
				clonedList.add(sourceList.get(i));
			}
		}
		
		return clonedList;
	}
	
	public static <T extends Comparable<? super T>> List<T> mergeUnique(Collection<T> collection1, Collection<T> collection2) {
		HashSet<T> sources = new HashSet<T>();
		
		sources.addAll(collection1);
		sources.addAll(collection2);
		
		return new ArrayList<T>(sources);
	}
	
	public static boolean isEmpty(Collection<?> collection) {
		if (collection == null) {
			return true;
		}
		
		return collection.isEmpty();
	}
	
	public static <T extends Comparable<? super T>> List<T> mergeUniqueSorted(Collection<T> collection1, Collection<T> collection2) {
		List<T> sortedList = CollectionUtil.mergeUnique(collection1, collection2);
		
		Collections.sort(sortedList);
		
		return sortedList;
	}
}