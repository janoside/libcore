package com.janoside.util;

public class Tuple<T, U> {
	
	private T item1;
	
	private U item2;
	
	public Tuple(T item1, U item2) {
		this.item1 = item1;
		this.item2 = item2;
	}
	
	public T getItem1() {
		return item1;
	}
	
	public void setItem1(T item1) {
		this.item1 = item1;
	}
	
	public U getItem2() {
		return item2;
	}
	
	public void setItem2(U item2) {
		this.item2 = item2;
	}
	
	public String toString() {
		return String.format("%s-%s", item1, item2);
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof Tuple) {
			Tuple t = (Tuple) o;
			
			if ((this.item1 == null) != (t.getItem1() == null)) {
				return false;
			}

			if ((this.item2 == null) != (t.getItem2() == null)) {
				return false;
			}
			
			if (this.item1 != null && !this.item1.equals(t.getItem1())) {
				return false;
			}
			
			if (this.item2 != null && !this.item2.equals(t.getItem2())) {
				return false;
			}
			
			return true;
			
		} else {
			return false;
		}
	}
}