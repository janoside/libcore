package com.janoside.util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LeakWatcher<T> {
	
	private static final Logger logger = LoggerFactory.getLogger(LeakWatcher.class.getSimpleName());
	
	private ScheduledExecutorService scheduledExecutorService;
	
	private List<WeakReference<T>> objectReferences;
	
	private List<String> objectNames;
	
	public LeakWatcher() {
		this.objectReferences = new ArrayList<WeakReference<T>>();
		this.objectNames = new ArrayList<String>();
		
		this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
	}
	
	public ScheduledFuture start(final Function objectReleasedCallback, long period) {
		return this.scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
				public void run() {
					checkReferences(objectReleasedCallback);
				}
			},
			period,
			period,
			TimeUnit.MILLISECONDS);
	}
	
	public synchronized void watch(T object) {
		WeakReference<T> ref = new WeakReference<T>(object);
		
		this.objectReferences.add(ref);
		this.objectNames.add(object.toString());
		
		logger.debug("LeakWatcher watching object: {}", object.toString());
	}
	
	private synchronized void checkReferences(Function objectReleasedCallback) {
		HashSet<String> unreleasedNames = new HashSet<String>();
		
		Iterator<WeakReference<T>> iterator = this.objectReferences.iterator();
		while (iterator.hasNext()) {
			WeakReference<T> ref = iterator.next();
			
			T object = ref.get();
			
			if (object != null) {
				unreleasedNames.add(object.toString());
				
			} else {
				iterator.remove();
			}
		}
		
		ArrayList<String> releasedNames = new ArrayList<String>(this.objectNames);
		releasedNames.removeAll(unreleasedNames);
		
		this.objectNames.removeAll(releasedNames);
		
		if (!releasedNames.isEmpty()) {
			for (String releasedName : releasedNames) {
				if (objectReleasedCallback != null) {
					objectReleasedCallback.run(releasedName);
				}
			}
			
			logger.debug(
					"LeakWatcher objects released({}): {}, remaining({}): {}",
					releasedNames.size(),
					releasedNames,
					this.objectNames.size(),
					this.objectNames);
		}
	}
}