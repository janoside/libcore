package com.janoside.util;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public final class UrlUtil {
	
	private UrlUtil() {}
	
	public static String getParameter(String urlString, String parameterName) {
		URL url = UrlUtil.urlFromString(urlString);
		
		String query = url.getQuery();
		
		if (StringUtil.hasText(query)) {
			StringTokenizer tokenizer = new StringTokenizer(query, "&");
			
			while (tokenizer.hasMoreTokens()) {
				String parameterNameAndValue = tokenizer.nextToken();
				
				int equalsIndex = parameterNameAndValue.indexOf("=");
				
				if (equalsIndex > 0) {
					if (parameterName.equals(UrlUtil.decode(parameterNameAndValue.substring(0, equalsIndex)))) {
						String value = parameterNameAndValue.substring(equalsIndex + 1);
						
						if (value.length() > 0) {
							return UrlUtil.decode(value);
							
						} else {
							return null;
						}
					}
				} else {
					return null;
				}
			}
		}
		
		return null;
	}
	
	public static Map<String, String> getParameters(String url) {
		URL u = UrlUtil.urlFromString(url);
		
		String query = u.getQuery();
		
		return UrlUtil.extractParameters(query);
	}
	
	public static Map<String, String> extractParameters(String parameterString) {
		HashMap<String, String> parameters = new HashMap<String, String>();
		
		if (StringUtil.hasText(parameterString)) {
			String cleanString = parameterString;
			
			if (cleanString.startsWith("?")) {
				cleanString = cleanString.substring(1);
			}
			
			StringTokenizer tokenizer = new StringTokenizer(cleanString, "&");
			
			while (tokenizer.hasMoreTokens()) {
				String parameterNameAndValue = tokenizer.nextToken();
				
				int equalsIndex = parameterNameAndValue.indexOf("=");
				
				if (equalsIndex > 0) {
					String value = parameterNameAndValue.substring(equalsIndex + 1);
					
					if (value.length() > 0) {
						parameters.put(UrlUtil.decode(parameterNameAndValue.substring(0, equalsIndex)), UrlUtil.decode(value));
					}
				}
			}
		}
		
		return parameters;
	}
	
	public static List<Tuple<String, String>> extractParametersAsList(String parameterString) {
		ArrayList<Tuple<String, String>> parameters = new ArrayList<Tuple<String, String>>();
		
		if (StringUtil.hasText(parameterString)) {
			String cleanString = parameterString;
			
			if (cleanString.startsWith("?")) {
				cleanString = cleanString.substring(1);
			}
			
			StringTokenizer tokenizer = new StringTokenizer(cleanString, "&");
			
			while (tokenizer.hasMoreTokens()) {
				String parameterNameAndValue = tokenizer.nextToken();
				
				int equalsIndex = parameterNameAndValue.indexOf("=");
				
				if (equalsIndex > 0) {
					String value = parameterNameAndValue.substring(equalsIndex + 1);
					
					if (value.length() > 0) {
						parameters.add(new Tuple<String, String>(UrlUtil.decode(parameterNameAndValue.substring(0, equalsIndex)), UrlUtil.decode(value)));
					}
				}
			}
		}
		
		return parameters;
	}
	
	public static String getDomain(String urlString) {
		String normalizedUrlString = urlString;
		
		if (!normalizedUrlString.startsWith("http")) {
			normalizedUrlString = "http://" + normalizedUrlString;
		}
		
		URL url = UrlUtil.urlFromString(normalizedUrlString);
		
		String[] components = url.getHost().split("\\.");
		
		if (components.length == 0) {
			return null;
			
		} else if (components.length == 1) {
			return components[0];
			
		} else {
			return components[components.length - 2] + "." + components[components.length - 1];
		}
	}
	
	public static String getFullDomain(String urlString) {
		String normalizedUrlString = urlString;
		
		if (!normalizedUrlString.startsWith("http")) {
			normalizedUrlString = "http://" + normalizedUrlString;
		}
		
		URL url = UrlUtil.urlFromString(normalizedUrlString);
		
		return url.getHost();
	}
	
	public static String getPortString(String urlString) {
		String normalizedUrlString = urlString;
		
		if (!normalizedUrlString.startsWith("http")) {
			normalizedUrlString = "http://" + normalizedUrlString;
		}
		
		URL url = UrlUtil.urlFromString(normalizedUrlString);
		
		if (url.getPort() != 80 && url.getPort() != -1) {
			return (":" + url.getPort());
			
		} else {
			return "";
		}
	}
	
	public static String getScheme(String urlString) {
		String normalizedUrlString = urlString;
		
		if (!normalizedUrlString.startsWith("http")) {
			normalizedUrlString = "http://" + normalizedUrlString;
		}
		
		URL url = UrlUtil.urlFromString(normalizedUrlString);
		
		return url.getProtocol();
	}
	
	public static String buildUrl(String protocol, String domain, int port, String path, Map<String, String> parameters) {
		StringBuilder buffer = new StringBuilder(200);
		
		buffer.append(protocol);
		buffer.append("://");
		buffer.append(domain);
		
		if ((!protocol.equalsIgnoreCase("https") || port != 443) &&
				(!protocol.equalsIgnoreCase("http") || port != 80) &&
				port != -1) {
			buffer.append(":");
			buffer.append(port);
		}
		
		buffer.append(path);
		
		if (parameters.size() > 0) {
			buffer.append("?");
			buffer.append(UrlUtil.buildParameterString(parameters));
		}
		
		return buffer.toString();
	}
	
	public static String buildBaseUrl(String host, int port) {
		if (port == 443) {
			return String.format("https://%s", host);
			
		} else if (port == 80) {
			return String.format("http://%s", host);
			
		} else {
			return String.format("http://%s:%d", host, port);
		}
	}
	
	/**
	 * Builds a string of the form:
	 * 
	 * UrlUtil.encode(param1Name)=UrlUtil.encode(param1Value)&...
	 * 
	 * Notice that the "?" starting the parameter string is not present.
	 * 
	 * @param parameters - Map of the form {param1Name=param1Value, ...}
	 * @return String concatenating all parameter names, values, and ampersands between
	 */
	public static String buildParameterString(Map<String, String> parameters) {
		StringBuilder buffer = new StringBuilder(200);
		
		boolean firstParam = true;
		for (Map.Entry<String, String> entry : parameters.entrySet()) {
			if (!firstParam) {
				buffer.append("&");
			}

			buffer.append(UrlUtil.encode(entry.getKey()));
			buffer.append("=");
			buffer.append(UrlUtil.encode(entry.getValue()));
			
			firstParam = false;
		}
		
		return buffer.toString();
	}
	
	public static String setParameter(String urlString, String parameterName, String parameterValue) {
		URL url = UrlUtil.urlFromString(urlString);
		
		Map<String, String> parameters = UrlUtil.getParameters(urlString);
		
		parameters.put(parameterName, parameterValue);
		
		return UrlUtil.buildUrl(url.getProtocol(), url.getHost(), url.getPort(), url.getPath(), parameters);
	}
	
	public static String encode(String value) {
		try {
			return URLEncoder.encode(value, "UTF-8");
			
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException("UTF-8 Unsupported", uee);
		}
	}
	
	public static String percentEncode(String value) {
		return UrlUtil.encode(value).replace("+", "%20");
	}
	
	public static String decode(String value) {
		try {
			return URLDecoder.decode(value, "UTF-8");
			
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException("UTF-8 Unsupported", uee);
		}
	}
	
	public static String makeAbsoluteUrl(String relativeUrl, String absoluteReference) {
		if (relativeUrl.startsWith("http://") || relativeUrl.startsWith("https://")) {
			return relativeUrl;
			
		} else {
			String normalizedUrlString = absoluteReference;
			
			if (!normalizedUrlString.startsWith("http")) {
				normalizedUrlString = "http://" + normalizedUrlString;
			}
			
			URL url = UrlUtil.urlFromString(normalizedUrlString);
			
			StringBuilder sb = new StringBuilder(url.getProtocol() + "://" + url.getHost());

			if ((url.getPort() > 0) && (url.getPort() != 80)) {
				sb.append(":" + url.getPort());
			}

			if (!relativeUrl.startsWith("/")) {
				if (StringUtil.hasText(url.getPath()) && url.getPath().contains("/")) {
					sb.append(url.getPath().substring(0, url.getPath().lastIndexOf("/") + 1));
				} else {
					sb.append("/");
				}
			}
			
			sb.append(relativeUrl);
			return sb.toString();
		}
	}
	
	private static URL urlFromString(String urlString) {
		try {
			return new URL(urlString);
			
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
	}
}