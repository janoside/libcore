package com.janoside.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public final class RandomUtil {
	
	private RandomUtil() {}
	
	static final Random sharedRandom = new Random(System.currentTimeMillis());
	
	private static final String[] words = new String[] { "then", "when", "how", "who", "orange", "cellar door", "cheese", "pizza", "sky", "blue", "fountain", "gorilla", "duck", "watermelon", "giraffe", "hip hop anonymous" };
	
	
	public static String randomWordFromLong(long l) {
		return RandomUtil.randomWord(new Random(l));
	}
	
	public static String randomWordFromInt(int i) {
		return RandomUtil.randomWord(new Random(i));
	}
	
	
	
	
	
	public static String randomSentence(String includedPhrase) {
		return RandomUtil.randomSentence(includedPhrase, sharedRandom);
	}
	
	public static String randomSentence(int wordCount) {
		return RandomUtil.randomSentence(wordCount, sharedRandom);
	}
	
	public static String randomSentence() {
		return RandomUtil.randomSentence(sharedRandom);
	}
	
	public static String randomWord(int length) {
		return RandomUtil.randomWord(length, sharedRandom);
	}
	
	public static String randomWord() {
		return RandomUtil.randomWord(sharedRandom);
	}
	
	public static String randomTransform(String phrase) {
		return RandomUtil.randomTransform(phrase, sharedRandom);
	}
	
	public static String randomUppercaseLetter() {
		return RandomUtil.randomUppercaseLetter(sharedRandom);
	}
	
	public static String randomLowercaseLetter() {
		return RandomUtil.randomLowercaseLetter(sharedRandom);
	}
	
	public static String randomStateAbbreviation() {
		return RandomUtil.randomStateAbbreviation(sharedRandom);
	}
	
	public static UUID randomUuid() {
		return RandomUtil.randomUuid(sharedRandom);
	}
	
	public static List<String> randomUuidList(int size) {
		return RandomUtil.randomUuidList(size, sharedRandom);
	}
	
	public static List<String> randomWordList(int minLength, int maxLength) {
		return RandomUtil.randomWordList(sharedRandom, minLength, maxLength);
	}
	
	public static List<String> randomWordList() {
		return RandomUtil.randomWordList(sharedRandom);
	}
	
	public static List<Long> randomLongList(int size) {
		return RandomUtil.randomLongList(size, sharedRandom);
	}
	
	public static <T> T randomSelection(List<T> list) {
		return RandomUtil.randomSelection(list, sharedRandom);
	}
	
	public static <T> T randomSelection(Collection<T> collection) {
		return RandomUtil.randomSelection(new ArrayList<T>(collection), sharedRandom);
	}
	
	public static <T> T randomSelection(T[] array) {
		return RandomUtil.randomSelection(array, sharedRandom);
	}
	
	public static byte[] randomByteArray(int size) {
		return RandomUtil.randomByteArray(size, sharedRandom);
	}
	
	public static long randomLong(long max) {
		return RandomUtil.randomLong(max, sharedRandom);
	}
	
	public static int randomInt(int max) {
		return RandomUtil.randomInt(max, sharedRandom);
	}
	
	public static long randomLong() {
		return RandomUtil.randomLong(sharedRandom);
	}
	
	public static int randomInt() {
		return RandomUtil.randomInt(sharedRandom);
	}
	
	public static byte randomByte() {
		return RandomUtil.randomByte(sharedRandom);
	}
	
	public static float randomFloat(float low, float high) {
		return RandomUtil.randomFloat(sharedRandom, low, high);
	}
	
	public static float randomFloat() {
		return RandomUtil.randomFloat(sharedRandom);
	}
	
	public static boolean randomBoolean(double chanceToBeTrue) {
		return RandomUtil.randomBoolean(chanceToBeTrue, sharedRandom);
	}
	
	
	
	
	
	
	
	public static String randomSentence(String includedPhrase, Random random) {
		StringBuilder buffer = new StringBuilder(100);
		
		int wordCount = 1 + RandomUtil.randomInt(3, random);
		for (int word = 0; word < wordCount; word++) {
			buffer.append(words[RandomUtil.randomInt(words.length, random)]);
			buffer.append(" ");
		}
		
		buffer.append(includedPhrase.trim());
		buffer.append(" ");
		
		wordCount = 1 + RandomUtil.randomInt(4, random);
		for (int word = 0; word < wordCount; word++) {
			buffer.append(words[RandomUtil.randomInt(words.length, random)]);
			
			if (word < (wordCount - 1)) {
				buffer.append(" ");
			}
		}
		
		return buffer.toString();
	}
	
	public static String randomSentence(int wordCount, Random random) {
		StringBuilder buffer = new StringBuilder(15 * wordCount);
		
		for (int i = 0; i < wordCount; i++) {
			buffer.append(words[RandomUtil.randomInt(words.length, random)]);
			
			if (i < (wordCount - 1)) {
				buffer.append(" ");
			}
		}
		
		return buffer.toString();
	}
	
	public static String randomSentence(Random random) {
		return RandomUtil.randomSentence(5 + RandomUtil.randomInt(7, random), random);
	}
	
	public static String randomWord(int length, Random random) {
		StringBuilder buffer = new StringBuilder(length);
		
		for (int i = 0; i < length; i++) {
			buffer.append(RandomUtil.randomLowercaseLetter(random));
		}
		
		return buffer.toString();
	}
	
	public static String randomWord(Random random) {
		return RandomUtil.randomWord(4 + RandomUtil.randomInt(7, random), random);
	}
	
	/**
	 * Randomly shuffles the words in the given phrase and randomly switches
	 * letter case.
	 * 
	 * @param phrase
	 *            any String value
	 * @return the string resulting from the random transformations
	 */
	public static String randomTransform(String phrase, Random random) {
		ArrayList<String> wordList = new ArrayList<String>(Arrays.asList(phrase.split("\\s+")));
		
		StringBuilder buffer = new StringBuilder(phrase.length());
		while (!wordList.isEmpty()) {
			buffer.append(wordList.remove(RandomUtil.randomInt(wordList.size(), random)));
			
			if (wordList.size() > 0) {
				buffer.append(" ");
			}
		}
		
		for (int i = 0; i < buffer.length(); i++) {
			if (RandomUtil.randomInt(100, random) > 50) {
				int charType = Character.getType(buffer.charAt(i));
				
				if (charType == Character.UPPERCASE_LETTER) {
					buffer.insert(i, (char) (buffer.charAt(i) + 32));
					buffer.deleteCharAt(i + 1);
					
				} else if (charType == Character.LOWERCASE_LETTER) {
					buffer.insert(i, (char) (buffer.charAt(i) - 32));
					buffer.deleteCharAt(i + 1);
				}
			}
		}
		
		return buffer.toString();
	}
	
	public static String randomUppercaseLetter(Random random) {
		return new String(new char[] { (char) (65 + random.nextInt(26)) });
	}
	
	public static String randomLowercaseLetter(Random random) {
		return new String(new char[] { (char) (97 + random.nextInt(26)) });
	}
	
	public static String randomStateAbbreviation(Random random) {
		ArrayList<String> stateAbbreviations = new ArrayList<String>(StateUtil.getAbbreviations());
		
		return stateAbbreviations.get(RandomUtil.randomInt(stateAbbreviations.size(), random));
	}
	
	public static UUID randomUuid(Random random) {
		return UUID.nameUUIDFromBytes(RandomUtil.randomByteArray(16, random));
	}
	
	public static List<String> randomUuidList(int size, Random random) {
		ArrayList<String> list = new ArrayList<String>();
		
		for (int i = 0; i < size; i++) {
			list.add(RandomUtil.randomUuid(random).toString());
		}
		
		return list;
	}
	
	public static List<String> randomWordList(Random random, int minLength, int maxLength) {
		int size = minLength + RandomUtil.randomInt(maxLength - minLength, random);
		
		ArrayList<String> list = new ArrayList<String>();
		
		for (int i = 0; i < size; i++) {
			list.add(RandomUtil.randomWord(random));
		}
		
		return list;
	}
	
	public static List<String> randomWordList(Random random) {
		return randomWordList(random, 3, 18);
	}
	
	public static List<Long> randomLongList(int size, Random random) {
		ArrayList<Long> list = new ArrayList<Long>();
		
		for (int i = 0; i < size; i++) {
			list.add((long) RandomUtil.randomInt(random));
		}
		
		return list;
	}
	
	public static <T> T randomSelection(Collection<T> collection, Random random) {
		return RandomUtil.randomSelection(new ArrayList<T>(collection), random);
	}
	
	public static <T> T randomSelection(List<T> list, Random random) {
		return list.get(RandomUtil.randomInt(list.size(), random));
	}
	
	public static <T> T randomSelection(T[] array, Random random) {
		return array[RandomUtil.randomInt(array.length, random)];
	}
	
	public static byte[] randomByteArray(int size, Random random) {
		byte[] array = new byte[size];
		
		for (int i = 0; i < size; i++) {
			array[i] = RandomUtil.randomByte(random);
		}
		
		return array;
	}
	
	public static long randomLong(long max, Random random) {
		if (max == 1) {
			return 0;
			
		} else {
			// http://stackoverflow.com/a/2546186/673828
			long bits, val;
			
			do {
				bits = (random.nextLong() << 1) >>> 1;
				
				val = bits % max;
				
			} while (bits - val + (max - 1) < 0L);
			
			return val;
		}
	}
	
	public static int randomInt(int max, Random random) {
		if (max == 1) {
			return 0;
			
		} else {
			return random.nextInt(max);
		}
	}
	
	public static long randomLong(Random random) {
		return random.nextLong();
	}
	
	public static int randomInt(Random random) {
		return random.nextInt();
	}
	
	public static byte randomByte(Random random) {
		byte[] array = new byte[1];
		
		random.nextBytes(array);
		
		return array[0];
	}
	
	public static float randomFloat(Random random, float low, float high) {
		return low + (high - low) * random.nextFloat();
	}
	
	/**
	 * Returns a random float:
	 * 
	 * 0.0f <= value <= 1.0f
	 * 
	 * @return the normalized float value
	 */
	public static float randomFloat(Random random) {
		return random.nextFloat();
	}
	
	public static boolean randomBoolean(double chanceToBeTrue, Random random) {
		return (RandomUtil.randomInt(100000000, random) < (int) (100000000 * chanceToBeTrue));
	}
}