package com.janoside.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;

public final class FileUtil {
	
	private FileUtil() {}
	
	public static String resolveFilepath(String filepath) {
		String expandedFilepath = filepath;
		
		if (filepath.startsWith("~" + File.separator)) {
			expandedFilepath = System.getProperty("user.home") + expandedFilepath.substring(1);
		}
		
		return expandedFilepath;
	}
	
	public static String read(String filepath) {
		try {
			StringBuilder fileData = new StringBuilder();
			
			BufferedReader reader = FileUtil.bufferedReaderForFile(filepath);
			
			try {
				String line = null;
				
				while ((line = reader.readLine()) != null) {
					fileData.append(line + "\n");
				}
			} finally {
				reader.close();
			}
			
			return fileData.toString();
			
		} catch (IOException ioe) {
			throw new RuntimeException("Failed to read file string content", ioe);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T readObjectFromFile(String filepath) {
		return FileUtil.readObjectFromFile(new File(filepath));
	}
	
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> T readObjectFromFile(File file) {
		ObjectInputStream inputStream = null;
		
		try {
			inputStream = new ObjectInputStream(new FileInputStream(file));
			
			return (T) inputStream.readObject();
			
		} catch (Exception e) {
			throw new RuntimeException("Failed to read object", e);
			
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
					
				} catch (IOException ioe) {
					// we tried to close, best we can do, no need to throw this
				}
			}
		}
	}
	
	public static <T extends Serializable> void writeObjectToFile(T object, String filepath) {
		FileUtil.writeObjectToFile(object, new File(filepath));
	}
	
	public static <T extends Serializable> void writeObjectToFile(T object, File file) {
		ObjectOutputStream outputStream = null;
		
		try {
			outputStream = new ObjectOutputStream(FileUtils.openOutputStream(file));
			
			outputStream.writeObject(object);
			
		} catch (Exception e) {
			throw new RuntimeException("Failed to write object", e);
			
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
					
				} catch (IOException ioe) {
					// we tried to close, best we can do, no need to throw this
				}
			}
		}
	}
	
	public static List<String> readLines(String filepath) {
		try {
			ArrayList<String> lines = new ArrayList<String>();
			
			BufferedReader reader = FileUtil.bufferedReaderForFile(filepath);
			
			try {
				String line = null;
				
				while ((line = reader.readLine()) != null) {
					lines.add(line);
				}
			} finally {
				reader.close();
			}
			
			return lines;
			
		} catch (IOException ioe) {
			throw new RuntimeException("Failed to read lines from file", ioe);
		}
	}
	
	public static Iterator<String> iterateLines(String filepath) {
		return new FileUtil.TextFileIterator(FileUtil.resolveFilepath(filepath));
	}
	
	public static BufferedReader bufferedReaderForFile(String filepath) {
		try {
			return new BufferedReader(new InputStreamReader(new FileInputStream(new File(FileUtil.resolveFilepath(filepath))), "UTF-8"));
			
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException("UTF-8 is dead, long live UTF-8?", uee);
			
		} catch (FileNotFoundException fnfe) {
			throw new RuntimeException(fnfe);
		}
	}
	
	public static void writeBytes(String filepath, byte[] bytes) {
		try {
			String resolvedFilepath = FileUtil.resolveFilepath(filepath);
			
			// create directory if it doesn't exist
			FileUtil.createDirectory(FileUtil.getDirectory(resolvedFilepath));
			
			FileOutputStream file = new FileOutputStream(resolvedFilepath);
			
			file.write(bytes);
			file.close();
			
		} catch (IOException ioe) {
			throw new RuntimeException("Failed to write bytes to file", ioe);
		}
	}
	
	public static void append(String filepath, String data) {
		try {
			FileOutputStream file = new FileOutputStream(FileUtil.resolveFilepath(filepath), true);
			DataOutputStream out = new DataOutputStream(file);
			out.writeBytes(data);
			out.flush();
			out.close();
			
		} catch (IOException ioe) {
			throw new RuntimeException("Failed to append string to file", ioe);
		}
	}
	
	public static void createDirectory(String directory) {
		try {
			if (!FileUtil.directoryExists(FileUtil.resolveFilepath(directory))) {
				if (!new File(FileUtil.resolveFilepath(directory)).mkdirs()) {
					throw new IOException("Failed to create directory '" + FileUtil.resolveFilepath(directory) + "'");
				}
			}
		} catch (IOException ioe) {
			throw new RuntimeException("Failed to create directory", ioe);
		}
	}
	
	public static boolean directoryExists(String directory) {
		File file = new File(FileUtil.resolveFilepath(directory));
		
		return (file.exists() && file.isDirectory());
	}
	
	public static List<String> getFiles(String dir) {
		ArrayList<String> files = new ArrayList<String>();
		
		FileUtil.getFiles(dir, files);
		
		return files;
	}
	
	private static void getFiles(String dir, List<String> files) {
		File directory = new File(FileUtil.resolveFilepath(dir));
		
		File[] directoryContents = directory.listFiles();
		
		for (File file : directoryContents) {
			if (file.isFile()) {
				files.add(file.getAbsolutePath());
				
			} else if (file.isDirectory()) {
				getFiles(file.getAbsolutePath(), files);
			}
		}
	}
	
	public static String getDirectory(String filepath) {
		if (filepath.endsWith("/")) {
			return filepath;
			
		} else {
			return filepath.substring(0, filepath.lastIndexOf('/'));
		}
	}
	
	public static byte[] getBytesFromFile(File file) {
		InputStream inputStream = null;
		
		try {
			inputStream = new FileInputStream(file);
			
			return StreamUtil.streamToByteArray(inputStream);
			
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
			
		} finally {
			if (inputStream != null) {
				try {
					// Close the input stream and return bytes
					inputStream.close();
					
				} catch (IOException ioe) {
					throw new RuntimeException(ioe);
				}
			}
		}
	}
	
	public static byte[] getBytesFromFile(String file) {
		return FileUtil.getBytesFromFile(new File(FileUtil.resolveFilepath(file)));
	}
	
	private static class TextFileIterator implements Iterator<String> {
		
		// The stream we're reading from
		BufferedReader in;
		
		// Return value of next call to next()
		String nextline;
		
		public TextFileIterator(String filename) {
			// Open the file and read and remember the first line.
			// We peek ahead like this for the benefit of hasNext().
			try {
				in = FileUtil.bufferedReaderForFile(filename);
				nextline = in.readLine();
				
			} catch (IOException e) {
				throw new IllegalArgumentException(e);
			}
		}
		
		// If the next line is non-null, then we have a next line
		public boolean hasNext() {
			return nextline != null;
		}
		
		// Return the next line, but first read the line that follows it.
		public String next() {
			try {
				String result = nextline;
				
				// If we haven't reached EOF yet
				if (nextline != null) {
					nextline = in.readLine(); // Read another line
					if (nextline == null)
						in.close(); // And close on EOF
				}
				
				// Return the line we read last time through.
				return result;
				
			} catch (IOException e) {
				throw new IllegalArgumentException(e);
			}
		}
		
		// The file is read-only; we don't allow lines to be removed.
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}
}