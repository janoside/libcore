package com.janoside.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public final class CombinationUtil {
	
	private CombinationUtil() {}
	
	private static ConcurrentHashMap<String, List<List<Integer>>> cache = new ConcurrentHashMap<String, List<List<Integer>>>();
	
	public static List<List<Integer>> allCombinations(List<Integer> values) {
		String cacheKey = values.toString();
		
		if (!cache.containsKey(cacheKey)) {
			ArrayList<List<Integer>> combinations = new ArrayList<List<Integer>>();
			
			if (values.size() == 1) {
				ArrayList<Integer> simpleList = new ArrayList<Integer>();
				simpleList.add(values.get(0));
				
				combinations.add(simpleList);
				
			} else {
				for (int i = 0; i < values.size(); i++) {
					ArrayList<Integer> otherValues = new ArrayList<Integer>(values.size() - 1);
					for (int j = 0; j < values.size(); j++) {
						if (i != j) {
							otherValues.add(values.get(j));
						}
					}
					
					List<List<Integer>> subCombinations = CombinationUtil.allCombinations(otherValues);
					
					for (List<Integer> combinationList : subCombinations) {
						combinationList.add(0, values.get(i));
					}
					
					combinations.addAll(subCombinations);
				}
			}
			
			cache.put(cacheKey, combinations);
		}
		
		return cache.get(cacheKey);
	}
}