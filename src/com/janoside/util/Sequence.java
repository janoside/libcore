package com.janoside.util;

public interface Sequence<T> {
	
	T getItem(int index);
}