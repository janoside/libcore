package com.janoside.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public final class GzipUtil {
	
	private GzipUtil() {}

	public static byte[] gzip(byte[] input) {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			GZIPOutputStream gzipStream = new GZIPOutputStream(outputStream);
			gzipStream.write(input);
			gzipStream.finish();
			outputStream.flush();
			
			return outputStream.toByteArray();
			
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}

	public static byte[] gzip(String input) {
		return GzipUtil.gzip(StringUtil.utf8BytesFromString(input));
	}
	
	public static byte[] gzip(String input, String charsetName) throws IOException {
		return GzipUtil.gzip(input.getBytes(charsetName));
	}
	
	public static byte[] gunzip(byte[] input) {
		try {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(input);
			GZIPInputStream gzipStream = new GZIPInputStream(inputStream);
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream(1024);
			byte[] buffer = new byte[1024];
	
			while (true) {
				int bytesRead = gzipStream.read(buffer);
	
				if (bytesRead == -1) {
					break;
				}
	
				outputStream.write(buffer, 0, bytesRead);
			}
	
			outputStream.flush();
			
			return outputStream.toByteArray();
			
		} catch (IOException ioe) {
			throw new RuntimeException(ioe);
		}
	}
	
	public static String gunzipString(byte[] input) {
		return StringUtil.utf8StringFromBytes(GzipUtil.gunzip(input));
	}
	
	public static String gunzipString(byte[] input, String charsetName) throws IOException {
		return new String(GzipUtil.gunzip(input), charsetName);
	}
}