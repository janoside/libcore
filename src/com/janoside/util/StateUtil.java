package com.janoside.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@SuppressWarnings("serial")
public final class StateUtil {
	
	private StateUtil() {}
	
	private static Pattern stateNamePattern = Pattern.compile("(alaska|alabama|arkansas|arizona|california|colorado|connecticut|washingtondc|delaware|florida|hawaii|idaho|illinois|indiana|iowa|kansas|kentucky|louisiana|maine|maryland|massachusetts|michigan|minnesota|mississippi|missouri|montana|nebraska|nevada|newhampshire|newjersey|newmexico|newyork|northcarolina|northdakota|ohio|oklahoma|oregon|pennsylvania|rhodeisland|southcarolina|southdakota|tennessee|texas|utah|vermont|virginia|washington|westvirginia|wisconsin|wyoming)", Pattern.CASE_INSENSITIVE);
	
	private static Pattern stateAbbreviationPattern = Pattern.compile("(ak|al|ar|az|ca|co|ct|dc|de|fl|ga|hi|id|il|in|ia|ks|ky|la|me|md|ma|mi|mn|ms|mo|mt|ne|nv|nh|nj|nm|ny|nc|nd|oh|ok|or|pa|ri|sc|sd|tn|tx|ut|vt|va|wa|wv|wi|wy)", Pattern.CASE_INSENSITIVE);
	
	private static final Map<String, String> namesByAbbreviation = Collections.unmodifiableMap(new HashMap<String, String>() {{
		put("AK", "Alaska");
		put("AL", "Alabama");
		put("AR", "Arkansas");
		put("AZ", "Arizona");
		put("CA", "California");
		put("CO", "Colorado");
		put("CT", "Connecticut");
		put("DC", "Washington D.C.");
		put("DE", "Delaware");
		put("FL", "Florida");
		put("GA", "Georgia");
		put("HI", "Hawaii");
		put("ID", "Idaho");
		put("IL", "Illinois");
		put("IN", "Indiana");
		put("IA", "Iowa");
		put("KS", "Kansas");
		put("KY", "Kentucky");
		put("LA", "Louisiana");
		put("ME", "Maine");
		put("MD", "Maryland");
		put("MA", "Massachusetts");
		put("MI", "Michigan");
		put("MN", "Minnesota");
		put("MS", "Mississippi");
		put("MO", "Missouri");
		put("MT", "Montana");
		put("NE", "Nebraska");
		put("NV", "Nevada");
		put("NH", "New Hampshire");
		put("NJ", "New Jersey");
		put("NM", "New Mexico");
		put("NY", "New York");
		put("NC", "North Carolina");
		put("ND", "North Dakota");
		put("OH", "Ohio");
		put("OK", "Oklahoma");
		put("OR", "Oregon");
		put("PA", "Pennsylvania");
		put("RI", "Rhode Island");
		put("SC", "South Carolina");
		put("SD", "South Dakota");
		put("TN", "Tennessee");
		put("TX", "Texas");
		put("UT", "Utah");
		put("VT", "Vermont");
		put("VA", "Virginia");
		put("WA", "Washington");
		put("WV", "West Virginia");
		put("WI", "Wisconsin");
		put("WY", "Wyoming");
	}});
	
	private static final Map<String, String> abbreviationsByName = Collections.unmodifiableMap(new HashMap<String, String>() {{
		put("Alaska", "AK");
		put("Alabama", "AL");
		put("Arkansas", "AR");
		put("Arizona", "AZ");
		put("California", "CA");
		put("Colorado", "CO");
		put("Connecticut", "CT");
		put("Washington D.C.", "DC");
		put("Delaware", "DE");
		put("Florida", "FL");
		put("Georgia", "GA");
		put("Hawaii", "HI");
		put("Idaho", "ID");
		put("Illinois", "IL");
		put("Indiana", "IN");
		put("Iowa", "IA");
		put("Kansas", "KS");
		put("Kentucky", "KY");
		put("Louisiana", "LA");
		put("Maine", "ME");
		put("Maryland", "MD");
		put("Massachusetts", "MA");
		put("Michigan", "MI");
		put("Minnesota", "MN");
		put("Mississippi", "MS");
		put("Missouri", "MO");
		put("Montana", "MT");
		put("Nebraska", "NE");
		put("Nevada", "NV");
		put("New Hampshire", "NH");
		put("New Jersey", "NJ");
		put("New Mexico", "NM");
		put("New York", "NY");
		put("North Carolina", "NC");
		put("North Dakota", "ND");
		put("North Carolina", "NC");
		put("Ohio", "OH");
		put("Oklahoma", "OK");
		put("Oregon", "OR");
		put("Pennsylvania", "PA");
		put("Rhode Island", "RI");
		put("South Carolina", "SC");
		put("South Dakota", "SD");
		put("Tennessee", "TN");
		put("Texas", "TX");
		put("Utah", "UT");
		put("Vermont", "VT");
		put("Virginia", "VA");
		put("Washington", "WA");
		put("West Virginia", "WV");
		put("Wisconsin", "WI");
		put("Wyoming", "WY");
	}});
	
	private static final Map<String, String> abbreviationsByNormalizedName = Collections.unmodifiableMap(new HashMap<String, String>() {{
		put("alaska", "AK");
		put("alabama", "AL");
		put("arkansas", "AR");
		put("arizona", "AZ");
		put("california", "CA");
		put("colorado", "CO");
		put("connecticut", "CT");
		put("washingtondc", "DC");
		put("delaware", "DE");
		put("florida", "FL");
		put("georgia", "GA");
		put("hawaii", "HI");
		put("idaho", "ID");
		put("illinois", "IL");
		put("indiana", "IN");
		put("iowa", "IA");
		put("kansas", "KS");
		put("kentucky", "KY");
		put("louisiana", "LA");
		put("maine", "ME");
		put("maryland", "MD");
		put("massachusetts", "MA");
		put("michigan", "MI");
		put("minnesota", "MN");
		put("mississippi", "MS");
		put("missouri", "MO");
		put("montana", "MT");
		put("nebraska", "NE");
		put("nevada", "NV");
		put("newhampshire", "NH");
		put("newjersey", "NJ");
		put("newmexico", "NM");
		put("newyork", "NY");
		put("northcarolina", "NC");
		put("northdakota", "ND");
		put("northcarolina", "NC");
		put("ohio", "OH");
		put("oklahoma", "OK");
		put("oregon", "OR");
		put("pennsylvania", "PA");
		put("rhodeisland", "RI");
		put("southcarolina", "SC");
		put("southdakota", "SD");
		put("tennessee", "TN");
		put("texas", "TX");
		put("utah", "UT");
		put("vermont", "VT");
		put("virginia", "VA");
		put("washington", "WA");
		put("westvirginia", "WV");
		put("wisconsin", "WI");
		put("wyoming", "WY");
	}});
	
	public static String getNameByAbbreviation(String stateAbbreviation) {
		return namesByAbbreviation.get(stateAbbreviation.toUpperCase());
	}
	
	public static String getAbbreviationByName(String stateName) {
		return abbreviationsByNormalizedName.get(stateName.toLowerCase().trim().replaceAll("\\W", ""));
	}
	
	public static String getNameByNormalizedName(String normalizedName) {
		return namesByAbbreviation.get(abbreviationsByNormalizedName.get(normalizedName));
	}
	
	public static String getNormalizedName(String name) {
		return name.toLowerCase().replaceAll("\\W", "");
	}
	
	public static boolean isStateName(String name) {
		return stateNamePattern.matcher(name).matches();
	}
	
	public static boolean isStateAbbreviation(String abbreviation) {
		return stateAbbreviationPattern.matcher(abbreviation).matches();
	}
	
	public static Map<String, String> getNamesByAbbreviation() {
		return namesByAbbreviation;
	}
	
	public static Map<String, String> getAbbreviationsByName() {
		return abbreviationsByName;
	}
	
	public static Collection<String> getNames() {
		return Collections.unmodifiableCollection(namesByAbbreviation.values());
	}
	
	public static Collection<String> getAbbreviations() {
		return Collections.unmodifiableCollection(namesByAbbreviation.keySet());
	}
}