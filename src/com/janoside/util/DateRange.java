package com.janoside.util;

import java.util.Date;

public class DateRange {
	
	private Date startDate;
	
	private Date endDate;
	
	public DateRange() {
	}
	
	public DateRange(Date startDate, Date endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
	}
	
	public Date getStartDate() {
		return (startDate != null ? new Date(startDate.getTime()) : null);
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = (startDate != null ? new Date(startDate.getTime()) : null);
	}
	
	public Date getEndDate() {
		return (endDate != null ? new Date(endDate.getTime()) : null);
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = (endDate != null ? new Date(endDate.getTime()) : null);
	}
	
	public boolean contains(Date date) {
		return (this.startDate.before(date) && this.endDate.after(date));
	}
	
	public boolean isOverlappedBy(DateRange dateRange) {
		long s1 = this.startDate.getTime();
		long s2 = dateRange.getStartDate().getTime();
		
		long e1 = this.endDate.getTime();
		long e2 = dateRange.getEndDate().getTime();
		
		if (s1 >= s2 && s1 <= e2) {
			return true;
		}
		
		if (e1 >= s2 && e1 <= e2) {
			return true;
		}
		
		if (s1 < s2 && e1 > e2) {
			return true;
		}
		
		return false;
	}
	
	public String toString() {
		return "DateRange(start=" + DateUtil.format("yyyy-MM-dd HH:mm:ss", this.startDate) + ", end=" + DateUtil.format("yyyy-MM-dd HH:mm:ss", this.endDate) + ")";
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof DateRange) {
			return o.toString().equals(this.toString());
			
		} else {
			return false;
		}
	}
}