package com.janoside.util;

public class Tuple3<T, U, V> {
	
	private T item1;
	
	private U item2;
	
	private V item3;
	
	public Tuple3(T item1, U item2, V item3) {
		this.item1 = item1;
		this.item2 = item2;
		this.item3 = item3;
	}
	
	public T getItem1() {
		return item1;
	}
	
	public void setItem1(T item1) {
		this.item1 = item1;
	}
	
	public U getItem2() {
		return item2;
	}
	
	public void setItem2(U item2) {
		this.item2 = item2;
	}
	
	public V getItem3() {
		return item3;
	}
	
	public void setItem3(V item3) {
		this.item3 = item3;
	}
	
	public String toString() {
		return String.format("%s-%s-%s", item1, item2, item3);
	}
	
	public int hashCode() {
		return this.toString().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o == null) {
			return false;
			
		} else if (o instanceof Tuple3) {
			Tuple3 t = (Tuple3) o;
			
			if ((this.item1 == null) != (t.getItem1() == null)) {
				return false;
			}

			if ((this.item2 == null) != (t.getItem2() == null)) {
				return false;
			}
			
			if ((this.item3 == null) != (t.getItem3() == null)) {
				return false;
			}
			
			if (this.item1 != null && !this.item1.equals(t.getItem1())) {
				return false;
			}
			
			if (this.item2 != null && !this.item2.equals(t.getItem2())) {
				return false;
			}
			
			if (this.item3 != null && !this.item3.equals(t.getItem3())) {
				return false;
			}
			
			return true;
			
		} else {
			return false;
		}
	}
}