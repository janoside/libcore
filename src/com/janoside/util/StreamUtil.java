package com.janoside.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public final class StreamUtil {
	
	private StreamUtil() {}
	
	public static String convertStreamToString(InputStream inputStream, String charset) {
		try {
			return IOUtils.toString(inputStream, charset);
			
		} catch (IOException ioe) {
			throw new RuntimeException("Error converting stream to string", ioe);
		}
	}
	
	public static String convertStreamToUtf8String(InputStream inputStream) {
		return StreamUtil.convertStreamToString(inputStream, "UTF-8");
	}
	
	public static byte[] streamToByteArray(InputStream inputStream) {
		try {
			return IOUtils.toByteArray(inputStream);
			
		} catch (IOException ioe) {
			throw new RuntimeException("Error converting stream to string", ioe);
		}
	}
}