package com.janoside.util;

import java.util.HashSet;
import java.util.Set;

public final class SetUtil {
	
	private SetUtil() {}
	
	public static <T> Set<T> setFromItem(T item) {
		HashSet<T> set = new HashSet<T>();
		set.add(item);
		
		return set;
	}
}