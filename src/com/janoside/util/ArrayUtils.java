package com.janoside.util;

import java.util.ArrayList;
import java.util.List;

import com.janoside.transform.ObjectTransformer;

public final class ArrayUtils {
	
	private ArrayUtils() {}
	
	public static <T> String toString(T[] array) {
		return ArrayUtils.toList(array).toString();
	}
	
	public static <T> List<T> toList(T[] array) {
		ArrayList<T> items = new ArrayList<T>(array.length);
		for (T item : array) {
			items.add(item);
		}
		
		return items;
	}
	
	public static <T> List<T> toListAfterTransform(T[] array, ObjectTransformer<T, T> transformer) {
		ArrayList<T> items = new ArrayList<T>(array.length);
		for (T item : array) {
			items.add(transformer.transform(item));
		}
		
		return items;
	}
	
	public static boolean isEmpty(Object[] array) {
		return (array == null || array.length == 0);
	}
}