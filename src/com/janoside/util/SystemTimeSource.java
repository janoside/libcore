package com.janoside.util;

public class SystemTimeSource implements TimeSource {
	
	public long getCurrentTime() {
		return System.currentTimeMillis();
	}
}