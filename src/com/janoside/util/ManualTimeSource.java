package com.janoside.util;

public class ManualTimeSource implements TimeSource {
	
	private long time;
	
	public long getCurrentTime() {
		return time;
	}
	
	public void setCurrentTime(long time) {
		this.time = time;
	}
}