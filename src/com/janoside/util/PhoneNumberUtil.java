package com.janoside.util;

public final class PhoneNumberUtil {
	
	private PhoneNumberUtil() {}
	
	public static boolean isPhoneNumber(String input) {
		if (input == null) {
			return false;
		}
		
		String phoneNumber = input.replaceAll("\\D", "");
		
		return (phoneNumber.length() == 10);
	}
	
	public static String formatPhoneNumber(String input) {
		String phoneNumber = input.replaceAll("\\D", "");
		
		StringBuilder buffer = new StringBuilder(20);
		
		buffer.append(phoneNumber.substring(0, 3));
		buffer.append("-");
		buffer.append(phoneNumber.substring(3, 6));
		buffer.append("-");
		buffer.append(phoneNumber.substring(6));
		
		return buffer.toString();
	}
}