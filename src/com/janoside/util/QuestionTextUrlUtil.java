package com.janoside.util;

import java.util.regex.Pattern;

public final class QuestionTextUrlUtil {
	
	private QuestionTextUrlUtil() {}
	
	private final static char[] escapeChars = {'~', '-', '/', '\\'};
	
	public static String encode(String questionText) {
		String encodedQuestion = questionText.toLowerCase().replaceAll(Pattern.quote("&") + "#([0-9]+);", "&amp;#$1;");
		
		for (char escapeChar : escapeChars) {
			encodedQuestion = encodedQuestion.replace(Character.toString(escapeChar), "&#" + ((int) escapeChar) + ";");
		}
		
		encodedQuestion = UrlUtil.encode(encodedQuestion);
		
		encodedQuestion = encodedQuestion.replace('+', '-');

		return encodedQuestion;
	}
	
	public static String decode(String encodedQuestion, boolean urlEncoded) {
		String decodedQuestion = encodedQuestion;
		
		if (urlEncoded) {
			decodedQuestion = UrlUtil.decode(decodedQuestion);
		}

		decodedQuestion = decodedQuestion.replace('-', ' ');
		
		for (char escapeChar : escapeChars) {
			decodedQuestion = decodedQuestion.replace("&#" + ((int) escapeChar) + ";", Character.toString(escapeChar));
		}

		decodedQuestion = decodedQuestion.replaceAll(Pattern.quote("&") + "amp;#([0-9]+);", "&#$1;");

		return decodedQuestion;
	}
}
