package com.janoside.util;

public class RoundRobinIntegerSequence implements Sequence<Integer> {
	
	private int itemCount;
	
	public Integer getItem(int index) {
		return index % this.itemCount;
	}
	
	public void setItemCount(int itemCount) {
		this.itemCount = itemCount;
	}
}