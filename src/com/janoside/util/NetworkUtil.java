package com.janoside.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class NetworkUtil {
	
	private NetworkUtil() {}
	
	private static final Logger logger = LoggerFactory.getLogger(NetworkUtil.class);
	
	private static String hostname = null;
	
	private static byte[] ipAddress = null;
	
	static {
		try {
			hostname = InetAddress.getLocalHost().getHostName();
			
		} catch (UnknownHostException e) {
			logger.error("Failed to get local hostname", e);
			
			hostname = "localhost";
		}
		
		try {
			ipAddress = InetAddress.getLocalHost().getAddress();
			
		} catch (UnknownHostException e) {
			logger.error("Failed to get local IP", e);
			
			ipAddress = new byte[] {127, 0, 0, 1};
		}
	}

	public static String getHostname() {
		return hostname;
	}
	
	public static byte[] getIpAddress() {
		return Arrays.copyOf(ipAddress, 4);
	}
}