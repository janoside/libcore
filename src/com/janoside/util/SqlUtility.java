package com.janoside.util;

public final class SqlUtility {
	
	private SqlUtility() {}
	
	public static String escape(String value) {
		return value.replace("'", "''");
	}
}