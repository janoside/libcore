package com.janoside.util;

public class Validator {
	
	public boolean isValidZipcode(String zipcode) {
		if (StringUtil.hasText(zipcode)) {
			if (zipcode.matches("^[0-9]{5}(-[0-9]{4})?$")) {
				return true;

			}
		}
		
		return false;
	}
}