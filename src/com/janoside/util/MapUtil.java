package com.janoside.util;

import java.util.HashMap;
import java.util.Map;

public final class MapUtil {
	
	private MapUtil() {}
	
	public static <T, U> Map<U, T> invertMap(Map<T, U> map) {
		HashMap<U, T> invertedMap = new HashMap<U, T>();
		
		for (Map.Entry<T, U> entry : map.entrySet()) {
			invertedMap.put(entry.getValue(), entry.getKey());
		}
		
		return invertedMap;
	}
	
	public static <T, U> Map<T, U> cloneMap(Map<T, U> sourceMap) {
		if (sourceMap == null) {
			return new HashMap<T, U>();
			
		} else {
			return new HashMap<T, U>(sourceMap);
		}
	}
}