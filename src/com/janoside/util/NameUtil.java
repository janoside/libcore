package com.janoside.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public final class NameUtil {
	
	private NameUtil() {}
	
	private static final String ShortMaleNamesString = "John,Dave,Paul,Mark,Gary,Tim,Jose,Eric,Carl,Ryan,Joe,Juan,Jack,Roy,Will,Fred,Bob,Vic,Todd,Alan,Sean,Earl,Jim,Tony,Luis,Mike,Dale,Glen,Jeff,Chad,Lee,Kyle,Ray,Joel,Don,Ed,Rick,Troy,Rand,Jay,Jim,Tom,Alex,Bill,Leo,Dean,Greg,Dan,Sam,Ben,Brad,Ron,Cory,Neil,Ted,Cody,Al,Guy,Hugh,Max,Ian,Ken,Ivan,Ross,Andy,Kirk,Seth,Kent,Wade,Joey,Nick,Evan,Otis,Luke,Doug,Lyle,Matt,Rex,Omar,Rudy,Pete,Owen,Mack,Levi,Jake,Drew,Bert,Phil,Saul,Toby,Abel,Otto,Bret,Eli,Stan,Hans,Rod,Hal,Gus,Kris,Loyd,Ty,Scot,Taye,Eddy,Cole,Dick,Cruz,Lane,Vito,Dirk,Adan,Zack,Alva,Rich,Thad,Elmo,Aron,Gil";
	
	private static final String ShortFemaleNamesString = "Mary,Lisa,Amy,Anna,Jean,Rose,Judy,Jane,Lori,Sara,Ruby,Lois,Rita,Edna,Kim,Emma,Eva,Gail,Jill,Erin,Lynn,June,Dana,Ida,Meg,Sue,Beth,Tara,Lucy,Gina,Joy,Nina,Leah,Olga,Faye,Ada,Iris,Jodi,Jan,Lola,Pam,Kara,Pat,Mona,Hope,Teri,Gwen,Kate,Lana,Elsa,Lela,Reba,Dena,Cleo,Lupe,Keri,Lily,Rene,Abby,Lara,Mia,Tia,Eve,Liza,Nita,Liz";
	
	private static final List<String> shortMaleNames;
	
	private static final List<String> shortFemaleNames;
	
	static {
		shortMaleNames = new ArrayList<String>(new HashSet<String>(StringUtil.split(ShortMaleNamesString, ",")));
		shortFemaleNames = new ArrayList<String>(new HashSet<String>(StringUtil.split(ShortFemaleNamesString, ",")));
	}
	
	public static List<String> getShortNames() {
		ArrayList<String> names = new ArrayList<String>(shortMaleNames);
		names.addAll(shortFemaleNames);
		
		Collections.shuffle(names);
		
		return names;
	}
	
	public static List<String> getShortMaleNames() {
		return new ArrayList<String>(shortMaleNames);
	}
	
	public static List<String> getShortFemaleNames() {
		return new ArrayList<String>(shortFemaleNames);
	}
	
	public static String getRandomShortName() {
		return RandomUtil.randomSelection(NameUtil.getShortNames());
	}
	
	public static String getRandomShortMaleName() {
		return RandomUtil.randomSelection(shortMaleNames);
	}
	
	public static String getRandomShortFemaleName() {
		return RandomUtil.randomSelection(shortMaleNames);
	}
}