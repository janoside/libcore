package com.janoside.util;

public interface Predicate<T> {
	
	boolean apply(T object);
}