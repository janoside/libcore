package com.janoside.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class RegexUtil {
	
	private RegexUtil() {}

	public static String patternEscape(String string) {
		return Pattern.quote(string);
	}
	
	public static String replacementEscape(String string) {
		return Matcher.quoteReplacement(string);
	}
}
