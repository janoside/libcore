package com.janoside.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ByteUtils {
	
	private ByteUtils() {}
	
	public static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		
		return buf.toString();
	}
	
	// Ref: http://stackoverflow.com/a/7983531/673828
	public static byte[] xor(byte[] data, byte[] key) {
		byte[] out = new byte[data.length];
		for (int i = 0; i < data.length; i++) {
			out[i] = (byte) (data[i] ^ key[i % key.length]);
		}
		
		return out;
	}
	
	public static List<byte[]> splitByteArray(byte[] data, byte[] separator) {
		ArrayList<byte[]> arrays = new ArrayList<byte[]>();
		
		int start = 0;
		
		int i = 0;
		while (i < data.length) {
			boolean matches = true;
			
			for (int j = 0; j < separator.length; j++) {
				if (data[i + j] != separator[j]) {
					matches = false;
					
					break;
				}
			}
			
			if (matches) {
				arrays.add(Arrays.copyOfRange(data, start, i));
				
				i += separator.length;
				start = i;
				
			} else {
				i++;
			}
		}
		
		if (start < data.length) {
			arrays.add(Arrays.copyOfRange(data, start, data.length));
		}
		
		return arrays;
	}
	
	public static List<byte[]> splitByteArrayByLengths(byte[] data, int[] chunkLengths) {
		int totalChunkLength = 0;
		for (int chunkLength : chunkLengths) {
			totalChunkLength += chunkLength;
		}
		
		if (data.length != totalChunkLength) {
			throw new RuntimeException("Invalid chunk lengths given, dataLength=" + data.length + ", totalChunkLength=" + totalChunkLength);
		}
		
		ArrayList<byte[]> arrays = new ArrayList<byte[]>();
		
		int start = 0;
		int chunkIndex = 0;
		while (start < data.length) {
			arrays.add(Arrays.copyOfRange(data, start, start + chunkLengths[chunkIndex]));
			
			start += chunkLengths[chunkIndex];
			chunkIndex++;
		}
		
		return arrays;
	}
}