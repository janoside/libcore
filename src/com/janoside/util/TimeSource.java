package com.janoside.util;

public interface TimeSource {
	
	long getCurrentTime();
}