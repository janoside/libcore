package com.janoside.util;

public interface Function<T> {
	
	void run(T t);
}