package com.janoside.util;

import java.io.File;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public final class ClassUtils {
	
	private ClassUtils() {}
	
	// http://www.dzone.com/snippets/get-all-classes-within-package
	public static Class[] getClasses(String packageName) {
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			assert classLoader != null;
			String path = packageName.replace('.', '/');
			Enumeration<URL> resources = classLoader.getResources(path);
			
			List<File> dirs = new ArrayList<File>();
			while (resources.hasMoreElements()) {
				URL resource = resources.nextElement();
				dirs.add(new File(resource.getFile()));
			}
			
			ArrayList<Class> classes = new ArrayList<Class>();
			for (File directory : dirs) {
				classes.addAll(findClasses(directory, packageName, false));
			}
			
			return classes.toArray(new Class[classes.size()]);
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public static List<Class> findClasses(File directory, String packageName, boolean recursive) {
		try {
			List<Class> classes = new ArrayList<Class>();
			if (!directory.exists()) {
				return classes;
			}
			
			File[] files = directory.listFiles();
			for (File file : files) {
				if (file.isDirectory() && recursive) {
					assert !file.getName().contains(".");
					classes.addAll(findClasses(file, packageName + "." + file.getName(), recursive));
					
				} else if (file.getName().endsWith(".class")) {
					classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
				}
			}
			
			return classes;
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	// Ref: http://stackoverflow.com/a/6094602/673828
	public static Object objectFromClassName(String className, Object... constructorArgs) {
		try {
			Class<?> clazz = Class.forName(className);
			Constructor<?> ctor = clazz.getConstructor(String.class);
			return ctor.newInstance(constructorArgs);
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
}