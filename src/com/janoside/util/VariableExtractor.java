package com.janoside.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class VariableExtractor {
	
	public List<String> getVariableNames(String content, String startTag) {
		HashSet<String> variableNames = new HashSet<String>();
		
		String contentCopy = content;
		while (contentCopy.contains(startTag)) {
			contentCopy = contentCopy.substring(contentCopy.indexOf(startTag) + 1);
			
			boolean done = false;
			for (int i = 0; i < contentCopy.length(); i++) {
				char c = contentCopy.charAt(i);
				
				if (!Character.isLetter(c) && !Character.isDigit(c) && c != '_') {
					if (i > 0) {
						variableNames.add(contentCopy.substring(0, i));
						
						done = true;
						
						break;
					}
				}
			}
			
			if (!done) {
				variableNames.add(contentCopy);
			}
		}
		
		return new ArrayList<String>(variableNames);
	}
}