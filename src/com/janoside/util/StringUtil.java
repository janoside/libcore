package com.janoside.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.janoside.json.JsonArray;
import com.janoside.json.JsonObject;
import com.janoside.text.TextTransformer;

public final class StringUtil {
	
	private StringUtil() {}
	
	private static final DecimalFormat MemoryDecimalFormat = new DecimalFormat("0.0#");
	
	public static List<String> transformEachInCollection(Collection<String> strings, TextTransformer transformer) {
		ArrayList<String> outputStrings = new ArrayList<String>(strings.size());
		
		for (String s : strings) {
			outputStrings.add(transformer.transform(s));
		}
		
		return outputStrings;
	}
	
	/**
	 * Calls input.replaceAll(Pattern.quote(match), Matcher.quoteReplacement(replacement))
	 * @param input
	 * @param match
	 * @param replacement
	 * @return
	 */
	public static String replaceAllSafely(String input, String match, String replacement) {
		return input.replaceAll(Pattern.quote(match), Matcher.quoteReplacement(replacement));
	}
	
	public static String utf8StringFromBytes(byte[] bytes) {
		return StringUtil.stringFromBytes(bytes, "UTF-8");
	}
	
	public static byte[] utf8BytesFromString(String s) {
		return StringUtil.bytesFromString(s, "UTF-8");
	}
	
	public static String stringFromBytes(byte[] bytes, String charset) {
		try {
			return new String(bytes, charset);
			
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException("UTF-8 is dead, head for the hills.");
		}
	}
	
	public static byte[] bytesFromString(String s, String charset) {
		try {
			return s.getBytes(charset);
			
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException("UTF-8 is dead, head for the hills.");
		}
	}
	
	public static String padBeginning(String s, char padding, int fullLength) {
		StringBuilder buffer = new StringBuilder(s);
		
		while (buffer.length() < fullLength) {
			buffer.insert(0, padding);
		}
		
		return buffer.toString();
	}
	
	public static String padIntWithZeroes(int i, int fullLength) {
		return StringUtil.padBeginning(Integer.toString(i), '0', fullLength);
	}
	
	public static String padEnd(String s, char padding, int fullLength) {
		StringBuilder buffer = new StringBuilder(s);
		
		while (buffer.length() < fullLength) {
			buffer.append(padding);
		}
		
		return buffer.toString();
	}
	
	public static String mergeCommaSeparatedStrings(String s1, String s2) {
		HashSet<String> sources = new HashSet<String>();
		
		if (StringUtil.hasText(s1)) {
			String[] existingSources = s1.split(",");
			for (String source : existingSources) {
				if (StringUtil.hasText(source)) {
					sources.add(source);
				}
			}
		}
		
		if (StringUtil.hasText(s2)) {
			String[] newSources = s2.split(",");
			for (String source : newSources) {
				if (StringUtil.hasText(source)) {
					sources.add(source);
				}
			}
		}
		
		ArrayList<String> mergedList = new ArrayList<String>(sources);
		Collections.sort(mergedList);
		
		return StringUtil.collectionToDelimitedString(mergedList, ",", "", "");
	}
	
	public static String capitalizeAllWords(String input) {
		String[] words = input.split("\\s+");
		
		StringBuilder buffer = new StringBuilder(input.length());
		
		int index = 0;
		for (String word : words) {
			if (index++ > 0) {
				buffer.append(" ");
			}
			
			buffer.append(StringUtil.capitalize(word));
		}
		
		return buffer.toString();
	}
	
	public static JsonObject buildJsonTreeFromPaths(List<String> paths, String pathSeparator) {
		JsonObject tree = new JsonObject();
		
		for (String path : paths) {
			JsonObject json = tree;
			
			List<String> parts = StringUtil.split(path, Pattern.quote(pathSeparator));
			
			for (int i = 0; i < parts.size() - 1; i++) {
				if (!json.has(parts.get(i))) {
					json.put(parts.get(i), new JsonObject());
				}
				
				json = json.getJsonObject(parts.get(i));
				
				if (i == parts.size() - 2) {
					if (!json.has("items")) {
						json.put("items", new JsonArray());
					}
					
					json.getJsonArray("items").put(new JsonObject().put("path", path).put("name", parts.get(parts.size() - 1)));
				}
			}
		}
		
		return tree;
	}
	
	public static List<String> split(String input, String splitRegex, boolean trim, boolean includeBlankStrings) {
		ArrayList<String> items = new ArrayList<String>();
		if (StringUtil.hasText(input)) {
			String[] itemStringArray = input.split(splitRegex);
			
			for (String itemString : itemStringArray) {
				String cleanItemString = trim ? itemString.trim() : itemString;
				
				if (includeBlankStrings) {
					items.add(cleanItemString);
					
				} else if (StringUtil.hasText(cleanItemString)) {
					items.add(cleanItemString);
				}
			}
		} else {
			if (trim) {
				if (includeBlankStrings) {
					items.add("");
				}
			} else {
				items.add(input);
			}
		}
		
		return items;
	}
	
	public static String formatAsCurrency(String floatValue) {
		String amountString = floatValue;
		
		if (amountString.contains(".")) {
			if (amountString.indexOf('.') < amountString.length() - 3) {
				amountString = amountString.substring(0, amountString.indexOf('.') + 3);
				
			} else if (amountString.indexOf('.') > amountString.length() - 3) {
				int addLength = (3 - (amountString.length() - amountString.indexOf('.')));
				
				amountString = StringUtil.padEnd(
						amountString,
						'0',
						amountString.length() + addLength);
			}
		} else {
			amountString = (amountString + ".00");
		}
		
		return amountString;
	}
	
	public static String formatDecimal(long value) {
		if (value < 1000) {
			return Long.toString(value);
		}
		
		StringBuilder buffer = new StringBuilder();
		
		String valueString = Long.toString(value);
		
		if (valueString.length() % 3 > 0) {
			buffer.append(valueString.substring(0, valueString.length() % 3));
			buffer.append(",");
			
			valueString = valueString.substring(valueString.length() % 3);
		}
		
		int commas = valueString.length() / 3;
		for (int i = 0; i < commas; i++) {
			buffer.append(valueString.substring(3 * i, 3 * (i + 1)));
			
			if (i < (commas - 1)) {
				buffer.append(",");
			}
		}
		
		return buffer.toString();
	}
	
	public static String formatDecimal(String value) {
		String valueString = value.toString();
		String floatingPointValueString = "";
		if (valueString.contains(".")) {
			floatingPointValueString = valueString.substring(valueString.indexOf('.'));
			valueString = valueString.substring(0, valueString.indexOf('.'));
		}
		
		if (valueString.length() < 4) {
			return (valueString + floatingPointValueString);
		}
		
		StringBuilder buffer = new StringBuilder();
		
		if (valueString.length() % 3 > 0) {
			buffer.append(valueString.substring(0, valueString.length() % 3));
			buffer.append(",");
			
			valueString = valueString.substring(valueString.length() % 3);
		}
		
		int commas = valueString.length() / 3;
		for (int i = 0; i < commas; i++) {
			buffer.append(valueString.substring(3 * i, 3 * (i + 1)));
			
			if (i < (commas - 1)) {
				buffer.append(",");
			}
		}
		
		buffer.append(floatingPointValueString);
		
		return buffer.toString();
	}
	
	public static List<String> split(String input, String splitRegex) {
		return StringUtil.split(input, splitRegex, true, false);
	}
	
	public static List<Integer> splitAndParseInt(String input, String splitRegex) {
		List<String> intStrings = StringUtil.split(input, splitRegex);
		
		ArrayList<Integer> ints = new ArrayList<Integer>();
		for (String intString : intStrings) {
			ints.add(Integer.parseInt(intString));
		}
		
		return ints;
	}
	
	public static List<Long> splitAndParseLong(String input, String splitRegex) {
		List<String> longStrings = StringUtil.split(input, splitRegex);
		
		ArrayList<Long> longs = new ArrayList<Long>();
		for (String longString : longStrings) {
			longs.add(Long.parseLong(longString));
		}
		
		return longs;
	}
	
	public static boolean isInteger(String input) {
		for (int i = 0; i < input.length(); i++) {
			if (!Character.isDigit(input.charAt(i))) {
				return false;
			}
		}
		
		return true;
	}
	
	public static String formatOpenCloseTags(String input, String openTag, String closeTag, String openTagReplacement, String closeTagReplacement) {
		boolean inside = false;
		
		StringBuilder buffer = new StringBuilder(input.length());
		
		int index = 0;
		while (index < input.length()) {
			if (inside && index <= (input.length() - closeTag.length()) && input.substring(index, index + closeTag.length()).equals(closeTag)) {
				buffer.append(closeTagReplacement);
				inside = !inside;
				
				index += closeTag.length();
				
			} else if (!inside && index <= (input.length() - openTag.length()) && input.substring(index, index + openTag.length()).equals(openTag) && StringUtil.countOccurrencesOf(input.substring(index + openTag.length()), closeTag) > 0) {
				buffer.append(openTagReplacement);
				inside = !inside;
				
				index += openTag.length();
				
			} else {
				buffer.append(input.charAt(index));
				
				index++;
			}
		}
		
		return buffer.toString();
	}
	
	public static String camelCaseToLowercaseDashSeparated(String input) {
		StringBuilder buffer = new StringBuilder((int) (input.length() * 1.1));
		buffer.append(Character.toLowerCase(input.charAt(0)));
		
		for (int i = 1; i < input.length(); i++) {
			if (Character.isUpperCase(input.charAt(i))) {
				buffer.append('-');
				buffer.append(Character.toLowerCase(input.charAt(i)));
				
			} else {
				buffer.append(input.charAt(i));
			}
		}
		
		return buffer.toString();
	}
	
	// Ref: http://stackoverflow.com/a/3758880/673828
	public static String bytesToMemoryString(long bytes, boolean si) {
		int unit = si ? 1000 : 1024;
	    if (bytes < unit) return bytes + " B";
	    int exp = (int) (Math.log(bytes) / Math.log(unit));
	    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
	    return String.format("%s %sB", MemoryDecimalFormat.format(bytes / Math.pow(unit, exp)), pre);
	}
	
	public static String convertIso8859ToUtf8(String input) {
		try {
			Charset iso = Charset.forName("ISO-8859-1");
			
			CharsetEncoder enc = iso.newEncoder();
			ByteBuffer bb = enc.encode(CharBuffer.wrap(input));
			
			return new String(bb.array(), "UTF-8");
			
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}
	
	public static boolean hasText(CharSequence str) {
		if (!hasLength(str)) {
			return false;
		}
		int strLen = str.length();
		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(str.charAt(i))) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean hasLength(CharSequence str) {
		return (str != null && str.length() > 0);
	}
	
	public static String capitalize(String str) {
		if (str == null || str.length() == 0) {
			return str;
		}
		StringBuilder sb = new StringBuilder(str.length());
		sb.append(Character.toUpperCase(str.charAt(0)));
		sb.append(str.substring(1));
		return sb.toString();
	}
	
	public static int countOccurrencesOf(String str, String sub) {
		if (str == null || sub == null || str.length() == 0 || sub.length() == 0) {
			return 0;
		}
		int count = 0;
		int pos = 0;
		int idx;
		while ((idx = str.indexOf(sub, pos)) != -1) {
			++count;
			pos = idx + sub.length();
		}
		return count;
	}
	
	public static String collectionToDelimitedString(Collection<?> coll, String delim, String prefix, String suffix) {
		if (CollectionUtil.isEmpty(coll)) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		Iterator<?> it = coll.iterator();
		while (it.hasNext()) {
			sb.append(prefix).append(it.next()).append(suffix);
			if (it.hasNext()) {
				sb.append(delim);
			}
		}
		return sb.toString();
	}
	
	public static String arrayToDelimitedString(Object[] arr, String delim) {
		if (ArrayUtils.isEmpty(arr)) {
			return "";
		}
		if (arr.length == 1) {
			return ObjectUtils.nullSafeToString(arr[0]);
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			if (i > 0) {
				sb.append(delim);
			}
			sb.append(arr[i]);
		}
		return sb.toString();
	}
	
	/**
	 * From http://stackoverflow.com/a/546729/673828
	 * 
	 * @param input
	 * @return
	 */
	public static String removeDiacritics(String input) {
		String nrml = Normalizer.normalize(input, Normalizer.Form.NFD);
		
		StringBuilder stripped = new StringBuilder();
		for (int i = 0; i < nrml.length(); ++i) {
			if (Character.getType(nrml.charAt(i)) != Character.NON_SPACING_MARK) {
				stripped.append(nrml.charAt(i));
			}
		}
		
		return stripped.toString();
	}
	
	public static String rangeForValue(long value, int stepSize) {
		long stepCount = value / stepSize;
		
		long min = stepCount * stepSize;
		long max = min + stepSize;
		
		return String.format(Locale.getDefault(), "%d-%d", min, max);
	}
}