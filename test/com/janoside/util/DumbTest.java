package com.janoside.util;

import org.junit.Test;

import com.janoside.JunitTestUtil;
import com.janoside.security.SecurityUtil;

public class DumbTest {
	
	@Test
	public void testPrivateConstructors() throws Exception {
		JunitTestUtil.testConstructorIsPrivate(ArrayUtils.class);
		JunitTestUtil.testConstructorIsPrivate(ByteUtils.class);
		JunitTestUtil.testConstructorIsPrivate(ClassUtils.class);
		JunitTestUtil.testConstructorIsPrivate(CollectionUtil.class);
		JunitTestUtil.testConstructorIsPrivate(CombinationUtil.class);
		JunitTestUtil.testConstructorIsPrivate(DateUtil.class);
		JunitTestUtil.testConstructorIsPrivate(FileUtil.class);
		JunitTestUtil.testConstructorIsPrivate(GzipUtil.class);
		JunitTestUtil.testConstructorIsPrivate(HashUtil.class);
		JunitTestUtil.testConstructorIsPrivate(MapUtil.class);
		JunitTestUtil.testConstructorIsPrivate(NameUtil.class);
		JunitTestUtil.testConstructorIsPrivate(NetworkUtil.class);
		JunitTestUtil.testConstructorIsPrivate(ObjectUtils.class);
		JunitTestUtil.testConstructorIsPrivate(PhoneNumberUtil.class);
		JunitTestUtil.testConstructorIsPrivate(QuestionTextUrlUtil.class);
		JunitTestUtil.testConstructorIsPrivate(RandomUtil.class);
		JunitTestUtil.testConstructorIsPrivate(RegexUtil.class);
		JunitTestUtil.testConstructorIsPrivate(SecurityUtil.class);
		JunitTestUtil.testConstructorIsPrivate(SetUtil.class);
		JunitTestUtil.testConstructorIsPrivate(SqlUtility.class);
		JunitTestUtil.testConstructorIsPrivate(StateUtil.class);
		JunitTestUtil.testConstructorIsPrivate(StatTrackerUtil.class);
		JunitTestUtil.testConstructorIsPrivate(StreamUtil.class);
		JunitTestUtil.testConstructorIsPrivate(StringUtil.class);
		JunitTestUtil.testConstructorIsPrivate(ThreadSafeDateParser.class);
		JunitTestUtil.testConstructorIsPrivate(UrlUtil.class);
	}
}