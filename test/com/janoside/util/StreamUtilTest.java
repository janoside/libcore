package com.janoside.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.janoside.JunitTestUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOUtils.class})
public class StreamUtilTest {
	
	@Test
	public void testCharsetError() throws Exception {
		try {
			StreamUtil.convertStreamToString(null, "abc");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testConstructor() throws Exception{
		JunitTestUtil.testConstructorIsPrivate(StreamUtil.class);
	}
	
	@Test
	public void testUtf8StreamToString() throws Exception {
		ByteArrayInputStream bais = new ByteArrayInputStream("abc".getBytes("UTF-8"));
		
		Assert.assertEquals("abc", StreamUtil.convertStreamToUtf8String(bais));
	}
	
	@Test
	public void testStreamToByteArray() {
		Assert.assertEquals("[97, 98, 99]", Arrays.toString(StreamUtil.streamToByteArray(new ByteArrayInputStream("abc".getBytes()))));
	}
	
	@Test
	public void testIOUtilsException() throws Exception {
		PowerMock.mockStatic(IOUtils.class);
		
		EasyMock.expect(IOUtils.toString((InputStream) EasyMock.anyObject(), EasyMock.anyString())).andThrow(new IOException());
		
		PowerMock.replayAll();
		
		try {
			StreamUtil.convertStreamToUtf8String(new ByteArrayInputStream("abc".getBytes()));
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		PowerMock.verifyAll();
	}
	
	@Test
	public void testStreamToByteArrayIOException() throws Exception {
		PowerMock.mockStatic(IOUtils.class);
		
		EasyMock.expect(IOUtils.toByteArray((InputStream) EasyMock.anyObject())).andThrow(new IOException());
		
		PowerMock.replayAll();
		
		try {
			StreamUtil.streamToByteArray(new ByteArrayInputStream("abc".getBytes()));
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		PowerMock.verifyAll();
	}
}