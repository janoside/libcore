package com.janoside.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FileUtilTest.class, File.class, FileInputStream.class, StreamUtil.class})
public class FileUtilTest {
	
	@Test
	public void testIterator() throws Exception {
		String uuid = UUID.randomUUID().toString();
		
		FileUtil.writeBytes("/tmp/" + uuid, "1\n2\n3\n4\n5\n6\n7\n8\n9\n10\n".getBytes());
		
		Iterator<String> lineIterator = FileUtil.iterateLines("/tmp/" + uuid);
		
		AtomicInteger counter = new AtomicInteger(0);
		while (lineIterator.hasNext()) {
			lineIterator.next();
			
			try {
				lineIterator.remove();
				
				throw new Exception();
				
			} catch (UnsupportedOperationException e) {
				// expected
			}
			
			counter.getAndIncrement();
		}
		
		Assert.assertEquals(10, counter.get());
	}
	
	@Test
	public void testGetBytesFromFile() throws Exception {
		String file = "/tmp/" + UUID.randomUUID().toString();
		
		FileUtil.writeBytes(file, "abc".getBytes());
		
		Assert.assertEquals("abc", new String(FileUtil.getBytesFromFile(file)));
	}
	
	@Test
	public void testGetFiles() {
		FileUtil.writeBytes("/tmp/" + UUID.randomUUID().toString(), "abc".getBytes());
		
		List<String> files = FileUtil.getFiles("/tmp/");
		
		Assert.assertNotNull(files);
		Assert.assertTrue(files.size() > 0);
	}
	
	@Test
	public void testCreateDir() {
		FileUtil.createDirectory("/tmp/" + UUID.randomUUID().toString() + "/");
	}
	
	@Test
	public void testReadWrite() {
		String filepath = "/tmp/" + UUID.randomUUID().toString();
		
		FileUtil.writeObjectToFile("abc", filepath);
		
		Assert.assertEquals("abc", FileUtil.readObjectFromFile(filepath));
		
		FileUtil.writeBytes(filepath, "abc".getBytes());
		
		Assert.assertEquals("abc", FileUtil.read(filepath).trim());
		
		FileUtil.writeBytes(filepath, "abc\ndef\nghi\n".getBytes());
		
		Assert.assertEquals("[abc, def, ghi]", FileUtil.readLines(filepath).toString());
		
		FileUtil.append(filepath, "jkl");
		
		Assert.assertEquals("[abc, def, ghi, jkl]", FileUtil.readLines(filepath).toString());
		
		new File(filepath).delete();
	}
}