package com.janoside.util;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class VariableExtractorTest {
	
	@Test
	public void testIt() {
		VariableExtractor ve = new VariableExtractor();
		
		List<String> variableNames = ve.getVariableNames("test $one test $two test", "$");
		
		Assert.assertEquals(2, variableNames.size());
		Assert.assertTrue(variableNames.contains("one"));
		Assert.assertTrue(variableNames.contains("two"));
	}
	
	@Test
	public void testEnd() {
		VariableExtractor ve = new VariableExtractor();
		
		List<String> variableNames = ve.getVariableNames("test $one test $two", "$");
		
		Assert.assertEquals(2, variableNames.size());
		Assert.assertTrue(variableNames.contains("one"));
		Assert.assertTrue(variableNames.contains("two"));
	}
	
	@Test
	public void testStart() {
		VariableExtractor ve = new VariableExtractor();
		
		List<String> variableNames = ve.getVariableNames("$one test $two test", "$");
		
		Assert.assertEquals(2, variableNames.size());
		Assert.assertTrue(variableNames.contains("one"));
		Assert.assertTrue(variableNames.contains("two"));
	}
}