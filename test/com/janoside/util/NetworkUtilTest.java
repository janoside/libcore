package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class NetworkUtilTest {
	
	@Test
	public void testBasic() {
		Assert.assertNotNull(NetworkUtil.getHostname());
		
		byte[] ip = NetworkUtil.getIpAddress();
		
		Assert.assertNotNull(ip);
	}
}