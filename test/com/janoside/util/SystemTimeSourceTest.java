package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class SystemTimeSourceTest {
	
	@Test
	public void testBasic() throws InterruptedException {
		SystemTimeSource sts = new SystemTimeSource();
		
		long now = System.currentTimeMillis();
		
		Thread.sleep(10);
		
		Assert.assertTrue(now < sts.getCurrentTime());
	}
}