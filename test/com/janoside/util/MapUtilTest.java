package com.janoside.util;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class MapUtilTest {
	
	@Test
	@SuppressWarnings("serial")
	public void testInvertMap() {
		HashMap<String, Integer> map = new HashMap<String, Integer>() {{
			put("abc", 1);
			put("def", 2);
			put("ghi", 3);
		}};
		
		Map<Integer, String> invertedMap = MapUtil.invertMap(map);
		
		Assert.assertTrue(invertedMap.containsKey(1));
		Assert.assertEquals("abc", invertedMap.get(1));
		Assert.assertTrue(invertedMap.containsKey(2));
		Assert.assertEquals("def", invertedMap.get(2));
		Assert.assertTrue(invertedMap.containsKey(3));
		Assert.assertEquals("ghi", invertedMap.get(3));
	}
	
	@Test
	public void testCloneMap() {
		Map map = new HashMap() {{
			put("abc", "cheese");
			put("ghi", "haha");
		}};
		
		Map map2 = MapUtil.cloneMap(map);
		
		Assert.assertTrue(map != map2);
		Assert.assertEquals(map, map2);
		
		Assert.assertTrue(MapUtil.cloneMap(null).isEmpty());
	}
}