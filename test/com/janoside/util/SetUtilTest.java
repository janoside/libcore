package com.janoside.util;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class SetUtilTest {
	
	@Test
	public void testSetFromItem() {
		Object item = new Object();
		
		Set<Object> set = SetUtil.setFromItem(item);
		
		Assert.assertTrue(set.contains(item));
		Assert.assertEquals("setSize", 1, set.size());
	}
}