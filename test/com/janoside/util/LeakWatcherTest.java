package com.janoside.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class LeakWatcherTest {
	
	@Test
	public void testIt() throws Exception {
		final List released = Collections.synchronizedList(new ArrayList());
		
		LeakWatcher lw = new LeakWatcher();
		lw.start(new Function() {
			public void run(Object o) {
				released.add(o);
			}
		}, 10);
		
		Object o = new Object() {
			public String toString() {
				return "theObject";
			}
		};
		
		lw.watch(o);
		
		Thread.sleep(20);
		
		o = null;
		
		System.gc();
		
		Thread.sleep(100);
		
		Assert.assertEquals("[theObject]", released.toString());
	}
}