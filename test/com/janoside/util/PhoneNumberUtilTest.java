package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class PhoneNumberUtilTest {
	
	@Test
	public void testIt() {
		Assert.assertEquals("123-456-7890", PhoneNumberUtil.formatPhoneNumber("1234567890"));
		Assert.assertEquals("123-456-7890", PhoneNumberUtil.formatPhoneNumber("123-456-7890"));
		
		Assert.assertFalse(PhoneNumberUtil.isPhoneNumber(null));
		Assert.assertFalse(PhoneNumberUtil.isPhoneNumber("abc"));
		Assert.assertTrue(PhoneNumberUtil.isPhoneNumber("a1b2c3456-7890"));
	}
}