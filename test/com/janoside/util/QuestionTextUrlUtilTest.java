package com.janoside.util;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class QuestionTextUrlUtilTest {
	
	@Test
	public void testEncodeAndDecode() {
		String[] questionTexts = new String[] {
			"Are aluminum cans really tens cents each when you recycle them in Michigan",
			"What is 5 - 10 / 9 * 20? And what does &#45; mean? And what does &#1; mean?  \\LOLZ\\CRAZY\\SIG\\~~~~~~~~~~~~",
			"When does 'Prototype' come out for XBox360",
			"What is your name? So what is your name",
			"Is Lagoon in Farmington, UT open this Saturday",
			"What are the best 6.5\" JL car speakers",
			"Why did the chicken cross the road? (other than to get to the other side)",
			"how many pounds of fuel does it take to launch the space shuttle into space?",
			"what are the best sites for legitimately downloading mp3s? Do any sites offer mp3s for $0.50 or less?"
		};
		
		for (String questionText : questionTexts) {
			System.out.println(questionText);
			
			String encoded = QuestionTextUrlUtil.encode(questionText);
			System.out.println(encoded);
			
			Assert.assertFalse("Special characters not encoded!", encoded.matches(".*[/\\\\~\\s].*"));
			
			String decoded = QuestionTextUrlUtil.decode(encoded, true);
			
			Assert.assertEquals("Decoded string does not match original string!", questionText.toLowerCase(), decoded);
		}
	}
	
	@Test
	@SuppressWarnings("serial")
	public void testExpected() {
		HashMap<String, String> input = new HashMap<String, String>() {{
			put("a-%3B-video-money-%2C-haha-pizza-%26%2345%3B-3", "a ; video money , haha pizza - 3");
			put("what-are-the-best-sites-for-legitimately-downloading-mp3s%3F-do-any-sites-offer-mp3s-for-%240.50-or-less%3F", "what are the best sites for legitimately downloading mp3s? do any sites offer mp3s for $0.50 or less?");
		}};
		
		for (Map.Entry<String, String> entry : input.entrySet()) {
			Assert.assertEquals(entry.getKey(), QuestionTextUrlUtil.encode(entry.getValue()));
			Assert.assertEquals(entry.getValue(), QuestionTextUrlUtil.decode(entry.getKey(), true));
		}
	}
}