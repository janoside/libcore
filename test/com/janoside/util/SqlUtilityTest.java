package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class SqlUtilityTest {
	
	@Test
	public void testIt() {
		Assert.assertEquals("abc", SqlUtility.escape("abc"));
		Assert.assertEquals("''abc''", SqlUtility.escape("'abc'"));
	}
}