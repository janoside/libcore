/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.janoside.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.JunitTestUtil;

/**
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @author Rick Evans
 */
public class ObjectUtilsTest {
	
	@Test
	public void testIsCheckedException() {
		Assert.assertTrue(ObjectUtils.isCheckedException(new Exception()));
		Assert.assertTrue(ObjectUtils.isCheckedException(new SQLException()));
		
		Assert.assertFalse(ObjectUtils.isCheckedException(new RuntimeException()));
		
		// Any Throwable other than RuntimeException and Error
		// has to be considered checked according to the JLS.
		Assert.assertTrue(ObjectUtils.isCheckedException(new Throwable()));
	}
	
	@Test
	public void testIsCompatibleWithThrowsClause() {
		Class<?>[] empty = new Class[0];
		Class<?>[] exception = new Class[] { Exception.class };
		Class<?>[] sqlAndIO = new Class[] { SQLException.class, IOException.class };
		Class<?>[] throwable = new Class[] { Throwable.class };
		
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new RuntimeException()));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new RuntimeException(), empty));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new RuntimeException(), exception));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new RuntimeException(), sqlAndIO));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new RuntimeException(), throwable));
		
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new Exception()));
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new Exception(), empty));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new Exception(), exception));
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new Exception(), sqlAndIO));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new Exception(), throwable));
		
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new SQLException()));
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new SQLException(), empty));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new SQLException(), exception));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new SQLException(), sqlAndIO));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new SQLException(), throwable));
		
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new Throwable()));
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new Throwable(), empty));
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new Throwable(), exception));
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new Throwable(), sqlAndIO));
		Assert.assertTrue(ObjectUtils.isCompatibleWithThrowsClause(new Throwable(), throwable));
		
		Assert.assertFalse(ObjectUtils.isCompatibleWithThrowsClause(new Exception(), (Class<?>[])null));
	}
	
	@Test
	public void testIsArray() {
		Assert.assertFalse(ObjectUtils.isArray(null));
		Assert.assertTrue(ObjectUtils.isArray(new Object[] {}));
		Assert.assertFalse(ObjectUtils.isArray(new Object()));
	}
	
	@Test
	public void testIsEmpty() {
		Assert.assertTrue(ObjectUtils.isEmpty(null));
		Assert.assertTrue(ObjectUtils.isEmpty(new Object[] {}));
		Assert.assertFalse(ObjectUtils.isEmpty(new Object[] {"abc"}));
	}
	
	@Test
	public void testContainsElement() {
		Assert.assertFalse(ObjectUtils.containsElement(null, "abc"));
		Assert.assertFalse(ObjectUtils.containsElement(new Object[] {"def"}, "abc"));
		Assert.assertFalse(ObjectUtils.containsElement(new Object[] {null}, "abc"));
		Assert.assertTrue(ObjectUtils.containsElement(new Object[] {null, "abc"}, "abc"));
	}
	
	@Test
	public void testToObjectArray() {
		int[] a = new int[] { 1, 2, 3, 4, 5 };
		Integer[] wrapper = (Integer[]) ObjectUtils.toObjectArray(a);
		Assert.assertTrue(wrapper.length == 5);
		for (int i = 0; i < wrapper.length; i++) {
			Assert.assertEquals(a[i], wrapper[i].intValue());
		}
	}
	
	@Test
	public void testToObjectArrayWithNull() {
		Object[] objects = ObjectUtils.toObjectArray(null);
		Assert.assertNotNull(objects);
		Assert.assertEquals(0, objects.length);
	}
	
	@Test
	public void testToObjectArrayWithEmptyPrimitiveArray() {
		Object[] objects = ObjectUtils.toObjectArray(new byte[] {});
		Assert.assertNotNull(objects);
		Assert.assertEquals(0, objects.length);
	}
	
	@Test
	public void testToObjectArrayWithNonArrayType() {
		try {
			ObjectUtils.toObjectArray("Not an []");
			Assert.fail("Must have thrown an IllegalArgumentException by this point.");
		} catch (IllegalArgumentException expected) {
		}
	}
	
	@Test
	public void testToObjectArrayWithNonPrimitiveArray() {
		String[] source = new String[] { "Bingo" };
		assertArrayEquals(source, ObjectUtils.toObjectArray(source));
	}
	
	@Test
	public void testAddObjectToArraySunnyDay() {
		String[] array = new String[] { "foo", "bar" };
		String newElement = "baz";
		Object[] newArray = ObjectUtils.addObjectToArray(array, newElement);
		Assert.assertEquals(3, newArray.length);
		Assert.assertEquals(newElement, newArray[2]);
	}
	
	@Test
	public void testAddObjectToArrayWhenEmpty() {
		String[] array = new String[0];
		String newElement = "foo";
		String[] newArray = ObjectUtils.addObjectToArray(array, newElement);
		Assert.assertEquals(1, newArray.length);
		Assert.assertEquals(newElement, newArray[0]);
	}
	
	@Test
	public void testAddObjectToSingleNonNullElementArray() {
		String existingElement = "foo";
		String[] array = new String[] { existingElement };
		String newElement = "bar";
		String[] newArray = ObjectUtils.addObjectToArray(array, newElement);
		Assert.assertEquals(2, newArray.length);
		Assert.assertEquals(existingElement, newArray[0]);
		Assert.assertEquals(newElement, newArray[1]);
	}
	
	@Test
	public void testAddObjectToSingleNullElementArray() {
		String[] array = new String[] { null };
		String newElement = "bar";
		String[] newArray = ObjectUtils.addObjectToArray(array, newElement);
		Assert.assertEquals(2, newArray.length);
		Assert.assertEquals(null, newArray[0]);
		Assert.assertEquals(newElement, newArray[1]);
	}
	
	@Test
	public void testAddObjectToNullArray() throws Exception {
		String newElement = "foo";
		String[] newArray = ObjectUtils.addObjectToArray(null, newElement);
		Assert.assertEquals(1, newArray.length);
		Assert.assertEquals(newElement, newArray[0]);
	}
	
	@Test
	public void testAddNullObjectToNullArray() throws Exception {
		Object[] newArray = ObjectUtils.addObjectToArray(null, null);
		Assert.assertEquals(1, newArray.length);
		Assert.assertEquals(null, newArray[0]);
	}
	
	@Test
	public void testNullSafeEqualsWithArrays() throws Exception {
		Assert.assertTrue(ObjectUtils.nullSafeEquals(new String[] { "a", "b", "c" }, new String[] { "a", "b", "c" }));
		Assert.assertTrue(ObjectUtils.nullSafeEquals(new int[] { 1, 2, 3 }, new int[] { 1, 2, 3 }));
	}
	
	@Test
	public void testHashCodeWithBooleanFalse() {
		int expected = Boolean.FALSE.hashCode();
		Assert.assertEquals(expected, ObjectUtils.hashCode(false));
	}
	
	@Test
	public void testHashCodeWithBooleanTrue() {
		int expected = Boolean.TRUE.hashCode();
		Assert.assertEquals(expected, ObjectUtils.hashCode(true));
	}
	
	@Test
	public void testHashCodeWithDouble() {
		double dbl = 9830.43;
		int expected = (new Double(dbl)).hashCode();
		Assert.assertEquals(expected, ObjectUtils.hashCode(dbl));
	}
	
	@Test
	public void testHashCodeWithFloat() {
		float flt = 34.8f;
		int expected = (new Float(flt)).hashCode();
		Assert.assertEquals(expected, ObjectUtils.hashCode(flt));
	}
	
	@Test
	public void testHashCodeWithLong() {
		long lng = 883l;
		int expected = (new Long(lng)).hashCode();
		Assert.assertEquals(expected, ObjectUtils.hashCode(lng));
	}
	
	@Test
	public void testIdentityToString() {
		Object obj = new Object();
		String expected = obj.getClass().getName() + "@" + ObjectUtils.getIdentityHexString(obj);
		String actual = ObjectUtils.identityToString(obj);
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testIdentityToStringWithNullObject() {
		Assert.assertEquals("", ObjectUtils.identityToString(null));
	}
	
	@Test
	public void testNullSafeHashCodeWithBooleanArray() {
		int expected = 31 * 7 + Boolean.TRUE.hashCode();
		expected = 31 * expected + Boolean.FALSE.hashCode();
		
		boolean[] array = { true, false };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithBooleanArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((boolean[]) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithByteArray() {
		int expected = 31 * 7 + 8;
		expected = 31 * expected + 10;
		
		byte[] array = { 8, 10 };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithByteArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((byte[]) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithCharArray() {
		int expected = 31 * 7 + 'a';
		expected = 31 * expected + 'E';
		
		char[] array = { 'a', 'E' };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithCharArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((char[]) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithDoubleArray() {
		long bits = Double.doubleToLongBits(8449.65);
		int expected = 31 * 7 + (int) (bits ^ (bits >>> 32));
		bits = Double.doubleToLongBits(9944.923);
		expected = 31 * expected + (int) (bits ^ (bits >>> 32));
		
		double[] array = { 8449.65, 9944.923 };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithDoubleArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((double[]) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithFloatArray() {
		int expected = 31 * 7 + Float.floatToIntBits(9.6f);
		expected = 31 * expected + Float.floatToIntBits(7.4f);
		
		float[] array = { 9.6f, 7.4f };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithFloatArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((float[]) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithIntArray() {
		int expected = 31 * 7 + 884;
		expected = 31 * expected + 340;
		
		int[] array = { 884, 340 };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithIntArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((int[]) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithLongArray() {
		long lng = 7993l;
		int expected = 31 * 7 + (int) (lng ^ (lng >>> 32));
		lng = 84320l;
		expected = 31 * expected + (int) (lng ^ (lng >>> 32));
		
		long[] array = { 7993l, 84320l };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithLongArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((long[]) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithObject() {
		String str = "Luke";
		Assert.assertEquals(str.hashCode(), ObjectUtils.nullSafeHashCode(str));
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectArray() {
		int expected = 31 * 7 + "Leia".hashCode();
		expected = 31 * expected + "Han".hashCode();
		
		Object[] array = { "Leia", "Han" };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((Object[]) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingBooleanArray() {
		Object array = new boolean[] { true, false };
		int expected = ObjectUtils.nullSafeHashCode((boolean[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingByteArray() {
		Object array = new byte[] { 6, 39 };
		int expected = ObjectUtils.nullSafeHashCode((byte[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingCharArray() {
		Object array = new char[] { 'l', 'M' };
		int expected = ObjectUtils.nullSafeHashCode((char[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingDoubleArray() {
		Object array = new double[] { 68930.993, 9022.009 };
		int expected = ObjectUtils.nullSafeHashCode((double[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingFloatArray() {
		Object array = new float[] { 9.9f, 9.54f };
		int expected = ObjectUtils.nullSafeHashCode((float[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingIntArray() {
		Object array = new int[] { 89, 32 };
		int expected = ObjectUtils.nullSafeHashCode((int[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingLongArray() {
		Object array = new long[] { 4389, 320 };
		int expected = ObjectUtils.nullSafeHashCode((long[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingObjectArray() {
		Object array = new Object[] { "Luke", "Anakin" };
		int expected = ObjectUtils.nullSafeHashCode((Object[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectBeingShortArray() {
		Object array = new short[] { 5, 3 };
		int expected = ObjectUtils.nullSafeHashCode((short[]) array);
		assertEqualHashCodes(expected, array);
	}
	
	@Test
	public void testNullSafeHashCodeWithObjectEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((Object) null));
	}
	
	@Test
	public void testNullSafeHashCodeWithShortArray() {
		int expected = 31 * 7 + 70;
		expected = 31 * expected + 8;
		
		short[] array = { 70, 8 };
		int actual = ObjectUtils.nullSafeHashCode(array);
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testNullSafeHashCodeWithShortArrayEqualToNull() {
		Assert.assertEquals(0, ObjectUtils.nullSafeHashCode((short[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithBooleanArray() {
		boolean[] array = { true, false };
		Assert.assertEquals("{true, false}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithBooleanArrayBeingEmpty() {
		boolean[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithBooleanArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((boolean[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithByteArray() {
		byte[] array = { 5, 8 };
		Assert.assertEquals("{5, 8}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithByteArrayBeingEmpty() {
		byte[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithByteArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((byte[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithCharArray() {
		char[] array = { 'A', 'B' };
		Assert.assertEquals("{'A', 'B'}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithCharArrayBeingEmpty() {
		char[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithCharArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((char[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithDoubleArray() {
		double[] array = { 8594.93, 8594023.95 };
		Assert.assertEquals("{8594.93, 8594023.95}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithDoubleArrayBeingEmpty() {
		double[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithDoubleArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((double[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithFloatArray() {
		float[] array = { 8.6f, 43.8f };
		Assert.assertEquals("{8.6, 43.8}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithFloatArrayBeingEmpty() {
		float[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithFloatArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((float[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithIntArray() {
		int[] array = { 9, 64 };
		Assert.assertEquals("{9, 64}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithIntArrayBeingEmpty() {
		int[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithIntArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((int[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithLongArray() {
		long[] array = { 434l, 23423l };
		Assert.assertEquals("{434, 23423}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithLongArrayBeingEmpty() {
		long[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithLongArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((long[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithPlainOldString() {
		Assert.assertEquals("I shoh love tha taste of mangoes", ObjectUtils.nullSafeToString("I shoh love tha taste of mangoes"));
	}
	
	@Test
	public void testNullSafeToStringWithObjectArray() {
		Object[] array = { "Han", new Long(43) };
		Assert.assertEquals("{Han, 43}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithObjectArrayBeingEmpty() {
		Object[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithObjectArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((Object[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithShortArray() {
		short[] array = { 7, 9 };
		Assert.assertEquals("{7, 9}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithShortArrayBeingEmpty() {
		short[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithShortArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((short[]) null));
	}
	
	@Test
	public void testNullSafeToStringWithStringArray() {
		String[] array = { "Luke", "Anakin" };
		Assert.assertEquals("{Luke, Anakin}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithStringArrayBeingEmpty() {
		String[] array = {};
		Assert.assertEquals("{}", ObjectUtils.nullSafeToString(array));
	}
	
	@Test
	public void testNullSafeToStringWithStringArrayEqualToNull() {
		Assert.assertEquals("null", ObjectUtils.nullSafeToString((String[]) null));
	}
	
	@Test
	public void testContainsConstant() {
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "FOO"), is(true));
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "foo"), is(true));
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "BaR"), is(true));
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "bar"), is(true));
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "BAZ"), is(true));
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "baz"), is(true));
		
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "BOGUS"), is(false));
		
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "FOO", true), is(true));
		assertThat(ObjectUtils.containsConstant(Tropes.values(), "foo", true), is(false));
	}
	
	@Test
	public void testCaseInsensitiveValueOf() {
		assertThat(ObjectUtils.caseInsensitiveValueOf(Tropes.values(), "foo"), is(Tropes.FOO));
		assertThat(ObjectUtils.caseInsensitiveValueOf(Tropes.values(), "BAR"), is(Tropes.BAR));
		try {
			ObjectUtils.caseInsensitiveValueOf(Tropes.values(), "bogus");
			Assert.fail("expected IllegalArgumentException");
		} catch (IllegalArgumentException ex) {
			assertThat(ex.getMessage(), is("constant [bogus] does not exist in enum type " + "com.janoside.util.ObjectUtilsTest$Tropes"));
		}
	}
	
	@Test
	public void testMissingMethods() throws Exception {
		Assert.assertEquals("java.lang.Object", ObjectUtils.nullSafeClassName(new Object()));
		Assert.assertEquals("null", ObjectUtils.nullSafeClassName(null));
		
		
		Assert.assertEquals("", ObjectUtils.getDisplayString(null));
		Assert.assertTrue(ObjectUtils.getDisplayString(new Object()).contains("Object"));
		
		
		JunitTestUtil.testConstructorIsPrivate(ObjectUtils.class);
	}
	
	@Test
	public void testCheckedException() {
		Assert.assertFalse(ObjectUtils.isCheckedException(new NullPointerException()));
		Assert.assertFalse(ObjectUtils.isCheckedException(new OutOfMemoryError()));
	}
	
	private void assertEqualHashCodes(int expected, Object array) {
		int actual = ObjectUtils.nullSafeHashCode(array);
		Assert.assertEquals(expected, actual);
		Assert.assertTrue(array.hashCode() != actual);
	}
	
	enum Tropes {
		FOO, BAR, baz
	}
	
}