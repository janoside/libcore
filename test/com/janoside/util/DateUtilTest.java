package com.janoside.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class DateUtilTest {
	
	@Test
	public void testFormatDate() {
		Date date = new Date();
		
		Assert.assertNotNull(DateUtil.formatDate(date));
		
		Assert.assertFalse(DateUtil.formatDate(date).equals(""));
		
		date = new Date(System.currentTimeMillis() - DateUtil.MinuteMillis);
		
		Assert.assertNotNull(DateUtil.formatDate(date));
		
		Assert.assertFalse(DateUtil.formatDate(date).equals(""));
		
		date = new Date(System.currentTimeMillis() - DateUtil.HourMillis);
		
		Assert.assertNotNull(DateUtil.formatDate(date));
		
		Assert.assertFalse(DateUtil.formatDate(date).equals(""));
		
		date = new Date(System.currentTimeMillis() - DateUtil.DayMillis);
		
		Assert.assertNotNull(DateUtil.formatDate(date));
		
		Assert.assertFalse(DateUtil.formatDate(date).equals(""));
		
		date = new Date(System.currentTimeMillis() - DateUtil.WeekMillis);
		
		Assert.assertNotNull(DateUtil.formatDate(date));
		
		Assert.assertFalse(DateUtil.formatDate(date).equals(""));
		
		date = new Date(System.currentTimeMillis() - DateUtil.MonthMillis);
		
		Assert.assertNotNull(DateUtil.formatDate(date));
		
		Assert.assertFalse(DateUtil.formatDate(date).equals(""));
		
		date = new Date(System.currentTimeMillis() - (DateUtil.MonthMillis * 12));
		
		Assert.assertNotNull(DateUtil.formatDate(date));
		
		Assert.assertFalse(DateUtil.formatDate(date).equals(""));
		
		date = new Date(0);
		
		Assert.assertNotNull(DateUtil.formatDate(date));
		
		Assert.assertFalse(DateUtil.formatDate(date).equals(""));
		
		
		Assert.assertEquals("1 hr 10 min ago", DateUtil.formatDate(new Date(System.currentTimeMillis() - 70 * DateUtil.MinuteMillis)));
		
		String time = DateUtil.format("hh:mmaa", new Date(System.currentTimeMillis() - 2 * DateUtil.HourMillis)).toLowerCase();
		Assert.assertEquals("2 days ago, " + time, DateUtil.formatDate(new Date(System.currentTimeMillis() - 50 * DateUtil.HourMillis)));
	}
	
	@Test
	public void testParseTimeDifference() throws Exception {
		Assert.assertEquals(60000, DateUtil.parseTimeDifference("60s"));
		Assert.assertEquals(3600000, DateUtil.parseTimeDifference("1h"));
		Assert.assertEquals(60000, DateUtil.parseTimeDifference("1m"));
		Assert.assertEquals(300000, DateUtil.parseTimeDifference("5m"));
		Assert.assertEquals(24 * 3600000, DateUtil.parseTimeDifference("24h"));
		Assert.assertEquals(2 * 24 * 3600000, DateUtil.parseTimeDifference("2d"));
		Assert.assertEquals(31L * 24L * 3600000L, DateUtil.parseTimeDifference("31d"));
		Assert.assertEquals(7 * 5 * 24L * 3600000L, DateUtil.parseTimeDifference("5w"));
		Assert.assertEquals(31L * 24L * 3600000L, DateUtil.parseTimeDifference("1M"));
		
		try {
			DateUtil.parseTimeDifference("1x");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testParse() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = sdf.parse("1990-01-01");
		
		Assert.assertEquals(date.getTime(), DateUtil.parse("yyyy-MM-dd", "1990-01-01").getTime());
	}
	
	@Test
	public void testFormat() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		Date date = new Date();
		
		Assert.assertEquals(sdf.format(date), DateUtil.format("yyyy-MM-dd HH:mm:ss.SSS", date));
	}
	
	@Test
	public void testHourFloored() {
		String s = DateUtil.format("yyyy-MM-dd HH:mm:ss", DateUtil.getHourFloored());
		
		Assert.assertTrue(s.endsWith("00:00"));
	}
	
	@Test
	public void testGetDay() {
		Date d1 = DateUtil.parse("yyyy-MM-dd HH:mm:ss", "2009-07-20 12:02:43");
		Date d2 = DateUtil.parse("yyyy-MM-dd HH:mm:ss", "2009-07-21 18:19:27");
		
		Date d12 = DateUtil.getDay(d1);
		Date d22 = DateUtil.getDay(d2);
		
		Assert.assertEquals(DateUtil.DayMillis, (d22.getTime() - d12.getTime()));
	}
	
	@Test
	public void testPrint() {
		StringBuilder buffer = new StringBuilder();
		
		long start = 1404576286256L;
		
		int daysBack = 29;
		while (daysBack >= 0) {
			Date date = DateUtil.getDay(new Date(start - daysBack * DateUtil.DayMillis));
			
			buffer.append(DateUtil.format("yyyy-MM-dd", date));
			
			daysBack--;
		}
		
		Assert.assertEquals(209528971, buffer.toString().hashCode());
	}
	
	@Test
	public void testDaysBetween() {
		List<String> days = DateUtil.getDaysBetween("2010-05-03", "2010-05-09", "yyyy-MM-dd");
		
		Assert.assertEquals("[2010-05-03, 2010-05-04, 2010-05-05, 2010-05-06, 2010-05-07, 2010-05-08, 2010-05-09]", days.toString());
	}
	
	@Test
	public void testAnother() {
		Assert.assertEquals(1262304000000L, DateUtil.parse("yyyy-MM-dd HH:mm:ssZZZ", "2010-01-01 00:00:00GMT").getTime());
	}
	
	@Test
	public void testMonthStartEnd() {
		Date[] dates = DateUtil.getMonthStartAndEnd(2013, 7);
		
		Assert.assertEquals("Mon Jul 01 00:00:00 EDT 2013 - Wed Jul 31 23:59:59 EDT 2013", dates[0] + " - " + dates[1]);
	}
	
	@Test
	public void testMonthsBetween() {
		Date start = DateUtil.parse("yyyy-MM", "2013-04");
		Date end = DateUtil.parse("yyyy-MM-dd HH:mm:ss.SSS", "2013-05-01 00:00:00.000");
		
		List<Date> dates = DateUtil.getMonthsBetween(start, end);
		
		Assert.assertEquals(2, dates.size());
		
		start = DateUtil.parse("yyyy-MM", "2013-04");
		end = DateUtil.parse("yyyy-MM-dd HH:mm:ss.SSS", "2013-05-04 10:03:01.020");
		
		dates = DateUtil.getMonthsBetween(start, end);
		
		Assert.assertEquals(2, dates.size());
	}
	
	@Test
	public void testDaysBetween2() {
		List<Date> dates = DateUtil.getDatesBetween(new Date(1404939317874L), new Date(1405939317874L), 10000000);
		
		Assert.assertEquals(101, dates.size());
		Assert.assertEquals(1723418035, dates.toString().hashCode());
		
		dates = DateUtil.getDatesBetween(new Date(1404939317874L), new Date(1405939317874L), 9999000);
		
		Assert.assertEquals(103, dates.size());
		Assert.assertEquals(-1247548263, dates.toString().hashCode());
		
		dates = DateUtil.getDatesBetween(new Date(1404939317773L), new Date(1404939317874L), 2);
		
		Assert.assertEquals(53, dates.size());
		Assert.assertEquals(-2078032189, dates.toString().hashCode());
	}
	
	@Test
	public void testGetToday() {
		Date d = DateUtil.getToday();
		
		Assert.assertEquals("00:00", DateUtil.format("HH:mm", d));
		Assert.assertEquals(DateUtil.format("yyyy-MM-dd", new Date()), DateUtil.format("yyyy-MM-dd", d));
	}
	
	@Test
	public void testParseSimple() throws Exception {
		Date d = DateUtil.parseSimple("1984-07-24");
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseSimple("1984-07-24-00:00:00");
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseSimple("1984-07-24 00:00:00");
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseSimple("1984-07-24 00:00");
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseSimple("1984-07-24-00:00");
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseSimple("00:00:00");
		Assert.assertEquals(DateUtil.getDay(new Date()).getTime(), d.getTime());
		
		d = DateUtil.parseSimple("00:00");
		Assert.assertEquals(DateUtil.getDay(new Date()).getTime(), d.getTime());
		
		try {
			DateUtil.parseSimple("junk");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testNulls() {
		Assert.assertEquals("", DateUtil.formatDate(null));
	}
	
	@Test
	public void testGetLastDayOfMonth() {
		Assert.assertEquals(28, DateUtil.getLastDayOfMonth(2, 2014));
		Assert.assertEquals(29, DateUtil.getLastDayOfMonth(2, 2016));
		
		for (int i = 2000; i <= 2020; i++) {
			Assert.assertEquals(30, DateUtil.getLastDayOfMonth(6, i));
			Assert.assertEquals(31, DateUtil.getLastDayOfMonth(7, i));
		}
	}
	
	@Test
	public void testGetCurrentYear() {
		Assert.assertEquals(DateUtil.format("yyyy", new Date()), Integer.toString(DateUtil.getCurrentYear()));
	}
	
	@Test
	public void testFormatDuration() {
		Assert.assertEquals("00:00:01", DateUtil.formatDuration(1000));
		Assert.assertEquals("00:00:10", DateUtil.formatDuration(10000));
		Assert.assertEquals("00:00:11", DateUtil.formatDuration(11000));
		Assert.assertEquals("00:01:40", DateUtil.formatDuration(100000));
		Assert.assertEquals("00:16:40", DateUtil.formatDuration(1000000));
		Assert.assertEquals("02:46:40", DateUtil.formatDuration(10000000));
		Assert.assertEquals("27:46:40", DateUtil.formatDuration(100000000));
		Assert.assertEquals("277:46:40", DateUtil.formatDuration(1000000000));
		Assert.assertEquals("2777:46:40", DateUtil.formatDuration(10000000000L));
	}
	
	@Test
	public void testFormatDurationAsText() {
		Assert.assertEquals("1 second", DateUtil.formatDurationAsText(1000));
		Assert.assertEquals("10 seconds", DateUtil.formatDurationAsText(10000));
		Assert.assertEquals("11 seconds", DateUtil.formatDurationAsText(11000));
		Assert.assertEquals("1 minute 40 seconds", DateUtil.formatDurationAsText(100000));
		Assert.assertEquals("16 minutes 40 seconds", DateUtil.formatDurationAsText(1000000));
		Assert.assertEquals("2 hours 46 minutes 40 seconds", DateUtil.formatDurationAsText(10000000));
		Assert.assertEquals("1 day 3 hours 46 minutes 40 seconds", DateUtil.formatDurationAsText(100000000));
		Assert.assertEquals("11 days 13 hours 46 minutes 40 seconds", DateUtil.formatDurationAsText(1000000000));
		Assert.assertEquals("115 days 17 hours 46 minutes 40 seconds", DateUtil.formatDurationAsText(10000000000L));
	}
	
	@Test
	public void testParseAsDateOrRelativeTimeSpan() throws Exception {
		ManualTimeSource mts = new ManualTimeSource();
		mts.setCurrentTime(459489600000L);
		
		Date d = DateUtil.parseAsDateOrRelativeTimeSpan("1984-07-24", mts);
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseAsDateOrRelativeTimeSpan("1984-07-24-00:00:00", mts);
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseAsDateOrRelativeTimeSpan("1984-07-24 00:00:00", mts);
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseAsDateOrRelativeTimeSpan("1984-07-24 00:00", mts);
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseAsDateOrRelativeTimeSpan("1984-07-24-00:00", mts);
		Assert.assertEquals(459489600000L, d.getTime());
		
		d = DateUtil.parseAsDateOrRelativeTimeSpan("00:00:00", mts);
		Assert.assertEquals(DateUtil.getDay(new Date(459489600000L)).getTime(), d.getTime());
		
		d = DateUtil.parseAsDateOrRelativeTimeSpan("00:00", mts);
		Assert.assertEquals(DateUtil.getDay(new Date(459489600000L)).getTime(), d.getTime());
		
		d = DateUtil.parseAsDateOrRelativeTimeSpan("1m", mts);
		Assert.assertEquals(459489540000L, d.getTime());
		
		try {
			DateUtil.parseAsDateOrRelativeTimeSpan("junk", mts);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}