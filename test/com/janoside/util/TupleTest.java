package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class TupleTest {
	
	@Test
	public void testIt() {
		Tuple t = new Tuple(1000, "abc");
		Assert.assertEquals("1000-abc", t.toString());
		
		t = new Tuple(null, "abc");
		Assert.assertEquals("null-abc", t.toString());
		
		Assert.assertEquals(null, t.getItem1());
		Assert.assertEquals("abc", t.getItem2());
		
		t.setItem1("1");
		t.setItem2("2");
		
		Assert.assertEquals("1", t.getItem1());
		Assert.assertEquals("2", t.getItem2());
		
		Assert.assertEquals("1-2".hashCode(), t.hashCode());
		
		Assert.assertTrue(t.equals(new Tuple("1", "2")));
		Assert.assertFalse(t.equals(new Tuple("1", "3")));
		Assert.assertFalse(t.equals(null));
		Assert.assertFalse(t.equals("1-2"));
		Assert.assertFalse(t.equals(new Tuple(null, null)));
		Assert.assertFalse(t.equals(new Tuple(null, "2")));
		Assert.assertFalse(t.equals(new Tuple("1", null)));
		
		Assert.assertTrue(new Tuple(null, null).equals(new Tuple(null, null)));
		Assert.assertFalse(new Tuple(null, "b").equals(new Tuple(null, null)));
		Assert.assertFalse(new Tuple(null, null).equals(new Tuple(null, "b")));
		Assert.assertFalse(new Tuple("a", null).equals(new Tuple(null, null)));
		Assert.assertFalse(new Tuple(null, null).equals(new Tuple("a", null)));
		Assert.assertTrue(new Tuple("a", null).equals(new Tuple("a", null)));
		Assert.assertTrue(new Tuple(null, "b").equals(new Tuple(null, "b")));
		Assert.assertFalse(new Tuple("a", "b").equals(new Tuple("a", null)));
		Assert.assertFalse(new Tuple("a", "b").equals(new Tuple(null, "b")));
		Assert.assertTrue(new Tuple("a", "b").equals(new Tuple("a", "b")));
		Assert.assertFalse(new Tuple("a", null).equals(new Tuple("b", null)));
	}
	
	@Test
	public void testIt3() {
		Tuple3 t = new Tuple3(1000, "abc", "one");
		Assert.assertEquals("1000-abc-one", t.toString());
		
		t = new Tuple3(null, "abc", 5);
		Assert.assertEquals("null-abc-5", t.toString());
		
		t.setItem1("1");
		Assert.assertEquals("1", t.getItem1());
		
		t.setItem2("2");
		Assert.assertEquals("2", t.getItem2());
		
		t.setItem3("3");
		Assert.assertEquals("3", t.getItem3());
	}
	
	@Test
	public void testTuple3Equals() {
		Assert.assertTrue(new Tuple3(null, null, null).equals(new Tuple3(null, null, null)));
		Assert.assertFalse(new Tuple3(null, "b", null).equals(new Tuple3(null, null, null)));
		Assert.assertFalse(new Tuple3(null, null, null).equals(new Tuple3(null, "b", null)));
		Assert.assertFalse(new Tuple3("a", null, null).equals(new Tuple3(null, null, null)));
		Assert.assertFalse(new Tuple3(null, null, null).equals(new Tuple3("a", null, null)));
		Assert.assertTrue(new Tuple3("a", null, null).equals(new Tuple3("a", null, null)));
		Assert.assertTrue(new Tuple3(null, "b", null).equals(new Tuple3(null, "b", null)));
		Assert.assertFalse(new Tuple3("a", "b", null).equals(new Tuple3("a", null, null)));
		Assert.assertFalse(new Tuple3("a", "b", null).equals(new Tuple3(null, "b", null)));
		Assert.assertTrue(new Tuple3("a", "b", null).equals(new Tuple3("a", "b", null)));
		Assert.assertFalse(new Tuple3("a", null, null).equals(new Tuple3("b", null, null)));
		
		Assert.assertFalse(new Tuple3(null, null, "a").equals(new Tuple3(null, null, null)));
		Assert.assertFalse(new Tuple3(null, null, null).equals(new Tuple3(null, null, "b")));
		
		Assert.assertFalse(new Tuple3(null, "b", null).equals(new Tuple3(null, "c", null)));
		Assert.assertFalse(new Tuple3(null, null, "c").equals(new Tuple3(null, null, "d")));
		Assert.assertTrue(new Tuple3(null, null, "c").equals(new Tuple3(null, null, "c")));
		
		Assert.assertFalse(new Tuple3(null, null, null).equals(null));
		Assert.assertFalse(new Tuple3(null, null, null).equals(new Object()));
		
		Assert.assertEquals("null-null-null".hashCode(), new Tuple3(null, null, null).hashCode());
	}
	
	@Test
	public void testTuple4Equals() {
		Assert.assertTrue(new Tuple4(null, null, null, null).equals(new Tuple4(null, null, null, null)));
		Assert.assertFalse(new Tuple4(null, "b", null, null).equals(new Tuple4(null, null, null, null)));
		Assert.assertFalse(new Tuple4(null, null, null, null).equals(new Tuple4(null, "b", null, null)));
		Assert.assertFalse(new Tuple4("a", null, null, null).equals(new Tuple4(null, null, null, null)));
		Assert.assertFalse(new Tuple4(null, null, null, null).equals(new Tuple4("a", null, null, null)));
		Assert.assertTrue(new Tuple4("a", null, null, null).equals(new Tuple4("a", null, null, null)));
		Assert.assertTrue(new Tuple4(null, "b", null, null).equals(new Tuple4(null, "b", null, null)));
		Assert.assertFalse(new Tuple4("a", "b", null, null).equals(new Tuple4("a", null, null, null)));
		Assert.assertFalse(new Tuple4("a", "b", null, null).equals(new Tuple4(null, "b", null, null)));
		Assert.assertTrue(new Tuple4("a", "b", null, null).equals(new Tuple4("a", "b", null, null)));
		Assert.assertFalse(new Tuple4("a", null, null, null).equals(new Tuple4("b", null, null, null)));
		
		Assert.assertFalse(new Tuple4(null, null, "a", null).equals(new Tuple4(null, null, null, null)));
		Assert.assertFalse(new Tuple4(null, null, null, null).equals(new Tuple4(null, null, "b", null)));
		
		Assert.assertFalse(new Tuple4(null, "b", null, null).equals(new Tuple4(null, "c", null, null)));
		Assert.assertFalse(new Tuple4(null, null, "c", null).equals(new Tuple4(null, null, "d", null)));
		Assert.assertTrue(new Tuple4(null, null, "c", null).equals(new Tuple4(null, null, "c", null)));
		
		Assert.assertFalse(new Tuple4(null, null, null, null).equals(null));
		Assert.assertFalse(new Tuple4(null, null, null, null).equals(new Object()));
		
		Assert.assertEquals("null-null-null-null".hashCode(), new Tuple4(null, null, null, null).hashCode());
		
		Assert.assertFalse(new Tuple4(null, null, null, "a").equals(new Tuple4(null, null, null, null)));
		Assert.assertFalse(new Tuple4(null, null, null, null).equals(new Tuple4(null, null, null, "b")));
		
		Assert.assertFalse(new Tuple4(null, null, null, "a").equals(new Tuple4(null, null, null, "b")));
		Assert.assertTrue(new Tuple4(null, null, null, "a").equals(new Tuple4(null, null, null, "a")));
	}
	
	@Test
	public void testTuple4() {
		Tuple4 t = new Tuple4("a", "b", "c", "d");
		
		t.setItem1("a");
		Assert.assertEquals("a", t.getItem1());
		
		t.setItem2("b");
		Assert.assertEquals("b", t.getItem2());
		
		t.setItem3("c");
		Assert.assertEquals("c", t.getItem3());
		
		t.setItem4("d");
		Assert.assertEquals("d", t.getItem4());
	}
	
	@Test
	public void testIt4() {
		Tuple4 t = new Tuple4(1000, "abc", null, false);
		Assert.assertEquals("1000-abc-null-false", t.toString());
	}
}