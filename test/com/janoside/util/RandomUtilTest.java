package com.janoside.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class RandomUtilTest {
	
	@SuppressWarnings("serial")
	private static final HashSet<String> letters = new HashSet<String>() {{
		add("a");
		add("b");
		add("c");
		add("d");
		add("e");
		add("f");
		add("g");
		add("h");
		add("i");
		add("j");
		add("k");
		add("l");
		add("m");
		add("n");
		add("o");
		add("p");
		add("q");
		add("r");
		add("s");
		add("t");
		add("u");
		add("v");
		add("w");
		add("x");
		add("y");
		add("z");
	}};
	
	@Test
	public void testRandomInt() {
		for (int i = 0; i < 1000; i++) {
			RandomUtil.randomInt();
		}
	}
	
	@Test
	public void testDistribution() {
		int trials = 10000;
		
		int count0 = 0;
		int count1 = 0;
		for (int i = 0; i < trials; i++) {
			int rand = RandomUtil.randomInt(2);
			
			if (rand == 0) {
				count0++;
				
			} else {
				count1++;
			}
		}
		
		Assert.assertEquals(trials, (count0 + count1));
		Assert.assertTrue(count0 > (int) (trials * 0.4) && count0 < (int)(trials * 0.6));
		Assert.assertTrue(count1 > (int) (trials * 0.4) && count1 < (int)(trials * 0.6));
	}
	
	@Test
	public void testRandomIntMax() {
		for (int i = 0; i < 1000; i++) {
			int val = RandomUtil.randomInt(1000);
			
			Assert.assertTrue(1000 > val);
			Assert.assertTrue(0 <= val);
		}
	}
	
	@Test
	public void testRandomLowercaseLetter() {
		for (int i = 0; i < 1000; i++) {
			Assert.assertTrue(letters.contains(RandomUtil.randomLowercaseLetter()));
		}
	}
	
	@Test
	public void testRandomUppercaseLetter() {
		for (int i = 0; i < 1000; i++) {
			Assert.assertTrue(letters.contains(RandomUtil.randomUppercaseLetter().toLowerCase()));
		}
	}
	
	@Test
	public void testRandomByte() {
		for (int i = 0; i < 1000; i++) {
			Assert.assertTrue(RandomUtil.randomByte() < 128);
		}
	}
	
	@Test
	public void testMore() {
		Random r = new Random(5);
		
		Assert.assertEquals(-4971030886054769832L, RandomUtil.randomLong(r));
		Assert.assertEquals("0.088258386", Float.toString(RandomUtil.randomFloat(r)));
		Assert.assertEquals(0, RandomUtil.randomLong(1, r));
		Assert.assertEquals(0, RandomUtil.randomInt(1, r));
		Assert.assertEquals("def", RandomUtil.randomSelection(new String[] {"abc", "def"}, r));
		Assert.assertEquals("abc", RandomUtil.randomSelection(Arrays.asList("abc", "def"), r));
		Assert.assertEquals("def", RandomUtil.randomSelection(new HashSet() {{ add("abc"); add("def"); }}, r));
		Assert.assertEquals("[cjptlkqyo, ppqruqx, ohfydel, xyelrl, parhscolt, dmnperkp, ldtuylxv]", RandomUtil.randomWordList(r).toString());
		Assert.assertEquals("[165111088, 1462809037, -1357025198, -148272824, -1790241980]", RandomUtil.randomLongList(5, r).toString());
		Assert.assertEquals("[0216fd1a-bf2a-3ad7-9add-79a289250c48, 17009dcb-97d4-3960-b2aa-378e50e1f8b0, f2010505-130c-3800-bb61-925d5440a62c]", RandomUtil.randomUuidList(3, r).toString());
		
		RandomUtil.sharedRandom.setSeed(5);
		
		Assert.assertEquals("0.73051983", Float.toString(RandomUtil.randomFloat()));
		Assert.assertEquals("aakeds", RandomUtil.randomWordFromInt(5));
		Assert.assertEquals("nmdjrbyent", RandomUtil.randomWordFromLong(10000000000L));
		Assert.assertEquals("how when blue pizza hip hop anonymous", RandomUtil.randomSentence(5));
		Assert.assertEquals("watermelon pizza xyz zz gorilla duck", RandomUtil.randomSentence("xyz zz"));
		Assert.assertEquals("cellar door duck watermelon who pizza giraffe gorilla giraffe", RandomUtil.randomSentence());
		Assert.assertEquals("uqxuohfyde", RandomUtil.randomWord());
		Assert.assertEquals("ltxye", RandomUtil.randomWord(5));
		Assert.assertEquals("XyZ ABc", RandomUtil.randomTransform("xyz abc"));
		Assert.assertEquals("PA", RandomUtil.randomStateAbbreviation());
		Assert.assertEquals("1aaca0c6-3a0d-31ac-9714-e0e2e143d9b0", RandomUtil.randomUuid().toString());
		Assert.assertEquals("[1c8c5667-78d9-3f9f-a0c7-fe568c1ed042, d98ec32f-a9d9-37b5-ad1e-d064b97b2274]", RandomUtil.randomUuidList(2).toString());
		Assert.assertEquals("[hrgafj, sshfkavgv, iuqqlnihqi, eqvztue, ftsxoffugd, jaineez, nklvrv, yfvxt, mfen, hyxvhf, vdwa, amxxpbojgl, ayujf]", RandomUtil.randomWordList().toString());
		Assert.assertEquals("[kppyaznjjz, sbxnwwnc, nywo]", RandomUtil.randomWordList(1, 4).toString());
		Assert.assertEquals("[-31313675, -1574253632, -1810531989]", RandomUtil.randomLongList(3).toString());
		Assert.assertEquals("ghi", RandomUtil.randomSelection(new String[] {"abc", "def", "ghi"}));
		Assert.assertEquals("def", RandomUtil.randomSelection(Arrays.asList("abc", "def", "ghi")));
		Assert.assertEquals("abc", RandomUtil.randomSelection(new HashSet(Arrays.asList("abc", "def", "ghi"))));
		Assert.assertEquals(3014570376490743217L, RandomUtil.randomLong());
		Assert.assertEquals("[-12, 15, -101, -119]", Arrays.toString(RandomUtil.randomByteArray(4)));
		Assert.assertEquals("GHi DEF AbC", RandomUtil.randomTransform("Abc Def GHI"));
		
		for (int i = 0; i < 100; i++) {
			long l = RandomUtil.randomLong(10);
			
			Assert.assertTrue(l <= 10 && l >= 0);
		}
		
		for (int i = 0; i < 100; i++) {
			float f = RandomUtil.randomFloat(1, 4);
			
			Assert.assertTrue(1 <= f && f <= 4);
		}
		
		Assert.assertTrue(RandomUtil.randomBoolean(1));
		Assert.assertTrue(RandomUtil.randomBoolean(1, new Random(5)));
		Assert.assertTrue(RandomUtil.randomBoolean(10));
		Assert.assertTrue(RandomUtil.randomBoolean(10, new Random(5)));
		Assert.assertFalse(RandomUtil.randomBoolean(0));
		Assert.assertFalse(RandomUtil.randomBoolean(0, new Random(5)));
		Assert.assertFalse(RandomUtil.randomBoolean(-10));
		Assert.assertFalse(RandomUtil.randomBoolean(-10, new Random(5)));
		
		int count = 0;
		for (int i = 0; i < 100000; i++) {
			if (RandomUtil.randomBoolean(0.5)) {
				count++;
			}
		}
		
		Assert.assertTrue(count >= 48000 && count <= 52000);
	}
	
	@Test
	public void testRandomStateAbbreviations() {
		Random r = new Random(5);
		
		ArrayList list = new ArrayList();
		for (int i = 0; i < 100; i++) {
			String s = RandomUtil.randomStateAbbreviation(r);
			
			Assert.assertEquals(2, s.length());
			
			list.add(s);
		}
		
		Assert.assertEquals(1965347293, list.toString().hashCode());
	}
	
	@Test
	public void testRandomUppercaseLetter2() {
		Random r = new Random(5);
		
		ArrayList list = new ArrayList();
		for (int i = 0; i < 100; i++) {
			String s = RandomUtil.randomUppercaseLetter(r);
			
			Assert.assertEquals(1, s.length());
			
			list.add(s);
		}
		
		Assert.assertEquals(-708791795, list.toString().hashCode());
	}
	
	@Test
	public void testRandomlyTransform() {
		Random r = new Random(5);
		
		String base = "abc def ghi jkl mno pqr stu vwx yz";
		
		ArrayList list = new ArrayList();
		for (int i = 0; i < 3; i++) {
			String s = RandomUtil.randomTransform(base, r);
			
			Assert.assertEquals(base.length(), s.length());
			
			list.add(s);
		}
		
		Assert.assertEquals("[yz deF PQr jkL GHi vwx MNo stu aBC, abc pqR yz Jkl MnO vwx sTU DeF GHI, jkl vWX ABc def MNO Yz pQr sTu Ghi]", list.toString());
	}
	
	@Test
	public void testRandomSentence() {
		Random r = new Random(5);
		
		ArrayList list = new ArrayList();
		for (int i = 0; i < 3; i++) {
			String s = RandomUtil.randomSentence(r);
			
			list.add(s);
		}
		
		Assert.assertEquals("[how when blue pizza hip hop anonymous pizza watermelon, cheese gorilla duck orange cellar door duck watermelon who pizza giraffe, giraffe cellar door who cellar door giraffe then pizza]", list.toString());
		
		
		list = new ArrayList();
		for (int i = 0; i < 3; i++) {
			String s = RandomUtil.randomSentence(4, r);
			
			list.add(s);
		}
		
		Assert.assertEquals("[cellar door cheese fountain hip hop anonymous, fountain giraffe who orange, fountain who then blue]", list.toString());
		
		
		list = new ArrayList();
		for (int i = 0; i < 3; i++) {
			String s = RandomUtil.randomSentence("funny cheese", r);
			
			Assert.assertTrue(s.contains("funny cheese"));
			
			list.add(s);
		}
		
		Assert.assertEquals("[sky pizza funny cheese sky, cellar door funny cheese watermelon pizza pizza, gorilla orange funny cheese how when]", list.toString());
	}
}