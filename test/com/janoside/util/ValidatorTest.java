package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class ValidatorTest {
	
	@Test
	public void testIt() {
		Validator v = new Validator();
		
		Assert.assertTrue(v.isValidZipcode("12345"));
		Assert.assertTrue(v.isValidZipcode("12345-1234"));
		
		Assert.assertFalse(v.isValidZipcode("abcde"));
		Assert.assertFalse(v.isValidZipcode("abcde-abcd"));
		
		Assert.assertFalse(v.isValidZipcode("123456"));
		Assert.assertFalse(v.isValidZipcode("123456789"));
	}
}