package com.janoside.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.text.TextTransformer;

public class StringUtilTest {
	
	@Test
	public void testPadBeginning1() {
		Assert.assertEquals("01", StringUtil.padBeginning("1", '0', 2));
	}
	
	@Test
	public void testPadBeginning2() {
		Assert.assertEquals("02", StringUtil.padBeginning("2", '0', 2));
	}
	
	@Test
	public void testPadBeginning3() {
		Assert.assertEquals("03", StringUtil.padBeginning("3", '0', 2));
	}
	
	@Test
	public void testPadBeginning4() {
		Assert.assertEquals("04", StringUtil.padBeginning("4", '0', 2));
	}
	
	@Test
	public void testPadEnd1() {
		Assert.assertEquals("10", StringUtil.padEnd("1", '0', 2));
	}
	
	@Test
	public void testPadEnd2() {
		Assert.assertEquals("20", StringUtil.padEnd("2", '0', 2));
	}
	
	@Test
	public void testPadEnd3() {
		Assert.assertEquals("30", StringUtil.padEnd("3", '0', 2));
	}
	
	@Test
	public void testPadEnd4() {
		Assert.assertEquals("40", StringUtil.padEnd("4", '0', 2));
	}
	
	@Test
	public void testUnrelated() {
		Assert.assertTrue("08b2e7c2-58c0-433a-a83e-c527a9fda2c5".matches("\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}"));
		Assert.assertTrue("2011-06-27 11:50:46".matches("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}"));
		Assert.assertTrue("janoside.2@gmail.com".matches("^[\\w\\.-]+@[\\w\\-]+\\.+[A-Za-z]{2,4}$"));
	}
	
	@Test
	public void testCurrency() {
		Assert.assertEquals("0.00", StringUtil.formatAsCurrency("0"));
		Assert.assertEquals("10.00", StringUtil.formatAsCurrency("10"));
		Assert.assertEquals("0.00", StringUtil.formatAsCurrency("0."));
		Assert.assertEquals("0.00", StringUtil.formatAsCurrency("0.0"));
		Assert.assertEquals("0.00", StringUtil.formatAsCurrency("0.000"));
		Assert.assertEquals("1000.00", StringUtil.formatAsCurrency("1000.000"));
		Assert.assertEquals("1000.00", StringUtil.formatAsCurrency("1000.00"));
	}
	
	@Test
	public void testDecimal() {
		Assert.assertEquals("1,000.00", StringUtil.formatDecimal("1000.00"));
		Assert.assertEquals("1,000", StringUtil.formatDecimal("1000"));
		Assert.assertEquals("100.00", StringUtil.formatDecimal("100.00"));
		Assert.assertEquals("100,000.00", StringUtil.formatDecimal("100000.00"));
	}
	
	@Test
	public void testFormatDecimalLong() {
		Assert.assertEquals("1,000", StringUtil.formatDecimal(1000L));
		Assert.assertEquals("1,000,000", StringUtil.formatDecimal(1000000L));
		Assert.assertEquals("10,000", StringUtil.formatDecimal(10000L));
	}
	
	@Test
	public void testFormatOpenCloseTags() {
		Assert.assertEquals("<a>a</a>", StringUtil.formatOpenCloseTags("'a'", "'", "'", "<a>", "</a>"));
	}
	
	@Test
	public void testCamelCaseToLowercaseDashSeparated() {
		Assert.assertEquals("game-server", StringUtil.camelCaseToLowercaseDashSeparated("GameServer"));
		Assert.assertEquals("game-server", StringUtil.camelCaseToLowercaseDashSeparated("gameServer"));
		Assert.assertEquals("room-server", StringUtil.camelCaseToLowercaseDashSeparated("RoomServer"));
		Assert.assertEquals("room-server", StringUtil.camelCaseToLowercaseDashSeparated("roomServer"));
		Assert.assertEquals("a-x-files-movie", StringUtil.camelCaseToLowercaseDashSeparated("AXFilesMovie"));
	}
	
	@Test
	public void testRemoveDiacritics() {
		Assert.assertEquals("cheeeese", StringUtil.removeDiacritics("chèéêëse"));
		Assert.assertEquals("cheese", StringUtil.removeDiacritics("chēėse"));
		Assert.assertEquals("R", StringUtil.removeDiacritics("Ȑ"));
	}
	
	@Test
	public void testJsonDirectoryTree() {
		List<String> paths = Arrays.asList("one.two.three", "one.three.four", "two.two.three", "two.two.four", "one", "x.y");
		
		System.out.println(StringUtil.buildJsonTreeFromPaths(paths, ".").toTabbedString());
	}
	
	@Test
	public void testMemoryDisplay() {
		Assert.assertEquals("1.0 kB", StringUtil.bytesToMemoryString(1001, true));
		Assert.assertEquals("1.02 kB", StringUtil.bytesToMemoryString(1025, true));
		Assert.assertEquals("1.12 kB", StringUtil.bytesToMemoryString(1125, true));
		Assert.assertEquals("1.0 MB", StringUtil.bytesToMemoryString(1000 * 1000, true));
		Assert.assertEquals("1.05 MB", StringUtil.bytesToMemoryString(1024 * 1024, true));
		Assert.assertEquals("518.98 MB", StringUtil.bytesToMemoryString(518979584, true));
		Assert.assertEquals("1.07 GB", StringUtil.bytesToMemoryString(1024 * 1024 * 1024, true));
		Assert.assertEquals("5.15 GB", StringUtil.bytesToMemoryString(5148979584L, true));
		Assert.assertEquals("1.1 TB", StringUtil.bytesToMemoryString(1024L * 1024L * 1024L * 1024L, true));
		Assert.assertEquals("2.51 PB", StringUtil.bytesToMemoryString((long)(1000000000000000L * 2.51), true));
		Assert.assertEquals("9.22 EB", StringUtil.bytesToMemoryString(Long.MAX_VALUE, true));
		
		Assert.assertEquals("1001 B", StringUtil.bytesToMemoryString(1001, false));
		Assert.assertEquals("1.0 KiB", StringUtil.bytesToMemoryString(1025, false));
		Assert.assertEquals("1.1 KiB", StringUtil.bytesToMemoryString(1125, false));
		Assert.assertEquals("976.56 KiB", StringUtil.bytesToMemoryString(1000 * 1000, false));
		Assert.assertEquals("1.0 MiB", StringUtil.bytesToMemoryString(1024 * 1024, false));
		Assert.assertEquals("494.94 MiB", StringUtil.bytesToMemoryString(518979584, false));
		Assert.assertEquals("1.0 GiB", StringUtil.bytesToMemoryString(1024 * 1024 * 1024, false));
		Assert.assertEquals("4.8 GiB", StringUtil.bytesToMemoryString(5148979584L, false));
		Assert.assertEquals("1.0 TiB", StringUtil.bytesToMemoryString(1024L * 1024L * 1024L * 1024L, false));
		Assert.assertEquals("2.23 PiB", StringUtil.bytesToMemoryString((long)(1000000000000000L * 2.51), false));
		Assert.assertEquals("8.0 EiB", StringUtil.bytesToMemoryString(Long.MAX_VALUE, false));
	}
	
	@Test
	public void testTransformEachInCollection() {
		List strs = Arrays.asList("abc", "def", "ghi");
		
		List<String> result = StringUtil.transformEachInCollection(strs, new TextTransformer() {
			public String transform(String value) {
				return "123" + value;
			}
		});
		
		Assert.assertEquals("[123abc, 123def, 123ghi]", result.toString());
	}
	
	@Test
	public void testPadInt() {
		Assert.assertEquals("3", StringUtil.padIntWithZeroes(3, 1));
		Assert.assertEquals("0003", StringUtil.padIntWithZeroes(3, 4));
	}
	
	@Test
	public void testUtf8() {
		String s = "abc";
		
		byte[] b = StringUtil.utf8BytesFromString(s);
		Assert.assertEquals(s, StringUtil.utf8StringFromBytes(b));
	}
	
	@Test
	public void testMerge() {
		Assert.assertEquals("abc,def,ghi", StringUtil.mergeCommaSeparatedStrings("abc,ghi", "def,ghi"));
		Assert.assertEquals("abc,def,ghi", StringUtil.mergeCommaSeparatedStrings("abc,def,ghi", ""));
		Assert.assertEquals("abc,def,ghi", StringUtil.mergeCommaSeparatedStrings("", "abc,def,ghi"));
		Assert.assertEquals("abc,def,ghi", StringUtil.mergeCommaSeparatedStrings(", ,,", "abc,def,ghi"));
		Assert.assertEquals("abc,def,ghi", StringUtil.mergeCommaSeparatedStrings("abc,def,ghi", ", ,,"));
	}
	
	@Test
	public void testCapitalize() {
		Assert.assertEquals("Abc Def", StringUtil.capitalizeAllWords("abc def"));
	}
	
	@Test
	public void testFormatDecimal() {
		Assert.assertEquals("987", StringUtil.formatDecimal(987));
		Assert.assertEquals("987,654", StringUtil.formatDecimal(987654));
	}
	
	@Test
	public void testSplitAndParse() throws Exception {
		Assert.assertEquals("[1, 2, 3]", StringUtil.splitAndParseInt("1,2,3", ",").toString());
		
		try {
			// overflow int
			Assert.assertEquals("[100000000000, 2, 3]", StringUtil.splitAndParseInt("100000000000,2,3", ",").toString());
			
			throw new Exception();
			
		} catch (NumberFormatException nfe) {
			// expected
		}
		
		Assert.assertEquals("[100000000000, 2, 3]", StringUtil.splitAndParseLong("100000000000,2,3", ",").toString());
		
		try {
			// overflow long
			Assert.assertEquals("[92233720368547758070, 2, 3]", StringUtil.splitAndParseLong("92233720368547758070,2,3", ",").toString());
			
			throw new Exception();
			
		} catch (NumberFormatException nfe) {
			// expected
		}
	}
	
	@Test
	public void testIsInteger() {
		Assert.assertTrue(StringUtil.isInteger("123456789000000"));
		Assert.assertFalse(StringUtil.isInteger("12345678900000XXX"));
	}
	
	@Test
	public void testArrayToDelimitedString() {
		Assert.assertEquals("", StringUtil.arrayToDelimitedString(new Object[] {}, ","));
		Assert.assertEquals("abc,def", StringUtil.arrayToDelimitedString(new Object[] {"abc", "def"}, ","));
		Assert.assertEquals("abc", StringUtil.arrayToDelimitedString(new Object[] {"abc"}, ","));
		Assert.assertEquals("null", StringUtil.arrayToDelimitedString(new Object[] {null}, ","));
	}
	
	@Test
	public void testRangeForValue() {
		Assert.assertEquals("10-20", StringUtil.rangeForValue(15, 10));
		Assert.assertEquals("0-1000", StringUtil.rangeForValue(500, 1000));
	}
	
	@Test
	public void testReplaceAllSafely() {
		Assert.assertEquals("abc123def", StringUtil.replaceAllSafely("abcxyzdef", "xyz", "123"));
		Assert.assertEquals("abc\\$$def", StringUtil.replaceAllSafely("abc[]'.def", "[]'.", "\\$$"));
	}
	
	@Test
	public void testSplit() {
		Assert.assertEquals(1, StringUtil.split("  ", ",", true, true).size());
		Assert.assertEquals("[  ]", StringUtil.split("  ", ",", false, true).toString());
		Assert.assertEquals(0, StringUtil.split("  ", ",", true, false).size());
		
		Assert.assertEquals("[abc, def, ghi]", StringUtil.split("abc,def,ghi", ",", true, true).toString());
		Assert.assertEquals("[abc, def, ghi]", StringUtil.split("abc,def,ghi ", ",", true, true).toString());
		Assert.assertEquals("[abc, def, ghi ]", StringUtil.split("abc,def,ghi ", ",", false, true).toString());
		Assert.assertEquals("[abc, def]", StringUtil.split("abc,def,   ", ",", true, false).toString());
	}
	
	@Test
	public void testHasLength() {
		Assert.assertTrue(StringUtil.hasLength(" "));
		Assert.assertFalse(StringUtil.hasLength(""));
		Assert.assertFalse(StringUtil.hasLength(null));
	}
	
	@Test
	public void testCapitalize2() {
		Assert.assertEquals("Abcdef", StringUtil.capitalize("abcdef"));
		Assert.assertEquals(null, StringUtil.capitalize(null));
		Assert.assertEquals("", StringUtil.capitalize(""));
		Assert.assertEquals(" ", StringUtil.capitalize(" "));
	}
	
	@Test
	public void testCountOccurrencesOf() {
		Assert.assertEquals(0, StringUtil.countOccurrencesOf(null, "abc"));
		Assert.assertEquals(0, StringUtil.countOccurrencesOf("abc", null));
		
		Assert.assertEquals(0, StringUtil.countOccurrencesOf("", "abc"));
		Assert.assertEquals(0, StringUtil.countOccurrencesOf("abc", ""));
	}
	
	@Test
	public void testCollectionToDelimited() {
		Assert.assertEquals("", StringUtil.collectionToDelimitedString(new ArrayList(), ",", "", ""));
	}
	
	@Test
	public void testConvertIso8859() {
		Assert.assertEquals("abc", StringUtil.convertIso8859ToUtf8("abc"));
	}
	
	@Test
	public void testNonsense() throws Exception {
		try {
			StringUtil.stringFromBytes("abc".getBytes(), "Nonsense");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			StringUtil.bytesFromString("abc", "Nonsense");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}