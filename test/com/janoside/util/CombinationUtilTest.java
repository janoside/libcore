package com.janoside.util;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class CombinationUtilTest {
	
	@Test
	public void testIt() {
		List<List<Integer>> result = CombinationUtil.allCombinations(Arrays.asList(1, 2, 3, 4, 5));
		
		Assert.assertEquals(-1552825802, result.toString().hashCode());
	}
}