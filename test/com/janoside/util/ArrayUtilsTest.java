package com.janoside.util;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.transform.ObjectTransformer;

public class ArrayUtilsTest {
	
	@Test
	public void testIt() {
		Integer[] array = new Integer[] {1, 2, 3};
		Assert.assertEquals("[1, 2, 3]", ArrayUtils.toList(array).toString());
		
		
		Assert.assertEquals("[abc, def]", ArrayUtils.toString(new String[] {"abc", "def"}));
	}
	
	@Test
	public void testToListAfterTransform() {
		List<String> list = ArrayUtils.toListAfterTransform(new String[] {"abc", "def"}, new ObjectTransformer<String, String>() {
			public String transform(String value) {
				return "123" + value;
			}
		});
		
		Assert.assertEquals("[123abc, 123def]", list.toString());
	}
	
	@Test
	public void testIsEmpty() {
		Assert.assertTrue(ArrayUtils.isEmpty(new Object[] {}));
		Assert.assertTrue(ArrayUtils.isEmpty(null));
	}
}