package com.janoside.util;

import java.net.MalformedURLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("serial")
public class UrlUtilTest {
	
	@Test
	public void testMakeAbsolute() {
		String fullyAbsoluteUrl = "http://www.monkey.com/images/cheese.gif";
		String absoluteUrl = "/images/cheese.gif";
		String relativeUrl = "images/cheese.gif";
		String alternateUrl = "http://www.monkey.com/homepage/images/cheese.gif";
		
		String absoluteReference = "http://www.monkey.com/homepage";
		String trickyReference = "http://www.monkey.com/homepage/level2";
		
		Assert.assertEquals(fullyAbsoluteUrl, UrlUtil.makeAbsoluteUrl(fullyAbsoluteUrl, absoluteReference));
		
		Assert.assertEquals(fullyAbsoluteUrl, UrlUtil.makeAbsoluteUrl(absoluteUrl, absoluteReference));
		
		Assert.assertEquals(fullyAbsoluteUrl, UrlUtil.makeAbsoluteUrl(absoluteUrl, trickyReference));
		
		Assert.assertEquals(fullyAbsoluteUrl, UrlUtil.makeAbsoluteUrl(relativeUrl, absoluteReference));
		
		Assert.assertEquals(alternateUrl, UrlUtil.makeAbsoluteUrl(relativeUrl, trickyReference));
	}
	
	@Test
	public void testSingleBasic() throws MalformedURLException {
		Assert.assertEquals("y", UrlUtil.getParameter("http://monkey.com?x=y", "x"));
	}
	
	@Test
	public void testSingleMultiple() throws MalformedURLException {
		Assert.assertEquals("y", UrlUtil.getParameter("http://monkey.com?a=b&c=d&x=y", "x"));
	}
	
	@Test
	public void testSingleMiddle() throws MalformedURLException {
		Assert.assertEquals("y", UrlUtil.getParameter("http://monkey.com?a=b&c=d&x=y&z=f", "x"));
	}
	
	@Test
	public void testSingleUnfound() throws MalformedURLException {
		Assert.assertNull(UrlUtil.getParameter("http://monkey.com?a=b&c=d", "x"));
	}
	
	@Test
	public void testSingleBlank() throws MalformedURLException {
		Assert.assertNull(UrlUtil.getParameter("http://monkey.com?a=b&x=&c=d", "x"));
	}
	
	@Test
	public void testSingleDecode() throws MalformedURLException {
		Assert.assertEquals("y z", UrlUtil.getParameter("http://monkey.com?a=b&c=d&x=y+z&z=f", "x"));
	}
	
	@Test
	public void testMultipleBasic() throws MalformedURLException {
		Map<String, String> parameters = UrlUtil.getParameters("http://monkey.com?x=y");
		
		Assert.assertTrue(parameters.containsKey("x"));
		Assert.assertEquals("y", parameters.get("x"));
	}
	
	@Test
	public void testMultipleMultiple() throws MalformedURLException {
		Map<String, String> parameters = UrlUtil.getParameters("http://monkey.com?a=b&c=d&x=y");
		
		Assert.assertTrue(parameters.containsKey("x"));
		Assert.assertEquals("y", parameters.get("x"));
		
		Assert.assertTrue(parameters.containsKey("a"));
		Assert.assertEquals("b", parameters.get("a"));
		
		Assert.assertTrue(parameters.containsKey("c"));
		Assert.assertEquals("d", parameters.get("c"));
	}
	
	@Test
	public void testMultipleBlank() throws MalformedURLException {
		Map<String, String> parameters = UrlUtil.getParameters("http://monkey.com?a=b&x=&c=d");
		
		Assert.assertTrue(parameters.containsKey("a"));
		Assert.assertEquals("b", parameters.get("a"));
		
		Assert.assertFalse(parameters.containsKey("x"));
		
		Assert.assertTrue(parameters.containsKey("c"));
		Assert.assertEquals("d", parameters.get("c"));
	}
	
	@Test
	public void testMultipleDecode() throws MalformedURLException {
		Map<String, String> parameters = UrlUtil.getParameters("http://monkey.com?a=b&c=d&x=y+z&z=f");
		
		Assert.assertTrue(parameters.containsKey("a"));
		Assert.assertEquals("b", parameters.get("a"));
		
		Assert.assertTrue(parameters.containsKey("c"));
		Assert.assertEquals("d", parameters.get("c"));
		
		Assert.assertTrue(parameters.containsKey("x"));
		Assert.assertEquals("y z", parameters.get("x"));
		
		Assert.assertTrue(parameters.containsKey("z"));
		Assert.assertEquals("f", parameters.get("z"));
	}
	
	@Test
	public void testSingleDomain() throws MalformedURLException {
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost/"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost?abc=def"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost/path?abc=def&ghi=jkl"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost/path1/path2"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("localhost/path1/path2"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("https://localhost/path1/path2"));
		
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost:8080/"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost:8080"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost:8080?abc=def"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost:8080/path?abc=def&ghi=jkl"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("http://localhost:8080/path1/path2"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("localhost:8080/path1/path2"));
		Assert.assertEquals("localhost", UrlUtil.getDomain("https://localhost:8080/path1/path2"));
	}
	
	@Test
	public void testDoubleDomain() throws MalformedURLException {
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com/"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com?abc=def"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com/path?abc=def&ghi=jkl"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com/path1/path2"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("localhost.monkey.com/path1/path2"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("https://localhost.monkey.com/path1/path2"));
		
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com:8080/"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com:8080"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com:8080?abc=def"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com:8080/path?abc=def&ghi=jkl"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://localhost.monkey.com:8080/path1/path2"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("localhost.monkey.com:8080/path1/path2"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("https://localhost.monkey.com:8080/path1/path2"));
	}
	
	@Test
	public void testTripleDomain() throws MalformedURLException {
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com/"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com?abc=def"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com/path?abc=def&ghi=jkl"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com/path1/path2"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("abc.localhost.monkey.com/path1/path2"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("https://abc.localhost.monkey.com/path1/path2"));
		
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com:8080/"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com:8080"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com:8080?abc=def"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com:8080/path?abc=def&ghi=jkl"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("http://abc.localhost.monkey.com:8080/path1/path2"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("abc.localhost.monkey.com:8080/path1/path2"));
		Assert.assertEquals("monkey.com", UrlUtil.getDomain("https://abc.localhost.monkey.com:8080/path1/path2"));
	}
	
	@Test
	public void testFullDomain() throws MalformedURLException {
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com/"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com?abc=def"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com/path?abc=def&ghi=jkl"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com/path1/path2"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("abc.localhost.monkey.com/path1/path2"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("https://abc.localhost.monkey.com/path1/path2"));
		
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com:8080/"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com:8080"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com:8080?abc=def"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com:8080/path?abc=def&ghi=jkl"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("http://abc.localhost.monkey.com:8080/path1/path2"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("abc.localhost.monkey.com:8080/path1/path2"));
		Assert.assertEquals("abc.localhost.monkey.com", UrlUtil.getFullDomain("https://abc.localhost.monkey.com:8080/path1/path2"));
	}
	
	@Test
	public void testPort() throws MalformedURLException {
		Assert.assertEquals(":8080", UrlUtil.getPortString("http://abc.localhost.monkey.com:8080/"));
		Assert.assertEquals("", UrlUtil.getPortString("http://abc.localhost.monkey.com/"));
		Assert.assertEquals("", UrlUtil.getPortString("abc.localhost.monkey.com/"));
	}
	
	@Test
	public void testNullQuery() throws MalformedURLException {
		UrlUtil.getParameters("http://monkey.com");
	}
	
	@Test
	public void testNullQuery2() throws MalformedURLException {
		UrlUtil.getParameter("http://monkey.com", "q");
	}
	
	@Test
	public void testBuildUrl() {
		String url = UrlUtil.buildUrl("http", "www.monkey.com", 80, "", new HashMap<String, String>() {{
			put("p1", "6");
			put("p2", "7");
		}});
		
		String expectedStart = "http://www.monkey.com?"; 
		Assert.assertEquals(expectedStart, url.substring(0, expectedStart.length()));
		Assert.assertTrue(url.contains("p1=6"));
		Assert.assertTrue(url.contains("p2=7"));
	}
	
	@Test
	public void testSetParameter() throws MalformedURLException {
		String url = "http://www.monkey.com/page?monkey=cheese";
		
		String newUrl = UrlUtil.setParameter(url, "monkey", "granola-bar");
		
		Assert.assertEquals("http://www.monkey.com/page?monkey=granola-bar", newUrl);
		Assert.assertFalse(newUrl.contains("monkey=cheese"));
		Assert.assertTrue(newUrl.contains("monkey=granola-bar"));
	}
	
	@Test
	public void testBases() {
		System.out.println("time=" + Long.toString(Math.abs(DateUtil.format("yyyy-MM-dd HH:mm:ss", new Date()).hashCode()), 36));
	}
	
	@Test
	public void testMakeAbsolute2() {
		Assert.assertEquals("http://cheese.com/abc", UrlUtil.makeAbsoluteUrl("abc", "http://cheese.com"));
	}
	
	@Test
	public void testPercentEncode() {
		Assert.assertEquals("abc%2Bdef", UrlUtil.percentEncode("abc+def"));
	}
	
	@Test
	public void testBuildBaseUrl() {
		Assert.assertEquals("https://google.com", UrlUtil.buildBaseUrl("google.com", 443));
		Assert.assertEquals("http://google.com", UrlUtil.buildBaseUrl("google.com", 80));
		Assert.assertEquals("http://google.com:123", UrlUtil.buildBaseUrl("google.com", 123));
	}
	
	@Test
	public void testGetScheme() throws Exception {
		Assert.assertEquals("http", UrlUtil.getScheme("http://google.com"));
		Assert.assertEquals("https", UrlUtil.getScheme("https://google.com"));
		Assert.assertEquals("http", UrlUtil.getScheme("google.com"));
	}
	
	@Test
	public void testExtractParametersAsList() {
		List list = UrlUtil.extractParametersAsList("abc=def&a1=b2");
		Assert.assertEquals("[abc-def, a1-b2]", list.toString());
		
		list = UrlUtil.extractParametersAsList("?abc=def&a1=b2");
		Assert.assertEquals("[abc-def, a1-b2]", list.toString());
	}
	
	@Test
	public void testGetDomainNull() {
		Assert.assertEquals("", UrlUtil.getDomain(""));
	}
}