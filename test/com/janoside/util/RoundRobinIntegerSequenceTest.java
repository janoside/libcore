package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class RoundRobinIntegerSequenceTest {
	
	@Test
	public void testIt() {
		RoundRobinIntegerSequence sequence = new RoundRobinIntegerSequence();
		sequence.setItemCount(5);
		
		Assert.assertEquals(0, sequence.getItem(0).intValue());
		Assert.assertEquals(1, sequence.getItem(1).intValue());
		Assert.assertEquals(2, sequence.getItem(2).intValue());
		Assert.assertEquals(3, sequence.getItem(3).intValue());
		Assert.assertEquals(4, sequence.getItem(4).intValue());
		Assert.assertEquals(0, sequence.getItem(5).intValue());
	}
}