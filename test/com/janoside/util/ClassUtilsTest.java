package com.janoside.util;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.app.Version;

public class ClassUtilsTest {
	
	@Test
	public void testIt() throws Exception {
		Class[] classes = ClassUtils.getClasses("com.janoside.app");
		HashSet set = new HashSet(Arrays.asList(classes));
		
		Assert.assertTrue(set.contains(Version.class));
		
		try {
			ClassUtils.getClasses(null);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testNewObject() throws Exception {
		Object v = ClassUtils.objectFromClassName("com.janoside.app.Version", "1.2.3");
		
		Assert.assertEquals("1.2.3", v.toString());
		
		
		try {
			ClassUtils.objectFromClassName("xyz", "1.2.3");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}