package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class HashUtilTest {
	
	@Test
	public void testDigest() throws Exception {
		try {
			HashUtil.getMessageDigest("Dumb");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		Assert.assertNotNull(HashUtil.getMessageDigest("MD5"));
	}
}