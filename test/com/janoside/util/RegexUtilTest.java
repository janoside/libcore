package com.janoside.util;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.junit.Test;

public class RegexUtilTest {
	
	@Test
	public void testPatternEscape() {
		String original = "()||\\";
		String escaped = RegexUtil.patternEscape(original);
		Assert.assertEquals("String does not match!", "\\Q()||\\\\E", escaped);
	}
	
	@Test
	public void testSomethihgg() {
		Pattern p = Pattern.compile("", Pattern.CASE_INSENSITIVE);
		Assert.assertFalse(p.matcher("cheese").matches());
		
		Pattern p2 = Pattern.compile(".*rating.*", Pattern.CASE_INSENSITIVE);
		Assert.assertTrue(p2.matcher("rating-transaction").matches());
	}
	
	@Test
	public void testReplacementEscape() {
		String original = "$1$2";
		String escaped = RegexUtil.replacementEscape(original);
		Assert.assertEquals("String does not match!", "\\$1\\$2", escaped);
	}
	
	@Test
	public void testSomeRegexReplacement() {
		Assert.assertEquals(" monkey", "<p> </p> monkey".replaceAll("<p>\\s+</p>", ""));
		
		System.out.println("ab-c".replaceAll("[^a-zA-Z0-9]", ""));
		System.out.println("ab-c".replaceAll("[^a-zA-Z0-9]]", ""));
	}
	
	@Test
	public void testCrap() {
		Pattern p = Pattern.compile("^((s1|main|www)\\.)?(autobook\\.me|ab-cdn\\.com|autobooknow\\.(me|net|co|org|info))(\\:8080)?$", Pattern.CASE_INSENSITIVE);
		
		Assert.assertTrue(p.matcher("autobook.me").matches());
		Assert.assertTrue(p.matcher("www.autobook.me").matches());
		Assert.assertTrue(p.matcher("www.autobooknow.me").matches());
		Assert.assertTrue(p.matcher("www.autobooknow.info").matches());
		
		Assert.assertTrue(p.matcher("autobook.me:8080").matches());
		Assert.assertTrue(p.matcher("www.autobook.me:8080").matches());
		Assert.assertTrue(p.matcher("www.autobooknow.me:8080").matches());
		Assert.assertTrue(p.matcher("www.autobooknow.info:8080").matches());
		
		Assert.assertFalse(p.matcher("www.autobook.nets").matches());
		Assert.assertFalse(p.matcher("www.autobook.com").matches());
		
		Assert.assertFalse(p.matcher("www.autobooknow.com").matches());
	}
	
	@Test
	public void testMoreCrap() {
		Pattern p = Pattern.compile("^m.*$", Pattern.CASE_INSENSITIVE);
		
		Assert.assertFalse(p.matcher("abc").matches());
		Assert.assertTrue(p.matcher("mabc").matches());
		Assert.assertFalse(p.matcher("Abc").matches());
		Assert.assertTrue(p.matcher("Mabc").matches());
		
		p = Pattern.compile("^([^m]).*$", Pattern.CASE_INSENSITIVE);
		
		Assert.assertTrue(p.matcher("abc").matches());
		Assert.assertFalse(p.matcher("mabc").matches());
		Assert.assertTrue(p.matcher("Abc").matches());
		Assert.assertFalse(p.matcher("Mabc").matches());
		Assert.assertTrue(p.matcher("ambc").matches());
	}
	
	@Test
	public void testWiki() {
		Pattern linkPattern = Pattern.compile("(\\[([^\\]]+)\\|([^\\]]*)\\])", Pattern.CASE_INSENSITIVE);
		
		String test = "ouhadfuhaf [abc|http://google.com] haha [one two three|http://one.com] cheese";
		
		Matcher m = linkPattern.matcher(test);
		while (m.find()) {
			String group = m.group(1);
			String text = m.group(2);
			String url = m.group(3);
			
			System.out.println(group + " " + text + " " + url);
			
			
			test = test.replaceAll(Pattern.quote(group), "<a href=\"" + url + "\" title=\"" + text + "\">" + text + "</a>");
		}
		
		System.out.println(test);
	}
	
	@Test
	public void testWordBoundary() {
		Pattern p = Pattern.compile(".*\\bse\\b.*", Pattern.CASE_INSENSITIVE);
		
		Assert.assertFalse(p.matcher("lacrosse").matches());
		Assert.assertTrue(p.matcher("ab se").matches());
		Assert.assertTrue(p.matcher("se abc").matches());
		
		System.out.println("lacrosse se cheese".replaceAll("(\\s|^)se(\\s|$)", " "));
	}
	
	@Test
	public void testWordBoundary2() {
		Pattern p = Pattern.compile(".*(\\s|^)se(\\s|$).*", Pattern.CASE_INSENSITIVE);
		
		Assert.assertFalse(p.matcher("lacrosse abc-se").matches());
		Assert.assertTrue(p.matcher("lacrosse abc-se se").matches());
		Assert.assertTrue(p.matcher("se lacrosse abc-se").matches());
	}
	
	@Test
	public void testUrlSomething() {
		Pattern p = Pattern.compile("^/find\\?query=(.*)$", Pattern.CASE_INSENSITIVE);
		
		Assert.assertTrue(p.matcher("/find?query=honda+pilot").matches());
	}
	
	@Test
	public void testUrlSomething2345() {
		Pattern p = Pattern.compile("regex|regex not", Pattern.CASE_INSENSITIVE);
		
		System.out.println(p.matcher("regex not").replaceFirst("cheese"));
	}
	
	@Test
	public void testUrlSometasdf45() {
		Pattern p = Pattern.compile("", Pattern.CASE_INSENSITIVE);
		
		Assert.assertFalse(p.matcher("regex not").matches());
		Assert.assertFalse(p.matcher("abc").matches());
		
		p = Pattern.compile("^(.*):(Page:[0-9]+|Manifest)$", Pattern.CASE_INSENSITIVE);
		
		Assert.assertFalse(p.matcher("regex not").matches());
		Assert.assertFalse(p.matcher("abc").matches());
		Assert.assertTrue(p.matcher("abc:Manifest").matches());
		Assert.assertTrue(p.matcher("abc:Page:1").matches());
		Assert.assertTrue(p.matcher("abc:Page:5000").matches());
	}
	
	/*
	public void testOnline() {
		String text = "test a=\"1\" b=\"2\" c=\"3\" bar d=\"4\" e=\"5\"";
		System.out.println("\n\n" + text + "\n");
		
		Matcher m1 = Pattern.compile("([a-z]*)((?:[ \t]+[a-z]=\"[0-9]\")*)").matcher(text);
		
		while (m1.find()) {
			
			System.out.println(m1.group(1));
			
			Matcher m2 = Pattern.compile("([a-z])=\"([0-9])\"").matcher(m1.group(2));
			
			while (m2.find()) {
				System.out.println("  " + m2.group(1) + " -> " + m2.group(2));
			}
		}
	}
	*/
	
	@Test
	public void testY() {
		ArrayList<String> yahtzees = new ArrayList<String>();
		ArrayList<String> straights = new ArrayList<String>();
		
		for (int die1 = 1; die1 <= 6; die1++) {
			for (int die2 = 1; die2 <= 6; die2++) {
				for (int die3 = 1; die3 <= 6; die3++) {
					for (int die4 = 1; die4 <= 6; die4++) {
						for (int die5 = 1; die5 <= 6; die5++) {
							boolean yahtzee = false;
							boolean straight = false;
							
							if (die1 == die2 && die2 == die3 && die3 == die4 && die4 == die5) {
								yahtzee = true;
							}
							
							Integer[] dice = new Integer[] { die1, die2, die3, die4, die5 };
							Arrays.sort(dice);
							
							int nextCount = 0;
							for (int i = 0; i < 4; i++) {
								if (dice[i] == (dice[i + 1] - 1)) {
									nextCount++;
								}
							}
							
							if (nextCount == 4) {
								straight = true;
							}
							
							if (yahtzee) {
								yahtzees.add(die1 + ", " + die2 + ", " + die3 + ", " + die4 + ", " + die5);
							}
							
							if (straight) {
								straights.add(die1 + ", " + die2 + ", " + die3 + ", " + die4 + ", " + die5);
							}
						}
					}
				}
			}
		}
		
		List<List<String>> straightLists = CollectionUtil.splitList(straights, 5);
		
		StringBuilder buffer = new StringBuilder();
		for (List<String> straightList : straightLists) {
			for (String straight : straightList) {
				buffer.append(straight).append("\t");
			}
			
			buffer.append("\n");
		}
		
		System.out.println("Yahtzees: " + yahtzees.size() + "\n" + yahtzees);
		System.out.println("Straights: " + straights.size() + "\n" + buffer.toString());
	}
	
	@Test
	public void testDns() {
		try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			displayStuff("local host", inetAddress);
			System.out.print("--------------------------");
			inetAddress = InetAddress.getByName("gameserver-001.astar.mobi");
			displayStuff("www.google.com", inetAddress);
			System.out.print("--------------------------");
			InetAddress[] inetAddressArray = InetAddress.getAllByName("gameserver-001-b36ef.astar.mobi");
			for (int i = 0; i < inetAddressArray.length; i++) {
				displayStuff("www.google.com #" + (i + 1), inetAddressArray[i]);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
	public static void displayStuff(String whichHost, InetAddress inetAddress) {
		System.out.println("--------------------------");
		System.out.println("Which Host:" + whichHost);
		System.out.println("Canonical Host Name:" + inetAddress.getCanonicalHostName());
		System.out.println("Host Name:" + inetAddress.getHostName());
		System.out.println("Host Address:" + inetAddress.getHostAddress());
	}
}