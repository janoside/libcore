package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class ManualTimeSourceTest {
	
	@Test
	public void testIt() {
		ManualTimeSource mts = new ManualTimeSource();
		mts.setCurrentTime(5);
		Assert.assertEquals(5, mts.getCurrentTime());
	}
}