package com.janoside.util;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ByteUtilsTest {
	
	@Test
	public void testIt() {
		List<byte[]> data = ByteUtils.splitByteArray(new byte[] {1,2,3,4,5,6,6,6,1,2,3,6,6,6,8,9,10}, new byte[]{6,6,6});
		
		Assert.assertEquals(3, data.size());
		Assert.assertEquals("[1, 2, 3, 4, 5]", Arrays.toString(data.get(0)));
		Assert.assertEquals("[1, 2, 3]", Arrays.toString(data.get(1)));
		Assert.assertEquals("[8, 9, 10]", Arrays.toString(data.get(2)));
		
		data = ByteUtils.splitByteArray(new byte[] {1,2,3,4,5,6,6,6,1,2,3,6,6,6}, new byte[]{6,6,6});
		
		Assert.assertEquals(2, data.size());
		Assert.assertEquals("[1, 2, 3, 4, 5]", Arrays.toString(data.get(0)));
		Assert.assertEquals("[1, 2, 3]", Arrays.toString(data.get(1)));
		
		data = ByteUtils.splitByteArray(new byte[] {1,2,3,4,5,6,6,6,1,2,3,6,6,6, 47}, new byte[]{6,6,6});
		
		Assert.assertEquals(3, data.size());
		Assert.assertEquals("[1, 2, 3, 4, 5]", Arrays.toString(data.get(0)));
		Assert.assertEquals("[1, 2, 3]", Arrays.toString(data.get(1)));
		Assert.assertEquals("[47]", Arrays.toString(data.get(2)));
	}
	
	@Test
	public void testSplitByLengths() {
		List<byte[]> data = ByteUtils.splitByteArrayByLengths(new byte[] {1,2,3,4,5,6,6,6,1,2,3,6,6,6,8,9,10}, new int[]{5,3,3,3,3});
		
		Assert.assertEquals(5, data.size());
		Assert.assertEquals("[1, 2, 3, 4, 5]", Arrays.toString(data.get(0)));
		Assert.assertEquals("[6, 6, 6]", Arrays.toString(data.get(1)));
		Assert.assertEquals("[1, 2, 3]", Arrays.toString(data.get(2)));
		Assert.assertEquals("[6, 6, 6]", Arrays.toString(data.get(3)));
		Assert.assertEquals("[8, 9, 10]", Arrays.toString(data.get(4)));
	}
	
	@Test
	public void testUneven() throws Exception{
		try {
			ByteUtils.splitByteArrayByLengths(new byte[] {1, 2, 3, 4, 5}, new int[] {2, 2, 2});
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}