package com.janoside.util;

import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class AdfUtilTest {
	
	@Test
	public void testIt() throws Exception {
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		
		Element adf = doc.createElement("adf");
		doc.appendChild(adf);
		
		Element prospectElement = doc.createElement("prospect");
		adf.appendChild(prospectElement);
		
		Element requestDate = doc.createElement("requestdate");
		prospectElement.appendChild(requestDate);
		
		Text text = doc.createTextNode(DateUtil.format("yyyy-MM-dd", DateUtil.parse("yyyy-MM-dd HH:mm", "2010-05-12 12:25")));
		requestDate.appendChild(text);
		
		Element vehicleElement = doc.createElement("vehicle");
		prospectElement.appendChild(vehicleElement);
		
		Element yearElement = doc.createElement("year");
		vehicleElement.appendChild(yearElement);
		
		text = doc.createTextNode("1999");
		yearElement.appendChild(text);
		
		Element makeElement = doc.createElement("make");
		vehicleElement.appendChild(makeElement);
		
		text = doc.createTextNode("Ford");
		makeElement.appendChild(text);
		
		Element modelElement = doc.createElement("model");
		vehicleElement.appendChild(modelElement);
		
		text = doc.createTextNode("Explorer");
		modelElement.appendChild(text);
		
		Element customerElement = doc.createElement("customer");
		prospectElement.appendChild(customerElement);
		
		Element contactElement = doc.createElement("contact");
		customerElement.appendChild(contactElement);
		
		Element nameElement = doc.createElement("name");
		nameElement.setAttribute("part", "full");
		contactElement.appendChild(nameElement);
		
		text = doc.createTextNode("abc");
		nameElement.appendChild(text);
		
		Element phoneElement = doc.createElement("phone");
		contactElement.appendChild(phoneElement);
		
		text = doc.createTextNode("555-555-5555");
		nameElement.appendChild(text);
		
		Element vendorElement = doc.createElement("vendor");
		prospectElement.appendChild(vendorElement);
		
		contactElement = doc.createElement("contact");
		vendorElement.appendChild(contactElement);
		
		nameElement = doc.createElement("name");
		nameElement.setAttribute("part", "full");
		contactElement.appendChild(nameElement);
		
		text = doc.createTextNode("Ray Skillman Shadeland Kia");
		nameElement.appendChild(text);
		
		//set up a transformer
		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans = transfac.newTransformer();
		trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		trans.setOutputProperty(OutputKeys.INDENT, "yes");

		//create string from xml tree
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		DOMSource source = new DOMSource(doc);
		trans.transform(source, result);
		String xmlString = sw.toString();
		
		System.out.println(xmlString);
	}
}