package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class StatTrackerUtilTest {
	
	@Test
	public void testIt() {
		Assert.assertEquals("abc", StatTrackerUtil.normalizeNameComponent("abc"));
		Assert.assertEquals("abc-def", StatTrackerUtil.normalizeNameComponent("abcDef"));
		Assert.assertEquals("abc-def", StatTrackerUtil.normalizeNameComponent("abc.def"));
		Assert.assertEquals("abc.def", StatTrackerUtil.normalizeNameComponent("abc:def"));
		Assert.assertEquals("abc-def", StatTrackerUtil.normalizeNameComponent("abc def"));
	}
}