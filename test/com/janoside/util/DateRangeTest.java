package com.janoside.util;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class DateRangeTest {
	
	@Test
	public void testIt() throws Exception {
		Date start = new Date(1000);
		Date end = new Date(2000);
		
		DateRange dr = new DateRange();
		dr.setStartDate(new Date(1000));
		dr.setEndDate(new Date(2000));
		
		Assert.assertTrue(dr.contains(new Date(start.getTime() + (end.getTime() - start.getTime()) / 2)));
		
		Assert.assertFalse(dr.contains(start));
		Assert.assertFalse(dr.contains(end));
		
		Date start2 = dr.getStartDate();
		Date end2 = dr.getEndDate();
		
		Assert.assertEquals(start, start2);
		Assert.assertFalse(start == start2);
		
		Assert.assertEquals(end, end2);
		Assert.assertFalse(end == end2);
		
		dr.setStartDate(null);
		dr.setEndDate(null);
		
		Assert.assertNull(dr.getStartDate());
		Assert.assertNull(dr.getEndDate());
		
		DateRange dr2 = new DateRange();
		dr2.setStartDate(new Date(1500));
		dr2.setEndDate(new Date(2500));
		
		try {
			Assert.assertFalse(dr.isOverlappedBy(dr2));
			
			throw new Exception();
			
		} catch (NullPointerException npe) {
			// expected
		}
		
		try {
			Assert.assertFalse(dr2.isOverlappedBy(dr));
			
			throw new Exception();
			
		} catch (NullPointerException npe) {
			// expected
		}
		
		dr.setStartDate(start);
		dr.setEndDate(end);
		
		Assert.assertEquals("DateRange(start=1969-12-31 19:00:01, end=1969-12-31 19:00:02)", dr.toString());
		Assert.assertEquals("DateRange(start=1969-12-31 19:00:01, end=1969-12-31 19:00:02)".hashCode(), dr.hashCode());
		
		Assert.assertTrue(dr.isOverlappedBy(dr2));
		Assert.assertTrue(dr2.isOverlappedBy(dr));
		
		dr2.setStartDate(new Date(1000));
		dr2.setEndDate(new Date(2000));
		
		Assert.assertTrue(dr.equals(dr2));
		Assert.assertFalse(dr.equals(null));
		Assert.assertFalse(dr.equals(new Object()));
		
		Assert.assertTrue(dr.isOverlappedBy(dr2));
		Assert.assertTrue(dr2.isOverlappedBy(dr));
		
		Assert.assertTrue(dr.isOverlappedBy(new DateRange(new Date(1100), new Date(1900))));
		Assert.assertTrue(dr.isOverlappedBy(new DateRange(new Date(900), new Date(2100))));
		
		Assert.assertTrue(dr.isOverlappedBy(new DateRange(new Date(2000), new Date(2100))));
		
		Assert.assertFalse(dr.isOverlappedBy(new DateRange(new Date(2001), new Date(2002))));
		Assert.assertFalse(dr.isOverlappedBy(new DateRange(new Date(900), new Date(999))));
	}
}