package com.janoside.util;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class GzipUtilTest {
	
	@Test
	public void testGzipGunzip() throws IOException {
		String testString = "This is a test right here.";
		byte[] testBytes = testString.getBytes();
		byte[] testBytesSpecific = testString.getBytes("UTF-8");
		
		Assert.assertEquals(GzipUtil.gunzipString(GzipUtil.gzip(testString)), testString);
		Assert.assertEquals(GzipUtil.gunzipString(GzipUtil.gzip(testString, "UTF-8"), "UTF-8"), testString);
		Assert.assertTrue(Arrays.equals(GzipUtil.gunzip(GzipUtil.gzip(testBytes)), testBytes));
		Assert.assertTrue(Arrays.equals(GzipUtil.gunzip(GzipUtil.gzip(testString, "UTF-8")), testBytesSpecific));
	}
	
	@Test
	public void testEmpty() throws Exception {
		try {
			GzipUtil.gzip((byte[]) null);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			GzipUtil.gunzip(new byte[] {});
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}