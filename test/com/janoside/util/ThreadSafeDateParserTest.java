package com.janoside.util;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

public class ThreadSafeDateParserTest {
	
	@Test
	public void testIt() {
		ExecutorService exec = Executors.newFixedThreadPool(50);
		
		for (int i = 0; i < 5000; i++) {
			exec.execute(new Runnable() {
				public void run() {
					Assert.assertEquals(1357016400000L, ThreadSafeDateParser.parseLongDate("2013-01-01", "yyyy-MM-dd"));
					
					Assert.assertEquals("2013-01-01", ThreadSafeDateParser.format(new Date(1357016400000L), "yyyy-MM-dd"));
					Assert.assertEquals("2013-01-01", ThreadSafeDateParser.format(1357016400000L, "yyyy-MM-dd"));
				}
			});
		}
	}
}