package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class StateUtilTest {
	
	@Test
	public void testIt() {
		Assert.assertEquals("OH", StateUtil.getAbbreviationByName("Ohio"));
		Assert.assertEquals("Ohio", StateUtil.getNameByAbbreviation("OH"));
		
		Assert.assertEquals("northdakota", StateUtil.getNormalizedName("North Dakota"));
		Assert.assertEquals("North Dakota", StateUtil.getNameByNormalizedName("northdakota"));
		
		Assert.assertTrue(StateUtil.isStateAbbreviation("oh"));
		Assert.assertFalse(StateUtil.isStateAbbreviation("zz"));
		
		Assert.assertTrue(StateUtil.isStateName("Ohio"));
		Assert.assertFalse(StateUtil.isStateName("Zippy"));
		
		Assert.assertEquals(-169105837, StateUtil.getNames().toString().hashCode());
		
		Assert.assertEquals(167746740, StateUtil.getNamesByAbbreviation().toString().hashCode());
		Assert.assertEquals(-1173436698, StateUtil.getAbbreviationsByName().toString().hashCode());
		
		Assert.assertEquals(-1093105732, StateUtil.getAbbreviations().toString().hashCode());
	}
}