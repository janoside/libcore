package com.janoside.util;

import org.junit.Test;

import com.janoside.JunitTestUtil;

public class UtilsTest {
	
	@Test
	public void testPrivateConstructors() throws Exception {
		JunitTestUtil.testConstructorIsPrivate(ByteUtils.class);
		JunitTestUtil.testConstructorIsPrivate(CollectionUtil.class);
		JunitTestUtil.testConstructorIsPrivate(DateUtil.class);
		JunitTestUtil.testConstructorIsPrivate(FileUtil.class);
		JunitTestUtil.testConstructorIsPrivate(GzipUtil.class);
		JunitTestUtil.testConstructorIsPrivate(HashUtil.class);
		JunitTestUtil.testConstructorIsPrivate(MapUtil.class);
		JunitTestUtil.testConstructorIsPrivate(NetworkUtil.class);
		JunitTestUtil.testConstructorIsPrivate(QuestionTextUrlUtil.class);
		JunitTestUtil.testConstructorIsPrivate(RandomUtil.class);
		JunitTestUtil.testConstructorIsPrivate(RegexUtil.class);
		JunitTestUtil.testConstructorIsPrivate(SetUtil.class);
		JunitTestUtil.testConstructorIsPrivate(StringUtil.class);
		JunitTestUtil.testConstructorIsPrivate(UrlUtil.class);
	}
}