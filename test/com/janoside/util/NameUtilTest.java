package com.janoside.util;

import org.junit.Assert;
import org.junit.Test;

public class NameUtilTest {
	
	@Test
	public void testIt() {
		Assert.assertEquals(184, NameUtil.getShortNames().size());
		Assert.assertEquals(118, NameUtil.getShortMaleNames().size());
		Assert.assertEquals(66, NameUtil.getShortFemaleNames().size());
		
		Assert.assertNotNull(NameUtil.getRandomShortFemaleName());
		Assert.assertNotNull(NameUtil.getRandomShortMaleName());
		Assert.assertNotNull(NameUtil.getRandomShortName());
	}
}