package com.janoside.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.transform.ObjectTransformer;

public class CollectionUtilTest {
	
	@Test
	public void testSplitList() {
		int size = 20;
		
		ArrayList<Long> list = new ArrayList<Long>();
		for (int i = 0; i < size; i++) {
			list.add((long) size);
		}
		
		List<List<Long>> listList = CollectionUtil.splitList(list, 10);
		Assert.assertEquals(2, listList.size());
		
		listList = CollectionUtil.splitList(list, 5);
		Assert.assertEquals(4, listList.size());
		
		listList = CollectionUtil.splitList(list, 7);
		Assert.assertEquals(3, listList.size());
	}
	
	@Test
	public void testCloneList() {
		ArrayList<String> stringList = new ArrayList<String>();
		stringList.add("1");
		stringList.add("2");
		stringList.add("3");
		
		List<String> newStringList = CollectionUtil.cloneList(stringList);
		
		Assert.assertEquals(3, newStringList.size());
		Assert.assertEquals("1", newStringList.get(0));
		Assert.assertEquals("2", newStringList.get(1));
		Assert.assertEquals("3", newStringList.get(2));
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void testCloneListNull() {
		List list = CollectionUtil.cloneList(null);
		
		Assert.assertTrue(list.isEmpty());
	}
	
	@Test
	public void testCloneListNullType() {
		ArrayList<String> list = null;
		List<String> clonedList = CollectionUtil.cloneList(list);
		
		Assert.assertTrue(clonedList.isEmpty());
	}
	
	@Test
	public void testSubList() {
		ArrayList<String> stringList = new ArrayList<String>();
		stringList.add("1");
		stringList.add("2");
		stringList.add("3");
		stringList.add("4");
		stringList.add("5");
		stringList.add("6");
		
		Assert.assertEquals(5, CollectionUtil.subList(stringList, 1).size());
		Assert.assertEquals(4, CollectionUtil.subList(stringList, 2).size());
		
		List<String> subStringList = CollectionUtil.subList(stringList, 3);
		
		Assert.assertEquals(3, subStringList.size());
		Assert.assertEquals("4", subStringList.get(0));
		Assert.assertEquals("5", subStringList.get(1));
		Assert.assertEquals("6", subStringList.get(2));
	}
	
	@Test
	public void testSubList2() {
		ArrayList<String> stringList = new ArrayList<String>();
		stringList.add("1");
		stringList.add("2");
		stringList.add("3");
		stringList.add("4");
		stringList.add("5");
		stringList.add("6");
		stringList.add("7");
		stringList.add("8");
		stringList.add("9");
		
		Assert.assertEquals(1, CollectionUtil.subList(stringList, 1, 1).size());
		Assert.assertEquals(2, CollectionUtil.subList(stringList, 1, 2).size());
		
		List<String> subStringList = CollectionUtil.subList(stringList, 6, 2);
		
		Assert.assertEquals(2, subStringList.size());
		Assert.assertEquals("7", subStringList.get(0));
		Assert.assertEquals("8", subStringList.get(1));
	}
	
	@Test
	public void testUnique() {
		List<String> list1 = Arrays.asList("one", "two", "four", "nine");
		List<String> list2 = Arrays.asList("three", "nine", "six");
		
		List<String> mergedList = CollectionUtil.mergeUnique(list1, list2);
		
		Assert.assertEquals(6, mergedList.size());
		Assert.assertTrue(mergedList.contains("one"));
		Assert.assertTrue(mergedList.contains("two"));
		Assert.assertTrue(mergedList.contains("four"));
		Assert.assertTrue(mergedList.contains("nine"));
		Assert.assertTrue(mergedList.contains("three"));
		Assert.assertTrue(mergedList.contains("six"));
	}
	
	@Test
	public void testUniqueNull() {
		List<String> list1 = Arrays.asList("one", "two", "four", "nine");
		List<String> list2 = null;
		
		int exceptionCount = 0;
		
		try {
			CollectionUtil.mergeUnique(list1, list2);
			
		} catch (NullPointerException npe) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
	
	@Test
	public void testUniqueSorted() {
		List<String> list1 = Arrays.asList("1", "2", "4", "9");
		List<String> list2 = Arrays.asList("3", "9", "6");
		
		List<String> mergedList = CollectionUtil.mergeUniqueSorted(list1, list2);
		
		Assert.assertEquals(6, mergedList.size());
		Assert.assertEquals("1", mergedList.get(0));
		Assert.assertEquals("2", mergedList.get(1));
		Assert.assertEquals("3", mergedList.get(2));
		Assert.assertEquals("4", mergedList.get(3));
		Assert.assertEquals("6", mergedList.get(4));
		Assert.assertEquals("9", mergedList.get(5));
	}
	
	@Test
	public void testUniqueSortedNull() {
		List<String> list1 = Arrays.asList("one", "two", "four", "nine");
		List<String> list2 = null;
		
		int exceptionCount = 0;
		
		try {
			CollectionUtil.mergeUniqueSorted(list1, list2);
			
		} catch (NullPointerException npe) {
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
	
	@Test
	public void testTransformList() {
		Assert.assertEquals("[123abc, 123def]", CollectionUtil.transformList(Arrays.asList("abc", "def"), new ObjectTransformer() {
			public Object transform(Object value) {
				return "123" + value;
			}
		}).toString());
		
		Assert.assertEquals("[]", CollectionUtil.transformList(null, new ObjectTransformer() {
			public Object transform(Object value) {
				return null;
			}
		}).toString());
	}
	
	@Test
	public void testIsEmpty() {
		Assert.assertTrue(CollectionUtil.isEmpty(null));
		Assert.assertTrue(CollectionUtil.isEmpty(new ArrayList()));
	}
	
	@Test
	public void testReverseList() {
		Assert.assertEquals("[def, abc]", CollectionUtil.reversedList(Arrays.asList("abc", "def")).toString());
	}
	
	@Test
	public void testTransformCollection() {
		Assert.assertEquals("[123abc]", CollectionUtil.transformCollection(new HashSet() {{ add("abc"); }}, new ObjectTransformer() {
			public Object transform(Object value) {
				return "123" + value;
			}
		}).toString());
		
		Assert.assertEquals(0, CollectionUtil.transformCollection(null, null).size());
	}
	
	@Test
	public void testListToMap() {
		Map m = CollectionUtil.listToMap(Arrays.asList("abc", "def"), new ObjectTransformer<String, String>() {
			public String transform(String value) {
				return "123" + value;
			}
		});
		
		Assert.assertEquals(2, m.size());
		Assert.assertTrue(m.containsKey("123abc"));
		Assert.assertEquals("abc", m.get("123abc"));
		Assert.assertTrue(m.containsKey("123def"));
		Assert.assertEquals("def", m.get("123def"));
	}
	
	@Test
	public void testCollectionToMap() {
		Map m = CollectionUtil.collectionToMap(new HashSet() {{ add("abc"); add("def"); }}, new ObjectTransformer<String, String>() {
			public String transform(String value) {
				return "123" + value;
			}
		});
		
		Assert.assertEquals(2, m.size());
		Assert.assertTrue(m.containsKey("123abc"));
		Assert.assertEquals("abc", m.get("123abc"));
		Assert.assertTrue(m.containsKey("123def"));
		Assert.assertEquals("def", m.get("123def"));
	}
	
	@Test
	public void testCollectionToSet() {
		Set s = CollectionUtil.collectionToSet(new HashSet() {{ add("abc"); add("def"); }}, new ObjectTransformer<String, String>() {
			public String transform(String value) {
				return "123" + value;
			}
		});
		
		Assert.assertEquals(2, s.size());
		Assert.assertTrue(s.contains("123abc"));
		Assert.assertTrue(s.contains("123def"));
	}
	
	@Test
	public void testSplitListByListCount() {
		List<List<Integer>> r = CollectionUtil.splitListByListCount(Arrays.asList(1, 2, 3, 4, 5), 2);
		Assert.assertEquals("[[1, 2, 3], [4, 5]]", r.toString());
		
		r = CollectionUtil.splitListByListCount(Arrays.asList(1, 2, 3, 4, 5), 1);
		Assert.assertEquals("[[1, 2, 3, 4, 5]]", r.toString());
		
		r = CollectionUtil.splitListByListCount(Arrays.asList(1, 2, 3, 4, 5), 3);
		Assert.assertEquals("[[1, 2], [3, 4], [5]]", r.toString());
		
		r = CollectionUtil.splitListByListCount(Arrays.asList(1, 2, 3, 4, 5, 6), 2);
		Assert.assertEquals("[[1, 2, 3], [4, 5, 6]]", r.toString());
	}
	
	@Test
	public void testListToGroupedMap() {
		Map m = CollectionUtil.listToGroupedMap(Arrays.asList("abc", "def", "abc"), new ObjectTransformer() {
			public Object transform(Object value) {
				return "123" + value;
			}
		});
		
		Assert.assertEquals(2, m.size());
		Assert.assertTrue(m.containsKey("123abc"));
		Assert.assertTrue(m.get("123abc") instanceof List);
		Assert.assertEquals(2, ((List) m.get("123abc")).size());
		Assert.assertEquals("[abc, abc]", ((List) m.get("123abc")).toString());
		Assert.assertTrue(m.containsKey("123def"));
		Assert.assertTrue(m.get("123def") instanceof List);
		Assert.assertEquals(1, ((List) m.get("123def")).size());
		Assert.assertEquals("[def]", ((List) m.get("123def")).toString());
	}
}