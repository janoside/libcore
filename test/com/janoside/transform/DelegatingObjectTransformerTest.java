package com.janoside.transform;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class DelegatingObjectTransformerTest {
	
	@Test
	public void testIt() {
		ObjectTransformer t1 = new ObjectTransformer() {
			public Object transform(Object value) {
				return "one";
			}
		};
		
		ObjectTransformer t2 = new ObjectTransformer() {
			public Object transform(Object value) {
				return "two";
			}
		};
		
		DelegatingObjectTransformer dot = new DelegatingObjectTransformer();
		dot.setTransformers(Arrays.asList(t1, t2));
		
		Assert.assertEquals("two", dot.transform("start"));
	}
}