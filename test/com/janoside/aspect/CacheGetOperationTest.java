package com.janoside.aspect;

import java.util.HashSet;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.cache.ObjectCache;
import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.util.Function;
import com.janoside.util.Tuple;

public class CacheGetOperationTest {
	
	@Test
	public void testIt() {
		final AtomicInteger hitCount = new AtomicInteger(0);
		final AtomicInteger missCount = new AtomicInteger(0);
		
		Function<Tuple<String, String>> hitCallback = new Function<Tuple<String, String>>() {
			public void run(Tuple<String, String> t) {
				hitCount.getAndIncrement();
			}
		};
		
		Function<String> missCallback = new Function<String>() {
			public void run(String s) {
				missCount.getAndIncrement();
			}
		};
		
		ObjectCache<String> cache = new ObjectCache<String>() {
			
			public String get(String key) {
				if (key.equals("abc")) {
					return "cache";
					
				} else if (key.equals("null")) {
					return null;
					
				} else {
					return key;
				}
			}
			
			public void remove(String key) {}
			public void put(String key, String value) {}
			public void put(String key, String object, long lifetime) {}
			public void clear() {}
			
			public int getSize() { return 0; }
			
			
		};
		
		Callable<String> xyzCallable = new Callable<String>() {
			public String call() {
				return "xyz";
			}
		};
		
		CacheGetOperation<String> op = new CacheGetOperation<String>("abc", xyzCallable);
		op.setCache(cache);
		op.setExceptionHandler(new StandardErrorExceptionHandler());
		op.setCacheHitCallback(hitCallback);
		op.setCacheMissCallback(missCallback);
		
		Assert.assertEquals("cache", op.call());
		Assert.assertEquals(1, hitCount.get());
		Assert.assertEquals(0, missCount.get());
		
		op.setKey("null");
		
		Assert.assertEquals("xyz", op.call());
		Assert.assertEquals(1, hitCount.get());
		Assert.assertEquals(1, missCount.get());
		
		op = new CacheGetOperation<String>();
		op.setKey("haha");
		op.setNonCacheOperation(xyzCallable);
		op.setCache(cache);
		op.setExceptionHandler(new StandardErrorExceptionHandler());
		op.setCacheHitCallback(hitCallback);
		op.setCacheMissCallback(missCallback);
		
		Assert.assertEquals("haha", op.call());
		Assert.assertEquals(2, hitCount.get());
		Assert.assertEquals(1, missCount.get());
		
		op.setEmptyKeysSet(new HashSet<String>() {{ add("null"); }});
		
		op.setKey("null");
		
		Assert.assertNull(op.call());
		Assert.assertEquals(2, hitCount.get());
		Assert.assertEquals(2, missCount.get());
		
		op.setKey("haha");
		
		Assert.assertEquals("haha", op.call());
		Assert.assertEquals(3, hitCount.get());
		Assert.assertEquals(2, missCount.get());
	}
}