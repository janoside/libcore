package com.janoside.aspect;

import java.util.concurrent.Callable;

import org.junit.Test;

public class RetrierTest {
	
	@Test
	public void testIt() {
		Callable<String> callable = new Callable<String>() {
			public String call() {
				return "abc";
			}
		};
		
		Retrier<String> op = new Retrier<String>();
		op.setCallable(callable);
		op.call();
	}
}