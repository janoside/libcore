package com.janoside.scheduling;

import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.janoside.exception.ExceptionHandler;

public class BasicSchedulerTest {
	
	private BasicScheduler scheduler;
	
	private CountingExceptionHandler countingExceptionHandler;
	
	@Test
	public void testSingle() throws Exception {
		final AtomicInteger counter = new AtomicInteger(0);
		
		this.scheduler.scheduleTask(new Runnable() {
				public void run() {
					counter.getAndIncrement();
				}
			},
			50);
		
		Thread.sleep(10);
		
		Assert.assertEquals(0, counter.get());
		
		Thread.sleep(50);
		
		Assert.assertEquals(1, counter.get());
	}
	
	@Test
	public void testSingleWithCancellation() throws InterruptedException {
		final AtomicInteger counter = new AtomicInteger(0);
		
		TimerTask task = this.scheduler.scheduleTask(new Runnable() {
				public void run() {
					counter.getAndIncrement();
				}
			},
			50);
		
		Thread.sleep(10);
		
		Assert.assertEquals(0, counter.get());
		task.cancel();
		
		Thread.sleep(50);
		
		Assert.assertEquals(0, counter.get());
	}
	
	@Test
	public void testException() throws Exception {
		this.scheduler.scheduleTask(new Runnable() {
			public void run() {
				throw new RuntimeException();
			}
		}, 5);
		
		Thread.sleep(10);
		
		Assert.assertEquals(1, this.countingExceptionHandler.getCount());
	}
	
	@Test
	public void testMultipleExceptions() throws Exception {
		this.scheduler.scheduleTaskAtConstantRate(new Runnable() {
			public void run() {
				throw new RuntimeException();
			}
		}, 5, 3);
		
		Thread.sleep(15);
		
		Assert.assertTrue(this.countingExceptionHandler.getCount() >= 3);
	}
	
	@Test
	public void testMultipleExceptions2() throws Exception {
		this.scheduler.scheduleTaskWithConstantIntervalBetween(new Runnable() {
			public void run() {
				throw new RuntimeException();
			}
		}, 5, 3);
		
		Thread.sleep(15);
		
		Assert.assertTrue(this.countingExceptionHandler.getCount() >= 3);
	}
	
	@Test
	public void testConstantRate() throws InterruptedException {
		final AtomicInteger counter1 = new AtomicInteger(0);
		final AtomicInteger counter2 = new AtomicInteger(0);
		
		this.scheduler.scheduleTaskAtConstantRate(new Runnable() {
				public void run() {
					counter1.getAndIncrement();
					
					try {
						Thread.sleep(25);
						
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
					
					counter2.getAndIncrement();
				}
			},
			50,
			50);
		
		Thread.sleep(10);
		
		// 10 elapsed
		Assert.assertEquals(0, counter1.get());
		Assert.assertEquals(0, counter2.get());
		
		Thread.sleep(50);
		
		// 60 elapsed
		Assert.assertEquals(1, counter1.get());
		Assert.assertEquals(0, counter2.get());
		
		Thread.sleep(20);
		
		// 80 elapsed
		Assert.assertEquals(1, counter1.get());
		Assert.assertEquals(1, counter2.get());
		
		Thread.sleep(25);
		
		// 105 elapsed
		Assert.assertEquals(2, counter1.get());
		Assert.assertEquals(1, counter2.get());
		
		Thread.sleep(25);
		
		// 130 elapsed
		Assert.assertEquals(2, counter1.get());
		Assert.assertEquals(2, counter2.get());
	}
	
	@Test
	public void testConstantIntervalBetween() throws InterruptedException {
		final AtomicInteger counter1 = new AtomicInteger(0);
		final AtomicInteger counter2 = new AtomicInteger(0);
		
		this.scheduler.scheduleTaskWithConstantIntervalBetween(new Runnable() {
				public void run() {
					counter1.getAndIncrement();
					
					try {
						Thread.sleep(25);
						
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
					
					counter2.getAndIncrement();
				}
			},
			50,
			50);
		
		Thread.sleep(10);
		
		// 10 elapsed
		Assert.assertEquals(0, counter1.get());
		Assert.assertEquals(0, counter2.get());
		
		Thread.sleep(50);
		
		// 60 elapsed
		Assert.assertEquals(1, counter1.get());
		Assert.assertEquals(0, counter2.get());
		
		Thread.sleep(20);
		
		// 80 elapsed
		Assert.assertEquals(1, counter1.get());
		Assert.assertEquals(1, counter2.get());
		
		Thread.sleep(25);
		
		// 105 elapsed
		Assert.assertEquals(1, counter1.get());
		Assert.assertEquals(1, counter2.get());
		
		Thread.sleep(25);
		
		// 130 elapsed
		Assert.assertEquals(2, counter1.get());
		Assert.assertEquals(1, counter2.get());
		
		Thread.sleep(25);
		// 155 elapsed
		Assert.assertEquals(2, counter1.get());
		Assert.assertEquals(2, counter2.get());
	}
	
	@Before
	public void setUp() {
		this.countingExceptionHandler = new CountingExceptionHandler();
		
		this.scheduler = new BasicScheduler();
		this.scheduler.setExceptionHandler(this.countingExceptionHandler);
	}
	
	private static class CountingExceptionHandler implements ExceptionHandler {
		
		private AtomicInteger counter = new AtomicInteger(0);
		
		public void handleException(Throwable t) {
			this.counter.getAndIncrement();
		}
		
		public void reset() {
			this.counter.getAndSet(0);
		}
		
		public int getCount() {
			return this.counter.get();
		}
	}
}