package com.janoside.pool;

public class TestObjectA implements Poolable {
	
	private String str;
	
	private int x;
	
	public boolean isValid() {
		return true;
	}
	
	public String getStr() {
		return this.str;
	}
	
	public void setStr(String str) {
		this.str = str;
	}
	
	public int getX() {
		return this.x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	@Override
	public String toString() {
		return "TestObjectA(str=" + this.str + ", x=" + this.x + ")";
	}
}