package com.janoside.pool;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.JunitTestUtil;

public class ManagedObjectPoolTest implements SimpleObjectFactory<TestObjectA> {
	
	@Test
	public void testNotSame() {
		ManagedObjectPool<TestObjectA> pool = new ManagedObjectPool<TestObjectA>();
		pool.setFactory(this);
		
		TestObjectA o1 = pool.borrowObject();
		TestObjectA o2 = pool.borrowObject();
		
		Assert.assertNotSame(o1, o2);
	}
	
	@Test
	public void testReuse() {
		ManagedObjectPool<TestObjectA> pool = new ManagedObjectPool<TestObjectA>();
		pool.setFactory(this);
		
		TestObjectA o1 = pool.borrowObject();
		
		pool.returnObject(o1);
		
		TestObjectA o2 = pool.borrowObject();
		
		Assert.assertSame(o1, o2);
	}
	
	@Test
	public void testActiveCount() {
		ManagedObjectPool<TestObjectA> pool = new ManagedObjectPool<TestObjectA>();
		pool.setFactory(this);
		
		TestObjectA o1 = pool.borrowObject();
		
		Assert.assertEquals(1, pool.getActiveCount());
		Assert.assertEquals(1, pool.viewUnreturnedObjects().size());
		
		pool.returnObject(o1);
		
		Assert.assertEquals(0, pool.getActiveCount());
		
		TestObjectA o2 = pool.borrowObject();
		
		Assert.assertEquals(1, pool.getActiveCount());
		
		Assert.assertSame(o1, o2);
	}
	
	@Test
	public void testCreatedObject() {
		ManagedObjectPool<TestObjectA> pool = new ManagedObjectPool<TestObjectA>();
		pool.setFactory(this);
		
		TestObjectA o1 = pool.borrowObject();
		
		pool.returnObject(o1);
		
		TestObjectA o2 = pool.borrowObject();
		
		Assert.assertSame(o1, o2);
	}
	
	@Test
	public void testMaxActive() {
		ManagedObjectPool<TestObjectA> pool = new ManagedObjectPool<TestObjectA>();
		pool.setFactory(this);
		pool.setMaxActive(2);
		pool.setFailWhenExhausted(true);
		
		Assert.assertEquals(2, pool.getMaxActive());
		Assert.assertEquals(true, pool.getFailWhenExhausted());
		
		pool.setFailWhenExhausted(false);
		
		Assert.assertEquals(false, pool.getFailWhenExhausted());
		
		pool.setFailWhenExhausted(true);
		
		pool.borrowObject();
		pool.borrowObject();
		
		int exceptionCount = 0;
		
		try {
			pool.borrowObject();
			
		} catch (Exception e) {
			e.printStackTrace();
			
			exceptionCount++;
		}
		
		Assert.assertEquals(1, exceptionCount);
	}
	
	@Test
	public void testClearWhileObjectCheckedOut() {
		ManagedObjectPool<TestObjectA> pool = new ManagedObjectPool<TestObjectA>();
		pool.setFactory(this);
		pool.setMaxActive(2);
		
		Assert.assertEquals(0, pool.getActiveCount());
		
		TestObjectA obj = pool.borrowObject();
		
		pool.clear();
		
		Assert.assertEquals(0, pool.getActiveCount());
		
		pool.returnObject(obj);
		
		Assert.assertEquals(0, pool.getActiveCount());
	}
	
	@Test
	public void testRemove() {
		ManagedObjectPool<TestObjectA> pool = new ManagedObjectPool<TestObjectA>();
		pool.setFactory(this);
		pool.setMaxActive(2);
		
		Assert.assertEquals(0, pool.getActiveCount());
		
		TestObjectA obj = pool.borrowObject();
		
		pool.removeObject(obj);
		
		Assert.assertEquals(0, pool.getActiveCount());
		
		TestObjectA obj2 = pool.borrowObject();
		
		Assert.assertNotSame(obj, obj2);
	}
	
	@Test
	public void testGenericDumbness() {
		ManagedObjectPool pool = new ManagedObjectPool();
		pool.setFactory(this);
		
		JunitTestUtil.reflectiveInvokeVoidInstanceMethod(pool, "borrowObject");
	}
	
	public TestObjectA createObject() {
		TestObjectA object = new TestObjectA();
		object.setStr("whoa");
		object.setX(9);
		
		return object;
	}
}