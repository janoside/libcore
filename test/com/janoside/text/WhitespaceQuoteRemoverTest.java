package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.JunitTestUtil;

public class WhitespaceQuoteRemoverTest {
	
	@Test
	public void testIt() {
		WhitespaceQuoteRemover tt = new WhitespaceQuoteRemover();
		
		Assert.assertEquals("abc", tt.transform(" abc "));
		
		Assert.assertEquals("abc", tt.transform(" \"abc "));
		Assert.assertEquals("abc", tt.transform(" abc\" "));
		Assert.assertEquals("abc", tt.transform(" \"abc\" "));
		Assert.assertEquals("abc", tt.transform(" \" abc\" "));
		Assert.assertEquals("abc", tt.transform(" \" abc \" "));
	}
	
	@Test
	public void testDumbness() {
		WhitespaceQuoteRemover tt = new WhitespaceQuoteRemover();
		
		JunitTestUtil.reflectiveInvokeInstanceMethodWhichTakesObjectWithArgument(tt, "transform", "abc");
	}
}