package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

public class WordCleaningTextTransformerTest {
	
	@Test
	public void testIt() {
		WordCleaningTextTransformer wctt = new WordCleaningTextTransformer();
		
		Assert.assertEquals("one two three", wctt.transform("one two three"));
		Assert.assertEquals("one two three", wctt.transform("one  two  three"));
		Assert.assertEquals("one two three", wctt.transform("one. two. three."));
		Assert.assertEquals("one two three", wctt.transform("one	two	three"));
		Assert.assertEquals("one two three", wctt.transform("one two three?"));
		Assert.assertEquals("one two three", wctt.transform("One Two THREE"));
		Assert.assertEquals("one two three", wctt.transform("one two+ 					three"));
		Assert.assertEquals("one 2 three", wctt.transform("one 2 three"));
		Assert.assertEquals("one two2 three", wctt.transform("one two2 three"));
		Assert.assertEquals("one two three", wctt.transform("one   two   .   three"));
	}
}