package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

public class UnchangingTextTransformerTest {
	
	@Test
	public void testIt() {
		UnchangingTextTransformer tt = new UnchangingTextTransformer();
		
		Assert.assertEquals("abc", tt.transform("abc"));
		Assert.assertEquals("♠", tt.transform("♠"));
	}
}