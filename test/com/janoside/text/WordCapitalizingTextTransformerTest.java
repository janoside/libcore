package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.JunitTestUtil;

public class WordCapitalizingTextTransformerTest {
	
	@Test
	public void testIt() {
		WordCapitalizingTextTransformer tt = new WordCapitalizingTextTransformer();
		
		Assert.assertEquals("One Two Three", tt.transform("one two three"));
		Assert.assertEquals("One-Two-Three", tt.transform("one-two-three"));
	}
	
	@Test
	public void testDumbness() {
		WordCapitalizingTextTransformer tt = new WordCapitalizingTextTransformer();
		
		JunitTestUtil.reflectiveInvokeInstanceMethodWhichTakesObjectWithArgument(tt, "transform", "abc");
	}
}