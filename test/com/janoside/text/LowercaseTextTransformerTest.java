package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

public class LowercaseTextTransformerTest {
	
	@Test
	public void testIt() {
		LowercaseTextTransformer tt = new LowercaseTextTransformer();
		
		Assert.assertEquals("abc", tt.transform("abc"));
		Assert.assertEquals("♠", tt.transform("♠"));
		Assert.assertEquals("abc", tt.transform("Abc"));
	}
}