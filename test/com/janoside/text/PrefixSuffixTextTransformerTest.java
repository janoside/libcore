package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.JunitTestUtil;

public class PrefixSuffixTextTransformerTest {
	
	@Test
	public void testIt() {
		PrefixSuffixTextTransformer tt = new PrefixSuffixTextTransformer();
		tt.setPrefix("abc");
		tt.setSuffix("xyz");
		
		Assert.assertEquals("abcdefghijklmnopqrstuvwxyz", tt.transform("defghijklmnopqrstuvw"));
		
		tt.setPrefix(null);
		
		Assert.assertEquals("defghijklmnopqrstuvwxyz", tt.transform("defghijklmnopqrstuvw"));
		
		tt.setSuffix("");
		
		Assert.assertEquals("defghijklmnopqrstuvw", tt.transform("defghijklmnopqrstuvw"));
	}
	
	@Test
	public void testDumbness() {
		PrefixSuffixTextTransformer tt = new PrefixSuffixTextTransformer();
		
		JunitTestUtil.reflectiveInvokeInstanceMethodWhichTakesObjectWithArgument(tt, "transform", "abc");
	}
}