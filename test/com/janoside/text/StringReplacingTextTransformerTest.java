package com.janoside.text;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

public class StringReplacingTextTransformerTest {
	
	@Test
	public void testIt() {
		StringReplacingTextTransformer tt = new StringReplacingTextTransformer();
		tt.setStringsToReplace(new HashMap<String, String>() {{
			put("abc", "def");
		}});
		
		Assert.assertEquals("defdef", tt.transform("abcdef"));
		
		Assert.assertEquals("{abc=def}", tt.viewStringsToReplace());
	}
}