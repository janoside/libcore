package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

public class HashCodeTextTransformerTest {
	
	@Test
	public void testIt() {
		HashCodeTextTransformer tt = new HashCodeTextTransformer();
		
		Assert.assertEquals("96354", tt.transform("abc"));
		Assert.assertEquals("9824", tt.transform("♠"));
		Assert.assertEquals("65602", tt.transform("Abc"));
	}
}