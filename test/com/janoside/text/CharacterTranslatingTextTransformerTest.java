package com.janoside.text;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

public class CharacterTranslatingTextTransformerTest {
	
	@Test
	@SuppressWarnings("serial")
	public void testIt() {
		CharacterTranslatingTextTransformer transformer = new CharacterTranslatingTextTransformer();
		HashMap<Character, Character> testMap = new HashMap<Character, Character>() {{
			put('+', '-');
			put('-', '+'); }};
		
		transformer.setTranslationMap(testMap);
		
		Assert.assertEquals("5+5-5", transformer.transform("5-5+5"));
	}
}
