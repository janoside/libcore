package com.janoside.text;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class DelegatingTextTransformerTest {
	
	@Test
	public void testIt() {
		ArrayList<TextTransformer> tts = new ArrayList<TextTransformer>();
		tts.add(new UnchangingTextTransformer());
		
		DelegatingTextTransformer tt = new DelegatingTextTransformer();
		tt.setTextTransformers(tts);
		
		Assert.assertEquals("abc", tt.transform("abc"));
	}
}