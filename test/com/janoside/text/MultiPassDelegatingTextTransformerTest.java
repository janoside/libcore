package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

public class MultiPassDelegatingTextTransformerTest {
	
	@Test
	public void testIt() {
		MultiPassDelegatingTextTransformer tt = new MultiPassDelegatingTextTransformer();
		tt.setInternalTextTransformer(new UnchangingTextTransformer());
		tt.setPassCount(2);
		
		Assert.assertEquals("abc", tt.transform("abc"));
	}
}