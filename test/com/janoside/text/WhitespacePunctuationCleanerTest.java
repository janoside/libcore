package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

public class WhitespacePunctuationCleanerTest {
	
	@Test
	public void testLeadingWhitespace() {
		WhitespacePunctuationCleaner cleaner = new WhitespacePunctuationCleaner();
		
		String cleanString = cleaner.transform("   leading whitespace");
		
		Assert.assertTrue(cleanString.startsWith("leading whitespace"));
	}
	
	@Test
	public void testTrailingWhitespace() {
		WhitespacePunctuationCleaner cleaner = new WhitespacePunctuationCleaner();
		
		String cleanString = cleaner.transform("trailing whitespace   ");
		
		Assert.assertTrue(cleanString.endsWith("trailing whitespace"));
	}
	
	@Test
	public void testTrailingQuestionMark() {
		WhitespacePunctuationCleaner cleaner = new WhitespacePunctuationCleaner();
		
		String cleanString = cleaner.transform("trailing questionmark?");
		
		Assert.assertTrue(cleanString.endsWith("trailing questionmark"));
	}
	
	@Test
	public void testTrailingPeriod() {
		WhitespacePunctuationCleaner cleaner = new WhitespacePunctuationCleaner();
		
		String cleanString = cleaner.transform("trailing period.");
		
		Assert.assertTrue(cleanString.endsWith("trailing period"));
	}
	
	@Test
	public void testTrailingExclamationPoint() {
		WhitespacePunctuationCleaner cleaner = new WhitespacePunctuationCleaner();
		
		String cleanString = cleaner.transform("trailing exclamation point!");
		
		Assert.assertTrue(cleanString.endsWith("trailing exclamation point"));
	}
}