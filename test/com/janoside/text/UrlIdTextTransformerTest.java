package com.janoside.text;

import org.junit.Assert;
import org.junit.Test;

public class UrlIdTextTransformerTest {
	
	@Test
	public void testIt() {
		UrlIdTextTransformer transformer = new UrlIdTextTransformer();
		
		Assert.assertEquals("pearl-jam-indianapolis-july-20-2010", transformer.transform("Pearl Jam, Indianapolis, July 20, 2010"));
		Assert.assertEquals("pearl-jam-indianapolis-july-20-2010", transformer.transform("Pearl-Jam, Indianapolis, July 20, 2010"));
	}
	
	@Test
	public void testLeadingTrailingDashes() {
		UrlIdTextTransformer transformer = new UrlIdTextTransformer();
		
		Assert.assertEquals("abc", transformer.transform("-Abc"));
		Assert.assertEquals("abc", transformer.transform("Abc-"));
	}
}