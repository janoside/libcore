package com.janoside.stats;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

public class PerformanceStatsTest {
	
	@Test
	public void testBasicAverage() {
		PerformanceStats performanceStats = new PerformanceStats();
		performanceStats.count(100);
		
		Assert.assertTrue(performanceStats.getAverage() > 99);
		Assert.assertTrue(performanceStats.getAverage() < 101);
	}
	
	@Test
	public void testMultipleAverage() {
		PerformanceStats performanceStats = new PerformanceStats();
		performanceStats.count(100);
		performanceStats.count(300);
		
		Assert.assertTrue(performanceStats.getAverage() > 199);
		Assert.assertTrue(performanceStats.getAverage() < 201);
	}
	
	@Test
	public void testCount() {
		PerformanceStats performanceStats = new PerformanceStats();
		for (int i = 0; i < 47; i++) {
			performanceStats.count(30);
		}
		
		Assert.assertEquals(47, performanceStats.getCount());
	}
	
	@Test
	public void testCountMultithread() throws InterruptedException {
		final int trials = 50;
		final int threadCount = 15;
		final int statCount = 100;
		
		for (int trial = 0; trial < trials; trial++) {
			final PerformanceStats performanceStats = new PerformanceStats();
			
			ExecutorService executor = Executors.newFixedThreadPool(100);
			
			final CountDownLatch latch = new CountDownLatch(threadCount);
			for (int i = 0; i < threadCount; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							for (int i = 0; i < statCount; i++) {
								performanceStats.count(10);
							}
						} catch (Throwable t) {
							t.printStackTrace();
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			executor.shutdown();
			
			latch.await();
			
			Assert.assertEquals("Failure on attempt " + (trial + 1), threadCount * statCount, performanceStats.getCount());
		}
	}
	
	@Test
	public void testMax() {
		PerformanceStats performanceStats = new PerformanceStats();
		performanceStats.count(100);
		performanceStats.count(300);
		
		Assert.assertEquals(300, performanceStats.getMax());
	}
	
	@Test
	public void testMin() {
		PerformanceStats performanceStats = new PerformanceStats();
		performanceStats.count(100);
		performanceStats.count(300);
		
		Assert.assertEquals(100, performanceStats.getMin());
	}
	
	@Test
	public void testStartEnd() throws InterruptedException {
		PerformanceStats performanceStats = new PerformanceStats();
		performanceStats.countStart(Thread.currentThread().getId());
		
		Assert.assertEquals(1, performanceStats.getActiveCount());
		
		Thread.sleep(15);
		
		performanceStats.countEnd(Thread.currentThread().getId());
		
		Assert.assertTrue(performanceStats.getMax() > 10);
		Assert.assertTrue(performanceStats.getMax() < 35);
	}
	
	@Test
	public void testStartEndMultithread() throws InterruptedException {
		int threadCount = 15;
		
		for (int trial = 0; trial < 20; trial++) {
			final PerformanceStats performanceStats = new PerformanceStats();
			
			ExecutorService executor = Executors.newFixedThreadPool(100);
			
			final CountDownLatch latch = new CountDownLatch(threadCount);
			for (int i = 0; i < threadCount; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							for (int i = 0; i < 100; i++) {
								performanceStats.countStart(Thread.currentThread().getId());
								
								performanceStats.countEnd(Thread.currentThread().getId());
							}
						} catch (Throwable t) {
							t.printStackTrace();
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			executor.shutdown();
			
			latch.await();
			
			Assert.assertEquals("Failure on attempt " + (trial + 1), 0, performanceStats.getActiveCount());
		}
	}
	
	@Test
	public void testTotal() {
		PerformanceStats performanceStats = new PerformanceStats();
		for (int i = 0; i < 47; i++) {
			performanceStats.count(30);
		}
		
		Assert.assertEquals(47 * 30, performanceStats.getTotal());
	}
	
	@Test
	public void testTotalMultithread() throws InterruptedException {
		int threadCount = 15;
		
		for (int trial = 0; trial < 20; trial++) {
			final PerformanceStats performanceStats = new PerformanceStats();
			
			ExecutorService executor = Executors.newFixedThreadPool(100);
			
			final CountDownLatch latch = new CountDownLatch(threadCount);
			for (int i = 0; i < threadCount; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							for (int i = 0; i < 100; i++) {
								performanceStats.count(10);
							}
						} catch (Throwable t) {
							t.printStackTrace();
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			executor.shutdown();
			
			latch.await();
			
			Assert.assertEquals("Failure on attempt " + (trial + 1), 10 * 1500, performanceStats.getTotal());
		}
	}
	
	@Test
	public void testPerformanceList() {
		PerformanceStats performanceStats = new PerformanceStats();
		performanceStats.count(47);
		performanceStats.count(470);
		performanceStats.count(4700);
		
		Assert.assertEquals("4700,470,47", performanceStats.getPerformanceList());
	}
	
	@Test
	public void countEndWithNoStart() {
		PerformanceStats performanceStats = new PerformanceStats();
		long val = performanceStats.countEnd(Thread.currentThread().getId());
		
		Assert.assertEquals(-1, val);
	}
}