package com.janoside.stats;

import org.junit.Assert;
import org.junit.Test;

public class ValueStatsTest {
	
	@Test
	public void testMathError() {
		ValueStats vs = new ValueStats();
		vs.count(1);
		vs.count(2);
		vs.count(4);
		
		Assert.assertEquals(2.33f, vs.getAverage(), 0);
	}
}