package com.janoside.stats;

import org.junit.Assert;
import org.junit.Test;

public class DataSeriesTest {
	
	@Test
	public void testSpikeUp() {
		DataSeries<Long> ds = new DataSeries<Long>();
		ds.setWindowSize(20);
		
		for (int i = 0; i < 19; i++) {
			ds.addValue(10L);
		}
		
		Assert.assertFalse(ds.isSpikingUpward(1));
		
		Assert.assertFalse(ds.isSpikingUpward(1));
		
		ds.addValue(10L);
		
		Assert.assertFalse(ds.isSpikingUpward(1));
		
		ds.addValue(20L);
		
		Assert.assertTrue(ds.isSpikingUpward(0.01));
		
		// this is the largest spike threshold that should be met
		Assert.assertTrue(ds.isSpikingUpward(400 / 210.0 - 1));
		
		// this is another value to test, less than the above; should be easily met
		Assert.assertTrue(ds.isSpikingUpward(400 / 210.0 - 1.1));
		Assert.assertFalse(ds.isSpikingUpward(1.01));
	}
	
	@Test
	public void testMisc() {
		DataSeries<Integer> ds = new DataSeries<Integer>();
		ds.setWindowSize(3);
		
		Assert.assertEquals(0, ds.getAverage(), 0);
		Assert.assertEquals(3, ds.getWindowSize());
		
		ds.addValue(3);
		
		Assert.assertEquals(3, ds.getMax().intValue());
		Assert.assertEquals(3, ds.getMin().intValue());
		
		ds.addValue(4);
		
		Assert.assertEquals(4, ds.getMax().intValue());
		Assert.assertEquals(3, ds.getMin().intValue());
		
		ds.addValue(5);
		
		Assert.assertEquals(5, ds.getMax().intValue());
		Assert.assertEquals(3, ds.getMin().intValue());
		
		Assert.assertEquals(5, ds.getLatestValue().intValue());
	}
	
	@Test
	public void testOldStuff() {
		DataSeries<Long> valueSet = new DataSeries<Long>();
		valueSet.setWindowSize(5);
		
		valueSet.addValue(10L);
		valueSet.addValue(10L);
		valueSet.addValue(10L);
		valueSet.addValue(10L);
		valueSet.addValue(10L);
		
		Assert.assertEquals(50.0, valueSet.getTotal(), 0);
		Assert.assertEquals(10.0, valueSet.getAverage(), 0);
		
		valueSet.addValue(20L);
		
		Assert.assertEquals(60.0, valueSet.getTotal(), 0);
		Assert.assertEquals(60.0 / 5, valueSet.getAverage(), 0);
		
		valueSet.addValue(20L);
		
		Assert.assertEquals(70.0, valueSet.getTotal(), 0);
		Assert.assertEquals(70.0 / 5, valueSet.getAverage(), 0);
	}
	
	@Test
	public void testResize() {
		DataSeries<Long> valueSet = new DataSeries<Long>();
		valueSet.setWindowSize(5);
		
		valueSet.addValue(10L);
		valueSet.addValue(10L);
		valueSet.addValue(10L);
		valueSet.addValue(10L);
		valueSet.addValue(10L);
		
		Assert.assertEquals(50.0, valueSet.getTotal(), 0);
		Assert.assertEquals(10.0, valueSet.getAverage(), 0);
		
		valueSet.addValue(20L);
		
		Assert.assertEquals(60.0, valueSet.getTotal(), 0);
		Assert.assertEquals(60.0 / 5, valueSet.getAverage(), 0);
		
		valueSet.setWindowSize(6);
		valueSet.addValue(20L);
		
		Assert.assertEquals(80.0, valueSet.getTotal(), 0);
		Assert.assertEquals(80.0 / 6, valueSet.getAverage(), 0);
	}
}