package com.janoside.stats;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Assert;
import org.junit.Test;

public class EventTrackerTest {
	
	@Test
	public void testCount() {
		EventTracker tracker = new EventTracker();
		tracker.setName("abc");
		
		Assert.assertEquals(-1, tracker.getSecondsSinceLastOccurrence());
		Assert.assertEquals("abc", tracker.getName());
		
		for (int i = 0; i < 975; i++) {
			tracker.increment();
		}
		
		Assert.assertEquals(975, tracker.getCount());
	}
	
	@Test
	public void testDefaultRate() {
		EventTracker tracker = new EventTracker();
		
		Assert.assertEquals(0.0f, tracker.getRate(), 0);
	}
	
	@Test
	public void testMisc() {
		EventTracker et = new EventTracker();
		et.setRateMeasureWindowSize(3);
		
		Assert.assertEquals(3, et.getRateMeasureWindowSize());
	}
	
	@Test
	public void testResetCount() {
		EventTracker tracker = new EventTracker();
		for (int i = 0; i < 975; i++) {
			tracker.increment();
		}
		
		Assert.assertEquals(975, tracker.getCount());
		
		tracker.reset();
		
		Assert.assertEquals(0, tracker.getCount());
	}
	
	@Test
	public void testResetRate() {
		EventTracker tracker = new EventTracker();
		for (int i = 0; i < 975; i++) {
			tracker.increment();
		}
		
		Assert.assertTrue(tracker.getRate() > 0);
		
		tracker.reset();
		
		Assert.assertEquals(0.0f, tracker.getRate(), 0);
	}
	
	@Test
	public void testRate() throws InterruptedException {
		int count = 150;
		long sleep = 5;
		
		EventTracker tracker = new EventTracker();
		
		for (int i = 0; i < count; i++) {
			tracker.increment();
			
			Thread.sleep(sleep);
		}
		
		Assert.assertEquals(count, tracker.getCount());
		Assert.assertTrue("Rate should be " + (1000.0 / sleep) + " but is " + tracker.getRate(), tracker.getRate() > 0.7 * (1000.0 / sleep));
		Assert.assertTrue("Rate should be " + (1000.0 / sleep) + " but is " + tracker.getRate(), tracker.getRate() < 1.3 * (1000.0 / sleep));
		
		tracker.reset();
		
		sleep = 5;
		
		for (int i = 0; i < count; i++) {
			tracker.increment();
			
			Thread.sleep(sleep);
		}
		
		Assert.assertEquals(count, tracker.getCount());
		Assert.assertTrue("Rate should be " + (1000.0 / sleep) + " but is " + tracker.getRate(), tracker.getRate() > 0.7 * (1000.0 / sleep));
		Assert.assertTrue("Rate should be " + (1000.0 / sleep) + " but is " + tracker.getRate(), tracker.getRate() < 1.3 * (1000.0 / sleep));
	}
	
	@Test
	public void testSecondsSinceLastOccurrence() throws InterruptedException {
		EventTracker tracker = new EventTracker();
		tracker.increment();
		
		Assert.assertTrue(tracker.getSecondsSinceLastOccurrence() < 1);
		
		Thread.sleep(1100);
		
		Assert.assertTrue(tracker.getSecondsSinceLastOccurrence() >= 1);
		Assert.assertTrue(tracker.getSecondsSinceLastOccurrence() <= 3);
	}
	
	@Test
	public void testCountMultithread() throws InterruptedException {
		for (int trial = 0; trial < 100; trial++) {
			final EventTracker tracker = new EventTracker();
			
			ExecutorService executor = Executors.newFixedThreadPool(100);
			
			int threads = 15;
			final int count = 100;
			
			final CountDownLatch latch = new CountDownLatch(threads);
			for (int i = 0; i < threads; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							for (int i = 0; i < count; i++) {
								tracker.increment();
							}
						} catch (Throwable t) {
							t.printStackTrace();
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			latch.await();
			
			Assert.assertEquals("Failure on attempt " + (trial + 1), 1500, tracker.getCount());
		}
	}
	
	@Test
	public void testLastOccurrence() {
		EventTracker tracker = new EventTracker();
		
		long start = System.currentTimeMillis();
		
		tracker.increment();
		
		long end = System.currentTimeMillis();
		
		Assert.assertTrue(tracker.getLastOccurenceTime() >= start);
		Assert.assertTrue(tracker.getLastOccurenceTime() <= end);
	}
	
	@Test
	public void testRateFast() throws InterruptedException {
		for (int trial = 0; trial < 100; trial++) {
			final EventTracker tracker = new EventTracker();
			
			ExecutorService executor = Executors.newFixedThreadPool(100);
			
			int threads = 15;
			final int count = 100;
			
			final CountDownLatch latch = new CountDownLatch(threads);
			for (int i = 0; i < threads; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							for (int i = 0; i < count; i++) {
								tracker.increment();
							}
						} catch (Throwable t) {
							t.printStackTrace();
							
						} finally {
							latch.countDown();
						}
					}
				});
			}
			
			executor.shutdown();
			
			latch.await();
			
			Assert.assertFalse("Whoops, it's infinity", tracker.getRate() == Float.POSITIVE_INFINITY);
			
			Assert.assertEquals("Failure on attempt " + (trial + 1), 1500, tracker.getCount());
		}
	}
}