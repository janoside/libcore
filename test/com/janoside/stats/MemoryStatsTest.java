package com.janoside.stats;

import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

public class MemoryStatsTest {
	
	@Test
	public void testThreadSafePerformances() throws InterruptedException {
		int trials = 2;
		int trialSize = 1000;
		
		for (int trial = 0; trial < trials; trial++) {
			final MemoryStats stats = new MemoryStats();
			stats.setThreadSafe(true);
			
			final AtomicInteger counter = new AtomicInteger(0);
			final AtomicInteger threadCounter = new AtomicInteger(0);
			
			long startTime = System.currentTimeMillis();
			
			ExecutorService executor = Executors.newFixedThreadPool(500);
			for (int i = 0; i < trialSize; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							String uuid = UUID.randomUUID().toString();
							
							stats.countPerformanceStart(uuid);
							
							try {
								Thread.sleep(100);
								
							} catch (InterruptedException e) {
								throw new RuntimeException(e);
							}
							
							stats.countPerformanceEnd(uuid);
							
						} catch (NullPointerException npe) {
							counter.getAndIncrement();
						}
						
						threadCounter.getAndIncrement();
					}
				});
			}
			
			Thread.sleep(100);
			
			while (threadCounter.get() < trialSize) {
				stats.clearPerformances();
			}
			
			executor.shutdown();
			executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
			
			System.out.println(System.currentTimeMillis() - startTime);
			
			Assert.assertEquals(0, counter.get());
		}
	}
	
	@Test
	public void testThreadUnsafePerformances() throws InterruptedException {
		int trials = 2;
		int trialSize = 1000;
		
		for (int trial = 0; trial < trials; trial++) {
			final MemoryStats stats = new MemoryStats();
			stats.setThreadSafe(false);
			
			final AtomicInteger counter = new AtomicInteger(0);
			final AtomicInteger threadCounter = new AtomicInteger(0);
			
			ExecutorService executor = Executors.newFixedThreadPool(500);
			for (int i = 0; i < trialSize; i++) {
				executor.execute(new Runnable() {
					public void run() {
						try {
							String uuid = UUID.randomUUID().toString();
							
							stats.countPerformanceStart(uuid);
							
							try {
								Thread.sleep(100);
								
							} catch (InterruptedException e) {
								throw new RuntimeException(e);
							}
							
							stats.countPerformanceEnd(uuid);
							
						} catch (NullPointerException npe) {
							counter.getAndIncrement();
						}
						
						threadCounter.getAndIncrement();
					}
				});
			}
			
			Thread.sleep(100);
			
			while (threadCounter.get() < trialSize) {
				stats.clearPerformances();
			}
			
			executor.shutdown();
			executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
			
			Assert.assertTrue(counter.get() >= 0);
		}
	}
	
	@Test
	public void testMisc() {
		MemoryStats ms = new MemoryStats();
		ms.countEvent("abc");
		
		Assert.assertEquals(1, ms.getEvents().get("abc").getCount());
		
		ms.countException(new RuntimeException());
		ms.countException(new RuntimeException("HAHA!"));
		
		Assert.assertEquals(2, ms.getExceptions().size());
		Assert.assertNotNull(ms.printExceptionReport());
		
		ms.countEvent("abc");
		ms.countPerformance("abc", 1);
		ms.countValue("abc", 2);
		
		Assert.assertEquals(1, ms.getEvents().size());
		Assert.assertEquals(1, ms.getPerformances().size());
		Assert.assertEquals(1, ms.getValues().size());
		
		ms.clearEvents();
		ms.clearExceptions();
		ms.clearPerformances();
		ms.clearValues();
		
		Assert.assertEquals(0, ms.getEvents().size());
		Assert.assertEquals(0, ms.getPerformances().size());
		Assert.assertEquals(0, ms.getValues().size());
		Assert.assertEquals(0, ms.getExceptions().size());
		
		Assert.assertFalse(ms.getThreadSafe());
		
		ms.setThreadSafe(true);
		
		Assert.assertTrue(ms.getThreadSafe());
	}
}