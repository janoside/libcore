package com.janoside.stats;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;

public class MemoryStatsStatTrackerTest {
	
	@Test
	public void testBasicFunctionality() throws Exception {
		MemoryStats ms = new MemoryStats();
		
		MemoryStatsStatTracker st1 = new MemoryStatsStatTracker();
		st1.setMemoryStats(ms);
		st1.setExceptionHandler(new StandardErrorExceptionHandler());
		
		DelegatingStatTracker st = new DelegatingStatTracker();
		st.addStatTracker(st1);
		
		st.trackEvent("event1");
		st.trackEvent("event3", 3);
		
		st.trackPerformance("perf1000", 1000);
		
		st.trackThreadPerformanceStart("threadPerf");
		
		Thread.sleep(10);
		
		st.trackThreadPerformanceEnd("threadPerf");
		
		st.trackValue("valueA", 1);
		st.trackValue("valueA", 2);
		
		Assert.assertEquals(1, ms.getEvents().get("event1").getCount());
		Assert.assertEquals(3, ms.getEvents().get("event3").getCount());
		
		Assert.assertEquals(1000, ms.getPerformances().get("perf1000").getAverage(), 0);
		Assert.assertTrue(ms.getPerformances().get("threadPerf").getAverage() >= 10);
		
		Assert.assertEquals(1.5, ms.getValues().get("valueA").getAverage(), 0);
		Assert.assertEquals(2, ms.getValues().get("valueA").getCount());
		
		
		Assert.assertNotNull(ms.printEventReport());
		Assert.assertNotNull(ms.printPerformanceReport());
		Assert.assertNotNull(ms.printExceptionReport());
		Assert.assertNotNull(ms.printValuesReport());
		Assert.assertNotNull(ms.printUnfinishedPerformances());
	}
	
	@Test
	public void testCleanup() throws Exception {
		MemoryStats ms = new MemoryStats();
		
		MemoryStatsStatTracker st1 = new MemoryStatsStatTracker(5);
		st1.setMemoryStats(ms);
		st1.setExceptionHandler(new StandardErrorExceptionHandler());
		
		final DelegatingStatTracker st = new DelegatingStatTracker();
		st.addStatTracker(st1);
		
		new Thread(new Runnable() {
			public void run() {
				st.trackThreadPerformanceStart("threadPerf");
			}
		}).start();
		
		
		Thread.sleep(10);
		
		
		Assert.assertNotNull(ms.printEventReport());
		Assert.assertNotNull(ms.printPerformanceReport());
		Assert.assertNotNull(ms.printExceptionReport());
		Assert.assertNotNull(ms.printValuesReport());
		Assert.assertNotNull(ms.printUnfinishedPerformances());
	}
}