package com.janoside.stats;

import org.junit.Test;

public class BlackHoleStatTrackerTest {
	
	@Test
	public void testIt() {
		BlackHoleStatTracker st = new BlackHoleStatTracker();
		
		st.trackEvent("abc");
		st.trackEvent("abc", 1);
		st.trackPerformance("abc", 1);
		st.trackThreadPerformanceStart("abc");
		st.trackThreadPerformanceEnd("abc");
		st.trackValue("abc", 1);
	}
}