package com.janoside.stats;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.DelegatingExceptionHandler;
import com.janoside.exception.ExceptionHandler;
import com.janoside.exception.StatTrackerExceptionHandler;
import com.janoside.util.RandomUtil;

public class DelegatingStatTrackerTest {
	
	@Test
	public void testDeadlockDecember2013() {
		DelegatingExceptionHandler exceptionHandler = new DelegatingExceptionHandler();
		
		MemoryStats memoryStats = new MemoryStats();
		
		MemoryStatsStatTracker memoryStatsStatTracker = new MemoryStatsStatTracker();
		memoryStatsStatTracker.setMemoryStats(memoryStats);
		memoryStatsStatTracker.setExceptionHandler(exceptionHandler);
		
		final DelegatingStatTracker statTracker = new DelegatingStatTracker();
		statTracker.setExceptionHandler(exceptionHandler);
		
		StatTrackerExceptionHandler statTrackerExceptionHandler = new StatTrackerExceptionHandler();
		statTrackerExceptionHandler.setStatTracker(statTracker);
		
		final AtomicInteger exceptionCounter = new AtomicInteger(0);
		
		exceptionHandler.addExceptionHandler(statTrackerExceptionHandler);
		exceptionHandler.addExceptionHandler(new ExceptionHandler() {
			public void handleException(Throwable t) {
				exceptionCounter.getAndIncrement();
				
				throw new RuntimeException("handleException...BOOM!");
			}
		});
		
		statTracker.addStatTracker(memoryStatsStatTracker);
		statTracker.addStatTracker(new StatTracker() {
			
			public void trackValue(String name, float value) {
			}
			
			public void trackThreadPerformanceStart(String name) {
			}
			
			public void trackThreadPerformanceEnd(String name) {
				if (RandomUtil.randomBoolean(0.01)) {
					throw new RuntimeException("trackThreadPerformanceEnd...BOOM!");
				}
			}
			
			public void trackPerformance(String name, long millis) {
			}
			
			public void trackEvent(String name) {
			}
			
			public void trackEvent(String name, int count) {
			}
		});
		
		int count = 10000;
		ExecutorService executorService = Executors.newFixedThreadPool(100);
		final CountDownLatch latch = new CountDownLatch(count);
		
		for (int i = 0; i < count; i++) {
			executorService.execute(new Runnable() {
				public void run() {
					try {
						statTracker.trackThreadPerformanceStart("abc");
						
						try {
							Thread.sleep(RandomUtil.randomLong(20));
							
						} catch (InterruptedException ie) {
							ie.printStackTrace();
						}
						
						statTracker.trackThreadPerformanceEnd("abc");
						
					} catch (Throwable t) {
						t.printStackTrace();
						
					} finally {
						latch.countDown();
					}
				}
			});
		}
		
		try {
			latch.await();
			
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		
		// there should be on the order of 100 exceptions
		Assert.assertTrue(exceptionCounter.get() > 75);
	}
	
	@Test
	public void testRemove() {
		MemoryStatsStatTracker st = new MemoryStatsStatTracker();
		
		final DelegatingStatTracker statTracker = new DelegatingStatTracker();
		statTracker.addStatTracker(st);
		statTracker.removeStatTracker(st);
	}
}