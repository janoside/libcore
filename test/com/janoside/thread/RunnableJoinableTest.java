package com.janoside.thread;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

public class RunnableJoinableTest {
	
	private static final ExecutorService executor = Executors.newCachedThreadPool();
	
	@Test
	public void testIt() {
		final AtomicInteger counter = new AtomicInteger(0);
		
		RunnableJoinable rj = new RunnableJoinable(new Runnable() {
			public void run() {
				counter.getAndIncrement();
			}
		});
		
		executor.execute(rj);
		
		rj.join();
		
		Assert.assertEquals(1, counter.get());
	}
	
	@Test
	public void testTimed() {
		final AtomicInteger counter = new AtomicInteger(0);
		
		RunnableJoinable rj = new RunnableJoinable(new Runnable() {
			public void run() {
				counter.getAndIncrement();
			}
		});
		
		executor.execute(rj);
		
		rj.join(1000);
		
		Assert.assertEquals(1, counter.get());
	}
	
	@Test
	public void testTimeout() {
		final AtomicInteger counter = new AtomicInteger(0);
		
		RunnableJoinable rj = new RunnableJoinable(new Runnable() {
			public void run() {
				counter.getAndIncrement();
			}
		});
		
		rj.join(0);
		
		Assert.assertEquals(0, counter.get());
	}
	
	@Test
	public void testInterrupt() throws Exception {
		final AtomicInteger counter = new AtomicInteger(0);
		
		RunnableJoinable rj = new RunnableJoinable(new Runnable() {
			public void run() {
				counter.getAndIncrement();
			}
		});
		
		final Thread testThread = Thread.currentThread();
		new Timer("abc").schedule(new TimerTask() {
				public void run() {
					testThread.interrupt();
				}
			},
			25);
		
		try {
			rj.join();
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		Assert.assertEquals(0, counter.get());
	}
	
	@Test
	public void testTimedInterrupt() throws Exception {
		final AtomicInteger counter = new AtomicInteger(0);
		
		RunnableJoinable rj = new RunnableJoinable(new Runnable() {
			public void run() {
				counter.getAndIncrement();
			}
		});
		
		final Thread testThread = Thread.currentThread();
		new Timer("abc").schedule(new TimerTask() {
				public void run() {
					testThread.interrupt();
				}
			},
			25);
		
		try {
			rj.join(1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		Assert.assertEquals(0, counter.get());
	}
	
	@Test
	public void testException() {
		RunnableJoinable rj = new RunnableJoinable(new Runnable() {
			public void run() {
				throw new RuntimeException("BOOM");
			}
		});
		
		executor.execute(rj);
		
		rj.join();
	}
}