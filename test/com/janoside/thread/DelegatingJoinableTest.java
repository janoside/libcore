package com.janoside.thread;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.ExceptionHandler;

public class DelegatingJoinableTest {
	
	@Test
	public void testNoLimit() throws InterruptedException {
		final int count = 5;
		final int smallDelay = 15;
		
		DelegatingJoinable dj = new DelegatingJoinable();
		
		final AtomicInteger counter = new AtomicInteger(0);
		
		ArrayList<Joinable> joinables = new ArrayList<Joinable>();
		for (int i = 0; i < count; i++) {
			Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						Thread.sleep(smallDelay);
						
					} catch (InterruptedException e) {
						e.printStackTrace();
						
					} finally {
						counter.getAndIncrement();
					}
				}
			});
			
			joinables.add(new ThreadJoinable(thread));
			
			thread.start();
		}
		
		dj.setJoinables(joinables);
		
		dj.join();
		
		Assert.assertEquals(count, counter.get());
	}
	
	@Test
	public void testLimit() throws InterruptedException {
		final int count = 5;
		final int smallDelay = 15;
		
		DelegatingJoinable dj = new DelegatingJoinable();
		
		final AtomicInteger counter = new AtomicInteger(0);
		
		ArrayList<Joinable> joinables = new ArrayList<Joinable>();
		for (int i = 0; i < count; i++) {
			final int index = i;
			
			Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						Thread.sleep((index + 1) * smallDelay);
						
					} catch (InterruptedException e) {
						e.printStackTrace();
						
					} finally {
						counter.getAndIncrement();
					}
				}
			});
			
			joinables.add(new ThreadJoinable(thread));
			
			thread.start();
		}
		
		dj.setJoinables(joinables);
		
		dj.join((int) (2.5 * smallDelay));
		
		Assert.assertEquals(2, counter.get());
	}
	
	@Test
	public void testExceptions() {
		final AtomicInteger counter = new AtomicInteger(0);
		
		ArrayList<Joinable> joinables = new ArrayList<Joinable>();
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Thread.sleep(100);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
					
				} finally {
					counter.getAndIncrement();
				}
			}
		};
		
		joinables.add(new RunnableJoinable(runnable));
		joinables.add(new RunnableJoinable(runnable));
		
		final Thread testThread = Thread.currentThread();
		new Timer("abc").schedule(new TimerTask() {
				public void run() {
					testThread.interrupt();
				}
			},
			15);
		
		new Timer("abc").schedule(new TimerTask() {
				public void run() {
					testThread.interrupt();
				}
			},
			30);
		
		final AtomicInteger exCounter = new AtomicInteger(0);
		ExceptionHandler eh = new ExceptionHandler() {
			public void handleException(Throwable t) {
				exCounter.getAndIncrement();
			}
		};
		
		DelegatingJoinable dj = new DelegatingJoinable(joinables);
		dj.setExceptionHandler(eh);
		
		dj.join();
		
		Assert.assertEquals(0, counter.get());
		Assert.assertEquals(1, exCounter.get());
	}
	
	@Test
	public void testExceptionsTimed() {
		final AtomicInteger counter = new AtomicInteger(0);
		
		ArrayList<Joinable> joinables = new ArrayList<Joinable>();
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					Thread.sleep(100);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
					
				} finally {
					counter.getAndIncrement();
				}
			}
		};
		
		joinables.add(new RunnableJoinable(runnable));
		joinables.add(new RunnableJoinable(runnable));
		
		final Thread testThread = Thread.currentThread();
		new Timer("abc").schedule(new TimerTask() {
				public void run() {
					testThread.interrupt();
				}
			},
			15);
		
		new Timer("abc").schedule(new TimerTask() {
				public void run() {
					testThread.interrupt();
				}
			},
			30);
		
		final AtomicInteger exCounter = new AtomicInteger(0);
		ExceptionHandler eh = new ExceptionHandler() {
			public void handleException(Throwable t) {
				exCounter.getAndIncrement();
			}
		};
		
		DelegatingJoinable dj = new DelegatingJoinable(joinables);
		dj.setExceptionHandler(eh);
		
		dj.join(1000);
		
		Assert.assertEquals(0, counter.get());
		Assert.assertEquals(1, exCounter.get());
	}
}