package com.janoside.thread;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

public class ThreadJoinableTest {
	
	@Test
	public void testInterrupt() throws Exception {
		final AtomicInteger counter = new AtomicInteger(0);
		
		final Thread th = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(1000);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		th.start();
		
		final ThreadJoinable tj = new ThreadJoinable(th);
		
		final Thread testThread = Thread.currentThread();
		new Timer("abc").schedule(new TimerTask() {
				public void run() {
					testThread.interrupt();
					th.interrupt();
				}
			},
			25);
		
		try {
			tj.join();
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		Assert.assertEquals(0, counter.get());
	}
	
	@Test
	public void testInterruptTimed() throws Exception {
		final AtomicInteger counter = new AtomicInteger(0);
		
		final Thread th = new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(1000);
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		
		th.start();
		
		final ThreadJoinable tj = new ThreadJoinable(th);
		
		final Thread testThread = Thread.currentThread();
		new Timer("abc").schedule(new TimerTask() {
				public void run() {
					testThread.interrupt();
					th.interrupt();
				}
			},
			25);
		
		try {
			tj.join(1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		Assert.assertEquals(0, counter.get());
	}
}