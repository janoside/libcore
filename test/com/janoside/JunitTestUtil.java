package com.janoside;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Assert;

public class JunitTestUtil {
	
	// Ref: http://sourceforge.net/p/cobertura/bugs/92/
	public static void reflectiveInvokeInstanceMethodWhichTakesObjectWithArgument(final Object instance, final String methodName, final Object arguments) {
		try {
			final Method method = instance.getClass().getMethod(methodName, new Class[] { Object.class });
			
			method.invoke(instance, arguments);
			
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void reflectiveInvokeVoidInstanceMethod(final Object instance, final String methodName) {
		try {
			final Method method = instance.getClass().getMethod(methodName);
			
			method.invoke(instance);
			
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void testConstructorIsPrivate(Class c) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		Constructor constructor = c.getDeclaredConstructor();
		
		Assert.assertTrue(Modifier.isPrivate(constructor.getModifiers()));
		
		constructor.setAccessible(true);
		constructor.newInstance();
	}
}