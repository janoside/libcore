package com.janoside.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class JsonArrayTest {
	
	@Test
	public void testSerialization() throws IOException {
		JsonObject json = new JsonObject();
		json.put("one", "HAHA");
		
		JsonArray jsonArray = new JsonArray();
		jsonArray.put(new JsonObject());
		jsonArray.put(json);
		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		ObjectOutputStream objectStream = new ObjectOutputStream(byteStream);
		
		objectStream.writeObject(jsonArray);
	}
	
	@Test
	public void testMore() {
		JsonArray ja = new JsonArray();
		ja.addAll(Arrays.asList("one", "two"));
		
		Assert.assertEquals("[\"one\",\"two\"]", ja.toString());
		Assert.assertTrue(ja.contains("one"));
		Assert.assertFalse(ja.contains("xyz"));
		Assert.assertTrue(ja.containsAll(Arrays.asList("one", "two")));
		Assert.assertFalse(ja.containsAll(Arrays.asList("one", "two", "xyz")));
		Assert.assertFalse(ja.isEmpty());
		Assert.assertEquals(2, ja.size());
		
		ja.clear();
		
		Assert.assertEquals("[]", ja.toString());
		Assert.assertTrue(ja.isEmpty());
		
		ja.addAll(Arrays.asList("one", "two"));
		ja.removeByIndex(0);
		
		Assert.assertEquals("[\"two\"]", ja.toString());
		Assert.assertEquals(1, ja.size());
		
		Assert.assertFalse(ja.remove("one"));
		Assert.assertEquals("[\"two\"]", ja.toString());
		
		Assert.assertTrue(ja.remove("two"));
		Assert.assertEquals("[]", ja.toString());
		
		ja.addAll(Arrays.asList("one", "two", "three"));
		ja.removeAll(Arrays.asList("one", "three"));
		
		Assert.assertEquals("[\"two\"]", ja.toString());
		
		ja.clear();
		ja.addAll(Arrays.asList("one", "two", "three"));
		ja.retainAll(Arrays.asList("one", "three"));
		
		Assert.assertEquals("[\"one\",\"three\"]", ja.toString());
		
		ja.retainAll(Arrays.asList("three", "one"));
		
		Assert.assertEquals("[\"one\",\"three\"]", ja.toString());
		
		Object[] arr = ja.toArray();
		
		Assert.assertEquals(2, arr.length);
		Assert.assertEquals("one", arr[0]);
		Assert.assertEquals("three", arr[1]);
		
		String[] arr2 = new String[]{};
		arr2 = (String[]) ja.toArray(arr2);
		
		Assert.assertEquals(2, arr.length);
		Assert.assertEquals("one", arr[0]);
		Assert.assertEquals("three", arr[1]);
		
		ja.add(new JsonArray(Arrays.asList(1, 2)));
		
		List list = ja.toList();
		
		Assert.assertEquals("[one, three, [1, 2]]", list.toString());
		
		Iterator iterator = ja.iterator();
		list = new ArrayList();
		while (iterator.hasNext()) {
			Object obj = iterator.next();
			
			list.add(obj);
		}
		
		Assert.assertEquals("[one, three, [1,2]]", list.toString());
		
		Assert.assertEquals("[\n  \"one\",\n  \"three\",\n  [\n    1,\n    2\n  ]\n]", ja.toString(2));
		Assert.assertEquals("[\n    \"one\",\n    \"three\",\n    [\n      1,\n      2\n    ]\n  ]", ja.toString(2, 2));
		
		Assert.assertEquals("[]", new JsonArray().toString(1, 1));
		Assert.assertEquals("[\"one\"]", new JsonArray(Arrays.asList("one")).toString(1, 1));
		Assert.assertEquals("[]", new JsonArray((List)null).toString());
	}
	
	@Test
	public void testParsing() throws Exception {
		try {
			new JsonArray("abc");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			new JsonArray("[");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		JsonArray j = new JsonArray("[]");
		Assert.assertEquals(0, j.length());
		
		j = new JsonArray("[1]");
		Assert.assertEquals(1, j.length());
		
		j = new JsonArray("[1,2      ,      3]");
		Assert.assertEquals(3, j.length());
		
		try {
			new JsonArray("[1;2]");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		j = new JsonArray("[1,2      ,     ]");
		Assert.assertEquals(2, j.length());
		
		j = new JsonArray("[1,2      ,    , ]");
		Assert.assertEquals(3, j.length());
		Assert.assertEquals(JsonObject.NULL, j.get(2));
	}
	
	@Test
	public void testGet() throws Exception {
		JsonArray j = new JsonArray();
		j.put(true);
		Assert.assertTrue(j.getBoolean(0));
		
		j.put("true");
		Assert.assertTrue(j.getBoolean(1));
		
		j.put("false");
		Assert.assertFalse(j.getBoolean(2));
		
		j.put(Boolean.TRUE);
		Assert.assertTrue(j.getBoolean(3));
		
		j.put(Boolean.FALSE);
		Assert.assertFalse(j.getBoolean(4));
		
		j.put("string");
		
		try {
			j.getBoolean(5);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		j.put(new Object());
		
		try {
			j.getBoolean(6);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		
		j.clear();
		
		j.put("1.0");
		Assert.assertEquals(1.0, j.getDouble(0), 0);
		
		j.put(1);
		Assert.assertEquals(1.0, j.getDouble(1), 0);
		
		j.put("onepointzero");
		
		try {
			j.getDouble(2);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		j.clear();
		
		j.put(1);
		Assert.assertEquals(1, j.getInt(0));
		
		j.put(1.0);
		Assert.assertEquals(1, j.getInt(1));
		
		j.put("1.0");
		Assert.assertEquals(1, j.getInt(2));
		
		
		j.clear();
		
		try {
			j.getJsonArray(0);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		j.put(new JsonArray());
		
		Assert.assertNotNull(j.getJsonArray(0));
		
		j.put(new Object());
		
		try {
			j.getJsonArray(1);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		
		j.clear();
		
		try {
			j.getJsonObject(0);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		j.put(new JsonObject());
		
		Assert.assertNotNull(j.getJsonObject(0));
		
		j.put(new Object());
		
		try {
			j.getJsonObject(1);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		
		
		j.clear();
		
		j.put(1);
		Assert.assertEquals(1, j.getLong(0));
		
		j.put(1.0);
		Assert.assertEquals(1, j.getLong(1));
		
		j.put("1.0");
		Assert.assertEquals(1, j.getLong(2));
		
		
		j.clear();
		
		j.put("abc");
		Assert.assertEquals("abc", j.getString(0));
		
		j.put(new JsonArray());
		Assert.assertEquals("[]", j.getString(1));
		
		
		
		j.clear();
		
		
		Assert.assertTrue(j.isNull(0));
		
		j.put("abc");
		Assert.assertFalse(j.isNull(0));
		
		j.put((Object) null);
		Assert.assertTrue(j.isNull(1));
		
		j.put(JsonObject.NULL);
		Assert.assertTrue(j.isNull(2));
		
		
		
		Assert.assertNull(j.opt(-1));
		Assert.assertNull(j.opt(10000));
		
		
		
		j.clear();
		
		
		j.put(true);
		Assert.assertTrue(j.optBoolean(0));
		Assert.assertFalse(j.optBoolean(1));
		
		
		j.clear();
		
		j.put(1.0);
		Assert.assertEquals(1.0, j.optDouble(0), 0);
		Assert.assertEquals(Double.NaN, j.optDouble(1), 0);
		
		
		j.clear();
		
		j.put(1);
		Assert.assertEquals(1, j.optInt(0));
		Assert.assertEquals(0, j.optInt(1));
		
		j.clear();
		
		j.put("one");
		Assert.assertNull(j.optJsonArray(0));
		Assert.assertNull(j.optJsonArray(1));
		
		j.put(new JsonArray());
		Assert.assertNotNull(j.optJsonArray(1));
		
		j.clear();
		
		j.put("one");
		Assert.assertNull(j.optJsonObject(0));
		Assert.assertNull(j.optJsonObject(1));
		
		j.put(new JsonObject());
		Assert.assertNotNull(j.optJsonObject(1));
		
		
		j.clear();
		
		j.put(1);
		Assert.assertEquals(1, j.optLong(0));
		Assert.assertEquals(0, j.optLong(1));
		
		
		j.clear();
		
		Assert.assertEquals("", j.optString(0));
		j.put("abc");
		Assert.assertEquals("abc", j.optString(0));
		j.put(new JsonArray());
		Assert.assertEquals("[]", j.optString(1));
		
		
		
		j.clear();
		
		j.put(false);
		Assert.assertFalse(j.getBoolean(0));
		
		j.put(Arrays.asList(1, 2));
		Assert.assertEquals("[false,[1,2]]", j.toString());
		
		j.put(1000000000000L);
		Assert.assertEquals("[false,[1,2],1000000000000]", j.toString());
		
		j.put(new HashMap() {{ put("abc", "def"); }});
		
		Assert.assertEquals("[false,[1,2],1000000000000,{\"abc\":\"def\"}]", j.toString());
		
		
		j.put(0, true);
		Assert.assertTrue(j.getBoolean(0));
		
		j.put(0, false);
		Assert.assertFalse(j.getBoolean(0));
		
		j.put(0, Arrays.asList(1, 2));
		Assert.assertEquals("[[1,2],[1,2],1000000000000,{\"abc\":\"def\"}]", j.toString());
		
		j.put(2, 1.0);
		Assert.assertEquals("[[1,2],[1,2],1,{\"abc\":\"def\"}]", j.toString());
		
		j.put(3, 3);
		Assert.assertEquals("[[1,2],[1,2],1,3]", j.toString());
		
		j.put(0, 0L);
		Assert.assertEquals("[0,[1,2],1,3]", j.toString());
		
		j.put(1, new HashMap() {{ put("abc", 1); }});
		Assert.assertEquals("[0,{\"abc\":1},1,3]", j.toString());
		
		try {
			j.put(-1, 0);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		j.clear();
		
		j.put(3, 3);
		Assert.assertEquals("[null,null,null,3]", j.toString());
		
		Assert.assertNull(j.toJsonObject(null));
		Assert.assertNull(j.toJsonObject(new JsonArray()));
		
		j.clear();
		
		Assert.assertNull(j.toJsonObject(new JsonArray(Arrays.asList("one"))));
		
		j = new JsonArray(Arrays.asList(1));
		JsonObject jo = j.toJsonObject(new JsonArray(Arrays.asList("one")));
		
		Assert.assertEquals("{\"one\":1}", jo.toString());
	}
	
	@Test
	public void testWrite() throws Exception {
		StringWriter sw = new StringWriter();
		
		JsonArray j = new JsonArray();
		j.put("abc");
		
		j.write(sw);
		
		Assert.assertEquals("[\"abc\"]", sw.toString());
		
		sw = new StringWriter();
		j.put("def");
		j.write(sw);
		
		Assert.assertEquals("[\"abc\",\"def\"]", sw.toString());
		
		sw = new StringWriter();
		j.put(new JsonObject().put("one", "two"));
		j.write(sw);
		
		Assert.assertEquals("[\"abc\",\"def\",{\"one\":\"two\"}]", sw.toString());
		
		
		sw = new StringWriter();
		j.put(new JsonArray(Arrays.asList("one")));
		j.write(sw);
		
		Assert.assertEquals("[\"abc\",\"def\",{\"one\":\"two\"},[\"one\"]]", sw.toString());
		
		try {
			j.write(new Writer() {
				public void write(char[] cbuf, int off, int len) throws IOException {
					throw new IOException();
				}
				
				public void flush() throws IOException {
				}
				
				public void close() throws IOException {
				}
			});
			
			throw new Exception();
			
		} catch (JsonException je) {
			// expected
			Assert.assertTrue(je.getCause() instanceof IOException);
		}
	}
	
	@Test
	public void testDumbConstructor() throws Exception {
		try {
			new JsonArray(new Object());
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}