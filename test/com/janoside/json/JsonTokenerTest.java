package com.janoside.json;

import org.junit.Assert;
import org.junit.Test;

public class JsonTokenerTest {
	
	@Test
	public void testNext() throws Exception {
		JsonTokener jt = new JsonTokener("{}");
		Assert.assertEquals('{', jt.next('{'));
		
		try {
			jt.next('a');
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}