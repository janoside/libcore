package com.janoside.json;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class JsonObjectTest {
	
	@Test
	public void testSerialization() throws IOException {
		JsonObject json = new JsonObject();
		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		ObjectOutputStream objectStream = new ObjectOutputStream(byteStream);
		
		objectStream.writeObject(json);
		
		System.out.println(byteStream.toByteArray());
	}
	
	@Test
	public void testToString() throws Exception {
		JsonObject j = new JsonObject();
		j.put("a", "1");
		
		Assert.assertEquals("{\"a\":\"1\"}", j.toString());
		
		j.put("b", "2");
		
		Assert.assertEquals("{\"b\":\"2\",\"a\":\"1\"}", j.toString());
		
		j.put("b", 1);
		j.put("c", 1L);
		j.put("d", true);
		
		JsonObject j2 = new JsonObject(j.toString());
		
		Assert.assertEquals("1", j2.get("a"));
		Assert.assertEquals(1, j2.get("b"));
		Assert.assertEquals(1, j2.get("c"));
		Assert.assertEquals(true, j2.get("d"));
		
		j.put("e", new JsonObject().put("one", "two"));
		j.put("f", new JsonArray(Arrays.asList("x")));
		
		j2 = new JsonObject(j.toString());
		
		Assert.assertEquals("{\"one\":\"two\"}", j2.get("e").toString());
		Assert.assertEquals("[\"x\"]", j2.get("f").toString());
		
		j.put("g", Arrays.asList("one", 2));
		
		j2 = new JsonObject(j.toString());
		
		Assert.assertEquals("[\"one\",2]", j2.get("g").toString());
		
		Map m = new HashMap() {
			{
				put("x", new HashMap() {
					{
						put("x", 3);
					}
				});
			}
		};
		j2 = new JsonObject(m);
		
		Assert.assertEquals("{\"x\":{\"x\":3}}", j2.toString());
		
		m = new HashMap() {
			{
				put("x", Arrays.asList("one", 2));
			}
		};
		j2 = new JsonObject(m);
		
		Assert.assertEquals("{\"x\":[\"one\",2]}", j2.toString());
		
		m = new HashMap() {
			{
				put("x", new Object[] { "one", 2 });
			}
		};
		j2 = new JsonObject(m);
		
		Assert.assertEquals("{\"x\":[\"one\",2]}", j2.toString());
		
		m = new HashMap() {
			{
				put("x", null);
			}
		};
		j2 = new JsonObject(m);
		
		Assert.assertEquals("{\"x\":null}", j2.toString());
		
		m = new HashMap() {
			{
				put("x", new JsonString() {
					public String toJsonString() {
						return "{}";
					}
				});
			}
		};
		j2 = new JsonObject(m);
		
		Assert.assertEquals("{\"x\":{}}", j2.toString());
		
		m = new HashMap() {
			{
				put("x", new JsonString() {
					public String toJsonString() {
						throw new RuntimeException();
					}
				});
			}
		};
		j2 = new JsonObject(m);
		
		try {
			Assert.assertEquals("{\"x\":{}}", j2.toString());
			
			throw new Exception();
			
		} catch (JsonException je) {
			// expected
		}
		
		m = new HashMap() {
			{
				put("x", JsonObject.NULL);
			}
		};
		j2 = new JsonObject(m);
		
		Assert.assertEquals("{\"x\":null}", j2.toString());
	}
	
	@Test
	public void testToStringMap() throws Exception {
		JsonObject j = new JsonObject();
		j.put("one", 1);
		
		Assert.assertEquals("{one=1}", j.toStringMap().toString());
		
		j.put("two", JsonObject.NULL);
		
		Assert.assertEquals("{two=null, one=1}", j.toStringMap().toString());
		
		j.put("two", (String) null);
		
		Assert.assertEquals("{one=1}", j.toStringMap().toString());
		
		try {
			j.put(null, 3);
			
			throw new Exception();
			
		} catch (JsonException je) {
			// expected
		}
	}
	
	@Test
	public void testNull() {
		Assert.assertEquals("null".hashCode(), JsonObject.NULL.hashCode());
	}
	
	@Test
	public void testToSortedString() {
		JsonObject j = new JsonObject();
		j.put("one", "1");
		j.put("two", "2");
		j.put("three", "3");
		
		Assert.assertEquals("{\"one\":\"1\",\"three\":\"3\",\"two\":\"2\"}", j.toSortedString());
		
		JsonObject j2 = new JsonObject(j.toSortedString());
		
		Assert.assertEquals(j.toSortedString(), j2.toSortedString());
		
		Assert.assertEquals("[one, three, two]", j.sortedKeySet().toString());
		
		j.put("ja", new JsonArray());
		
		Assert.assertEquals("{\"one\":\"1\",\"three\":\"3\",\"two\":\"2\",\"ja\":[]}", j.toSortedString());
		
		j.put("ja", new JsonArray(Arrays.asList(1, 2, 3)));
		
		Assert.assertEquals("{\"one\":\"1\",\"three\":\"3\",\"two\":\"2\",\"ja\":[1,2,3]}", j.toSortedString());
	}
	
	@Test
	public void testParse() {
		JsonObject j = new JsonObject("{\"abc\":\"def\"}");
		Assert.assertEquals("{\"abc\":\"def\"}", j.toString());
		
		j = new JsonObject("{\"abc\"=\"def\"}");
		Assert.assertEquals("{\"abc\":\"def\"}", j.toString());
		
		j = new JsonObject("{\"abc\"=>\"def\"}");
		Assert.assertEquals("{\"abc\":\"def\"}", j.toString());
		
		j = new JsonObject("{\"abc\"=>\"def\";\"abc2\":\"def2\"}");
		Assert.assertEquals(2, j.length());
		Assert.assertEquals("[abc, abc2]", j.sortedKeySet().toString());
	}
	
	@Test
	public void testToMap() {
		JsonObject j = new JsonObject();
		j.put("Int", 1);
		j.put("String", "string");
		j.put("Long", 1L);
		j.put("Boolean", true);
		j.put("Double", 1.2);
		j.put("JsonObject", new JsonObject().put("four", "four"));
		j.put("JsonArray", new JsonArray("[\"one\", {\"two\":\"three\"}]"));
		
		Assert.assertEquals(1, j.getInt("Int"));
		Assert.assertEquals("string", j.getString("String"));
		Assert.assertEquals(1L, j.getLong("Long"));
		Assert.assertEquals(true, j.getBoolean("Boolean"));
		Assert.assertEquals(1.2, j.getDouble("Double"), 0);
		
		Assert.assertEquals(1, j.optInt("Int"));
		Assert.assertEquals("string", j.optString("String"));
		Assert.assertEquals(1L, j.optLong("Long"));
		Assert.assertEquals(true, j.optBoolean("Boolean"));
		Assert.assertEquals(1.2, j.optDouble("Double"), 0);
		
		Assert.assertEquals(1, j.optInt("Int", -1));
		Assert.assertEquals("string", j.optString("String", "xyz"));
		Assert.assertEquals(1L, j.optLong("Long", 2L));
		Assert.assertEquals(true, j.optBoolean("Boolean", false));
		Assert.assertEquals(1.2, j.optDouble("Double", 1.3), 0);
		
		Assert.assertEquals(0, j.optInt("Int2"));
		Assert.assertEquals("", j.optString("String2"));
		Assert.assertEquals(0L, j.optLong("Long2"));
		Assert.assertEquals(false, j.optBoolean("Boolean2"));
		Assert.assertEquals(Double.NaN, j.optDouble("Double2"), 0);
		
		Assert.assertTrue(j.has("Int"));
		Assert.assertFalse(j.has("Int2"));
		
		Map<String, Object> map = j.toMap();
		
		Assert.assertEquals(1, map.get("Int"));
		Assert.assertEquals("string", map.get("String"));
		Assert.assertTrue(map.get("JsonObject") instanceof Map);
		Assert.assertTrue(map.get("JsonArray") instanceof List);
		Assert.assertEquals("four", ((Map) map.get("JsonObject")).get("four"));
		Assert.assertEquals("one", ((List) map.get("JsonArray")).get(0));
		Assert.assertEquals("three", ((Map) ((List) map.get("JsonArray")).get(1)).get("two"));
		
		Assert.assertEquals(1028066909, j.toString(3).hashCode());
		
		JsonObject j2 = new JsonObject(map);
		
		// j has JsonObject and JsonArray internall, which are change to Map/List respecively
		// by toMap(), thus inequality
		Assert.assertFalse(j.toSortedString().equals(j2.toSortedString()));
		
		j.remove("JsonObject");
		j.remove("JsonArray");
		
		map = j.toMap();
		
		j2 = new JsonObject(map);
		
		Assert.assertEquals(j.toSortedString(), j2.toSortedString());
	}
	
	@Test
	public void testBooleanX() throws Exception {
		JsonObject j = new JsonObject();
		j.put("abc", false);
		
		Assert.assertFalse(j.getBoolean("abc"));
		
		j.put("abc", "true");
		
		Assert.assertTrue(j.getBoolean("abc"));
		
		j.put("abc", "junk");
		
		try {
			j.getBoolean("abc");
			
			throw new Exception();
			
		} catch (JsonException je) {
			// expected
		}
		
		j.put("abc", new Object());
		
		try {
			j.getBoolean("abc");
			
			throw new Exception();
			
		} catch (JsonException je) {
			// expected
		}
	}
	
	@Test
	public void testNumberToString() throws Exception {
		Assert.assertEquals("1", JsonObject.numberToString(1));
		Assert.assertEquals("1", JsonObject.numberToString(1f));
		
		try {
			Assert.assertEquals("null", JsonObject.numberToString((Integer) null));
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		Assert.assertEquals("1000000000000000000", JsonObject.numberToString(1000000000000000000L));
		Assert.assertEquals("1", JsonObject.numberToString(1.0));
		Assert.assertEquals("1.1", JsonObject.numberToString(1.1));
		Assert.assertEquals("1.1", JsonObject.numberToString(1.1f));
		
		Assert.assertEquals("1.1E20", JsonObject.numberToString(1.1e20));
	}
	
	@Test
	public void testToString2() {
		Assert.assertEquals("{}", new JsonObject((Map) null).toString());
		Assert.assertEquals("{}", new JsonObject().toString(4));
		Assert.assertEquals("{}", new JsonObject().toHtmlString());
		
		JsonObject oneFive = new JsonObject().put("one", 5);
		
		Assert.assertEquals("{\n\t\"one\": 5\n}", oneFive.toTabbedString());
		Assert.assertEquals("{<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\"one\": 5<br/>}", oneFive.toHtmlString());
		Assert.assertEquals("{<br/>&nbsp;&nbsp;&nbsp;\"one\": 5<br/>}", oneFive.toHtmlString(3));
	}
	
	@Test
	public void testParseFailures() throws Exception {
		try {
			new JsonObject("abc");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			new JsonObject("{");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			new JsonObject("{\"abc\"-\"def\"}");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			new JsonObject("{\"abc\":\"def\" \"abc2\":1}");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testGetFailuresAndMore() throws Exception {
		JsonObject j = new JsonObject().put("abc", "def");
		
		try {
			j.getJsonArray("abc");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		j = new JsonObject().put("abc", new JsonArray(Arrays.asList("one")));
		Assert.assertEquals("[\"one\"]", j.getJsonArray("abc").toString());
		
		j = new JsonObject().put("abc", new JsonObject());
		Assert.assertEquals("{}", j.getJsonObject("abc").toString());
		
		Assert.assertEquals("[abc]", j.keySet().toString());
		Assert.assertEquals("[]", new JsonObject().keySet().toString());
		
		Assert.assertEquals("[abc]", Arrays.toString(JsonObject.getNames(j)));
		Assert.assertEquals("null", Arrays.toString(JsonObject.getNames(new JsonObject())));
	}
	
	@Test
	public void testAccumulate() {
		JsonObject j = new JsonObject();
		j.accumulate("abc", 1);
		j.accumulate("abc", 2);
		j.accumulate("abc", 3);
		
		Assert.assertEquals("{\"abc\":[1,2,3]}", j.toString());
		
		j.remove("abc");
		
		j.accumulate("abc", new JsonArray());
		j.accumulate("abc", new JsonArray());
		
		Assert.assertEquals("{\"abc\":[[],[]]}", j.toString());
	}
	
	@Test
	public void testAppend() throws Exception {
		JsonObject j = new JsonObject();
		j.append("abc", 1);
		j.append("abc", 2);
		j.append("abc", 3);
		
		Assert.assertEquals("{\"abc\":[1,2,3]}", j.toString());
		
		j.remove("abc");
		
		j.append("abc", new JsonArray());
		j.append("abc", new JsonArray());
		
		Assert.assertEquals("{\"abc\":[[],[]]}", j.toString());
		
		j.remove("abc");
		
		j.put("abc", "string");
		
		try {
			j.append("abc", "string2");
			
			throw new Exception();
			
		} catch (JsonException je) {
			// expected
		}
	}
	
	@Test
	public void testStringToValue() {
		Assert.assertEquals("", JsonObject.stringToValue(""));
		Assert.assertTrue(JsonObject.stringToValue("false") == Boolean.FALSE);
		Assert.assertTrue(JsonObject.stringToValue("FAlsE") == Boolean.FALSE);
		Assert.assertTrue(JsonObject.stringToValue("null") == JsonObject.NULL);
		
		Assert.assertEquals(1.1, JsonObject.stringToValue("1.1"));
		Assert.assertEquals(1.1, JsonObject.stringToValue("+1.1"));
		Assert.assertEquals(-1.1, JsonObject.stringToValue("-1.1"));
		Assert.assertEquals(0.1, JsonObject.stringToValue(".1"));
		Assert.assertEquals(2, JsonObject.stringToValue("0x00002"));
		Assert.assertEquals(2, JsonObject.stringToValue("0X00002"));
		Assert.assertEquals("0a00002", JsonObject.stringToValue("0a00002"));
		Assert.assertEquals("some-string", JsonObject.stringToValue("some-string"));
		Assert.assertEquals(1, JsonObject.stringToValue("01"));
		Assert.assertEquals("0x", JsonObject.stringToValue("0x"));
		Assert.assertEquals("0xg", JsonObject.stringToValue("0xg"));
		Assert.assertEquals(1000000000000L, JsonObject.stringToValue("1000000000000"));
	}
	
	@Test
	public void testTestValidity() throws Exception {
		try {
			JsonObject.testValidity(Double.NaN);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			JsonObject.testValidity(Double.POSITIVE_INFINITY);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			JsonObject.testValidity(Double.NEGATIVE_INFINITY);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			JsonObject.testValidity(Float.NaN);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			JsonObject.testValidity(Float.NEGATIVE_INFINITY);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			JsonObject.testValidity(Float.NEGATIVE_INFINITY);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		JsonObject.testValidity(null);
	}
	
	@Test
	public void testValueToString() throws Exception {
		Assert.assertEquals("null", JsonObject.valueToString(JsonObject.NULL, 1, 1));
		Assert.assertEquals("null", JsonObject.valueToString(null, 1, 1));
		
		Assert.assertEquals("123", JsonObject.valueToString(new JsonString() {
			public String toJsonString() {
				return "123";
			}
		}, 1, 1));
		
		String s = JsonObject.valueToString(new JsonString() {
			public String toJsonString() {
				throw new RuntimeException();
			}
		}, 1, 1);
		
		Assert.assertTrue(s.contains("JsonObjectTest"));
		
		Assert.assertEquals("{\"one\": \"two\"}", JsonObject.valueToString(new HashMap() {
			{
				put("one", "two");
			}
		}, 1, 1));
		
		String s1 = JsonObject.valueToString(Arrays.asList(1, 2), 1, 1);
		String s2 = "[\n  1,\n  2\n ]".trim();
		
		Assert.assertEquals(s2, s1);
		
		s1 = JsonObject.valueToString(new Object[] { 1, 2 }, 1, 1);
		s2 = "[\n  1,\n  2\n ]".trim();
		
		Assert.assertEquals(s2, s1);
	}
	
	@Test
	public void testPutOpt() {
		JsonObject j = new JsonObject();
		Assert.assertEquals(0, j.length());
		
		j.putOpt(null, null);
		Assert.assertEquals(0, j.length());
		
		j.putOpt(null, "abc");
		Assert.assertEquals(0, j.length());
		
		j.putOpt("abc", null);
		Assert.assertEquals(0, j.length());
		
		j.putOpt("abc", "abc");
		Assert.assertEquals(1, j.length());
	}
	
	@Test
	public void testPutOnce() throws Exception {
		JsonObject j = new JsonObject();
		Assert.assertEquals(0, j.length());
		
		j.putOnce(null, null);
		Assert.assertEquals(0, j.length());
		
		j.putOnce(null, "abc");
		Assert.assertEquals(0, j.length());
		
		j.putOnce("abc", null);
		Assert.assertEquals(0, j.length());
		
		j.put("abc", "abc");
		
		try {
			j.putOnce("abc", "abc");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPutMore() {
		JsonObject j = new JsonObject();
		j.put("one", new HashMap() {
			{
				put("abc", "def");
			}
		});
		
		Assert.assertEquals("{\"one\":{\"abc\":\"def\"}}", j.toString());
	}
	
	@Test
	public void testSomeOptStuff() {
		JsonObject j = new JsonObject();
		
		Assert.assertNull(j.optJsonArray("abc"));
		Assert.assertNull(j.optJsonObject("abc"));
		
		j.put("abc", new JsonArray());
		
		Assert.assertNotNull(j.optJsonArray("abc"));
		
		j.put("abc", new JsonObject());
		
		Assert.assertNotNull(j.optJsonObject("abc"));
		
		j.put("double", 1);
		
		Assert.assertEquals(1.0, j.optDouble("double"), 0);
		
		j.put("double", "1");
		
		Assert.assertEquals(1.0, j.optDouble("double"), 0);
		Assert.assertNull(j.opt("xyz"));
		
		j.put("xyz", "xyz");
		
		Assert.assertNotNull(j.opt("xyz"));
		Assert.assertNull(j.opt(null));
	}
	
	@Test
	public void testWrite() throws Exception {
		StringWriter sw = new StringWriter();
		
		JsonObject j = new JsonObject();
		j.put("abc", "def");
		
		j.write(sw);
		
		Assert.assertEquals("{\"abc\":\"def\"}", sw.toString());
		
		sw = new StringWriter();
		j.put("ghi", "jkl");
		j.write(sw);
		
		Assert.assertEquals("{\"abc\":\"def\",\"ghi\":\"jkl\"}", sw.toString());
		
		sw = new StringWriter();
		j.put("abc", new JsonObject().put("one", "two"));
		j.write(sw);
		
		Assert.assertEquals("{\"abc\":{\"one\":\"two\"},\"ghi\":\"jkl\"}", sw.toString());
		
		sw = new StringWriter();
		j.put("abc", new JsonArray(Arrays.asList("one")));
		j.write(sw);
		
		Assert.assertEquals("{\"abc\":[\"one\"],\"ghi\":\"jkl\"}", sw.toString());
		
		try {
			j.write(new Writer() {
				public void write(char[] cbuf, int off, int len) throws IOException {
					throw new IOException();
				}
				
				public void flush() throws IOException {
				}
				
				public void close() throws IOException {
				}
			});
			
			throw new Exception();
			
		} catch (JsonException je) {
			// expected
			Assert.assertTrue(je.getCause() instanceof IOException);
		}
	}
	
	@Test
	public void testMoreParsing() throws Exception {
		try {
			new JsonObject("{\"abc\n");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		
		JsonObject j = new JsonObject("{\"abc\\n\":1}");
		Assert.assertEquals("{\"abc\\n\":1}", j.toString());
		
		j = new JsonObject("{\"abc\\b\":1}");
		Assert.assertEquals("{\"abc\\b\":1}", j.toString());
		
		j = new JsonObject("{\"abc\\t\":1}");
		Assert.assertEquals("{\"abc\\t\":1}", j.toString());
		
		j = new JsonObject("{\"abc\\f\":1}");
		Assert.assertEquals("{\"abc\\f\":1}", j.toString());
		
		j = new JsonObject("{\"abc\\r\":1}");
		Assert.assertEquals("{\"abc\\r\":1}", j.toString());
		
		j = new JsonObject("{\"abc\\u2100\":1}");
		Assert.assertEquals("{\"abc℀\":1}", j.toString());
		
		j = new JsonObject("{\"abc\\x21\":1}");
		Assert.assertEquals("{\"abc!\":1}", j.toString());
		
		j = new JsonObject("{\"abc\\y21\":1}");
		Assert.assertEquals("{\"abcy21\":1}", j.toString());
	}
	
	@Test
	public void testTypeConversions() throws Exception {
		JsonObject j = new JsonObject();
		j.put("Int", "1");
		j.put("Boolean", "true");
		j.put("Double", "1.2");
		j.put("Long", "1");
		
		Assert.assertEquals(1, j.getInt("Int"));
		Assert.assertEquals(true, j.getBoolean("Boolean"));
		Assert.assertEquals(1.2, j.getDouble("Double"), 0);
		Assert.assertEquals(1, j.getLong("Long"));
		
		j.put("Boolean", "false");
		
		Assert.assertEquals(false, j.getBoolean("Boolean"));
		
		j.put("Int", "a");
		j.put("Boolean", "a");
		j.put("Double", "a");
		j.put("Long", "a");
		
		try {
			j.getInt("Int");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			j.getLong("Long");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			j.getBoolean("Boolean");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			j.getDouble("Double");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			j.getJsonArray("Double");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			j.getJsonObject("Double");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testStatic() {
		Assert.assertEquals("null", JsonObject.doubleToString(Double.NaN));
		Assert.assertEquals("null", JsonObject.doubleToString(Double.POSITIVE_INFINITY));
		Assert.assertEquals("null", JsonObject.doubleToString(Double.NEGATIVE_INFINITY));
		Assert.assertEquals("1.2", JsonObject.doubleToString(1.2));
		Assert.assertEquals("1", JsonObject.doubleToString(1));
		Assert.assertEquals("1.0E18", JsonObject.doubleToString(1000000000000000000L));
	}
	
	@Test
	public void testStupidConstructors() {
		JsonObject j = new JsonObject();
		j.put("one", "1");
		j.put("two", "2");
		j.put("three", "3");
		
		JsonObject j2 = new JsonObject(j, new String[] {});
		Assert.assertEquals(0, j2.length());
		
		j2 = new JsonObject(j, new String[] { "one" });
		Assert.assertEquals(1, j2.length());
		Assert.assertEquals("1", j2.get("one"));
		
		j2 = new JsonObject(j, new String[] { "three", "two" });
		Assert.assertEquals(2, j2.length());
		Assert.assertEquals("2", j2.get("two"));
		Assert.assertEquals("3", j2.get("three"));
	}
	
	@Test
	public void testQuote() {
		Assert.assertEquals("\"\"", JsonObject.quote(""));
		Assert.assertEquals("\"\"", JsonObject.quote(null));
		Assert.assertEquals("\"\\tabc\"", JsonObject.quote("\tabc"));
		Assert.assertEquals("\"\\nabc\"", JsonObject.quote("\nabc"));
		Assert.assertEquals("\"\\fabc\"", JsonObject.quote("\fabc"));
		Assert.assertEquals("\"\\rabc\"", JsonObject.quote("\rabc"));
		Assert.assertEquals("\"\\babc\"", JsonObject.quote("\babc"));
		Assert.assertEquals("\"\\\"\"", JsonObject.quote("\""));
		Assert.assertEquals("\"/\"", JsonObject.quote("/"));
		Assert.assertEquals("\"<\\/abc>\"", JsonObject.quote("</abc>"));
		Assert.assertEquals("\"\\u0081\"", JsonObject.quote(""));
		Assert.assertEquals("\"\\u2059\"", JsonObject.quote("⁙"));
		
		StringBuilder buffer = new StringBuilder();
		buffer.append((char) (' ' - 1));
		
		Assert.assertEquals("\"\\u001f\"", JsonObject.quote(buffer.toString()));
		
		Assert.assertEquals("\"℁\"", JsonObject.quote("℁"));
	}
	
	private static class TestThing {
		
		private String cheese;
		
		private int x;
		
		private boolean xyz;
		
		public String getCheese() {
			return cheese;
		}
		
		public void setCheese(String cheese) {
			this.cheese = cheese;
		}
		
		public int getX() {
			return x;
		}
		
		public void setX(int x) {
			this.x = x;
		}
		
		public boolean isXyz() {
			return xyz;
		}
		
		public void setXyz(boolean xyz) {
			this.xyz = xyz;
		}
	}
	
	private static class TestThing2 extends TestThing {
		
		private double d;
		
		public double getD() {
			return d;
		}
		
		public void setD(double d) {
			this.d = d;
		}
	}
	
	private static class TestThing3 extends TestThing2 {
		
		private Object o;
		
		private float f;
		
		private byte b;
		
		private char c;
		
		private short s;
		
		private long l;
		
		private String ABC;
		
		public float getF() {
			return f;
		}
		
		public void setF(float f) {
			this.f = f;
		}
		
		public byte getB() {
			return b;
		}
		
		public void setB(byte b) {
			this.b = b;
		}
		
		public char getC() {
			return c;
		}
		
		public void setC(char c) {
			this.c = c;
		}
		
		public short getS() {
			return s;
		}
		
		public void setS(short s) {
			this.s = s;
		}
		
		public long getL() {
			return l;
		}
		
		public void setL(long l) {
			this.l = l;
		}
		
		public Object getO() {
			return o;
		}
		
		public void setO(Object o) {
			this.o = o;
		}
		
		public String getABC() {
			return ABC;
		}
		
		public void setABC(String aBC) {
			ABC = aBC;
		}
	}
	
	private static class TestThing4 {
		
		private Object[] array = new Object[] {};
		
		private Map map = new HashMap();
		
		private List list = new ArrayList();
		
		public Object[] getArray() {
			return array;
		}
		
		public void setArray(Object[] array) {
			this.array = array;
		}
		
		public Map getMap() {
			return map;
		}
		
		public void setMap(Map map) {
			this.map = map;
		}
		
		public List getList() {
			return list;
		}
		
		public void setList(List list) {
			this.list = list;
		}
	}
}