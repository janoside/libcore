package com.janoside.hash;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LegacyMd5Hasher implements Hasher<String, String> {
	
	private static final String HEXES = "0123456789abcdef";
	
	public String hash(String value) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(value.getBytes("UTF-8"), 0, value.length());
			
			byte[] sum = messageDigest.digest();
			
			final StringBuilder hex = new StringBuilder(2 * sum.length);
			for (final byte b : sum) {
				hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
			}
			
			return hex.toString();
			
		} catch (NoSuchAlgorithmException nsae) {
			throw new RuntimeException("MD5 algorithm not supported", nsae);
			
		} catch (UnsupportedEncodingException uee) {
			throw new RuntimeException("UTF-8 not supported", uee);
		}
	}
}