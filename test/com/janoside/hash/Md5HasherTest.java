package com.janoside.hash;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.codec.Base91Encoder;
import com.janoside.codec.DelegatingStringEncoder;
import com.janoside.util.RandomUtil;

public class Md5HasherTest {
	
	@Test
	public void testIt() {
		Md5Hasher hasher = new Md5Hasher();
		
		Assert.assertEquals("900150983cd24fb0d6963f7d28e17f72", hasher.hash("abc"));
	}
	
	@Test
	public void testLegacyComparison() {
		compareHashers(new LegacyMd5Hasher(), new Md5Hasher(), false);
	}
	
	@Test
	public void testMore() throws Exception {
		testHasher(new Md5Hasher(), "c4e4fe9e93da7490957f8a746aa6bf8f", "e6b4152ce7f2a0bf64e862e9b762efe7");
		
		testHasher(new Sha1StringHasher(), "58bcf44089d9ec442c39d2fa0aa51c469ce2ab8d", "292d9352014d28437ff877797d8ca4f9c44f8563");
		testHasher(new Sha256StringHasher(), "eb73c8a8bc8c8a190a44efc8d6341ff67c2fee7b13a7af38ec0969a12b56d5b6", "cc64007b4385462fdb21cb6278340930b04c71d75b6fd88e418f1c1abe6f43bd");
		testHasher(new Sha512StringHasher(), "f4eb9eb433ed3aad1e16b357a8ace9305dfbf54b247f18ad6f364dc405bb073eee3b33a4ae70b3720da1ad04d9512547ebe19266293034d918e7e89f3ff4dfa8", "1169136ca53f6fe8a729893e6d0ab1c191b235f3c4edf77bb8999b11fa6e831932f90982e465cf9f5961859630965cb0351fd197675142e396c7dc55cb04c2df");
	}
	
	@Test
	public void testWeirdChars() throws Exception {
		Random r = new Random(5);
		String s = new String(RandomUtil.randomByteArray(500, r), "UTF-8");
		
		Assert.assertEquals("bc32b39086430e5f04cb1ad582e4a4e3", new Md5Hasher().hash(s));
	}
	
	public void testHasher(Hasher<String, String> hasher, String s1, String s2) throws Exception {
		StringBuilder buffer = new StringBuilder();
		Random r = new Random(5);
		for (int i = 0; i < 1000; i++) {
			buffer.append(RandomUtil.randomUuid(r));
		}
		
		Assert.assertEquals(s1, hasher.hash(buffer.toString()));
		
		buffer = new StringBuilder();
		r = new Random(5);
		for (int i = 0; i < 20; i++) {
			buffer.append(new String(RandomUtil.randomByteArray(50, r), "UTF-8"));
			
			System.out.println(buffer.toString().hashCode());
		}
		
		System.out.println(Arrays.toString(buffer.toString().substring(0, 20).getBytes()));
		
		Assert.assertEquals(s2, hasher.hash(buffer.toString()));
	}
	
	public void compareHashers(Hasher h1, Hasher h2, boolean weirdData) {
		Random r = new Random(5);
		for (int i = 0; i < 100; i++) {
			String uuid = RandomUtil.randomUuid(r).toString();
			
			Assert.assertEquals(h1.hash(uuid), h2.hash(uuid));
		}
		
		// all base 91 chars
		for (int i = 0; i < 100; i++) {
			DelegatingStringEncoder enc = new DelegatingStringEncoder();
			enc.setEncoder(new Base91Encoder());
			
			String s = enc.encode(new String(RandomUtil.randomByteArray(500, r)));
			
			Assert.assertEquals(h1.hash(s), h2.hash(s));
		}
		
		List s = Arrays.asList("abcdefghijklmnopqrstuvwxyz1234567890~!@#$%^&*()-`_=+,./<>?;':\"[]{}\\|");
		
		for (Object x : s) {
			Assert.assertEquals(h1.hash(x.toString()), h2.hash(x.toString()));
		}
		
		if (weirdData) {
			StringBuilder buffer = new StringBuilder();
			r = new Random(5);
			for (int i = 0; i < 100; i++) {
				buffer.append(new String(RandomUtil.randomByteArray(50, r)));
			}
			
			Assert.assertEquals(h1.hash(buffer.toString()), h2.hash(buffer.toString()));
		}
	}
}