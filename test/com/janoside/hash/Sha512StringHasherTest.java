package com.janoside.hash;

import org.junit.Assert;
import org.junit.Test;

public class Sha512StringHasherTest {
	
	@Test
	public void testBasic() {
		Sha512StringHasher hasher = new Sha512StringHasher();
		
		Assert.assertEquals("9ad04ef778df4a633fc01164d6b485ce0c6cbf7cabd0405ec9eb0b37c2f65f295b5a4384a59f1e6e8bc48c65c3e2e457c12013cd91d55d62016b6eebf34f5c87", hasher.hash("cheese"));
	}
}