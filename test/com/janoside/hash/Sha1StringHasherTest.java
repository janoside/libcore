package com.janoside.hash;

import org.junit.Assert;
import org.junit.Test;

public class Sha1StringHasherTest {
	
	@Test
	public void testBasic() {
		Sha1StringHasher hasher = new Sha1StringHasher();
		
		Assert.assertEquals("85ce0f13a55fe8f3591107acbf0fc66b732c095f", hasher.hash("cheese"));
		
		hasher.setSalt("haha");
		
		Assert.assertEquals("107ac96ea7c60be71d69473130f979ef2c62b6ce", hasher.hash("cheese"));
	}
	
	@Test
	public void testSomething() {
		this.printSomething("");
	}
	
	private void printSomething(String something) {
		System.out.println(something + " -> " + new Sha1StringHasher().hash(something));
	}
}