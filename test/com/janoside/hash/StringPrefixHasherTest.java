package com.janoside.hash;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

public class StringPrefixHasherTest {
	
	@Test
	@SuppressWarnings("serial")
	public void testBasic() {
		StringPrefixHasher hasher = new StringPrefixHasher();
		hasher.setHashcodesByPrefix(new HashMap<String, Integer>() {{
			put("one:", 1);
			put("two:", 2);
		}});
		
		Assert.assertEquals(1, hasher.hash("one:haha").intValue());
		Assert.assertEquals(2, hasher.hash("two:cheese").intValue());
		Assert.assertEquals(-1, hasher.hash("three:cheesey").intValue());
	}
}