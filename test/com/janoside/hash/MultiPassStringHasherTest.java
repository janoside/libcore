package com.janoside.hash;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.JunitTestUtil;

public class MultiPassStringHasherTest {
	
	@Test
	public void testIt() {
		MultiPassStringHasher hasher = new MultiPassStringHasher();
		hasher.setHasher(new Sha1StringHasher());
		hasher.setPassCount(4);
		
		String value = hasher.hash("cheese");
		
		Assert.assertEquals("e8d5dabe1a002ec8ad3cec047491c4ef890523c2", hasher.hash("cheese"));
		
		for (int i = 0; i < 10; i++) {
			hasher.setPassCount(5 + i);
			
			Assert.assertFalse(value.equals(hasher.hash("cheese")));
		}
	}
	
	@Test
	public void testAnother() {
		MultiPassStringHasher hasher = new MultiPassStringHasher();
		hasher.setHasher(new Sha256StringHasher());
		hasher.setPassCount(1000);
		
		String hash = hasher.hash("dandan");
		
		System.out.println(hash);
	}
	
	@Test
	public void testDumbness() {
		MultiPassStringHasher hasher = new MultiPassStringHasher();
		hasher.setHasher(new Sha1StringHasher());
		hasher.setPassCount(1);
		
		JunitTestUtil.reflectiveInvokeInstanceMethodWhichTakesObjectWithArgument(hasher, "hash", "abc");
	}
}