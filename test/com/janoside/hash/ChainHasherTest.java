package com.janoside.hash;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class ChainHasherTest {
	
	@Test
	public void testBasic() {
		ChainHasher hasher = new ChainHasher();
		hasher.setHashers(Arrays.asList(new Sha256StringHasher(), new Sha512StringHasher()));
		
		Assert.assertEquals("948c5b8651a11fa7a43a295f5696abe84bb21e724b605f44a69d8a92f3444a8066c62612bbe1b9f219d2a113296508604bb8b58e1965c6e1cf20be07d1d147ee", hasher.hash("cheese"));
	}
}