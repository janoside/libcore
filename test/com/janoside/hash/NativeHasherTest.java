package com.janoside.hash;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class NativeHasherTest {
	
	@Test
	public void testIt() {
		NativeHasher<String> hasher = new NativeHasher<String>();
		
		Random r = new Random(5);
		for (int i = 0; i < 1000; i++) {
			String str = new String(RandomUtil.randomByteArray(100, r));
			
			Assert.assertEquals(str.hashCode(), hasher.hash(str).intValue());
		}
	}
}