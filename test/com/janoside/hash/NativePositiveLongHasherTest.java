package com.janoside.hash;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class NativePositiveLongHasherTest {
	
	@Test
	public void testIt() {
		NativePositiveLongHasher<String> hasher = new NativePositiveLongHasher<String>();
		
		ArrayList<String> strings = new ArrayList<String>(Arrays.asList("test", "one", "asldhifaouhdf"));
		
		for (String str : strings) {
			int hashCode = str.hashCode();
			if (hashCode < 0) {
				hashCode = -hashCode;
			}
			
			long longHashCode = (long) hashCode;
			
			Assert.assertEquals(longHashCode, hasher.hash(str).longValue());
		}
	}
	
	@Test
	public void testDealerIds() {
		List<String> ids = Arrays.asList("clay-cooley-suzuki-dallas-dallas-tx", "clay-cooley-suzuki-arlington-tx", "freeman-toyota-hurst-tx", "freeman-honda-dallas-tx", "waxahachie-autoplex-dallas-tx", "fred-haas-toyota-world-spring-tx", "legacy-ford-rosenburg-tx", "kahlo-chrysler-jeep-dodge-noblesville-in", "ray-skillman-shadeland-kia-indianapolis-in", "ray-skillman-buick-gmc-indianapolis-in");
		
		for (String id : ids) {
			System.out.println(id + " - " + new NativePositiveLongHasher<String>().hash(id));
		}
	}
}