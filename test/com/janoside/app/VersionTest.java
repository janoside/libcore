package com.janoside.app;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.JunitTestUtil;

public class VersionTest {
	
	@Test
	public void testIt() {
		Version v1 = new Version("1.0.1");
		Version v2 = new Version("1");
		
		Assert.assertTrue(v1.greaterThan(v2));
		Assert.assertTrue(v2.lessThan(v1));
		
		
		v1 = new Version("1.0.1");
		v2 = new Version("0");
		
		Assert.assertTrue(v1.greaterThan(v2));
		Assert.assertTrue(v2.lessThan(v1));
		
		
		v1 = new Version("1.0.1");
		v2 = new Version("1.0.0");
		
		Assert.assertTrue(v1.greaterThan(v2));
		Assert.assertTrue(v2.lessThan(v1));
		
		
		v1 = new Version("1");
		v2 = new Version("0.0.1");
		
		Assert.assertTrue(v1.greaterThan(v2));
		Assert.assertTrue(v2.lessThan(v1));
		
		
		v1 = new Version("1.0.1");
		v2 = new Version("1.0.1");
		
		Assert.assertFalse(v1.greaterThan(v2));
		Assert.assertFalse(v2.lessThan(v1));
		Assert.assertTrue(v2.equals(v1));
		Assert.assertFalse(v2.equals(new Version("1.0.55")));
		Assert.assertFalse(v2.equals(null));
		Assert.assertFalse(v2.equals("haha"));
		Assert.assertTrue(v1.equals(v2));
		Assert.assertEquals("1.0.1", v2.toString());
		Assert.assertEquals("1.0.1".hashCode(), v2.hashCode());
	}
	
	@Test
	public void testBeta() {
		Version v1 = new Version("1");
		Version v2 = new Version("1b1");
		
		Assert.assertTrue(v1.greaterThan(v2));
		Assert.assertTrue(v2.lessThan(v1));
		
		v1 = new Version("1b1.0.3");
		v2 = new Version("1b1.0.2");
		
		Assert.assertTrue(v1.greaterThan(v2));
		Assert.assertTrue(v2.lessThan(v1));
		Assert.assertEquals("1b1.0.2", v2.toString());
	}
	
	@Test
	public void testGenericCompareTo() {
		Version v1 = new Version("1");
		
		JunitTestUtil.reflectiveInvokeInstanceMethodWhichTakesObjectWithArgument(v1, "compareTo", new Version("2"));
	}
}