package com.janoside.exception;

import java.util.Arrays;

import org.junit.Test;

import com.janoside.stats.MemoryStats;
import com.janoside.stats.MemoryStatsStatTracker;

public class StatTrackerExceptionHandlerTest {
	
	@Test
	public void testIt() {
		OutOfMemoryError oome = new OutOfMemoryError();
		oome.setStackTrace(new StackTraceElement[] {});
		
		TestException te = new TestException(oome);
		TestException2 te2 = new TestException2(te);
		
		RuntimeException re = new RuntimeException(te2);
		
		MemoryStats ms = new MemoryStats();
		MemoryStatsStatTracker st = new MemoryStatsStatTracker();
		st.setMemoryStats(ms);
		
		StatTrackerExceptionHandler eh = new StatTrackerExceptionHandler();
		eh.setStatTracker(st);
		eh.handleException(re);
		
		System.out.println(ms.printEventReport());
	}
	
	@Test
	public void testIt2() {
		RuntimeException re = new RuntimeException();
		
		MemoryStats ms = new MemoryStats();
		MemoryStatsStatTracker st = new MemoryStatsStatTracker();
		st.setMemoryStats(ms);
		
		StatTrackerExceptionHandler eh = new StatTrackerExceptionHandler();
		eh.setRelevantPackages(Arrays.asList("com_cheese"));
		eh.setStatTracker(st);
		eh.handleException(re);
		
		System.out.println(ms.printEventReport());
	}
	
	public static class TestException extends RuntimeException {
		
		public TestException(Throwable cause) {
			super(cause);
		}
		
		public StackTraceElement[] getStackTrace() {
			return null;
		}
	}
	
	public static class TestException2 extends RuntimeException {
		
		public TestException2(Throwable cause) {
			super(cause);
		}
		
		public StackTraceElement[] getStackTrace() {
			return new StackTraceElement[] {};
		}
	}
}