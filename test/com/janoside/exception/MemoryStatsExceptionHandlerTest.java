package com.janoside.exception;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.stats.MemoryStats;
import com.janoside.util.StringUtil;

public class MemoryStatsExceptionHandlerTest {
	
	@Test
	public void testBasic() {
		MemoryStats memoryStats = new MemoryStats();
		
		MemoryStatsExceptionHandler handler = new MemoryStatsExceptionHandler();
		handler.setMemoryStats(memoryStats);
		
		try {
			throw new Exception("testMessage");
			
		} catch (Exception e) {
			handler.handleException(e);
		}
		
		Assert.assertTrue(StringUtil.hasText(memoryStats.printExceptionReport()));
		Assert.assertTrue(memoryStats.printExceptionReport().contains("testMessage"));
	}
}