package com.janoside.exception;

import org.junit.Test;

public class LoggingExceptionHandlerTest {
	
	@Test
	public void testBasic() {
		LoggingExceptionHandler handler = new LoggingExceptionHandler();
		
		try {
			throw new Exception("test");
			
		} catch (Exception e) {
			handler.handleException(e);
		}
	}
	
	@Test
	public void testNoMessage() {
		LoggingExceptionHandler handler = new LoggingExceptionHandler();
		
		try {
			throw new Exception();
			
		} catch (Exception e) {
			handler.handleException(e);
		}
	}
}