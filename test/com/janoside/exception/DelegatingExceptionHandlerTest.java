package com.janoside.exception;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class DelegatingExceptionHandlerTest {
	
	@Test
	public void testSingle() {
		DelegatingExceptionHandler handler = new DelegatingExceptionHandler();
		DummyExceptionHandler dummyHandler = new DummyExceptionHandler();
		
		ArrayList<ExceptionHandler> handlers = new ArrayList<ExceptionHandler>();
		handlers.add(dummyHandler);
		
		handler.setExceptionHandlers(handlers);
		
		try {
			throw new Exception("test");
			
		} catch (Exception e) {
			handler.handleException(e);
		}
		
		Assert.assertEquals("Internal exception handler was not notified of exception", 1, dummyHandler.exceptionsHandled);
	}
	
	@Test
	public void testMultiple() {
		DelegatingExceptionHandler handler = new DelegatingExceptionHandler();
		
		ArrayList<ExceptionHandler> handlers = new ArrayList<ExceptionHandler>();
		handlers.add(new DummyExceptionHandler());
		handlers.add(new DummyExceptionHandler());
		handlers.add(new DummyExceptionHandler());
		
		handler.setExceptionHandlers(handlers);
		
		try {
			throw new Exception("test");
			
		} catch (Exception e) {
			handler.handleException(e);
		}
		
		int total = 0;
		for (ExceptionHandler dummyHandler : handlers) {
			total += ((DummyExceptionHandler) dummyHandler).exceptionsHandled;
		}
		
		Assert.assertEquals("Internal exception handler(s) not notified of exception", 3, total);
	}
	
	@Test
	public void testRuntimeAdd() {
		DelegatingExceptionHandler handler = new DelegatingExceptionHandler();
		
		ArrayList<ExceptionHandler> handlers = new ArrayList<ExceptionHandler>();
		handlers.add(new DummyExceptionHandler());
		handlers.add(new DummyExceptionHandler());
		handlers.add(new DummyExceptionHandler());
		
		handler.setExceptionHandlers(handlers);
		
		DummyExceptionHandler otherHandler = new DummyExceptionHandler();
		
		handler.addExceptionHandler(otherHandler);
		
		try {
			throw new Exception("test");
			
		} catch (Exception e) {
			handler.handleException(e);
		}
		
		int total = 0;
		for (ExceptionHandler dummyHandler : handlers) {
			total += ((DummyExceptionHandler) dummyHandler).exceptionsHandled;
		}
		
		Assert.assertEquals("Internal exception handler(s) not notified of exception", 4, total + otherHandler.exceptionsHandled);
	}
	
	@Test
	public void testNullStacktrace() {
		RuntimeException re = new RuntimeException("extreme", new RuntimeException());
		re.setStackTrace(new StackTraceElement[] {});
		
		DelegatingExceptionHandler handler = new DelegatingExceptionHandler();
		handler.handleException(re);
	}
	
	@Test
	public void testStackOverflow() {
		StackOverflowError nuke = new StackOverflowError();
		
		DelegatingExceptionHandler handler = new DelegatingExceptionHandler();
		handler.handleException(nuke);
	}
	
	@Test
	public void testNullCause() {
		RuntimeException re = new RuntimeException("some junk", null);
		re.setStackTrace(new StackTraceElement[] {});
		
		DelegatingExceptionHandler handler = new DelegatingExceptionHandler();
		handler.handleException(re);
	}
	
	@Test
	public void testEvilInternalExceptionHandler() {
		DelegatingExceptionHandler handler = new DelegatingExceptionHandler();
		handler.addExceptionHandler(new EvilExceptionHandler());
		
		handler.handleException(new RuntimeException());
	}
	
	@Test
	public void testFailureToGetStacktrace() {
		RuntimeException e1 = new RuntimeException("e1");
		e1.setStackTrace(new StackTraceElement[] {});
		
		RuntimeException e2 = new RuntimeException("e2", e1);
		e2.setStackTrace(new StackTraceElement[] {});
		
		RuntimeException e3 = new RuntimeException("e3", e2);
		e3.setStackTrace(new StackTraceElement[] {});
		
		RuntimeException e4 = new RuntimeException("e4", e3);
		e4.setStackTrace(new StackTraceElement[] {});
		
		RuntimeException e5 = new RuntimeException("e5", e4);
		e5.setStackTrace(new StackTraceElement[] {});
		
		DelegatingExceptionHandler handler = new DelegatingExceptionHandler();
		handler.handleException(e5);
	}
	
	private static class DummyExceptionHandler implements ExceptionHandler {
		
		private int exceptionsHandled = 0;
		
		public void handleException(Throwable t) {
			this.exceptionsHandled++;
		}
		
		public void reset() {
			this.exceptionsHandled = 0;
		}
	}
	
	private static class EvilExceptionHandler implements ExceptionHandler {
		
		public void handleException(Throwable t) {
			throw new StackOverflowError("DIE");
		}
	}
}