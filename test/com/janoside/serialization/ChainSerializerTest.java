package com.janoside.serialization;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.StringUtil;

public class ChainSerializerTest {
	
	@Test
	public void testIt() {
		StringByteArraySerializer s2 = new StringByteArraySerializer();
		
		ArrayList<Serializer> serializers = new ArrayList<Serializer>();
		serializers.add(new Serializer<String, String>() {
			public String serialize(String object) {
				byte[] bytes = StringUtil.utf8BytesFromString(object);
				
				return Arrays.toString(bytes);
			}
			
			public String deserialize(String transport) {
				String byteArray = transport.substring(1);
				byteArray = byteArray.substring(0, byteArray.length() - 1);
				
				List<String> parts = StringUtil.split(byteArray, ",", true, false);
				
				byte[] data = new byte[parts.size()];
				for (int i = 0; i < parts.size(); i++) {
					data[i] = Byte.parseByte(parts.get(i));
				}
				
				return StringUtil.utf8StringFromBytes(data);
			}
		});
		serializers.add(s2);
		
		ChainSerializer<String, byte[]> s = new ChainSerializer<String, byte[]>();
		s.setSerializers(serializers);
		
		Assert.assertEquals("[91, 57, 55, 44, 32, 57, 56, 44, 32, 57, 57, 93]", Arrays.toString(s.serialize("abc")));
		Assert.assertEquals("abc", s.deserialize(new byte[] {91, 57, 55, 44, 32, 57, 56, 44, 32, 57, 57, 93}));
		
		s2.setCharsetName("ASCII");
		
		Assert.assertEquals("[91, 57, 55, 44, 32, 57, 56, 44, 32, 57, 57, 93]", Arrays.toString(s.serialize("abc")));
		Assert.assertEquals("abc", s.deserialize(new byte[] {91, 57, 55, 44, 32, 57, 56, 44, 32, 57, 57, 93}));
	}
}