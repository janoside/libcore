package com.janoside.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(BasicHttpClient.class)
public class BasicHttpClientTest {
	
	private BasicHttpClient httpClient;
	
	private org.apache.commons.httpclient.HttpClient httpClientMock;
	
	private HttpClientParams httpClientParamsMock;
	
	@Before
	public void setUp() throws Exception {
		httpClientMock = PowerMock.createMock(org.apache.commons.httpclient.HttpClient.class);
		httpClientParamsMock = EasyMock.createNiceMock(HttpClientParams.class);
		
		EasyMock.expect(httpClientMock.getParams()).andReturn(httpClientParamsMock).anyTimes();
	}
	
	@After
	public void tearDown() throws Exception {
		PowerMock.verifyAll();
	}
	
	@Test
	public void testGet() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(GetMethod.class, EasyMock.anyString()).andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(getMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String output = this.httpClient.get("http://www.google.com/");
		Assert.assertNotNull(output);
		Assert.assertFalse(output.isEmpty());
	}
	
	@Test
	public void testGetTimeout() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(getMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(GetMethod.class, EasyMock.anyString()).andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(getMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String output = this.httpClient.get("http://www.google.com/", 1000);
		Assert.assertNotNull(output);
		Assert.assertFalse(output.isEmpty());
	}
	
	@Test
	public void testGetUnsupportedCharset() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(getMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(GetMethod.class, EasyMock.anyString()).andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UnsupportedCharset").anyTimes();
		EasyMock.expect(getMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.get("http://www.google.com/", 1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testGetFail() throws Exception {
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(500);
		PowerMock.expectNew(GetMethod.class, EasyMock.anyString()).andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.get("http://www.google.com/");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testGetFailIoException() throws Exception {
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andThrow(new IOException());
		PowerMock.expectNew(GetMethod.class, EasyMock.anyString()).andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.get("http://www.google.com/");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testGetBytes() throws Exception {
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(getMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(GetMethod.class, EasyMock.anyString()).andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseContentLength()).andReturn(3L);
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(getMethodMock.getResponseBody()).andReturn(new byte[] {1, 2, 3});
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		Assert.assertEquals("[1, 2, 3]", Arrays.toString(this.httpClient.getBytes("url", 1000)));
	}
	
	@Test
	public void testGetBytesFail() throws Exception {
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(getMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(500);
		PowerMock.expectNew(GetMethod.class, EasyMock.anyString()).andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.getBytes("url", 1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testGetBytesFailIoException() throws Exception {
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andThrow(new IOException());
		PowerMock.expectNew(GetMethod.class, EasyMock.anyString()).andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.getBytes("http://www.google.com/", 0);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testGetWithParams() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(getMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(GetMethod.class, "abc?id=x").andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(getMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.get("abc", new HashMap() {{ put("id", "x"); }}, 1000);
		
		Assert.assertEquals("abc", s);
	}
	
	@Test
	public void testGetWithParamsHttpError() throws Exception {
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(getMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(500);
		PowerMock.expectNew(GetMethod.class, "abc?id=x").andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.get("abc", new HashMap() {{ put("id", "x"); }}, 1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testGetWithParamsIoException() throws Exception {
		GetMethod getMethodMock = PowerMock.createMock(GetMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(getMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andThrow(new IOException());
		PowerMock.expectNew(GetMethod.class, "abc?id=x").andReturn(getMethodMock);
		
		EasyMock.expect(getMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		getMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.get("abc", new HashMap() {{ put("id", "x"); }}, 1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithParams() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc?id=x").andReturn(postMethodMock);
		
		//EasyMock.expect(postMethodMock.getResponseContentLength()).andReturn(3L);
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post("abc", new HashMap() {{ put("id", "x"); }}, 1000);
		
		Assert.assertEquals("abc", s);
	}
	
	@Test
	public void testPostWith500() throws Exception {
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(500);
		PowerMock.expectNew(PostMethod.class, "abc?id=x").andReturn(postMethodMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.post("abc", new HashMap() {{ put("id", "x"); }}, 1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithIoException() throws Exception {
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andThrow(new IOException());
		PowerMock.expectNew(PostMethod.class, "abc?id=x").andReturn(postMethodMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.post("abc", new HashMap() {{ put("id", "x"); }}, 1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithParamsNoTimeout() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc?id=x").andReturn(postMethodMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post("abc", new HashMap() {{ put("id", "x"); }});
		
		Assert.assertEquals("abc", s);
	}
	
	@Test
	public void testPostWithNameValuePairArray() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		NameValuePair[] postData = new NameValuePair[] { new NameValuePair("id", "x") };
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		
		//EasyMock.expect(postMethodMock.getResponseContentLength()).andReturn(3L);
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.setRequestBody(postData);
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post("abc", postData, 1000);
		
		Assert.assertEquals("abc", s);
	}
	
	@Test
	public void testPostWithNameValuePairArrayAnd500() throws Exception {
		NameValuePair[] postData = new NameValuePair[] { new NameValuePair("id", "x") };
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(500);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		postMethodMock.setRequestBody(postData);
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.post("abc", postData, 1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithNameValuePairArrayAndIoException() throws Exception {
		NameValuePair[] postData = new NameValuePair[] { new NameValuePair("id", "x") };
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andThrow(new IOException());
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		postMethodMock.setRequestBody(postData);
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.post("abc", postData, 1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithNameValuePairArrayAndNoTimeout() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		NameValuePair[] postData = new NameValuePair[] { new NameValuePair("id", "x") };
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		
		//EasyMock.expect(postMethodMock.getResponseContentLength()).andReturn(3L);
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.setRequestBody(postData);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post("abc", postData);
		
		Assert.assertEquals("abc", s);
	}
	
	@Test
	public void testPostWithLotsOfParamsString() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("response".getBytes());
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		StringRequestEntity stringRequestEntityMock = PowerMock.createMock(StringRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(StringRequestEntity.class, "postData", "text/plain", "UTF-8").andReturn(stringRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.setRequestEntity(stringRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", "text/plain");
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("header1", "headerValue1");
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post(
				"abc",
				new HashMap() {{ put("header1", "headerValue1"); }},
				"postData",
				"text/plain",
				"UTF-8",
				1000);
		
		Assert.assertEquals("response", s);
	}
	
	@Test
	public void testPostWithLotsOfParamsStringWith500() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("abc".getBytes());
		
		String contentType = "xxx";
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		StringRequestEntity stringRequestEntityMock = PowerMock.createMock(StringRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(500);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(StringRequestEntity.class, "postData", contentType, "UnsupportedEncoding").andReturn(stringRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsString()).andReturn("abc");
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.setRequestEntity(stringRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", contentType);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("header1", "headerValue1");
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.post(
				"abc",
				new HashMap() {{ put("header1", "headerValue1"); }},
				"postData",
				contentType,
				"UnsupportedEncoding",
				1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithLotsOfParamsStringWithIoException() throws Exception {
		String contentType = "xxx";
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		StringRequestEntity stringRequestEntityMock = PowerMock.createMock(StringRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andThrow(new IOException());
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(StringRequestEntity.class, "postData", contentType, "UnsupportedEncoding").andReturn(stringRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		postMethodMock.setRequestEntity(stringRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", contentType);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("header1", "headerValue1");
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.post(
				"abc",
				new HashMap() {{ put("header1", "headerValue1"); }},
				"postData",
				contentType,
				"UnsupportedEncoding",
				1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithLotsOfParamsByteArray() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("response".getBytes());
		byte[] postData = "postData".getBytes();
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		ByteArrayRequestEntity byteArrayRequestEntityMock = PowerMock.createMock(ByteArrayRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(ByteArrayRequestEntity.class, postData, "text/plain").andReturn(byteArrayRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.setRequestEntity(byteArrayRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", "text/plain");
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("header1", "headerValue1");
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post(
				"abc",
				new HashMap() {{ put("header1", "headerValue1"); }},
				postData,
				"text/plain",
				1000);
		
		Assert.assertEquals("response", s);
	}
	
	@Test
	public void testPostWithLotsOfParamsByteArrayWith500() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("response".getBytes());
		byte[] postData = "postData".getBytes();
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		ByteArrayRequestEntity byteArrayRequestEntityMock = PowerMock.createMock(ByteArrayRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(500);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(ByteArrayRequestEntity.class, postData, "text/plain").andReturn(byteArrayRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		EasyMock.expect(postMethodMock.getResponseBodyAsString()).andReturn("response");
		
		postMethodMock.setRequestEntity(byteArrayRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", "text/plain");
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("header1", "headerValue1");
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.post(
					"abc",
					new HashMap() {{ put("header1", "headerValue1"); }},
					postData,
					"text/plain",
					1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithLotsOfParamsByteArrayWithIoException() throws Exception {
		byte[] postData = "postData".getBytes();
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		ByteArrayRequestEntity byteArrayRequestEntityMock = PowerMock.createMock(ByteArrayRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andThrow(new IOException());
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(ByteArrayRequestEntity.class, postData, "text/plain").andReturn(byteArrayRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		
		postMethodMock.setRequestEntity(byteArrayRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", "text/plain");
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("header1", "headerValue1");
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		try {
			this.httpClient.post(
					"abc",
					new HashMap() {{ put("header1", "headerValue1"); }},
					postData,
					"text/plain",
					1000);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPostWithThreeStringsAndTimeout() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("response".getBytes());
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		StringRequestEntity stringRequestEntityMock = PowerMock.createMock(StringRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(StringRequestEntity.class, "postData", "text/plain", "UTF-8").andReturn(stringRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.setRequestEntity(stringRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", "text/plain");
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post(
				"abc",
				"postData",
				"text/plain",
				"UTF-8",
				1000);
		
		Assert.assertEquals("response", s);
	}
	
	@Test
	public void testPostWithOneStringAndTimeout() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("response".getBytes());
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		StringRequestEntity stringRequestEntityMock = PowerMock.createMock(StringRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(StringRequestEntity.class, "postData", "text/plain", "UTF-8").andReturn(stringRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.setRequestEntity(stringRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", "text/plain");
		EasyMock.expectLastCall();
		
		httpMethodParamsMock.setSoTimeout(1000);
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post(
				"abc",
				"postData",
				1000);
		
		Assert.assertEquals("response", s);
	}
	
	@Test
	public void testPostWithOneString() throws Exception {
		ByteArrayInputStream inputStream = new ByteArrayInputStream("response".getBytes());
		
		PostMethod postMethodMock = PowerMock.createMock(PostMethod.class);
		HttpMethodParams httpMethodParamsMock = PowerMock.createMock(HttpMethodParams.class);
		StringRequestEntity stringRequestEntityMock = PowerMock.createMock(StringRequestEntity.class);
		
		EasyMock.expect(postMethodMock.getParams()).andReturn(httpMethodParamsMock).anyTimes();
		EasyMock.expect(httpClientMock.executeMethod(EasyMock.anyObject(GetMethod.class))).andReturn(200);
		PowerMock.expectNew(PostMethod.class, "abc").andReturn(postMethodMock);
		PowerMock.expectNew(StringRequestEntity.class, "postData", "text/plain", "UTF-8").andReturn(stringRequestEntityMock);
		
		EasyMock.expect(postMethodMock.getResponseCharSet()).andReturn("UTF-8").anyTimes();
		EasyMock.expect(postMethodMock.getResponseBodyAsStream()).andReturn(inputStream);
		
		postMethodMock.setRequestEntity(stringRequestEntityMock);
		EasyMock.expectLastCall();
		
		postMethodMock.setRequestHeader("Content-Type", "text/plain");
		EasyMock.expectLastCall();
		
		postMethodMock.releaseConnection();
		EasyMock.expectLastCall().anyTimes();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient(httpClientMock);
		
		String s = this.httpClient.post(
				"abc",
				"postData");
		
		Assert.assertEquals("response", s);
		
		Assert.assertEquals(0, this.httpClient.getBytesReceived());
		Assert.assertEquals(0, this.httpClient.getGetRequestCount());
		Assert.assertEquals(Float.NaN, this.httpClient.getGetRequestSuccessRate(), 0);
		Assert.assertEquals(1, this.httpClient.getPostRequestCount());
		Assert.assertEquals(1.0f, this.httpClient.getPostRequestSuccessRate(), 0);
		Assert.assertEquals("apache-commons-3.1", this.httpClient.getUserAgent());
	}
	
	@Test
	public void testEmptyConstructor() throws Exception {
		httpClientMock = PowerMock.createMock(org.apache.commons.httpclient.HttpClient.class);
		httpClientParamsMock = EasyMock.createNiceMock(HttpClientParams.class);
		
		EasyMock.expect(httpClientMock.getParams()).andReturn(httpClientParamsMock).anyTimes();
		PowerMock.expectNew(org.apache.commons.httpclient.HttpClient.class).andReturn(httpClientMock);
		
		httpClientMock.setHttpConnectionManager((HttpConnectionManager) EasyMock.anyObject());
		EasyMock.expectLastCall();
		
		httpClientMock.setParams((HttpClientParams) EasyMock.anyObject());
		EasyMock.expectLastCall();
		
		PowerMock.replayAll();
		
		this.httpClient = new BasicHttpClient();
		this.httpClient.setHttpClient(httpClientMock);
		this.httpClient.setUserAgent("abc");
		this.httpClient.setAllowSelfSignedCertificates(true);
	}
}