package com.janoside.http;

import java.security.KeyStore;
import java.security.cert.X509Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({TrustManagerFactory.class, EasyX509TrustManagerTest.class, EasyX509TrustManager.class})
@PowerMockIgnore({"javax.security.*"})
public class EasyX509TrustManagerTest {
	
	@Test
	public void testIt() throws Exception {
		PowerMock.mockStatic(TrustManagerFactory.class);
		
		KeyStore keystoreMock = PowerMock.createMock(KeyStore.class);
		X509TrustManager trustManagerMock = PowerMock.createMock(X509TrustManager.class);
		TrustManagerFactory trustManagerFactoryMock = PowerMock.createMock(TrustManagerFactory.class);
		X509Certificate certMock = PowerMock.createMock(X509Certificate.class);
		
		X509Certificate[] certs = new X509Certificate[] { certMock };
		
		EasyMock.expect(TrustManagerFactory.getDefaultAlgorithm()).andReturn("algo");
		EasyMock.expect(TrustManagerFactory.getInstance("algo")).andReturn(trustManagerFactoryMock);
		EasyMock.expect(trustManagerFactoryMock.getTrustManagers()).andReturn(new TrustManager[] {trustManagerMock});
		
		trustManagerFactoryMock.init(keystoreMock);
		EasyMock.expectLastCall();
		
		trustManagerMock.checkClientTrusted(certs, "authType");
		EasyMock.expectLastCall();
		
		EasyMock.expect(trustManagerMock.getAcceptedIssuers()).andReturn(certs).anyTimes();
		
		certMock.checkValidity();
		EasyMock.expectLastCall();
		
		PowerMock.replayAll();
		
		EasyX509TrustManager tm = new EasyX509TrustManager(keystoreMock);
		tm.checkClientTrusted(certs, "authType");
		
		tm.checkServerTrusted(certs, "authType");
		
		Assert.assertEquals(1, tm.getAcceptedIssuers().length);
		Assert.assertEquals(certs[0], tm.getAcceptedIssuers()[0]);
		
		PowerMock.verifyAll();
	}
	
	@Test
	public void testAnotherBranch() throws Exception {
		PowerMock.mockStatic(TrustManagerFactory.class);
		
		KeyStore keystoreMock = PowerMock.createMock(KeyStore.class);
		X509TrustManager trustManagerMock = PowerMock.createMock(X509TrustManager.class);
		TrustManagerFactory trustManagerFactoryMock = PowerMock.createMock(TrustManagerFactory.class);
		X509Certificate certMock = PowerMock.createMock(X509Certificate.class);
		X509Certificate certMock2 = PowerMock.createMock(X509Certificate.class);
		
		X509Certificate[] certs = new X509Certificate[] { certMock, certMock2 };
		
		EasyMock.expect(TrustManagerFactory.getDefaultAlgorithm()).andReturn("algo");
		EasyMock.expect(TrustManagerFactory.getInstance("algo")).andReturn(trustManagerFactoryMock);
		EasyMock.expect(trustManagerFactoryMock.getTrustManagers()).andReturn(new TrustManager[] {trustManagerMock});
		
		trustManagerFactoryMock.init(keystoreMock);
		EasyMock.expectLastCall();
		
		trustManagerMock.checkServerTrusted(certs, "authType");
		EasyMock.expectLastCall();
		
		PowerMock.replayAll();
		
		EasyX509TrustManager tm = new EasyX509TrustManager(keystoreMock);
		tm.checkServerTrusted(certs, "authType");
		
		PowerMock.verifyAll();
	}
}