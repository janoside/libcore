package com.janoside.http;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.security.KeyStore;
import java.security.SecureRandom;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({
	SSLContext.class,
	EasySSLProtocolSocketFactory.class,
	EasySSLProtocolSocketFactoryTest.class,
	InetSocketAddress.class,
	TrustManagerFactory.class})
public class EasySSLProtocolSocketFactoryTest {
	
	@Test
	public void testSimple() throws Exception {
		PowerMock.mockStatic(SSLContext.class);
		PowerMock.mockStatic(TrustManagerFactory.class);
		
		SSLContext sslContextMock = PowerMock.createMock(SSLContext.class);
		TrustManagerFactory trustManagerFactoryMock = PowerMock.createMock(TrustManagerFactory.class);
		X509TrustManager trustManagerMock = PowerMock.createMock(X509TrustManager.class);
		SSLSocketFactory sslSocketFactoryMock = PowerMock.createMock(SSLSocketFactory.class);
		SSLSocket socketMock = PowerMock.createMock(SSLSocket.class);
		
		EasyMock.expect(SSLContext.getInstance("SSL")).andReturn(sslContextMock);
		EasyMock.expect(TrustManagerFactory.getDefaultAlgorithm()).andReturn("WHOA");
		EasyMock.expect(TrustManagerFactory.getInstance(EasyMock.anyString())).andReturn(trustManagerFactoryMock);
		EasyMock.expect(sslContextMock.getSocketFactory()).andReturn(sslSocketFactoryMock).anyTimes();
		
		sslContextMock.init(
				(KeyManager[]) EasyMock.eq(null),
				(TrustManager[]) EasyMock.anyObject(),
				(SecureRandom) EasyMock.eq(null));
		
		EasyMock.expectLastCall();
		
		
		trustManagerFactoryMock.init((KeyStore) null);
		EasyMock.expectLastCall();
		
		EasyMock.expect(trustManagerFactoryMock.getTrustManagers()).andReturn(new TrustManager[] {trustManagerMock});
		
		EasyMock.expect(sslSocketFactoryMock.createSocket("abc", 123)).andReturn(socketMock);
		
		PowerMock.replayAll();
		
		EasySSLProtocolSocketFactory fac = new EasySSLProtocolSocketFactory();
		Assert.assertEquals(socketMock, fac.createSocket("abc", 123));
		
		PowerMock.verifyAll();
		PowerMock.resetAll();
	}
	
	@Test
	public void test2() throws Exception {
		PowerMock.mockStatic(SSLContext.class);
		PowerMock.mockStatic(TrustManagerFactory.class);
		
		SSLContext sslContextMock = PowerMock.createMock(SSLContext.class);
		TrustManagerFactory trustManagerFactoryMock = PowerMock.createMock(TrustManagerFactory.class);
		X509TrustManager trustManagerMock = PowerMock.createMock(X509TrustManager.class);
		SSLSocketFactory sslSocketFactoryMock = PowerMock.createMock(SSLSocketFactory.class);
		SSLSocket socketMock = PowerMock.createMock(SSLSocket.class);
		
		EasyMock.expect(SSLContext.getInstance("SSL")).andReturn(sslContextMock);
		EasyMock.expect(TrustManagerFactory.getDefaultAlgorithm()).andReturn("WHOA");
		EasyMock.expect(TrustManagerFactory.getInstance(EasyMock.anyString())).andReturn(trustManagerFactoryMock);
		EasyMock.expect(sslContextMock.getSocketFactory()).andReturn(sslSocketFactoryMock).anyTimes();
		
		sslContextMock.init(
				(KeyManager[]) EasyMock.eq(null),
				(TrustManager[]) EasyMock.anyObject(),
				(SecureRandom) EasyMock.eq(null));
		
		EasyMock.expectLastCall();
		
		
		trustManagerFactoryMock.init((KeyStore) null);
		EasyMock.expectLastCall();
		
		EasyMock.expect(trustManagerFactoryMock.getTrustManagers()).andReturn(new TrustManager[] {trustManagerMock});
		
		EasyMock.expect(sslSocketFactoryMock.createSocket("abc", 123, null, 345)).andReturn(socketMock);
		
		PowerMock.replayAll();
		
		EasySSLProtocolSocketFactory fac = new EasySSLProtocolSocketFactory();
		Assert.assertFalse(fac.equals(null));
		Assert.assertFalse(fac.equals(new Object()));
		Assert.assertTrue(fac.equals(fac));
		Assert.assertTrue(fac.equals(new EasySSLProtocolSocketFactory()));
		Assert.assertEquals(EasySSLProtocolSocketFactory.class.hashCode(), fac.hashCode());
		
		Assert.assertEquals(socketMock, fac.createSocket("abc", 123, null, 345));
		
		PowerMock.verifyAll();
		PowerMock.resetAll();
	}
	
	@Test
	public void test3() throws Exception {
		PowerMock.mockStatic(SSLContext.class);
		PowerMock.mockStatic(TrustManagerFactory.class);
		
		SSLContext sslContextMock = PowerMock.createMock(SSLContext.class);
		TrustManagerFactory trustManagerFactoryMock = PowerMock.createMock(TrustManagerFactory.class);
		X509TrustManager trustManagerMock = PowerMock.createMock(X509TrustManager.class);
		SSLSocketFactory sslSocketFactoryMock = PowerMock.createMock(SSLSocketFactory.class);
		SSLSocket sslSocketMock = PowerMock.createMock(SSLSocket.class);
		Socket socketMock = PowerMock.createMock(Socket.class);
		
		EasyMock.expect(SSLContext.getInstance("SSL")).andReturn(sslContextMock);
		EasyMock.expect(TrustManagerFactory.getDefaultAlgorithm()).andReturn("WHOA");
		EasyMock.expect(TrustManagerFactory.getInstance(EasyMock.anyString())).andReturn(trustManagerFactoryMock);
		EasyMock.expect(sslContextMock.getSocketFactory()).andReturn(sslSocketFactoryMock).anyTimes();
		
		sslContextMock.init(
				(KeyManager[]) EasyMock.eq(null),
				(TrustManager[]) EasyMock.anyObject(),
				(SecureRandom) EasyMock.eq(null));
		
		EasyMock.expectLastCall();
		
		
		trustManagerFactoryMock.init((KeyStore) null);
		EasyMock.expectLastCall();
		
		EasyMock.expect(trustManagerFactoryMock.getTrustManagers()).andReturn(new TrustManager[] {trustManagerMock});
		
		EasyMock.expect(sslSocketFactoryMock.createSocket(socketMock, "abc", 123, true)).andReturn(sslSocketMock);
		
		PowerMock.replayAll();
		
		EasySSLProtocolSocketFactory fac = new EasySSLProtocolSocketFactory();
		Assert.assertEquals(sslSocketMock, fac.createSocket(socketMock, "abc", 123, true));
		
		PowerMock.verifyAll();
		PowerMock.resetAll();
	}
	
	@Test
	public void testTimeoutZero() throws Exception {
		PowerMock.mockStatic(SSLContext.class);
		PowerMock.mockStatic(TrustManagerFactory.class);
		
		SSLContext sslContextMock = PowerMock.createMock(SSLContext.class);
		TrustManagerFactory trustManagerFactoryMock = PowerMock.createMock(TrustManagerFactory.class);
		X509TrustManager trustManagerMock = PowerMock.createMock(X509TrustManager.class);
		SSLSocketFactory sslSocketFactoryMock = PowerMock.createMock(SSLSocketFactory.class);
		SSLSocket sslSocketMock = PowerMock.createMock(SSLSocket.class);
		HttpConnectionParams httpConnectionParamsMock = PowerMock.createMock(HttpConnectionParams.class);
		InetAddress inetAddress = InetAddress.getByName("localhost");
		
		
		EasyMock.expect(SSLContext.getInstance("SSL")).andReturn(sslContextMock);
		EasyMock.expect(TrustManagerFactory.getDefaultAlgorithm()).andReturn("WHOA");
		EasyMock.expect(TrustManagerFactory.getInstance(EasyMock.anyString())).andReturn(trustManagerFactoryMock);
		EasyMock.expect(sslContextMock.getSocketFactory()).andReturn(sslSocketFactoryMock).anyTimes();
		EasyMock.expect(httpConnectionParamsMock.getConnectionTimeout()).andReturn(0);
		
		sslContextMock.init(
				(KeyManager[]) EasyMock.eq(null),
				(TrustManager[]) EasyMock.anyObject(),
				(SecureRandom) EasyMock.eq(null));
		
		EasyMock.expectLastCall();
		
		
		trustManagerFactoryMock.init((KeyStore) null);
		EasyMock.expectLastCall();
		
		EasyMock.expect(trustManagerFactoryMock.getTrustManagers()).andReturn(new TrustManager[] {trustManagerMock});
		
		EasyMock.expect(sslSocketFactoryMock.createSocket("abc", 123, inetAddress, 234)).andReturn(sslSocketMock);
		
		PowerMock.replayAll();
		
		EasySSLProtocolSocketFactory fac = new EasySSLProtocolSocketFactory();
		Assert.assertEquals(sslSocketMock, fac.createSocket("abc", 123, inetAddress, 234, httpConnectionParamsMock));
		
		PowerMock.verifyAll();
		PowerMock.resetAll();
	}
	
	@Test
	public void testTimeoutNonZero() throws Exception {
		PowerMock.mockStatic(SSLContext.class);
		PowerMock.mockStatic(TrustManagerFactory.class);
		
		SSLContext sslContextMock = PowerMock.createMock(SSLContext.class);
		TrustManagerFactory trustManagerFactoryMock = PowerMock.createMock(TrustManagerFactory.class);
		X509TrustManager trustManagerMock = PowerMock.createMock(X509TrustManager.class);
		SSLSocketFactory sslSocketFactoryMock = PowerMock.createMock(SSLSocketFactory.class);
		SSLSocket sslSocketMock = PowerMock.createMock(SSLSocket.class);
		HttpConnectionParams httpConnectionParamsMock = PowerMock.createMock(HttpConnectionParams.class);
		InetAddress inetAddressMock = PowerMock.createMock(InetAddress.class);
		final SocketAddress localSocketAddressMock = PowerMock.createMock(SocketAddress.class);
		final SocketAddress remoteSocketAddressMock = PowerMock.createMock(SocketAddress.class);
		
		
		SocketAddressFactory sockAddrFacMock = new SocketAddressFactory() {
			public SocketAddress create(String host, int port) {
				return remoteSocketAddressMock;
			}
			
			public SocketAddress create(InetAddress host, int port) {
				return localSocketAddressMock;
			}
		};
		
		
		EasyMock.expect(SSLContext.getInstance("SSL")).andReturn(sslContextMock);
		EasyMock.expect(TrustManagerFactory.getDefaultAlgorithm()).andReturn("WHOA");
		EasyMock.expect(TrustManagerFactory.getInstance(EasyMock.anyString())).andReturn(trustManagerFactoryMock);
		EasyMock.expect(sslContextMock.getSocketFactory()).andReturn(sslSocketFactoryMock).anyTimes();
		EasyMock.expect(httpConnectionParamsMock.getConnectionTimeout()).andReturn(3);
		EasyMock.expect(sslSocketFactoryMock.createSocket()).andReturn(sslSocketMock).anyTimes();
		
		sslContextMock.init(
				(KeyManager[]) EasyMock.eq(null),
				(TrustManager[]) EasyMock.anyObject(),
				(SecureRandom) EasyMock.eq(null));
		
		EasyMock.expectLastCall();
		
		
		trustManagerFactoryMock.init((KeyStore) null);
		EasyMock.expectLastCall();
		
		EasyMock.expect(trustManagerFactoryMock.getTrustManagers()).andReturn(new TrustManager[] {trustManagerMock});
		
		sslSocketMock.bind(localSocketAddressMock);
		EasyMock.expectLastCall();
		
		sslSocketMock.connect(remoteSocketAddressMock, 3);
		EasyMock.expectLastCall();
		
		PowerMock.replayAll();
		
		EasySSLProtocolSocketFactory fac = new EasySSLProtocolSocketFactory();
		fac.setSocketAddressFactory(sockAddrFacMock);
		
		Assert.assertEquals(sslSocketMock, fac.createSocket("abc", 123, inetAddressMock, 234, httpConnectionParamsMock));
		
		PowerMock.verifyAll();
		PowerMock.resetAll();
	}
	
	@Test
	public void testInitException() throws Exception {
		PowerMock.mockStatic(SSLContext.class);
		PowerMock.mockStatic(TrustManagerFactory.class);
		
		SSLContext sslContextMock = PowerMock.createMock(SSLContext.class);
		TrustManagerFactory trustManagerFactoryMock = PowerMock.createMock(TrustManagerFactory.class);
		X509TrustManager trustManagerMock = PowerMock.createMock(X509TrustManager.class);
		SSLSocketFactory sslSocketFactoryMock = PowerMock.createMock(SSLSocketFactory.class);
		SSLSocket sslSocketMock = PowerMock.createMock(SSLSocket.class);
		HttpConnectionParams httpConnectionParamsMock = PowerMock.createMock(HttpConnectionParams.class);
		InetAddress inetAddressMock = PowerMock.createMock(InetAddress.class);
		final SocketAddress localSocketAddressMock = PowerMock.createMock(SocketAddress.class);
		final SocketAddress remoteSocketAddressMock = PowerMock.createMock(SocketAddress.class);
		
		
		SocketAddressFactory sockAddrFacMock = new SocketAddressFactory() {
			public SocketAddress create(String host, int port) {
				return remoteSocketAddressMock;
			}
			
			public SocketAddress create(InetAddress host, int port) {
				return localSocketAddressMock;
			}
		};
		
		
		EasyMock.expect(SSLContext.getInstance("SSL")).andReturn(sslContextMock);
		EasyMock.expect(TrustManagerFactory.getDefaultAlgorithm()).andReturn("WHOA");
		EasyMock.expect(TrustManagerFactory.getInstance(EasyMock.anyString())).andReturn(trustManagerFactoryMock);
		EasyMock.expect(sslContextMock.getSocketFactory()).andReturn(sslSocketFactoryMock).anyTimes();
		EasyMock.expect(httpConnectionParamsMock.getConnectionTimeout()).andReturn(3);
		EasyMock.expect(sslSocketFactoryMock.createSocket()).andReturn(sslSocketMock).anyTimes();
		
		sslContextMock.init(
				(KeyManager[]) EasyMock.eq(null),
				(TrustManager[]) EasyMock.anyObject(),
				(SecureRandom) EasyMock.eq(null));
		
		EasyMock.expectLastCall().andThrow(new RuntimeException());
		
		
		trustManagerFactoryMock.init((KeyStore) null);
		EasyMock.expectLastCall();
		
		EasyMock.expect(trustManagerFactoryMock.getTrustManagers()).andReturn(new TrustManager[] {trustManagerMock});
		
		sslSocketMock.bind(localSocketAddressMock);
		EasyMock.expectLastCall();
		
		sslSocketMock.connect(remoteSocketAddressMock, 3);
		EasyMock.expectLastCall();
		
		PowerMock.replayAll();
		
		EasySSLProtocolSocketFactory fac = new EasySSLProtocolSocketFactory();
		fac.setSocketAddressFactory(sockAddrFacMock);
		
		try {
			fac.createSocket("abc", 123, inetAddressMock, 234, httpConnectionParamsMock);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}