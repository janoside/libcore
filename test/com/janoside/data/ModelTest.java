package com.janoside.data;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class ModelTest {
	
	@Test
	public void testIt() {
		Model m = new Model() {
		};
		
		m.setId(5);
		Assert.assertEquals(5, m.getId());
		
		m.setCreatedAt(new Date(1000));
		Assert.assertEquals(1000, m.getCreatedAt().getTime());
		
		m.setCreatedAt(null);
		Assert.assertNull(m.getCreatedAt());
		
		m.setCreatedBy(7);
		Assert.assertEquals(7, m.getCreatedBy());
		
		m.setUpdatedAt(new Date(1000));
		Assert.assertEquals(1000, m.getUpdatedAt().getTime());
		
		m.setUpdatedAt(null);
		Assert.assertNull(m.getUpdatedAt());
		
		m.setUpdatedBy(9);
		Assert.assertEquals(9, m.getUpdatedBy());
		
		Assert.assertFalse(m.isEncrypted());
		m.setEncrypted(true);
		Assert.assertTrue(m.isEncrypted());
		
		Assert.assertFalse(m.isDecrypted());
		m.setDecrypted(true);
		Assert.assertTrue(m.isDecrypted());
	}
}