package com.janoside.factory;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Test;

public class DelegatingKeyedObjectFactoryTest {
	
	@Test
	public void testIt() {
		ArrayList<KeyedObjectFactory> factories = new ArrayList<KeyedObjectFactory>();
		factories.add(new KeyedObjectFactory() {
			public <T> T createObject(String type) {
				if (type.equals("str")) {
					return (T) "cheese";
				}
				
				return null;
			}
		});
		
		DelegatingKeyedObjectFactory f = new DelegatingKeyedObjectFactory();
		f.setFactories(factories);
		
		Assert.assertEquals("cheese", f.createObject("str"));
		Assert.assertNull(f.createObject("str2"));
	}
}