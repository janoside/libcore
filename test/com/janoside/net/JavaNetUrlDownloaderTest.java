package com.janoside.net;

import java.io.File;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.FileUtil;
import com.janoside.util.RandomUtil;
import com.janoside.util.StringUtil;

public class JavaNetUrlDownloaderTest {
	
	@Test
	public void testIt() throws Exception {
		String uuid = RandomUtil.randomUuid().toString();
		String filepath = filepathFromUuid(uuid);
		String url = urlFromFilepath(filepath);
		
		FileUtil.writeBytes(filepath, StringUtil.utf8BytesFromString("abcdef"));
		
		JavaNetUrlDownloader dl = new JavaNetUrlDownloader();
		
		Assert.assertEquals("text/plain", dl.getContentType(url));
		Assert.assertEquals("abcdef", StringUtil.utf8StringFromBytes(dl.getData(url)));
		
		long diff = System.currentTimeMillis() - dl.getLastModified(url).getTime();
		Assert.assertTrue("Diff: " + diff, diff < 2000);
		
		
		try {
			dl.getData("file:///tmp/non-existent-file-" + UUID.randomUUID().toString());
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			dl.getData("malformed-url");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		
		filepath = filepathFromUuid(RandomUtil.randomUuid().toString());
		url = urlFromFilepath(filepath);
		
		File file = new File(filepath);
		file.setLastModified(0);
		
		Assert.assertNull(dl.getLastModified(url));
	}
	
	private String urlFromFilepath(String filepath) {
		return String.format("file://%s", filepath);
	}
	
	private String filepathFromUuid(String uuid) {
		return String.format("/tmp/JavaNetUrlDownloaderTest-%s.txt", uuid);
	}
}