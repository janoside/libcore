package com.janoside.net;

import java.util.Map;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.janoside.http.BasicHttpClient;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FileUploaderTest.class, FileUploader.class, BasicHttpClient.class})
public class FileUploaderTest {
	
	@Test
	public void testIt() throws Exception {
		BasicHttpClient httpClientMock = PowerMock.createMock(BasicHttpClient.class);
		
		PowerMock.expectNew(BasicHttpClient.class).andReturn(httpClientMock);
		
		EasyMock.expect(httpClientMock.post(
				(String) EasyMock.anyObject(),
				(Map) EasyMock.anyObject(),
				(byte[]) EasyMock.anyObject(),
				(String) EasyMock.anyObject(),
				EasyMock.anyInt())).andReturn("success");
		
		PowerMock.replayAll();
		
		FileUploader fu = new FileUploader();
		Assert.assertEquals("success", fu.upload("abc", "abc".getBytes()));
		
		PowerMock.verifyAll();
	}
}