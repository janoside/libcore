package com.janoside.codec;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class GzipBase91StringEncoderTest {
	
	@Test
	public void testIt() {
		ArrayList<BinaryEncoder> encoders = new ArrayList<BinaryEncoder>();
		encoders.add(new GzipEncoder());
		encoders.add(new Base91Encoder());
		
		DelegatingStringEncoder enc = new DelegatingStringEncoder();
		enc.setEncoder(new ChainEncoder(encoders));
		
		Random random = new Random(5);
		String uuid = RandomUtil.randomUuid(random).toString();
		
		String x = enc.encode(uuid);
		
		Assert.assertEquals("af)AAAAAAAv(2iT*m6|TzO`#<4@P=lV*91%;{0FT>mLd{0|RrI.(]@)#EAT,<_rMBAAA", x);
		
		x = enc.decode(x);
		
		Assert.assertEquals(uuid, x);
	}
}