package com.janoside.codec;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class HexByteArrayStringEncoderTest {
	
	@Test
	public void testIt() {
		HexByteArrayStringEncoder enc = new HexByteArrayStringEncoder();
		byte[] data = enc.decode(enc.encode("abc".getBytes()));
		
		Assert.assertEquals("[97, 98, 99]", Arrays.toString(data));
	}
	
	@Test
	public void testException() throws Exception {
		HexByteArrayStringEncoder enc = new HexByteArrayStringEncoder();
		
		try {
			enc.decode("abc");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}