package com.janoside.codec;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class GzipBase64StringEncoderTest {
	
	@Test
	public void testIt() {
		Random random = new Random(5);
		String uuid = RandomUtil.randomUuid(random).toString();
		
		GzipBase64StringEncoder enc = new GzipBase64StringEncoder();
		String x = enc.encode(uuid);
		
		Assert.assertEquals("H4sIAAAAAAAAADMxTUxMMU5L0bU0tLTQNU5MMdW1tDQ20E1LSUsxMzAysDBPtQQAVEbxbyQAAAA=", x);
		
		x = enc.decode(x);
		
		Assert.assertEquals(uuid, x);
	}
}