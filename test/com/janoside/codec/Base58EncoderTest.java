package com.janoside.codec;

import org.junit.Assert;
import org.junit.Test;

public class Base58EncoderTest {
	
	@Test
	public void testIt() {
		Base58Encoder enc = new Base58Encoder();
		byte[] x = enc.encode(new byte[] {});
		
		Assert.assertEquals(0, x.length);
	}
}