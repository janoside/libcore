package com.janoside.codec;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.JunitTestUtil;
import com.janoside.hash.Sha256StringHasher;
import com.janoside.util.RandomUtil;
import com.janoside.util.StringUtil;

public class Base58Test {
	
	@Test
	public void testDecodeEmptyArray() {
		byte[] data = Base58.decode(new byte[] {});
		
		Assert.assertEquals(0, data.length);
	}
	
	@Test
	public void testEncodeLeadingZeroes() {
		byte[] data = Base58.encode(new byte[] {0, 0, 0, 5, 6, 4});
		
		Assert.assertEquals("[49, 49, 49, 50, 103, 115, 68]", Arrays.toString(data));
	}
	
	@Test
	public void testEncodeAllZeroes() {
		byte[] data = Base58.encode(new byte[] {0, 0, 0, 0});
		
		Assert.assertEquals("[49, 49, 49, 49]", Arrays.toString(data));
	}
	
	@Test
	public void testCrazyData() {
		Random r = new Random(5);
		Base58Encoder enc = new Base58Encoder();
		
		StringBuilder buffer = new StringBuilder();
		for (int i = 0; i < 150; i++) {
			byte[] data = RandomUtil.randomByteArray(250, r);
			
			buffer.append(StringUtil.utf8StringFromBytes(enc.encode(data)));
		}
		
		Sha256StringHasher hasher = new Sha256StringHasher();
		
		Assert.assertEquals("2d689720f83943905b3a6e74596e6290a4716acfdffc4d1887559e931ec6ef04", hasher.hash(buffer.toString()));
	}
	
	@Test
	public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		JunitTestUtil.testConstructorIsPrivate(Base58.class);
	}
}