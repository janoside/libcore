package com.janoside.codec;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class GzipBase58StringEncoderTest {
	
	@Test
	public void testIt() {
		Random random = new Random(5);
		String uuid = RandomUtil.randomUuid(random).toString();
		
		GzipBase58StringEncoder enc = new GzipBase58StringEncoder();
		String x = enc.encode(uuid);
		
		Assert.assertEquals("rYdXZbmCvQShXoivrrX6fWBt39CZgsL3iJ8r3KKAo31K23C4ur2EvmBw7oPko6zAPPqfFRWQyg1u", x);
		
		x = enc.decode(x);
		
		Assert.assertEquals(uuid, x);
	}
	
	@Test
	public void testWrongCharDecode() throws Exception {
		GzipBase58StringEncoder enc = new GzipBase58StringEncoder();
		
		try {
			enc.decode("-");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}