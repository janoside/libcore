package com.janoside.security;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.security.encryption.NoopEncryptor;

public class KeystoreTest {
	
	@Test
	public void testIt() {
		Keystore ks = new Keystore();
		ks.setEncryptor(new NoopEncryptor());
		ks.addKeypair("g1", "abc", "def");
		ks.addKeypair("g1", "abc2", "def2");
		
		Assert.assertEquals("def", ks.getPrivateKey("abc"));
		
		Assert.assertEquals(2, ks.getKeypairGroup("g1").size());
		
		Keypair kp = ks.getKeypairByPublicKey("abc");
		
		Assert.assertEquals("g1", kp.getGroupName());
		Assert.assertEquals("abc", kp.getPublicKey());
		Assert.assertEquals("def", kp.getEncryptedPrivateKey());
		
		Keypair kp2 = new Keypair();
		kp2.setPublicKey("pub");
		kp2.setEncryptedPrivateKey("encPriv");
		
		ks.addKeypair(kp2);
		
		Assert.assertEquals(kp2, ks.getKeypairByPublicKey("pub"));
	}
}