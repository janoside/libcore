package com.janoside.security;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class HmacSha1SignerTest {
	
	@Test
	public void testIt() throws Exception {
		Assert.assertEquals("a5892d899601befea5eb25379148ff9071f45d84", new HmacSha1Signer().sign("abc", "one"));
		
		Random r = new Random(1);
		StringBuilder contentBuffer = new StringBuilder();
		for (int i = 0; i < 1000; i++) {
			contentBuffer.append(RandomUtil.randomUuid(r).toString());
		}
		
		String key = RandomUtil.randomUuid(r).toString();
		
		Assert.assertEquals("9f3dd1758e49e93d1b9086cf4cd17aa4874fa35f", new HmacSha1Signer().sign(contentBuffer.toString(), key));
		
		
		r = new Random(1);
		contentBuffer = new StringBuilder();
		for (int i = 0; i < 1000; i++) {
			contentBuffer.append(new String(RandomUtil.randomByteArray(50, r), "UTF-8"));
		}
		
		key = RandomUtil.randomUuid(r).toString();
		
		Assert.assertEquals("29b26c9aa9f19311158bad92edaafb03c23d1755", new HmacSha1Signer().sign(contentBuffer.toString(), key));
	}
}