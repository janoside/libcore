package com.janoside.security;

import org.junit.Assert;
import org.junit.Test;

public class RateLimiterTest {
	
	@Test
	public void testBasic() {
		RateLimiter rateLimiter = new RateLimiter();
		rateLimiter.setPeriod(1000);
		rateLimiter.setMaxOccurrencesPerPeriod(10);
		
		Assert.assertEquals(1000, rateLimiter.getPeriod());
		Assert.assertEquals(0, rateLimiter.getCurrentPeriodStart());
		
		int allowances = 0;
		for (int i = 0; i < 25; i++) {
			if (rateLimiter.canPerform()) {
				allowances++;
			}
		}
		
		Assert.assertEquals("allowances", 10, allowances);
		Assert.assertEquals(10, rateLimiter.getCurrentPeriodOccurrences());
		Assert.assertTrue(System.currentTimeMillis() - rateLimiter.getCurrentPeriodStart() < 1000);
	}
	
	@Test
	public void testReset() throws InterruptedException {
		RateLimiter rateLimiter = new RateLimiter();
		rateLimiter.setPeriod(25);
		rateLimiter.setMaxOccurrencesPerPeriod(10);
		
		int allowances = 0;
		for (int i = 0; i < 25; i++) {
			if (rateLimiter.canPerform()) {
				allowances++;
			}
		}
		
		Thread.sleep(25);
		
		for (int i = 0; i < 25; i++) {
			if (rateLimiter.canPerform()) {
				allowances++;
			}
		}
		
		Assert.assertEquals("allowances", 20, allowances);
	}
	
	@Test
	public void testNonsense() {
		RateLimiter rl = new RateLimiter();
		rl.setMaxOccurrencesPerPeriod(0);
		Assert.assertEquals(0, rl.getMaxOccurrencesPerPeriod());
		Assert.assertEquals(0, rl.getCurrentPeriodOccurrences());
		
		Assert.assertFalse(rl.canPerform());
	}
}