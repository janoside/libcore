package com.janoside.security.encryption;

import java.util.Arrays;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class AesBinaryEncryptorTest {
	
	@Test
	public void testStoredEncryptedValueWithPasswordAndSalt() {
		AesBinaryEncryptor e = new AesBinaryEncryptor("monkey");
		
		Assert.assertEquals("12348", new String(e.decrypt(new byte[] {20, -50, -40, -62, 9, -114, 43, -39, 124, -39, 56, 52, -79, 45, -122, 105, 0, 0, 0, 22, -81, 77, -33, 35, 119, 38, -103, -85, 120, -56, -12, 5, -36, 65, -32, -33, 68, 29, 122, 98, -22, -114, 27, -30, 88, 47, -6, -6, 48, 50, 53, 54, 48, 48, 48, 48, 48, 57, 50, 49, 53, 52})));
		Assert.assertEquals("12348", new String(e.decrypt(new byte[] {118, -40, 123, -116, 87, -1, -22, 55, 120, -43, -64, 39, -102, 53, 64, -45, 0, 0, 0, 22, -81, 77, -32, -109, 100, -12, -22, -8, -123, 67, 121, -106, -41, -79, -57, -10, -103, 106, 91, 57, 55, -40, 18, 115, 59, 22, 42, -59, 48, 50, 53, 54, 48, 48, 48, 48, 48, 54, 56, 56, 52, 56})));
	}
	
	@Test
	public void testStoredEncryptedValue() {
		AesBinaryEncryptor e = new AesBinaryEncryptor("cheese");
		
		Assert.assertEquals("12348", new String(e.decrypt(new byte[] {7, -24, 111, -117, -98, 100, -69, 95, 39, 102, -99, 57, 5, -28, 73, 0, 0, 0, 0, 22, -81, 77, -30, 46, 86, 89, 79, -5, -125, 105, 99, -77, 75, -59, -68, 26, 55, 115, -79, 76, 81, 34, 67, -53, -93, 116, -99, 7, 48, 50, 53, 54, 48, 48, 48, 48, 48, 56, 54, 48, 52, 56})));
	}
	
	@Test
	public void testEncryptDecrypt() {
		AesBinaryEncryptor encryptor = new AesBinaryEncryptor("some sweet password", 256, 1000);
		
		for (int i = 0; i < 100; i++) {
			String text = RandomUtil.randomWord(25 + RandomUtil.randomInt(100));
			
			byte[] output = encryptor.encrypt(text.getBytes());
			
			Assert.assertTrue("original cleartext vs. decrypted ciphertext", Arrays.equals(text.getBytes(), encryptor.decrypt(output)));
		}
	}
	
	@Test
	public void testSomethingLong() {
		AesBinaryEncryptor e = new AesBinaryEncryptor("abcd");
		
		Assert.assertEquals("this is the awesomest thing I've ever encrypted", new String(e.decrypt(new byte[] {-48, -57, 39, 21, 28, -7, -117, 35, -33, 13, -116, 3, -17, 25, 76, -114, 12, -127, 35, 99, -84, 57, 118, -56, -119, -67, 97, 14, -94, 73, 23, 32, 124, 110, 22, -18, 112, -128, 25, 19, -126, 73, 85, -96, -72, -123, 62, 49, 0, 0, 0, 22, -81, 77, -29, -72, -82, 96, 17, -106, -122, 80, 76, -16, 116, -52, 108, 11, -88, 122, 12, 34, 60, -35, 78, 83, 27, -90, 9, 17, 48, 50, 53, 54, 48, 48, 48, 48, 48, 56, 55, 53, 55, 48})));
	}
	
	@Test
	public void testStuff() {
		AesBinaryEncryptor e = new AesBinaryEncryptor("whoo hoo");
		
		Random r = new Random(5);
		for (int i = 0; i < 10; i++) {
			String content = RandomUtil.randomSentence(20, r);
			
			byte[] data = e.encrypt(content.getBytes());
			
			Assert.assertEquals(content, new String(e.decrypt(data)));
		}
	}
	
	@Test
	public void testVeryLongPassword() {
		AesBinaryEncryptor e = new AesBinaryEncryptor(RandomUtil.randomWord(10000));
		
		Random r = new Random(5);
		for (int i = 0; i < 10; i++) {
			String content = RandomUtil.randomSentence(20, r);
			
			byte[] data = e.encrypt(content.getBytes());
			
			Assert.assertEquals(content, new String(e.decrypt(data)));
		}
	}
	
	@Test
	public void testNull() throws Exception {
		AesBinaryEncryptor e = new AesBinaryEncryptor(RandomUtil.randomWord(10));
		
		try {
			e.decrypt(null);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		try {
			e.encrypt(null);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testPasswordMatters() throws Exception {
		AesBinaryEncryptor e1 = new AesBinaryEncryptor(RandomUtil.randomWord(100));
		AesBinaryEncryptor e2 = new AesBinaryEncryptor(RandomUtil.randomWord(100));
		
		String text = "abcdef";
		
		byte[] data = e1.encrypt(text.getBytes());
		
		try {
			Assert.assertFalse(text.equals(new String(e2.decrypt(data))));
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
}