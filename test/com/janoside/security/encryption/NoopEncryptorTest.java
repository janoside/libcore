package com.janoside.security.encryption;

import org.junit.Assert;
import org.junit.Test;

public class NoopEncryptorTest {
	
	@Test
	public void testIt() {
		NoopEncryptor enc = new NoopEncryptor();
		
		Assert.assertEquals("12345", enc.encrypt("12345"));
		Assert.assertEquals("12345", enc.decrypt("12345"));
	}
}