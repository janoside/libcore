package com.janoside.security.encryption;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.exception.StandardErrorExceptionHandler;
import com.janoside.stats.MemoryStats;
import com.janoside.stats.MemoryStatsStatTracker;

public class MigrationEncryptorTest {
	
	@Test
	public void testIt() {
		AesEncryptor e1 = new AesEncryptor("ahhhhhh");
		
		AesEncryptor e2 = new AesEncryptor("abc");
		
		String plaintext = "abcdef";
		
		MemoryStats memoryStats = new MemoryStats();
		
		MemoryStatsStatTracker st = new MemoryStatsStatTracker();
		st.setMemoryStats(memoryStats);
		
		MigrationEncryptor encryptor = new MigrationEncryptor();
		encryptor.setOldEncryptor(e1);
		encryptor.setNewEncryptor(e2);
		encryptor.setStatTracker(st);
		encryptor.setExceptionHandler(new StandardErrorExceptionHandler());
		
		String ciphertext1 = e1.encrypt(plaintext);
		String ciphertext2 = e2.encrypt(plaintext);
		
		Assert.assertEquals(plaintext, encryptor.decrypt(ciphertext1));
		Assert.assertEquals(plaintext, encryptor.decrypt(ciphertext2));
		Assert.assertEquals(plaintext, encryptor.decrypt(ciphertext1));
		Assert.assertEquals(plaintext, encryptor.decrypt(ciphertext2));
		
		encryptor = new MigrationEncryptor();
		encryptor.setOldEncryptor(e2);
		encryptor.setNewEncryptor(e1);
		encryptor.setExceptionHandler(new StandardErrorExceptionHandler());
		
		Assert.assertEquals(plaintext, encryptor.decrypt(ciphertext1));
		Assert.assertEquals(plaintext, encryptor.decrypt(ciphertext2));
		Assert.assertEquals(plaintext, encryptor.decrypt(ciphertext1));
		Assert.assertEquals(plaintext, encryptor.decrypt(ciphertext2));
		
		String x = encryptor.encrypt("abc");
		Assert.assertEquals("abc", e1.decrypt(x));
		
		Assert.assertEquals(258005645, memoryStats.printEventReport().hashCode());
	}
}