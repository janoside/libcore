package com.janoside.security.encryption;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.codec.Base58Encoder;

public class AesEncryptorTest {
	
	@Test
	public void testStoredEncryptedValueWithPasswordAndSalt() {
		AesEncryptor e = new AesEncryptor("monkey");
		
		Assert.assertEquals("12348", e.decrypt("zHJYHICw/X3VGxSaHDUF4wAAABavXFBd8YWGceipemzfN+PAKaDA7ihnP20ITB2JMDI1NjAwMDAwOTQzMjA="));
	}
	
	@Test
	public void testStoredEncryptedValueWithPassword() {
		AesEncryptor e = new AesEncryptor("monkey");
		
		Assert.assertEquals("12348", e.decrypt("RNuTO3GE0xhxIJggrIncpgAAABavXFE0Eywl1FBex9U644bSgR7KBNjQmfN5vZ/lMDI1NjAwMDAwODQxOTM="));
	}
	
	@Test
	public void testDifferentEncoder() {
		AesEncryptor e = new AesEncryptor("monkey");
		e.setEncoder(new Base58Encoder());
		
		Assert.assertEquals("12348", e.decrypt("8Y7KhFSyYdUchqGDun3hMJzQ3UUmonDfC3GZFLgRJNJZxy7UisLJdh5KR2mUNWVeCewmfz5S8wMZzyTs5QVMb"));
	}
	
	@Test
	public void testStoredEncryptedValue() {
		AesEncryptor e = new AesEncryptor("hahaa");
		
		System.out.println("3=" + e.encrypt("12348"));
		
		Assert.assertEquals("12348", e.decrypt("iFYLSAR3c0xNsLrTzchrcwAAABavXFIM1szDWnFk5hHNzLSddQTKkGwreqqtxGm7MDI1NjAwMDAwODgwNDY="));
	}
	
	@Test
	public void testEncryptDecrypt() {
		AesEncryptor encryptor = new AesEncryptor("some sweet password", 256, 5000);
		
		Random random = new Random();
		
		String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		
		for (int i = 0; i < 10; i++) {
			int length = random.nextInt(25);
			
			StringBuilder buffer = new StringBuilder(length);
			for (int j = 0; j < length; j++) {
				buffer.append(chars.charAt(random.nextInt(chars.length())));
			}
			
			String output = encryptor.encrypt(buffer.toString());
			
			Assert.assertEquals("original cleartext vs. decrypted ciphertext", buffer.toString(), encryptor.decrypt(output));
		}
	}
	
	@Test
	public void testSomethingLong() {
		AesEncryptor e = new AesEncryptor("whoa");
		
		System.out.println("4=" + e.encrypt("this is the awesomest thing I've ever encrypted"));
		
		Assert.assertEquals("this is the awesomest thing I've ever encrypted", e.decrypt("wpuzhsH2Hg00AxSx4WicdWS/wtaoY6yQvNKEfhwbwBN+aqsM/RPV28HeCgSuP7EmAAAAFq9cU/+ofVP6tDfP+XFAWQNjLU848xMcAbbrczMwMjU2MDAwMDA3MTgwNA=="));
	}
	
	@Test
	public void testStuff() {
		AesEncryptor e = new AesEncryptor("aosdufhaosdfuhuoafoasufgoafgoaufgaosfguaofgaosfgaoudfgauosdfgaosdfug237rt23790rg723fg02f");
		
		System.out.println(e.encrypt("cheese is the best stuff in the world"));
	}
	
	@Test
	public void testReal() {
		//this.decrypt("", "");
	}
	
	public void encrypt(String password, String content) {
		AesEncryptor enc = new AesEncryptor(password);
		
		System.out.println("encrypt(" + content + ") -> " + enc.encrypt(content));
	}
	
	public void decrypt(String password, String content) {
		AesEncryptor enc = new AesEncryptor(password);
		
		System.out.println("decrypt(" + content + ") -> " + enc.decrypt(content));
	}
	
	@Test
	public void testSpeed() {
		AesEncryptor encryptor = new AesEncryptor("a00tysd0t7sdf0sdtfo23ugro", 256, 200);
		
		int count = 1000;
		
		ArrayList<String> things = new ArrayList<String>(count);
		ArrayList<String> things2 = new ArrayList<String>(count);
		
		for (int i = 0; i < count; i++) {
			things.add(UUID.randomUUID().toString());
		}
		
		long startTime = System.currentTimeMillis();
		
		for (String thing : things) {
			things2.add(encryptor.encrypt(thing));
		}
		
		System.out.println("Encrypt: " + (System.currentTimeMillis() - startTime));
		startTime = System.currentTimeMillis();
		
		for (String thing : things2) {
			encryptor.decrypt(thing);
		}
		
		System.out.println("Decrypt: " + (System.currentTimeMillis() - startTime));
		startTime = System.currentTimeMillis();
	}
}