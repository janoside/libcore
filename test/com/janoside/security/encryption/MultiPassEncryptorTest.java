package com.janoside.security.encryption;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.codec.Base64Encoder;

public class MultiPassEncryptorTest {
	
	@Test
	public void testEncryptDecrypt() {
		MultiPassBinaryEncryptor enc = new MultiPassBinaryEncryptor();
		enc.setEncryptor(new AesBinaryEncryptor("abc"));
		enc.setPassCount(5);
		
		System.out.println(new String(new Base64Encoder().encode(enc.encrypt("cheese".getBytes()))));
		
		Assert.assertEquals("cheese", new String(enc.decrypt(enc.encrypt("cheese".getBytes()))));
	}
}