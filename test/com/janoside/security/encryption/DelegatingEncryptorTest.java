package com.janoside.security.encryption;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.codec.Base64Encoder;
import com.janoside.codec.Base91Encoder;

public class DelegatingEncryptorTest {
	
	@Test
	public void testIt() {
		TextEncryptor de = new TextEncryptor();
		de.setEncryptor(new AesBinaryEncryptor("cheese"));
		de.setEncoder(new Base91Encoder());
		
		System.out.println("det1=" + de.encrypt("abcdef"));
		
		Assert.assertEquals("abcdef", de.decrypt("YbF:|bHj^q3|s9WbG2SGAA[9[l[FWEGSDy;bcN~+pH&^RtV_G{p0$=p}1UWiVdwJ,(KC{QK)gb&kA"));
		
		de.setEncoder(new Base64Encoder());
		
		System.out.println("det2=" + de.encrypt("abcdef"));
		
		Assert.assertEquals("abcdef", de.decrypt("lmGpFgYV75VbON/ZkVRfJQAAABavYTQrsgraB1dYSlio7qp1EV33h8GhBpW49TmIMDI1NjAwMDAwNjYxMjQ="));
	}
}