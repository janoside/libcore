package com.janoside.security.encryption;

import org.apache.commons.codec.binary.Base64;
import org.junit.Assert;
import org.junit.Test;

import com.janoside.codec.Base58Encoder;
import com.janoside.util.RandomUtil;

public class XorEncryptorTest {
	
	@Test
	public void testIt() {
		String data = "abcdef";
		
		XorEncryptor enc = new XorEncryptor("asdfuaghfo9agsdfayogsfasdf");
		
		Assert.assertEquals("ABEHAhAH", enc.encrypt(data));
		Assert.assertEquals(data, enc.decrypt(enc.encrypt(data)));
		
		data = "asdfouashdfaghef07f72-02g7327f0weg7f07wegf0sd7fg0sd7t6f0sdf";
		
		Assert.assertEquals("AAAAABoUBhsOC18AABsBAFFOCVBBS1FBA1FSQVMARRYCD1EJCVYQFgMAUQoLUBUBUQAAURVFAlYGBQE=", enc.encrypt(data));
		Assert.assertEquals(data, enc.decrypt(enc.encrypt(data)));
		
		enc.setEncoder(new Base58Encoder());
		
		Assert.assertEquals("1111AHG7Ko7F2q74eZZKLZqSU4g9fSwpG2wp6Bq5pKTu2WX6AD63X461hRVQQykqdj2apzxNNYz3CLg", enc.encrypt(data));
	}
	
	@Test
	public void testSetup() {
		int chainLength = 5;
		
		String data = "xyz";
		
		for (int i = 0; i < chainLength; i++) {
			String password = new String(Base64.encodeBase64(RandomUtil.randomByteArray(128)));
			
			XorEncryptor enc = new XorEncryptor(password);
			
			data = enc.encrypt(data);
			
			System.out.println("password " + i + ": " + password + "\n\tdata: " + data);
		}
	}
}