package com.janoside.security;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.ConnectException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.hash.Md5Hasher;
import com.janoside.util.DateUtil;

public class SecurityUtilTest {
	
	
	
	@Test
	public void testSecretKeyFactory() throws Exception {
		Assert.assertNotNull(SecurityUtil.getSecretKeyFactory("PBKDF2WithHmacSHA1"));
		
		try {
			SecurityUtil.getSecretKeyFactory("nonsense-algorithm");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testKeyFactory() throws Exception {
		Assert.assertNotNull(SecurityUtil.getKeyFactory("RSA"));
		
		try {
			SecurityUtil.getKeyFactory("nonsense-algorithm");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
	}
	
	@Test
	public void testSign() {
		HmacSha1Signer signer = new HmacSha1Signer();
		Md5Hasher privateKeyHasher = new Md5Hasher();
		
		HashMap params = new HashMap() {{
			put("abc", "def");
			put("ghi", "jkl");
			put("123", "456");
		}};
		
		String x = SecurityUtil.sign(
				signer,
				privateKeyHasher,
				params,
				1000,
				"publicKey",
				"privateKey",
				new HashSet() {{ add("ghi"); }});
		
		Assert.assertEquals("a73f2b5869ffbbe96bb181560005ac52a4f8fe2b", x);
		
		String y = SecurityUtil.sign(
				signer,
				privateKeyHasher,
				params,
				1000,
				"publicKey",
				"privateKey",
				new HashSet() {{ add("abc"); }});
		
		Assert.assertEquals("06a97bbe55d0cce24002601ff7e42d0031536c30", y);
		
		String z = SecurityUtil.sign(
				signer,
				privateKeyHasher,
				params,
				1000,
				"publicKey",
				"privateKey",
				new HashSet());
		
		Assert.assertEquals("de5979da169b4d133161e26c9a1f87cce18f51b7", z);
		
		String z1 = SecurityUtil.sign(
				signer,
				privateKeyHasher,
				params,
				1000,
				"publicKey",
				"privateKey");
		
		Assert.assertEquals(z, z1);
	}
	
	@Test
	public void testThing() {
		String x = SecurityUtil.getProvidersAndSupportedAlgorithms();
		
		System.out.println(x);
		
		Assert.assertNotNull(x);
	}
	
	@Test
	public void testGetMac() throws Exception {
		Assert.assertNotNull(SecurityUtil.getMac("abc".getBytes(), "HmacSHA1"));
		
		try {
			SecurityUtil.getMac("abc".getBytes(), "nonsense-algorithm");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			//  expected
		}
		
		try {
			SecurityUtil.getMac((SecretKeySpec) null, "HmacSHA1");
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			re.printStackTrace();
		}
	}
}