package com.janoside.security;

import org.junit.Assert;
import org.junit.Test;

public class KeypairTest {
	
	@Test
	public void testIt() throws Exception {
		Keypair kp = new Keypair();
		kp.setPublicKey("pub1");
		
		Assert.assertFalse(kp.equals(null));
		Assert.assertFalse(kp.equals(new Object()));
		Assert.assertFalse(kp.equals("abc"));
		
		Keypair kp2 = new Keypair();
		
		try {
			// NPE since kp2.publicKey is null
			kp.equals(kp2);
			
			throw new Exception();
			
		} catch (RuntimeException re) {
			// expected
		}
		
		kp2.setPublicKey("pub1");
		
		Assert.assertTrue(kp.equals(kp2));
	}
}