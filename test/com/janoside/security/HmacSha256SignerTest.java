package com.janoside.security;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.util.RandomUtil;

public class HmacSha256SignerTest {
	
	@Test
	public void testIt() throws Exception {
		Assert.assertEquals("98ad86344c358cf0438511f7a7ade7cf57da69bf19981213f54293ebd0bdc851", new HmacSha256Signer().sign("cheese", "abcdef"));
		
		Random r = new Random(1);
		StringBuilder contentBuffer = new StringBuilder();
		for (int i = 0; i < 1000; i++) {
			contentBuffer.append(RandomUtil.randomUuid(r).toString());
		}
		
		String key = RandomUtil.randomUuid(r).toString();
		
		Assert.assertEquals("9c02e3ea2e6181bcab8d8d04c52821fcb1c5ce190660fcbdaade629f21378a07", new HmacSha256Signer().sign(contentBuffer.toString(), key));
		
		
		r = new Random(1);
		contentBuffer = new StringBuilder();
		for (int i = 0; i < 1000; i++) {
			contentBuffer.append(new String(RandomUtil.randomByteArray(50, r), "UTF-8"));
		}
		
		key = RandomUtil.randomUuid(r).toString();
		
		Assert.assertEquals("cbde1f724c3721416a1665ef39b398d0e752872e484f20561b7706bd60eddeb4", new HmacSha256Signer().sign(contentBuffer.toString(), key));
	}
}