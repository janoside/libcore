package com.janoside.security;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;

import org.junit.Assert;
import org.junit.Test;

import com.janoside.codec.Base64Encoder;

public class RsaPublicKeyGeneratorTest {
	
	@Test
	public void testIt() throws Exception {
		Base64Encoder enc = new Base64Encoder();
		
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(512);
        
        KeyPair keypair = keyGen.genKeyPair();
        
        byte[] publicKeyBytes = keypair.getPublic().getEncoded();
		
        String b64PublicKey = new String(enc.encode(publicKeyBytes));
        
		RsaPublicKeyGenerator gen = new RsaPublicKeyGenerator();
		PublicKey publicKey = gen.generatePublicKey(b64PublicKey);
		
		Assert.assertEquals(publicKey, keypair.getPublic());
	}
}