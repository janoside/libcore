package com.janoside.environment;

import org.junit.Assert;
import org.junit.Test;

public class EnvironmentTest {
	
	@Test
	public void testIt() {
		Environment env = new Environment();
		Assert.assertEquals("local", env.getName());
		Assert.assertEquals("unspecified-app", env.getAppName());
		Assert.assertEquals("unspecified-server", env.getHardwareId());
		Assert.assertEquals("unknown", env.getSourcecodeVersion());
		
		env.setName("envName");
		Assert.assertEquals("envName", env.getName());
		
		env.setAppName("appName");
		Assert.assertEquals("appName", env.getAppName());
		
		env.setHardwareId("hardwareId");
		Assert.assertEquals("hardwareId", env.getHardwareId());
		
		env.setSourcecodeVersion("sourcecodeVersion");
		Assert.assertEquals("sourcecodeVersion", env.getSourcecodeVersion());
		
		System.setProperty("astar.sourcecodeVersion", "abcdef");
		
		env = new Environment();
	}
}