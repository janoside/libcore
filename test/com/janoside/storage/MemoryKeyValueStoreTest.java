package com.janoside.storage;

import java.util.HashSet;

import org.junit.Assert;
import org.junit.Test;

public class MemoryKeyValueStoreTest {
	
	@Test
	public void testBasic() {
		MemoryKeyValueStore kvs = new MemoryKeyValueStore();
		kvs.put("abc", "def");
		
		Assert.assertEquals(1, kvs.getSize());
		Assert.assertEquals("def", kvs.get("abc"));
		
		kvs.remove("abc2");
		
		Assert.assertEquals(1, kvs.getSize());
		Assert.assertEquals("def", kvs.get("abc"));
		
		kvs.remove("abc");
		
		Assert.assertEquals(0, kvs.getSize());
		Assert.assertNull(kvs.get("abc"));
		
		kvs.put("abc", "def");
		kvs.put("abc2", "def2");
		
		Assert.assertEquals(2, kvs.getSize());
		Assert.assertEquals(new HashSet() {{ add("abc"); add("abc2"); }}, kvs.getKeys());
		
		kvs.clear();
		
		Assert.assertEquals(0, kvs.getSize());
	}
}